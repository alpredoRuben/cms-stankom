<?php

namespace App\Console\Commands;

use App\Mail\ActivationAcountEmail;
use App\Models\MailSchedule;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailScheduleCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailShedule:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run command mail scheduling';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Run Mail Schedule Command Console");

        $mailSchedules = MailSchedule::whereIn('type', ['activation', 'verify'])
            ->where("send", false)
            ->where("created_at", ">=", Carbon::now()->subDays()->toDateTimeString())->get();

        $totalCount = count($mailSchedules);

        if ($totalCount > 0) {
            Log::info("Mail Schedule Cron is working fine!");
            foreach ($mailSchedules as $value) {
                $email = $value->data["email"];
                $name = $value->data["name"];
                $token = $value->data["token"];
                $password = $value->data["password"];
                Mail::to($email)->send(
                    new ActivationAcountEmail($name, $email, $password, $token, $value->type)
                );
                $this->updateExecute($value->id);
            }
        }
    }

    public function updateExecute($id)
    {
        $update = MailSchedule::find($id);
        $update->send = true;
        $update->send_at =  Carbon::now()->toDateTimeString();
        $update->save();
        $update->delete();
    }
}
