<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && $user->is_login == true) {
            if ($user->login_date != null && !empty($user->login_date)) {
                return redirect()->back()->with('messageError', 'Akun anda sedang digunakan di perangkat lain');
            }
        }

        if ($user && !$user->status) {
            return redirect()->back()->with('messageError', 'Maaf, anda tidak terdaftar');
        }

        $request->authenticate();

        User::where('id', Auth::user()->id)->update([
            'is_login' => true,
            'login_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->user()->id);
        $user->is_login = false;
        $user->login_at = null;
        $user->save();

        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
