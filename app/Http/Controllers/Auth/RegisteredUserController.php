<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ActivationAcountEmail;
use App\Models\MailSchedule;
use App\Models\User;
use App\Models\UserVerification;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required|unique:user_profiles,nik',
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => trim($request->first_name . ' ' . $request->last_name),
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);


        if (!$user) {
            return redirect()->back()->with('messageError', 'Proses registrasi gagal. Mohon periksa kembali data yang dimasukkan');
        }


        if (!$user->userProfile) {
            $user->userProfile()->create([
                "nik" => $request->nik,
                "first_name" => $request->first_name,
                "last_name" => $request->last_name,
                "date_joined" => Carbon::now()->format('Y-m-d'),
            ]);
        }


        $token = Str::random(60);

        if (!$user->verifyUser) {
            $user->verifyUser()->create([
                "token" => $token,
            ]);
        }

        $role = Role::where('name', 'peserta')->first();

        if ($role) {
            $user->assignRole([$role->id]);
        }

        MailSchedule::create([
            "type" => "verify",
            "data" => json_encode([
                "token" => $token,
                "password" => null,
                "email" => $user->email,
                "name" => $user->name,
            ])
        ]);

        return Redirect::route('register')->with('messageSuccess', 'Pendaftaran berhasil. Silahkan cek email anda');
    }

    public function activationAccount($token)
    {
        $verifyUser = UserVerification::where('token', $token)->first();
        if ($verifyUser) {
            $user = $verifyUser->user;

            if (!$user->email_verified_at) {
                $user->email_verified_at = Carbon::now();
                $user->status = true;
                $user->save();
                return Redirect::route('login')->with('messageSuccess', 'Your account has been activated');
            } else {
                return Redirect::route('login')->with('messageSuccess', 'Your account has already been activated');
            }
        } else {
            return Redirect::route('login')->with('messageError', 'Something went wrong!!');
        }
    }

    public function verificationEmail($token)
    {
        $verifyUser = UserVerification::where('token', $token)->first();
        if ($verifyUser) {
            $user = $verifyUser->user;

            if (!$user->email_verified_at) {
                $user->email_verified_at = Carbon::now();
                $user->status = true;
                $user->save();
                return Redirect::route('login')->with('messageSuccess', 'Your email has been verified');
            } else {
                return Redirect::route('login')->with('messageSuccess', 'Your email has already been verified');
            }
        } else {
            return Redirect::route('login')->with('messageError', 'Something went wrong!!');
        }
    }
}
