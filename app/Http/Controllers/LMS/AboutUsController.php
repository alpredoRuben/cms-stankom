<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Traits\GlobalManager;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Exception;

class AboutUsController extends Controller
{

    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "about_us/{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return env("APP_URL") . "/storage/{$path}/{$filename}";
    }

    public function index()
    {
        $totalAboutUs = AboutUs::count();
        return view('pages.setting.about.index', compact('totalAboutUs'));
    }


    public function create()
    {
        return view('pages.setting.about.create');
    }

    public function edit($id)
    {
        $record = AboutUs::find($id);
        return view('pages.setting.about.update', compact('record'));
    }

    public function find($id)
    {
        try {
            $data = AboutUs::find($id);

            return response()->json([
                "error" => false,
                "results" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "content" => "required",
            "photo" => "required|mimes:png,jpg",
        ]);

        $filepath = null;
        $strUnique = time() . '' . Str::random(8);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $filepath = $this->uploadFile($file,  $strUnique);
        }

        try {
            $data = AboutUs::create([
                "filepath" => $filepath ?? null,
                "content" => $request->content,
                "str_unique" => $strUnique
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data tentang kami berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "content" => "required",
        ]);


        $data = AboutUs::find($id);
        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data tentang kami tidak ditemukan"
            ], 404);
        }

        $filepath = null;
        if ($request->hasFile('photo')) {
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $filepath = $this->uploadFile($file,  $data->str_unique);
            }
        }

        try {
            $data->content = $request->content;
            $data->filepath = $filepath == null ? $data->filepath : $filepath;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data tentang kami berhasil ditambahkan",
                "results" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
