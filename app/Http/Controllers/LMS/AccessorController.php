<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\Accessor;
use App\Models\UserProfile;
use App\Traits\StaticProperties;
use Exception;
use Illuminate\Support\Facades\DB;

class AccessorController extends Controller
{
    use StaticProperties;

    public function index()
    {
        $filterBox = [
            [
                "id" => "nik",
                "text" => "NIK"
            ],
            [
                "id" => "name",
                "text" => "Nama"
            ],
            [
                "id" => "email",
                "text" => "Email"
            ],
            [
                "id" => "phone_number",
                "text" => "No. Ponsel"
            ],
            [
                "id" => "lsp_name",
                "text" => "Nama LSP"
            ]
        ];


        return view("pages.master.acessors.index", compact("filterBox"));
    }

    public function getAccessor()
    {
        try {
            $term = trim(request()->input("term"));
            $records = Accessor::where("name", "ilike", "%{$term}%")
                ->orderBy("name", "desc")
                ->get(['id', DB::raw('name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function create()
    {
        return view('pages.master.acessors.create');
    }

    public function edit($id)
    {
        $accessor = Accessor::find($id);
        return view('pages.master.acessors.update', compact('accessor'));
    }

    public function find($id)
    {
        try {
            $data = Accessor::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $request->validate(
            [
                "nik" => "required|unique:accessors,nik",
                "name" => "required",
                "lsp_id" => "required",
                "email" => "required|email|unique:accessors,email",
            ],
            [
                "nik.required" => "The :attribute field can not be blank value.",
                "name.required" => "The :attribute field can not be blank value.",
                "lsp_id.required" => "The :attribute field can not be blank value.",
                "email.required" => "The :attribute field can not be blank value.",
            ]
        );

        $find = UserProfile::where("nik", $request->nik)->first();

        if ($find) {
            return response()->json([
                "error" => true,
                "message" => "Ops.. Maaf nik yang anda masukkan teridentifikasi sebagai data pengguna LMS Stankom"
            ], 400);
        }

        try {
            $data = Accessor::create([
                "nik" => $request->nik,
                "name" => $request->name,
                "email" => $request->email,
                "phone_number" => $request->phone_number ?? null,
                "description" => $request->description ?? null,
                "lsp_id" => $request->lsp_id
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data assesor berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** UPDATE */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                "nik" => "unique:accessors,nik," . $id,
                "lsp_id" => "required",
                "email" => "unique:accessors,email," . $id,
            ],
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }

        $find = UserProfile::where("nik", $request->nik)->first();

        if ($find) {
            return response()->json([
                "error" => true,
                "message" => "Ops.. Maaf nik yang anda masukkan teridentifikasi sebagai data pengguna LMS Stankom"
            ], 400);
        }

        $data = Accessor::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data assesor tidak ditemukan"
            ], 404);
        }


        try {
            $data->nik = $request->nik ?? $data->nik;
            $data->name = $request->name ?? $data->name;
            $data->email = $request->email ?? $data->email;
            $data->phone_number = $request->phone_number ?? $data->phone_number;
            $data->description = $request->description ?? $data->description;
            $data->lsp_id = $request->lsp_id;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data assesor berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = Accessor::find($id);

        try {
            $name = $data->nik . " - " . $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data assesor, ({$name}), berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** RESTORE */
    public function restore($id)
    {
        try {
            Accessor::withTrashed()->find($id)->restore();

            return response()->json([
                "error" => false,
                "message" => "Data assesor berhasil diaktifkan",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function showExport()
    {
        $userProperties = $this->getStaticAccessorProperties();
        return view('pages.master.acessors.export_view', compact('userProperties'));
    }
}
