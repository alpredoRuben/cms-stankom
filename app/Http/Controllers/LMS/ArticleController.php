<?php

namespace App\Http\Controllers\LMS;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Traits\GlobalManager;
use App\Models\Article;
use App\Models\ArticleTag;
use App\Models\ArticleTagList;
use Exception;

class ArticleController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";

        $path = "articles/{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return [
            "success" => true,
            "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
            "filename" => $filename,
        ];
    }

    public function getArticleTags()
    {
        try {
            $records = ArticleTag::query();

            if (request()->input("term")) {
                $term = Str::lower(trim(request()->input("term")));
                $records = $records->where("tag_name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("tag_name", "desc")->get(['id', DB::raw('tag_name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function index()
    {
        return view("pages.master.articles.index");
    }

    public function create()
    {
        return view("pages.master.articles.create");
    }

    public function edit($id)
    {
        $record = Article::with(["articleTagList"])->find($id);
        $tags = "";

        if (count($record->articleTagList) > 0) {
            foreach ($record->articleTagList as $key => $value) {
                $tags .= $value->tag->tag_name;

                if ($key < count($record->articleTagList) - 1) {
                    $tags .= ", ";
                }
            }
        } else {
            $tags = "";
        }

        return view("pages.master.articles.update", compact('record', 'tags'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "article_title" => "required|unique:articles,article_title",
                "article_photo" => "mimes:png,jpg,jpeg",
                "article_content"  => "required",
                "article_tags" => "required"
            ]
        );

        $title = trim($request->article_title) ?? "";

        try {
            $article = Article::create([
                "article_title" => $title,
                "article_date" => Carbon::parse(Carbon::now())->format("Y-m-d"),
                "article_content" => $request->article_content,
            ]);

            if (!$article) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal menambahkan berita baru"
                ], 400);
            }

            if ($request->has('article_photo')) {
                if ($request->hasFile("article_photo")) {
                    $file = $request->file("article_photo");
                    $dataFile = $this->uploadFile($file, "article_{$article->id}");

                    if ($dataFile && $dataFile["success"] == true) {
                        $article = Article::find($article->id);
                        $article->article_photo = $dataFile["filepath"];
                        $article->timestamps = false;
                        $article->save();
                    }
                }
            }


            $splitTags = $request->article_tags ? explode(",", trim($request->article_tags)) : [];
            if (count($splitTags) > 0) {

                foreach ($splitTags as $value) {
                    $tagSlug = Str::slug(Str::lower(trim($value)));
                    $tag = ArticleTag::where('tag_slug', $tagSlug)->first();

                    if (!$tag) {
                        $tag = ArticleTag::create([
                            "tag_name" => Str::lower(trim($value)),
                            "tag_slug" => $tagSlug
                        ]);
                    }

                    $findTagList = ArticleTagList::where("article_id", $article->id)
                        ->where("tag_id", $tag->id)
                        ->first();

                    if (!$findTagList) {
                        ArticleTagList::create([
                            "article_id" => $article->id,
                            "tag_id" => $tag->id
                        ]);
                    }
                }
            }

            return response()->json([
                "error" => false,
                "message" => "Berita baru berhasil ditambahkan",
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "article_title" => "required|unique:articles,article_title," . $id,
                "article_photo" => "mimes:png,jpg,jpeg",
                "article_content"  => "required",
                "article_tags" => "required"
            ]
        );

        $article = Article::find($id);
        if (!$article) {
            return response()->json([
                "error" => true,
                "message" => "Data berita tidak ditemukan"
            ], 404);
        }

        $title = trim($request->article_title) ?? "";


        try {

            $article->article_title = $title;
            $article->article_content = $request->article_content;
            $article->save();

            if (!$article) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal mengubah data berita"
                ], 400);
            }

            if ($request->has('article_photo')) {

                if ($request->hasFile("article_photo")) {
                    $file = $request->file("article_photo");
                    $dataFile = $this->uploadFile($file, "article_{$article->id}");

                    if ($dataFile && $dataFile["success"] == true) {
                        $article = Article::find($article->id);
                        $article->article_photo = $dataFile["filepath"];
                        $article->timestamps = false;
                        $article->save();
                    }
                }
            }

            $splitTags = $request->article_tags ? explode(",", trim($request->article_tags)) : [];
            if (count($splitTags) > 0) {
                $tagId = [];
                foreach ($splitTags as $value) {
                    $tagSlug = Str::slug(Str::lower(trim($value)));
                    $tag = ArticleTag::where('tag_slug', $tagSlug)->first();

                    if (!$tag) {
                        $tag = ArticleTag::create([
                            "tag_name" => Str::lower(trim($value)),
                            "tag_slug" => $tagSlug
                        ]);
                    }

                    $listTag = ArticleTagList::where("article_id", $article->id)
                        ->where("tag_id", $tag->id)
                        ->first();

                    if (!$listTag) {
                        ArticleTagList::create([
                            "article_id" => $article->id,
                            "tag_id" => $tag->id
                        ]);
                    }

                    array_push($tagId, $tag->id);
                }

                ArticleTagList::where("article_id", $article->id)->whereNotIn("tag_id", $tagId)->delete();
            }

            return response()->json([
                "error" => false,
                "message" => "Berita baru berhasil diperbarui"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = article::find($request->id);
            $message = $data->status == false ? "dinonaktifkan" : "diaktifkan kembali";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data berita {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function setHeadline($id)
    {
        $article = Article::find($id);
        $article->is_headline = true;
        $article->save();
        if (!$article) {
            return response()->json([
                "error" => true,
                "message" => "Gagal menetapkan headline"
            ], 400);
        }
        return response()->json([
            "error" => true,
            "message" => "Berhasil menetapkan headline"
        ], 200);
    }

    public function disabledHeadline($id)
    {
        $article = Article::find($id);
        $article->is_headline = false;
        $article->save();
        if (!$article) {
            return response()->json([
                "error" => true,
                "message" => "Nonaktif headline gagal"
            ], 400);
        }
        return response()->json([
            "error" => true,
            "message" => "Nonaktif headline berhasil"
        ], 200);
    }
}
