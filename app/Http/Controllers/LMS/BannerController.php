<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Traits\GlobalManager;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Exception;

class BannerController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "banners/{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return env("APP_URL") . "/storage/{$path}/{$filename}";
    }

    public function index()
    {
        return view("pages.setting.banner.index");
    }

    public function show($id)
    {
        $record = Banner::find($id);
        return view("pages.setting.banner.show", compact("record"));
    }

    public function edit($id)
    {
        $record = Banner::find($id);
        return view("pages.setting.banner.update", compact("record"));
    }

    public function create()
    {
        return view("pages.setting.banner.create");
    }

    public function store(Request $request)
    {
        $request->validate([
            "banner_title" => "required|unique:banners,banner_title",
            "banner_content" => "required",
            "banner_order" => "required|unique:banners,banner_order",
            "banner_image" => "required|mimes:png,jpg",
            "link" => "required"
        ]);

        $filepath = null;
        $strUnique = time() . '' . Str::random(8);
        if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $filepath = $this->uploadFile($file,  $strUnique);
        }

        $findBanner = Banner::where('banner_order', $request->banner_order)->first();

        if ($findBanner) {
            return response()->json([
                "error" => false,
                "message" => "Nomor urut {$request->banner_order} sudah ada"
            ], 400);
        }

        $title = trim($request->banner_title);
        $slug = Str::slug(Str::lower($request->banner_title));

        try {
            $data = Banner::create([
                'banner_title' => $title,
                'banner_slug' => $slug,
                'banner_order' => $request->banner_order,
                'link' => $request->link,
                'banner_content' => $request->banner_content,
                'banner_image' => $filepath,
                "str_unique" => $strUnique
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data banner berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "banner_title" => "required|unique:banners,banner_title," . $id,
            "banner_content" => "required",
            "banner_order" => "required|unique:banners,banner_order," . $id,
        ]);

        $data = Banner::find($id);
        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data banner tidak ditemukan"
            ], 404);
        }

        $filepath = $data->banner_image;
        if ($request->hasFile('banner_image')) {
            if ($request->hasFile('banner_image')) {
                $file = $request->file('banner_image');
                $filepath = $this->uploadFile($file,  $data->str_unique);
            }
        }

        $title = trim($request->banner_title);
        $slug = Str::slug(Str::lower($request->banner_title));


        try {
            $data->banner_title = $title;
            $data->banner_slug = $slug;
            $data->banner_order = $request->banner_order;
            $data->banner_content = $request->banner_content;
            $data->banner_image = $filepath;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data banner berhasil ditambahkan",
                "results" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function delete($id)
    {
    }
}
