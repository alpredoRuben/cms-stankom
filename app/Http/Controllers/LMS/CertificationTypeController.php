<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\CertificationType;
use Exception;
use Illuminate\Support\Facades\DB;

class CertificationTypeController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Bidang Sertifikasi"],
            ["id" => "description", "text" => "Keterangan"],
        ];
        return view("pages.master.certification_type.index", compact('filterBox'));
    }

    public function getCertificationTypes()
    {
        $records = CertificationType::query();

        if (request()->input("term")) {
            $term = trim(request()->input("term"));
            $records = $records->where("name", "ilike", "%{$term}%");
        }

        $records = $records->orderBy("name", "desc")->get(['id', DB::raw('name as text')]);
        return response()->json([
            "results" => $records,
        ]);
    }


    public function find($id)
    {
        try {
            $data = CertificationType::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:certification_types,name'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }

        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data = CertificationType::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => $request->description ?? null,
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data bidang sertifikasi berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** UPDATE */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:certification_types,name,' . $id
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }


        $data = CertificationType::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data bidang sertifikasi tidak ditemukan"
            ], 404);
        }


        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->description = $request->description ?? $data->description;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data bidang sertifikasi berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = CertificationType::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data bidang sertifikasi, '{$name}', berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** RESTORE */
    public function restore($id)
    {
        try {
            $data = CertificationType::withTrashed()->find($id);
            $name = $data->name;
            $data->restore();

            return response()->json([
                "error" => false,
                "message" => "Data {$name} berhasil diaktifkan kembali",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
