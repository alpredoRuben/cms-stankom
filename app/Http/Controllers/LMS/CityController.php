<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\City;
use Exception;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Kabupaten/Kota"],
            ["id" => "province_name", "text" => "Nama Propinsi"]
        ];
        return view('pages.master.locations.city.index', compact('filterBox'));
    }

    public function getCities()
    {
        try {
            $records = City::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")
                ->get(['id', DB::raw('name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    /** FIND */
    public function find($id)
    {
        try {
            $data = City::with([
                "province"
            ])->find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $request->validate([
            'province' => 'required',
            'name' => 'required|unique:cities,name'
        ], [
            'province.required' => 'The :attribute field can not be blank value.',
            'name.required' => 'The :attribute field can not be blank value.'
        ]);

        $name = Str::lower(trim($request->name));
        $slug = Str::slug($name);

        try {
            $data = City::create([
                "province_id" => $request->province,
                "name" => ucwords($name),
                "slug" => $slug,
            ]);

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Data kabupaten/kota gagal ditambahkan"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Data kabupaten/kota berhasil ditambahkan"
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    /** UPDATE */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required|unique:cities,name,' . $id,
                'province' => 'required',
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.',
                'province.required' => 'The :attribute field can not be blank value.'
            ]
        );

        $data = City::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data kabupaten/kota tidak ditemukan"
            ], 404);
        }


        try {
            $name = Str::lower(trim($request->name));
            $slug = Str::slug($name);

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->province_id = $request->province == 'same' ? $data->province_id : $request->province;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data kabupaten/kota berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = City::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data kabupaten/kota '{$name}' berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function restore($id)
    {
        try {
            $data = City::withTrashed()->find($id);
            $name = $data->name;
            $data->restore();

            return response()->json([
                "error" => false,
                "message" => "Data {$name} berhasil dipulihkan kembali",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
