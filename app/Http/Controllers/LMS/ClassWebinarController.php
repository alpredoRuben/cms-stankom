<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\ClassWebinar;
use App\Traits\GlobalManager;
use App\Traits\StaticProperties;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ClassWebinarController extends Controller
{
    use GlobalManager, StaticProperties;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "kelas_webinar/kelas{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return [
            "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
            "filename" => $filename,
        ];
    }

    public function index()
    {
        $filterBox = [
            [
                "id" => "class_name",
                "text" => "Nama Kelas"
            ],
            [
                "id" => "class_type",
                "text" => "Tipe Kelas"
            ],
            [
                "id" => "trainer",
                "text" => "Nama Pengajar"
            ],
        ];

        return view("pages.version.classes.index", compact('filterBox'));
    }

    public function getClassWebinars()
    {
        try {

            $records = ClassWebinar::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("class_name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("class_name", "desc")->get(['id', DB::raw('class_name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function getAll()
    {
        $records = ClassWebinar::where('status', true)->get();
        return response()->json([
            "results" => $records,
        ]);
    }

    public function getWebinarQuiz()
    {
        $records = ClassWebinar::where('status', true)
            ->where('has_quiz', true)
            ->get();
        return response()->json([
            "results" => $records,
        ]);
    }


    public function show($id)
    {
        $record = ClassWebinar::with([
            "trainer" => function ($q) {
                $q->select("id", "name", "email");
            },
            "typeClass" => function ($q) {
                $q->select("id", "name", "description");
            },
        ])->find($id);

        $record->open_date = Carbon::parse($record->open_date)->format('d F Y');
        $record->close_date = Carbon::parse($record->close_date)->format('d F Y');

        return view("pages.version.classes.show", compact("record"));
    }

    public function create()
    {
        return view("pages.version.classes.create");
    }

    public function edit($id)
    {
        $record = ClassWebinar::with([
            "trainer" => function ($q) {
                $q->select("id", "name", "email");
            },
            "typeClass" => function ($q) {
                $q->select("id", "name", "description");
            },
        ])->find($id);
        return view("pages.version.classes.update", compact("record"));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "class_name" => "required|unique:class_webinars,class_name",
                "class_type_id" => "required",
                "user_id" => "required",
                "open_date" => "required",
                "close_date" => "required",
                "class_time" => "required",
                "duration" => "required",
                "quota" => "required",
                "has_quiz" => "required",
                "has_sertificated" => "required"
            ]
        );

        try {

            $name = trim($request->class_name);

            $data = ClassWebinar::create([
                "class_name" => $name,
                "class_type_id" => $request->class_type_id,
                "user_id" => $request->user_id,
                "open_date" => Carbon::parse($request->open_date)->format("Y-m-d"),
                "close_date" => Carbon::parse($request->close_date)->format("Y-m-d"),
                "class_time" => Carbon::parse($request->class_time)->timezone("Asia/Jakarta")->format("H:i:s"),
                "duration" => $request->duration,
                "quota" => $request->quota,
                "link" => $request->link ?? null,
                "has_quiz" => $request->has_quiz ?? false,
                "has_sertificated" => $request->has_sertificated ?? false,
                "description" => $request->description ?? null,
                "status" => true
            ]);


            if ($request->hasFile("file_images")) {
                $file = $request->file('file_images');
                $upload = $this->uploadFile($file, $data->id);
                $data->filepath = $upload["filepath"];
                $data->timestamps = false;
                $data->save();
            }

            return response()->json([
                "error" => false,
                "message" => "Data kelas/webinar berhasil ditambahkan",
                "results" => $data,
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "class_name" => "required|unique:class_webinars,class_name," . $id,
                "class_type_id" => "required",
                "user_id" => "required",
                "open_date" => "required",
                "close_date" => "required",
                "class_time" => "required",
                "duration" => "required",
                "quota" => "required",
                "has_quiz" => "required",
                "has_sertificated" => "required"
            ]
        );

        $name = trim($request->class_name);

        $data = ClassWebinar::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data kelas/webinar tidak ditemukan"
            ], 404);
        }

        if ($request->hasFile("file_images") && $request->file('file_images')) {
            $file = $request->file('file_images');
            $upload = $this->uploadFile($file, $data->id);
            $data->filepath = $upload["filepath"];
            $data->timestamps = false;
            $data->save();
        }

        try {
            $data->class_name =  $name;
            $data->class_type_id =  $request->class_type_id;
            $data->user_id =  $request->user_id;
            $data->open_date =  Carbon::parse($request->open_date)->format("Y-m-d");
            $data->close_date =  Carbon::parse($request->close_date)->format("Y-m-d");
            $data->class_time =  Carbon::parse($request->class_time)->timezone("Asia/Jakarta")->format("H:i:s");
            $data->duration =  $request->duration;
            $data->quota =  $request->quota;
            $data->link =  $request->link ?? null;
            $data->has_quiz =  $request->has_quiz ?? false;
            $data->has_sertificated =  $request->has_sertificated ?? false;
            $data->description = $request->description ?? $data->description;
            $data->save();


            return response()->json([
                "error" => false,
                "message" => "Data kelas/webinar berhasil diperbarui",
                "results" => $data,
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = ClassWebinar::find($request->id);
            $message = $data->status == false ? "dinonaktifkan" : "diaktifkan kembali";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Kelas (Webinar) {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function exportShow()
    {
        $webinarProperties = $this->getStaticWebinar();
        return view('pages.version.classes.export_view', compact('webinarProperties'));
    }
}
