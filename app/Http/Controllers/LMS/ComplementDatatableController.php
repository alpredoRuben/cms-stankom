<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\ClassWebinar;
use App\Models\LearningModule;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ComplementDatatableController extends Controller
{
    public function getLearningModules()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }


        $data = LearningModule::where('status',  $history == "deleted" ? false : true);

        if ($category != '' && $category != '-') {
            switch ($category) {
                case 'module_name':
                    $data = $data->where("module_name", "ilike", "%{$search}%");
                    break;

                case 'category_module':
                    $data = $data->whereHas("categoryModule", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;

                case 'sub_category':
                    $data = $data->whereHas("subCategoryModule", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;

                case 'type_module':
                    $data = $data->whereHas("typeModule", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;

                default:
                    break;
            }
        }

        $data = $data->orderBy("id", "desc");

        return DataTables::of($data)
            ->addColumn('category_name', function ($item) {
                return $item->categoryModule ? $item->categoryModule->name : "";
            })
            ->addColumn('sub_category_name', function ($item) {
                return $item->subCategoryModule ? $item->subCategoryModule->name : "";
            })
            ->addColumn('tipe_name', function ($item) {
                return $item->typeModule ? $item->typeModule->name : "";
            })
            ->addColumn("download", function ($item) {
                return $item->can_download ? '<span class="badge bg-info">YA</span>' : '<span class="badge bg-default">TIDAK</span>';
            })
            ->addColumn("quiz", function ($item) {
                return $item->has_quiz ? 'ADA' : 'TIDAK ADA';
            })
            ->addColumn("status", function ($item) {
                return $item->status == true ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->status) {
                    $str .= '<a href="' . route('learning_modules.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    $str .= '<a href="' . route('learning_modules.show', $item->id) . '" class="btn btn-primary btn-rounded mx-2 py-2 px-4"><i class="fas fa-eye"></i> Detail</a>&nbsp;';

                    $str .= "<button type='button' class='btn btn-warning btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= "</button>&nbsp;";
                } else {

                    $str .= "<button type='button' class='btn btn-success btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-check'></i> Aktifkan";
                    $str .= "</button>&nbsp;";
                }

                return $str;
            })
            ->editColumn('module_desc', function ($item) {
                $module_desc = $item->module_desc ? strip_tags($item->module_desc) : "";
                return strlen($module_desc) > 100 ? substr($module_desc, 0, 100) . "....(more)" : $module_desc;
            })
            ->addIndexColumn()
            ->rawColumns([
                "category_name",
                "sub_category_name",
                "tipe_name",
                "download",
                "quiz",
                "status",
                "action"
            ])
            ->make(true);
    }

    public function getClassWebinar()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }


        $data = ClassWebinar::where('status',  $history == "deleted" ? false : true);

        if ($category != '' && $category != '-') {
            switch ($category) {
                case 'class_name':
                    $data = $data->where("class_name", "ilike", "%{$search}%");
                    break;

                case 'class_type':
                    $data = $data->whereHas("typeClass", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;

                case 'trainer':
                    $data = $data->whereHas("trainer", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;
                default:
                    break;
            }
        }

        $data = $data->orderBy("id", "desc");

        return DataTables::of($data)
            ->addColumn('type_class_name', function ($item) {
                return $item->typeClass ? $item->typeClass->name : "";
            })
            ->editColumn('open_date', function ($item) {
                return Carbon::parse($item->open_date)->format("Y-m-d");
            })
            ->editColumn('close_date', function ($item) {
                return Carbon::parse($item->close_date)->format("Y-m-d");
            })
            ->addColumn('trainer_name', function ($item) {
                return $item->trainer ? $item->trainer->name : "";
            })
            ->addColumn("has_quiz", function ($item) {
                return $item->has_quiz ? "<span class='badge bg-success text-white'>YA</span>" : "TIDAK";
            })
            ->editColumn("has_sertificated", function ($item) {
                return $item->has_sertificated ? "<span class='badge bg-success text-white'>YA</span>" : "TIDAK";
            })
            ->editColumn("class_time", function ($item) {
                return $item->class_time ? Carbon::parse($item->class_time)->format("H:i:s") : "";
            })
            ->addColumn("status", function ($item) {
                return $item->status == true ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->status) {
                    $str .= '<a href="' . route('class_webinar.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    $str .= '<a href="' . route('class_webinar.show', $item->id) . '" class="btn btn-primary btn-rounded mx-2 py-2 px-4"><i class="fas fa-eye"></i> Detail</a>&nbsp;';

                    $str .= "<button type='button' class='btn btn-warning btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= "</button>&nbsp;";
                } else {

                    $str .= "<button type='button' class='btn btn-success btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-check'></i> Aktifkan";
                    $str .= "</button>&nbsp;";
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns([
                "type_class_name",
                "trainer_name",
                "has_quiz",
                "has_sertificated",
                "status",
                "action"
            ])
            ->make(true);
    }
}
