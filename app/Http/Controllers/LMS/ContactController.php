<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Exception;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $totalContact = Contact::count();
        return view('pages.setting.contact.index', compact('totalContact'));
    }

    public function find($id)
    {
        try {
            $data = Contact::find($id);

            return response()->json([
                "error" => false,
                "results" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            "company_name" => "required",
            "address" => "required",
            "phone" => "required",
            "email" => "required|email",
        ]);

        $name = trim($request->company_name);

        try {
            $data = Contact::create([
                "company_name" => $name,
                "address" => $request->address,
                "phone" => $request->phone,
                "email" => $request->email,
                "website" => $request->website ?? null
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data kontak berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "company_name" => "required",
            "address" => "required",
            "phone" => "required",
            "email" => "required|email",
        ]);

        $name = trim($request->company_name);

        $data = Contact::find($id);
        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data kontak tidak ditemukan"
            ], 404);
        }

        try {
            $data->company_name = $name;
            $data->address = $request->address;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->website = $request->website ?? null;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data kontak berhasil ditambahkan",
                "results" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
