<?php

namespace App\Http\Controllers\LMS;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Accessor;
use App\Models\Article;
use App\Models\CategoryModule;
use App\Models\CertificationType;
use Yajra\DataTables\DataTables;


use App\Models\Province;
use App\Models\City;
use App\Models\Distric;
use App\Models\Education;
use App\Models\JobProfession;
use App\Models\LSP;
use App\Models\News;
use App\Models\PostalCode;
use App\Models\Quiz;
use App\Models\SubCategoryModule;
use App\Models\TypeClass;
use App\Models\TypeModule;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Support\Carbon;

class DatatablesController extends Controller
{
    function get_words($sentence, $count = 10)
    {
        preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
        return $matches[0];
    }

    /** Get Provinces */
    public function getProvinces()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;


        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = Province::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = Province::query();
        }

        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    /** Get Cities */
    public function getCities()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;


        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = City::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = City::query();
        }

        if ($category != '' && $category != '-') {
            if ($category == 'province_name') {
                $data = $data->whereHas("province", function ($query) use ($search) {
                    $query->where("name", "ilike", "%{$search}%");
                });
            } else {
                $data = $data->where("{$category}", "ilike", "%{$search}%");
            }
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {

                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addColumn('province_name', function ($item) {
                return $item->province ? $item->province->name : "";
            })
            ->orderColumn('province_name', function ($query, $order) {
                $query->orderBy('province_name', $order);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'province_name'])
            ->make(true);
    }

    /** GET DISTRICTS */
    public function getDistricts()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;


        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = Distric::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = Distric::query();
        }

        if ($category != '' && $category != '-') {
            if ($category == 'city_name') {
                $data = $data->whereHas("city", function ($query) use ($search) {
                    $query->where("name", "ilike", "%{$search}%");
                });
            } else {
                $data = $data->where("{$category}", "ilike", "%{$search}%");
            }
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addColumn('city_name', function ($item) {
                return $item->city ? $item->city->name : "";
            })
            ->orderColumn('city_name', function ($query, $order) {
                $query->orderBy('city_name', $order);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'city_name'])
            ->make(true);
    }

    /** GET WARDS (KELURAHAN) */
    public function getWards()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;


        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = Ward::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = Ward::query();
        }

        if ($category != '' && $category != '-') {
            if ($category == 'district_name') {
                $data = $data->whereHas("district", function ($query) use ($search) {
                    $query->where("name", "ilike", "%{$search}%");
                });
            } else if ($category == 'postal_code') {
                $data = $data->whereHas("postalCodes", function ($query) use ($search) {
                    $query->where("code", "ilike", "%{$search}%");
                });
            } else {
                $data = $data->where("{$category}", "ilike", "%{$search}%");
            }
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('kecamatan', function ($item) {
                if ($item->district) {
                    return $item->district->name;
                }
                return "";
            })
            ->addColumn('kode_pos', function ($item) {
                if ($item->postalCodes) {
                    return $item->postalCodes->code;
                }
                return "";
            })
            ->addColumn('action', function ($item) {
                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'kecamatan', 'kode_pos'])
            ->make(true);
    }

    /** GET MASTER USERS */
    public function getMasterUsers()
    {
        $user = request()->user();

        $data = User::with(["userProfile"]);

        if (request()->has('status') && !empty(request()->get('status'))) {
            $status = request()->get('status') == '0' ? false : true;
            $data = $data->where('status', $status);
        }

        if (request()->has('role') && request()->get('role') != '') {
            $role = request()->get('role');
            $data = $data->whereHas("roles", function ($q) use ($role) {
                $q->where("id", $role);
            });
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
            $data = $data->where("name", "ilike", "%{$search}%")
                ->orWhereHas('userProfile', function ($q) use ($search) {
                    $q->where("nik", "ilike", "%{$search}%");
                })
                ->orWhere("email", "ilike", "%{$search}%")
                ->whereHas('userProfile', function ($q) use ($search) {
                    $q->where("phone_number", "ilike", "%{$search}%");
                });
        }

        $data = $data->orderBy('id', 'desc');

        return DataTables::of($data)
            ->addColumn('rolename', function ($item) {
                if (count($item->roles) > 0) {
                    return ucwords($item->roles[0]->name);
                }
                return "";
            })
            ->addColumn('activation', function ($item) {
                if ($item->email_verified_at) {
                    return '<span class="badge bg-primary">' . Carbon::parse($item->email_verified_at)->format('Y-m-d H:i:s') . '</span>';
                } else {
                    return '<span class="badge bg-warning">Menunggu</span>';
                }
            })
            ->addColumn('kode_pos', function ($item) {
                if ($item->userProfile && $item->userProfile->ward_id) {
                    $postalCode = PostalCode::where("ward_id", $item->userProfile->ward_id)->first();

                    return $postalCode ? $postalCode->code : "";
                }
                return "";
            })
            ->addColumn('kelurahan', function ($item) {
                if ($item->userProfile && $item->userProfile->ward_id) {
                    return $item->userProfile->ward->name;
                }
                return "";
            })
            ->addColumn('kecamatan', function ($item) {
                if ($item->userProfile && $item->userProfile->ward_id) {
                    return $item->userProfile->ward()->first()->district->name;
                }
                return "";
            })
            ->addColumn('kabupaten', function ($item) {
                if ($item->userProfile && $item->userProfile->ward_id) {
                    $ward = $item->userProfile->ward()->first();
                    return $ward->district()->first()->city->name;
                }
                return "";
            })
            ->addColumn('propinsi', function ($item) {
                if ($item->userProfile && $item->userProfile->ward_id) {
                    $ward = $item->userProfile->ward()->first();
                    $city = $ward->district()->first()->city()->first();
                    return $city->province()->first()->name;
                }
                return "";
            })
            ->addColumn('birthdate', function ($item) {
                if (!$item->userProfile) return "";
                return Carbon::parse($item->userProfile->birth_date)->format("Y-m-d");
            })
            ->addColumn('phone_number', function ($item) {
                if (!$item->userProfile) return "";
                return $item->userProfile->phone_number ?? "";
            })
            ->addColumn('nik', function ($item) {
                if ($item->userProfile && $item->userProfile->nik) {
                    return $item->userProfile->nik;
                }

                return "";
            })
            ->addColumn('alamat', function ($item) {
                if ($item->userProfile && $item->userProfile->address) {
                    return $item->userProfile->address;
                }

                return "";
            })
            ->addColumn('gender', function ($item) {
                if ($item->userProfile && $item->userProfile->gender) {
                    return $item->userProfile->gender;
                }

                return "";
            })
            ->addColumn('education', function ($item) {
                if ($item->userProfile && $item->userProfile->education_id) {
                    return $item->userProfile->education->name;
                }

                return "";
            })
            ->addColumn('job_desc', function ($item) {
                if ($item->userProfile && $item->userProfile->job_profession_id) {
                    return $item->userProfile->job_profession->name;
                }

                return "";
            })
            ->editColumn('status', function ($item) {
                return $item->status == 'active' ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addColumn('action', function ($item) use ($user) {
                $str  = '';

                if ($user->hasRole('superadmin')) {
                    if ($item->status == false) {
                        if ($item->email_verified_at == null) {
                            $str .= '<a href="' . route('master.users.main.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';
                        }

                        $str .= "<button type='button' class='btn btn-primary btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                        $str .=  "<i class='fas fa-check'></i> Aktifkan";
                        $str .= "</button>&nbsp;";
                    } else {

                        $str .= '<a href="' . route('master.users.main.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                        $str .= "<button type='button' class='btn btn-success btn-rounded mx-2 py-2 px-4' onclick='eventUploadImage({$item->id}, {$item->status})'>";
                        $str .=  "<i class='fas fa-upload'></i> Upload Foto";
                        $str .= "</button>&nbsp;";


                        $str .= "<button type='button' class='btn btn-danger btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                        $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                        $str .= "</button>&nbsp;";
                    }
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns([
                "activation",
                "kode_pos",
                "kelurahan",
                "kecamatan",
                "kabupaten",
                "propinsi",
                "phone_number",
                "nik",
                "alamat",
                "gender",
                "education",
                "job_desc",
                "status",
                "action",
            ])
            ->make(true);
    }

    /** GET CATEGORY MODULE */
    public function getCategoryModules()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = CategoryModule::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = CategoryModule::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('id', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }


    /** GET CATEGORY MODULE */
    public function getSubCategoryModules()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = SubCategoryModule::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = SubCategoryModule::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('id', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    /** GET TYPE MODULE */
    public function getTypeModules()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = TypeModule::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = TypeModule::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('id', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str = '';
                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }
                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }


    /** GET ACCESSOR */
    public function getAccessors()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = Accessor::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = Accessor::query();
        }


        if ($category != '' && $category != '-') {

            if ($category == 'lsp_name') {
                $data = $data->whereHas('lsp', function ($q) use ($search) {
                    $q->where("lsp_name", "ilike", "%{$search}%");
                });
            } else {
                $data = $data->where("{$category}", "ilike", "%{$search}%");
            }
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('lsp_name', function ($item) {
                if ($item->lsp) {
                    return $item->lsp->lsp_name;
                }
                return "";
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= "<i class='fas fa-check'></i> Aktifkan";
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<a href="' . route('master.accessor.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    $str .= '<button type="button" class="btn btn-warning btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= '</button>&nbsp;';
                }

                return $str;
            })
            ->addColumn("status", function ($item) {
                return $item->deleted_at == null ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns(["action", "status", "lsp_name"])
            ->make(true);
    }

    /** GET CERTIFICATION TYPE */
    public function getCertificationTypes()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = CertificationType::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = CertificationType::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= "<i class='fas fa-check'></i> Aktifkan";
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-warning btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= '</button>&nbsp;';
                }

                return $str;
            })
            ->addColumn("status", function ($item) {
                return $item->deleted_at == null ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    /** GET EDUCATION */
    public function getEducation()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = Education::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = Education::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    /** GET JOB PROFESSION */
    public function getJobProfession()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = JobProfession::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = JobProfession::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    /** GET TYPE CLASSES */
    public function getTypeClasses()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');
        $data  = null;

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }

        if ($history == "deleted") {
            $data = TypeClass::withTrashed()->where('deleted_at', '!=', null);
        } else {
            $data = TypeClass::query();
        }


        if ($category != '' && $category != '-') {
            $data = $data->where("{$category}", "ilike", "%{$search}%");
        }

        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->deleted_at != null) {
                    $str .= '<button type="button" class="btn btn-primary btn-rounded mx-2 py-2 px-4" onclick="eventRestoreData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash-restore"></i> Restore';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                    $str .= '<button type="button" class="btn btn-danger btn-rounded mx-2 py-2 px-4" onclick="eventDeleteData(' . $item->id . ')">';
                    $str .= '<i class="fas fa-trash"></i> Hapus';
                    $str .= '</button>&nbsp;';


                    $segment = $item->status ? "btn-warning" : "btn-success";

                    $str .= "<button type='button' class='btn {$segment} btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";

                    if ($item->status == true) {
                        $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    } else {
                        $str .=  "<i class='fas fa-check'></i> Aktifkan";
                    }

                    $str .= "</button>&nbsp;";
                }



                return $str;
            })
            ->addColumn('status', function ($item) {
                return $item->status ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    /** GET LSP  */
    public function getLSP()
    {
        $search = "";
        $category = "";
        $history = request()->get('history');

        if (request()->has('category') && !empty(request()->get('category'))) {
            $category = request()->get('category');
        }

        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
        }


        $data = LSP::with(["accessors"])->where('status',  $history == "deleted" ? false : true);

        if ($category != '' && $category != '-') {
            switch ($category) {
                case 'lsp_name':
                    $data = $data->where("lsp_name", "ilike", "%{$search}%");
                    break;

                case 'certification_type':
                    $data = $data->whereHas("certificationType", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;

                case 'accessor':
                    $data = $data->whereHas("accessor", function ($q) use ($search) {
                        $q->where("name", "ilike", "%{$search}%");
                    });
                    break;
                default:
                    break;
            }
        }

        $data = $data->orderBy("updated_at", "desc");

        return DataTables::of($data)
            ->addColumn('total_accessors', function ($item) {
                return $item->accessors ? $item->accessors()->count() : 0;
            })
            ->addColumn('certification_name', function ($item) {
                return $item->certificationType ? $item->certificationType->name : "";
            })
            ->addColumn("status", function ($item) {
                return $item->status == true ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->status) {
                    $str .= '<a href="' . route('master.lsp.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    $str .= '<a href="' . route('master.lsp.show', $item->id) . '" class="btn btn-primary btn-rounded mx-2 py-2 px-4"><i class="fas fa-eye"></i> Detail</a>&nbsp;';

                    $str .= "<button type='button' class='btn btn-warning btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= "</button>&nbsp;";
                } else {

                    $str .= "<button type='button' class='btn btn-success btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-check'></i> Aktifkan";
                    $str .= "</button>&nbsp;";
                }

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns([
                "certification_name",
                "status",
                "action"
            ])
            ->make(true);
    }


    /** GET NEWS */
    public function getNews()
    {

        $getHeadline = News::where('is_headline', true)->first();

        $data = News::with([
            "newsTagList"
        ]);

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $data = $data->whereHas("newsTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }

        if (request()->has('start_date') && !empty(request()->get('start_date'))) {
            $startDate = request()->get('start_date');
            $data = $data->where('news_date', '>=', Carbon::parse($startDate)->format('Y-m-d'));
        }

        if (request()->has('end_date') && !empty(request()->get('end_date'))) {
            $endDate = request()->get('end_date');
            $data = $data->where('news_date', '<=', Carbon::parse($endDate)->format('Y-m-d'));
        }


        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
            $data = $data->where("news_title", "ilike", "%${search}%");
        }


        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('headlines', function ($item) use ($getHeadline) {
                if ($getHeadline && $getHeadline->id == $item->id) {
                    return '<span class="text-success fs-4"><i class="fas fa-check-circle"></i></span>';
                }
                return "";
            })
            ->addColumn('action', function ($item) use ($getHeadline) {
                $str  = '';

                if ($item->status != true) {
                    $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="eventChangeStatus(' . $item->id . ')">';
                    $str .= '<i class="fas fa-check"></i> Aktifkan';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<a href="' . route('master.news.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    if ($item->is_headline == true) {
                        if ($item->id == $getHeadline->id) {
                            $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="disableHeadline(' . $item->id . ')">';
                            $str .= '<i class="far fa-newspaper"></i> Disabled Headline';
                            $str .= '</button>&nbsp;';
                        }
                    } else {
                        if (!$getHeadline) {
                            $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="setHeadline(' . $item->id . ')">';
                            $str .= '<i class="far fa-newspaper"></i> Headline';
                            $str .= '</button>&nbsp;';
                        }
                    }



                    $str .= '<button type="button" class="btn btn-warning btn-rounded mx-2 py-2 px-4" onclick="eventChangeStatus(' . $item->id . ')">';
                    $str .= '<i class="fas fa-times"></i> Non Aktifkan';
                    $str .= '</button>&nbsp;';
                }


                return $str;
            })
            ->addColumn('tags', function ($item) {
                if (count($item->newsTagList) > 0) {
                    $split = "";
                    foreach ($item->newsTagList as $key => $value) {
                        $split .= $value->tag->tag_name;

                        if ($key < count($item->newsTagList) - 1) {
                            $split .= ", ";
                        }
                    }

                    return $split;
                }

                return "";
            })
            ->editColumn('news_content', function ($item) {
                $newsContent = $item->news_content ? strip_tags($item->news_content) : "";
                return strlen($newsContent) > 200 ? substr($newsContent, 0, 200) . "....(more)" : $newsContent;
            })
            ->editColumn('news_date', function ($item) {
                return Carbon::parse($item->news_date)->format('d F Y');
            })
            ->editColumn('status', function ($item) {
                return $item->status ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tags', 'status', 'headlines'])
            ->make(true);
    }

    /** GET ARTICLES */
    public function getArticles()
    {

        $getHeadline = Article::where('is_headline', true)->first();

        $data = Article::with([
            "articleTagList"
        ]);

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $data = $data->whereHas("articleTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }

        if (request()->has('start_date') && !empty(request()->get('start_date'))) {
            $startDate = request()->get('start_date');
            $data = $data->where('article_date', '>=', Carbon::parse($startDate)->format('Y-m-d'));
        }

        if (request()->has('end_date') && !empty(request()->get('end_date'))) {
            $endDate = request()->get('end_date');
            $data = $data->where('article_date', '<=', Carbon::parse($endDate)->format('Y-m-d'));
        }


        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
            $data = $data->where("article_title", "ilike", "%${search}%");
        }


        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->addColumn('headlines', function ($item) use ($getHeadline) {
                if ($getHeadline && $getHeadline->id == $item->id) {
                    return '<span class="text-success fs-4"><i class="fas fa-check-circle"></i></span>';
                }
                return "";
            })
            ->addColumn('action', function ($item) use ($getHeadline) {
                $str  = '';

                if ($item->status != true) {
                    $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="eventChangeStatus(' . $item->id . ')">';
                    $str .= '<i class="fas fa-check"></i> Aktifkan';
                    $str .= '</button>&nbsp;';
                } else {

                    $str .= '<a href="' . route('master.articles.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';



                    if ($item->is_headline == true) {
                        if ($item->id == $getHeadline->id) {
                            $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="disableHeadline(' . $item->id . ')">';
                            $str .= '<i class="far fa-newspaper"></i> Disabled Headline';
                            $str .= '</button>&nbsp;';
                        }
                    } else {
                        if (!$getHeadline) {
                            $str .= '<button type="button" class="btn btn-success btn-rounded mx-2 py-2 px-4" onclick="setHeadline(' . $item->id . ')">';
                            $str .= '<i class="far fa-newspaper"></i> Headline';
                            $str .= '</button>&nbsp;';
                        }
                    }

                    $str .= '<button type="button" class="btn btn-warning btn-rounded mx-2 py-2 px-4" onclick="eventChangeStatus(' . $item->id . ')">';
                    $str .= '<i class="fas fa-times"></i> Non Aktifkan';
                    $str .= '</button>&nbsp;';
                }


                return $str;
            })
            ->addColumn('tags', function ($item) {
                if (count($item->articleTagList) > 0) {
                    $split = "";
                    foreach ($item->articleTagList as $key => $value) {
                        $split .= $value->tag->tag_name;

                        if ($key < count($item->articleTagList) - 1) {
                            $split .= ", ";
                        }
                    }

                    return $split;
                }

                return "";
            })
            ->editColumn('article_content', function ($item) {
                $newsContent = $item->article_content ? strip_tags($item->article_content) : "";
                return strlen($newsContent) > 200 ? substr($newsContent, 0, 200) . "....(more)" : $newsContent;
            })
            ->editColumn('news_date', function ($item) {
                return Carbon::parse($item->article_date)->format('d F Y');
            })
            ->editColumn('status', function ($item) {
                return $item->status ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tags', 'status', 'headlines'])
            ->make(true);
    }

    /** QUIS */
    public function getQuiz()
    {
        $status = request()->has('status') && request()->get('status') != 'active' ? false : true;


        $data = Quiz::with([
            "classWebinar",
            "learningModule",
        ])->where('status', $status);



        if (request()->has('webinar_id') && !empty(request()->get('webinar_id'))) {
            $data = $data->where('webinar_id', request()->get('webinar_id'));
        }

        if (request()->has('learning_id') && !empty(request()->get('learning_id'))) {
            $data = $data->where('learning_id', request()->get('learning_id'));
        }

        if (request()->has('sertificated') && !empty(request()->get('sertificated'))) {
            $sertificated = request()->get('sertificated') == '1' ? true : false;
            $data = $data->where('has_sertificated', $sertificated);
        }


        if (request()->has('search') && !empty(request()->get('search'))) {
            $search = request()->get('search');
            $data = $data->where("name", "ilike", "%${search}%")
                ->orWhere("min_scored", "ilike", "%${search}%");
        }


        $data = $data->orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->editColumn('code', function ($item) {
                if ($item->code) {
                    return "<span class='badge bg-light text-success bagde-success'>{$item->code}</span>";
                }

                return "";
            })
            ->editColumn('has_sertificated', function ($item) {
                return $item->has_sertificated ? "Bersertifikat" : "Tidak Bersertifikat";
            })
            ->editColumn('status', function ($item) {
                return $item->status ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                if ($item->status == false) {
                    $str .= "<button type='button' class='btn btn-success btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-check'></i> Aktifkan";
                    $str .= "</button>&nbsp;";
                } else {
                    $str .= '<a href="' . route('master.quiz.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                    $str .= "<button type='button' class='btn btn-warning btn-rounded mx-2 py-2 px-4' onclick='eventChangeStatus({$item->id}, {$item->status})'>";
                    $str .=  "<i class='fas fa-times'></i> Non Aktifkan";
                    $str .= "</button>&nbsp;";
                }

                return $str;
            })
            ->addColumn('webinar_name', function ($item) {
                if ($item->classWebinar) {
                    return $item->classWebinar->class_name;
                }
                return "";
            })

            ->addColumn('learning_name', function ($item) {
                if ($item->learningModule) {
                    return $item->learningModule->module_name;
                }
                return "";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'code', 'learning_name', 'status', 'webinar_name', 'has_sertificated'])
            ->make(true);
    }
}
