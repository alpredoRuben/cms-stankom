<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\City;
use App\Models\Distric;
use Exception;
use Illuminate\Support\Facades\DB;

class DistrictController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Kecamatan"],
            ["id" => "city_name", "text" => "Nama Kabupaten/Kota"]
        ];
        return view('pages.master.locations.district.index', compact('filterBox'));
    }

    public function getDistricts()
    {
        try {
            $records = Distric::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")
                ->get(['id', DB::raw('name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    /** FIND */
    public function find($id)
    {
        try {
            $data = Distric::with(["city"])->find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $request->validate(
            [
                'city' => 'required',
                'name' => 'required|unique:cities,name'
            ],
            [
                'city.required' => 'The :attribute field can not be blank value.',
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        $name = Str::lower(trim($request->name));
        $slug = Str::slug($name);

        try {

            $data = Distric::create([
                "city_id" => $request->city,
                "name" => ucwords($name),
                "slug" => $slug,
            ]);

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Data kecamatan gagal ditambahkan"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Data kecamatan berhasil ditambahkan"
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    /** UPDATE */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required|unique:cities,name,' . $id,
                'city' => 'required',
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.',
                'city.required' => 'The :attribute field can not be blank value.'
            ]
        );


        $data = Distric::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data kecamatan tidak ditemukan"
            ], 404);
        }


        try {
            $name = Str::lower(trim($request->name));
            $slug = Str::slug($name);

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->city_id = $request->city == 'same' ? $data->city_id : $request->city;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data kecamatan berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = Distric::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data kecamatan '{$name}' berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function restore($id)
    {
        try {
            $data = Distric::withTrashed()->find($id);
            $name = $data->name;
            $data->restore();

            return response()->json([
                "error" => false,
                "message" => "Data {$name} berhasil dipulihkan kembali",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
