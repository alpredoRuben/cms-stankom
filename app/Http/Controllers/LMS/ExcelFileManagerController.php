<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Imports\LearningModelImport;
use App\Models\CategoryModule;
use App\Models\LearningModule;
use App\Models\SubCategoryModule;
use App\Models\TypeModule;
use App\Traits\StaticProperties;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use Exception;

class ExcelFileManagerController extends Controller
{
    use StaticProperties;

    private function getHeaders($data, $filterData)
    {
        $headers = [];
        foreach ($data as $item) {
            $find = collect($filterData)->filter(function ($f) use ($item) {
                return $f["desc"] == $item;
            });

            if ($find) {
                array_push($headers, $find->first()["column"]);
            }
        }
        return $headers;
    }

    private function extractRowExcel($rows, $static_data)
    {
        $headers = array();
        $data = array();

        foreach ($rows[0] as $key => $value) {
            if ($key == 0) {
                $headers = $this->getHeaders($value, $static_data);
            } else {
                $data[] = array_combine($headers, $value);
            }
        }
        return $data;
    }

    public function importLearningModule(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required'
        ]);

        $extension = File::extension($request->file('import_file')->getClientOriginalName());

        if ($extension != 'xlsx' && $extension != 'xls') {
            return response()->json([
                'status' => false,
                'message' => 'Extensi file tidak mendukung'
            ], 200);
        }


        $excelRows = Excel::toArray(new LearningModelImport, $request->file('import_file'));
        $staticData = $this->excelModuleStaticProperties();
        $rows = $this->extractRowExcel($excelRows, $staticData);

        $failedInsert = array();

        DB::beginTransaction();

        try {

            for ($index = 0; $index < count($rows); $index++) {
                $item = $rows[$index];
                $moduleName = $item["module_name"];
                $categoryName = $item["category_module_id"];
                $subCategoryName = $item["sub_category_module_id"];
                $typeName = $item["type_module_id"];

                if (!empty($typeName) && !empty($subCategoryName) && !empty($categoryName)) {
                    $module = LearningModule::where("module_name", "ilike", "%{$moduleName}%")->first();

                    if ($module) {
                        continue;
                    }

                    $categoryModule = CategoryModule::where("name", "ilike", "%{$categoryName}%")
                        ->first();

                    if (!$categoryModule) {
                        continue;
                    }

                    $subModule = SubCategoryModule::where("name", "ilike", "%{$subCategoryName}%")
                        ->first();

                    if (!$subModule) {
                        continue;
                    }

                    $typeModule = TypeModule::where("name", "ilike", "%{$typeName}%")
                        ->first();

                    if (!$typeModule) {
                        continue;
                    }

                    if (!$module) {
                        $insertModule = LearningModule::create([
                            "category_module_id" => $categoryModule->id,
                            "sub_category_module_id" => $subModule->id,
                            "type_module_id" => $typeModule->id,
                            "module_name" => $moduleName,
                            "module_desc" => $item["module_desc"] ?? null,
                            "can_download" => Str::lower($item["can_download"]) == "ya" ? true : false,
                            "has_quiz" => Str::lower($item["has_quiz"]) == "ya" ? true : false,
                            "status" => true
                        ]);

                        if (!$insertModule) {
                            array_push($failedInsert, ["module_name" => $moduleName]);
                        }
                    }
                } else {
                    continue;
                }
            }

            DB::commit();

            if (count($failedInsert) > 0) {

                return response()->json([
                    "error" => true,
                    "message" => "Beberapa data modul pembelajaran gagal disimpan",
                    "results" => $failedInsert
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Data modul pembelajaran berhasil disimpan",
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
