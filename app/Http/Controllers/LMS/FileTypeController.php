<?php

namespace App\Http\Controllers\LMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FileType;
use Exception;

class FileTypeController extends Controller
{
    public function getAll()
    {
        try {
            $data = FileType::get();
            return response()->json([
                "error" => false,
                "message" => "Get all file types",
                "results" => $data
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
