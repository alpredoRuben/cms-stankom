<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\Accessor;
use App\Models\City;
use App\Models\ClassWebinar;
use App\Models\LSP;
use App\Models\User;
use App\Models\Ward;
use App\Traits\FileManager;
use App\Traits\ImportManager;
use App\Traits\StaticProperties;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class FilesManagerController extends Controller
{
    use ImportManager, StaticProperties, FileManager;

    private function setToArray($data)
    {
        $result = array();
        foreach ($data as $value) {
            array_push($result, $value);
        }

        return $result;
    }

    private function attachColumnAndField($staticData, $selections, $filename = 'export_dummy.csv')
    {
        $results = [
            "headers" => array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename={$filename}",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            ),
            "columns" => array(),
            "fields" => array(),
        ];

        foreach ($staticData as $item) {
            $find = collect($selections)->filter(function ($x) use ($item) {
                return $item[0] == $x;
            });

            if (count($find) > 0) {
                array_push($results['columns'], $item[1]);
                array_push($results['fields'], $item[0]);
            }
        }

        return $results;
    }

    /** USER => DOWNLOAD CSV */
    public function downloadUsersCSV()
    {
        $data = $this->getFileFromTemplates('pengguna.csv');
        return Response::download($data["file"], $data["filename"], $data["headers"]);
    }

    /** USER => IMPORT CSV */
    public function importUserCSV(Request $request)
    {
        if ($request->hasFile('import_file') && $request->file('import_file')->isValid()) {

            $file = $request->file('import_file');
            $type = $file->getClientOriginalExtension();

            if ($type <> 'csv') {
                return response()->json([
                    "error" => true,
                    "message" => "Format extensi file tidak mendukung. Wajib menggunakan file CSV"
                ], 400);
            }



            $data = $this->extractCSVToArray($file->getRealPath(), ",");

            if ($data == false) {
                return response()->json([
                    "error" => true,
                    "message" => "File yang anda upload tidak dapat diakses"
                ], 400);
            }

            if (count($data) > 0) {
                $results = $this->storeImportUser($data);
                return response()->json([
                    "error" => false,
                    "message" => "Import data pengguna berhasil. Total data duplikat {$results['duplicated']} dan Total insert {$results['totalInsert']}",
                    "resutls" => $results,
                ], 200);
            } else {
                return response()->json([
                    "error" => true,
                    "message" => "Data dari file yang di upload kosong"
                ], 400);
            }
        } else {
            return response()->json([
                "error" => true,
                "message" => "File tidak ditemukan, Silahkan upload file csv anda"
            ], 400);
        }
    }

    /** USER => EXPORT CSV */
    public function exportUserCSV()
    {
        $skip = request()->get('offset') ? (int) request()->get('offset') : "";
        $perRow = request()->get('limit') ? (int) request()->get('limit') : "";
        $rolename = request()->get('rolename');
        $selections = request()->get('selections');
        $fileName = 'export_pengguna.csv';
        $attachData = $this->attachColumnAndField($this->getStaticUserProperties(), $selections, $fileName);

        $data = User::with(['userProfile']);

        if ($rolename != 'all') {
            $data = $data->whereHas("roles", function ($q) use ($rolename) {
                $q->where("name", $rolename);
            });
        }

        if (request()->has('type') && request()->get('type') == 'all') {
            $data = $data->get();
        } else {
            $skip = request()->get('offset') ? (int) request()->get('offset') : 0;
            $perRow = request()->get('limit') ? (int) request()->get('limit') : 10;
            $data = $data->skip($skip)->take($perRow)->get();
        }

        $callback = function () use ($data, $attachData) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $attachData['columns']);

            foreach ($data as $value) {
                $kelurahan = "";
                $kecamatan = "";
                $kabupaten = "";
                $propinsi = "";
                $kode_pos = "";

                if ($value->userProfile && $value->userProfile->ward_id) {
                    $ward =  Ward::find($value->userProfile->ward_id);
                    $city = City::find($ward->district->city_id);

                    $kode_pos = $ward->postalCodes->code;
                    $kelurahan = $ward->name;
                    $kecamatan = $ward->district->name;
                    $kabupaten = $city->name;
                    $propinsi = $city->province->name;
                }

                $row = array();

                for ($i = 0; $i < count($attachData['columns']); $i++) {
                    $tmp = '';
                    switch ($attachData['fields'][$i]) {
                        case 'nik':
                            $tmp = $value->userProfile ? (string) $value->userProfile->nik ?? "" : "";
                            break;
                        case 'name':
                            $tmp = $value->name;
                            break;

                        case 'email':
                            $tmp = $value->email;
                            break;

                        case 'gender':
                            $tmp = $value->userProfile ? $value->userProfile->gender ?? "" : "";
                            break;

                        case 'birthdate':
                            $tmp = $value->userProfile ? Carbon::parse($value->userProfile->birth_date)->format('Y-m-d') ?? "" : "";
                            break;

                        case 'phone':
                            $tmp = $value->userProfile && $value->userProfile->phone_number ? (string) $value->userProfile->phone_number  : "";
                            break;

                        case 'address':
                            $tmp = $value->userProfile ? $value->userProfile->address ?? "" : "";
                            break;

                        case 'join':
                            $tmp = $value->userProfile ? Carbon::parse($value->userProfile->date_joined)->format('Y-m-d') ?? "" : "";
                            break;

                        case 'role':
                            $tmp = count($value->roles) > 0 ? $value->roles[0]->name ?? "" : "";
                            break;

                        case 'postal_code':
                            $tmp = $kode_pos;
                            break;

                        case 'ward':
                            $tmp = $kelurahan;
                            break;


                        case 'district':
                            $tmp = $kecamatan;
                            break;

                        case 'city':
                            $tmp = $kabupaten;
                            break;

                        case 'province':
                            $tmp = $propinsi;
                            break;

                        case 'education':
                            $tmp = $value->userProfile && $value->userProfile->education_id ? $value->userProfile->education->name : "";
                            break;

                        case 'profession':
                            $tmp = $value->userProfile && $value->userProfile->job_profession_id ? $value->userProfile->job_profession->name : "";
                            break;

                        default:
                            break;
                    }

                    $row[$attachData['columns'][$i]] = $tmp;
                }

                $arrays = $this->setToArray($row);
                fputcsv($file, $arrays);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $attachData['headers']);
    }

    /** ACCESSOR => DOWNLOAD CSV */
    public function downloadAccessorCSV()
    {
        $data = $this->getFileFromTemplates('csv_accessor.csv');
        return Response::download($data["file"], $data["filename"], $data["headers"]);
    }

    /** ACCESSOR => IMPORT CSV */
    public function importAccessorCSV(Request $request)
    {
        if ($request->hasFile('import_file') && $request->file('import_file')->isValid()) {
            Log::info("Has file");

            $file = $request->file('import_file');
            $type = $file->getClientOriginalExtension();

            if ($type <> 'csv') {
                return response()->json([
                    "error" => true,
                    "message" => "Format extensi file tidak mendukung. Wajib menggunakan file CSV"
                ], 400);
            }

            $data = $this->extractCSVToArray($file->getRealPath(), ",");
            if ($data == false) {
                return response()->json([
                    "error" => true,
                    "message" => "File yang anda upload tidak dapat diakses"
                ], 400);
            }

            if (count($data) <= 0) {
                return response()->json([
                    "error" => true,
                    "message" => "Data dari file yang di upload kosong"
                ], 400);
            }

            $results = $this->storeImportAccessor($data);
            return response()->json([
                "error" => false,
                "message" => "Proses import data assessor selesai. Total Data:  Duplikat {$results['duplicated']},  Masuk {$results['totalInsert']}, Tidak Sesuai {$results['invalid']}",
                "resutls" => $results,
            ], 200);
        } else {
            return response()->json([
                "error" => true,
                "message" => "File tidak ditemukan, Silahkan upload file csv anda"
            ], 400);
        }
    }


    /** ACCESSOR => EXPORT CSV */
    public function exportAccessorCSV()
    {
        $skip = request()->get('offset') ? (int) request()->get('offset') : "";
        $perRow = request()->get('limit') ? (int) request()->get('limit') : "";
        $selections = request()->get('selections');
        $fileName = 'export_accessor.csv';

        $attachData = $this->attachColumnAndField($this->getStaticAccessorProperties(), $selections, $fileName);

        $data = Accessor::query();

        if (request()->has('type') && request()->get('type') == 'all') {
            $data = $data->get();
        } else {
            $skip = request()->get('offset') ? (int) request()->get('offset') : 0;
            $perRow = request()->get('limit') ? (int) request()->get('limit') : 10;
            $data = $data->skip($skip)->take($perRow)->get();
        }

        $callback = function () use ($data, $attachData) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $attachData['columns']);

            foreach ($data as $value) {
                $row = array();

                for ($i = 0; $i < count($attachData['columns']); $i++) {
                    $tmp = $value[$attachData['fields'][$i]];
                    $row[$attachData['columns'][$i]] = $tmp;
                }

                $arrays = $this->setToArray($row);
                fputcsv($file, $arrays);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $attachData['headers']);
    }


    /** LSP => EXPORT CSV */
    public function exportLSPCSV()
    {
        $skip = request()->get('offset') ? (int) request()->get('offset') : "";
        $perRow = request()->get('limit') ? (int) request()->get('limit') : "";
        $selections = request()->get('selections');
        $fileName = 'export_lsp.csv';

        $attachData = $this->attachColumnAndField($this->getStaticLSPProperties(), $selections, $fileName);

        $data = LSP::with([
            "certificationType",
            "accessors"
        ]);

        if (request()->has('type') && request()->get('type') == 'all') {
            $data = $data->get();
        } else {
            $skip = request()->get('offset') ? (int) request()->get('offset') : 0;
            $perRow = request()->get('limit') ? (int) request()->get('limit') : 10;
            $data = $data->skip($skip)->take($perRow)->get();
        }

        $callback = function () use ($data, $attachData) {
            $file = fopen('php://output', 'w');
            $columns = $attachData["columns"];
            array_push($columns, "Assessor");

            fputcsv($file, $columns);
            foreach ($data as $value) {
                $row = array();

                for ($i = 0; $i < count($attachData['columns']); $i++) {
                    $tmp = '';
                    switch ($attachData['fields'][$i]) {
                        case 'certification_type_id':
                            $tmp = $value->certificationType->name;
                            break;

                        default:
                            $tmp = $value[$attachData['fields'][$i]];
                            break;
                    }

                    $tmp =
                        $row[$attachData['columns'][$i]] = $tmp;
                }

                if ($value->accessors && count($value->accessors) > 0) {
                    $row["accessors"] = collect($value->accessors)->implode('name', ', ');
                } else {
                    $row["accessors"] = "";
                }

                $arrays = $this->setToArray($row);
                fputcsv($file, $arrays);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $attachData['headers']);
    }


    /** LEARNING MODULE */
    public function downloadLearningModules()
    {
        $data = $this->getFileFromTemplates('learning-module.xlsx');
        return Response::download($data["file"], $data["filename"], $data["headers"]);
    }


    /** CLASS WEBINAR */
    public function exportClassWebinar()
    {
        $skip = request()->get('offset') ? (int) request()->get('offset') : "";
        $perRow = request()->get('limit') ? (int) request()->get('limit') : "";
        $selections = request()->get('selections');
        $fileName = 'export_class_webinar.csv';

        $attachData = $this->attachColumnAndField($this->getStaticWebinar(), $selections, $fileName);

        $data = ClassWebinar::with([
            "trainer",
            "typeClass",
            "userWebinars"
        ]);

        if (request()->has('type') && request()->get('type') == 'all') {
            $data = $data->get();
        } else {
            $skip = request()->get('offset') ? (int) request()->get('offset') : 0;
            $perRow = request()->get('limit') ? (int) request()->get('limit') : 10;
            $data = $data->skip($skip)->take($perRow)->get();
        }

        $callback = function () use ($data, $attachData) {
            $file = fopen('php://output', 'w');
            $columns = $attachData["columns"];
            fputcsv($file, $columns);

            foreach ($data as $value) {
                $row = array();

                for ($i = 0; $i < count($attachData['columns']); $i++) {
                    $tmp = '';
                    switch ($attachData['fields'][$i]) {
                        case 'class_type':
                            $tmp = $value->typeClass->name;
                            break;

                        case 'trainer':
                            $tmp = $value->trainer->name;
                            break;

                        case 'total_user':
                            $tmp = $value->userWebinars ? $value->userWebinars()->count() : 0;
                            break;

                        case 'has_quiz':
                            $tmp = $value->has_quiz ? "Tersedia" : "Tidak Tersedia";
                            break;

                        case 'has_sertificated':
                            $tmp = $value->has_sertificated ? "Bersertifikat" : "Tidak Bersertifikat";
                            break;

                        case 'status':
                            $tmp = $value->status ? "Aktif" : "Tidak Aktif";
                            break;

                        default:
                            $tmp = $value[$attachData['fields'][$i]];
                            break;
                    }

                    $tmp =
                        $row[$attachData['columns'][$i]] = $tmp;
                }


                $arrays = $this->setToArray($row);
                fputcsv($file, $arrays);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $attachData['headers']);
    }
}
