<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LSP;
use App\Traits\StaticProperties;
use Exception;
use Illuminate\Support\Facades\DB;

class LSPController extends Controller
{
    use StaticProperties;

    public function index()
    {
        $filterBox = [
            [
                "id" => "lsp_name",
                "text" => "Nama LSP"
            ],
            [
                "id" => "certification_type",
                "text" => "Bidang Sertifikasi"
            ],

        ];

        return view("pages.master.lsp.index", compact('filterBox'));
    }

    public function getLSP()
    {
        try {
            $records = LSP::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("lsp_name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("lsp_name", "desc")
                ->get(['id', DB::raw('lsp_name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function show($id)
    {
        $record = LSP::with([
            "certificationType" => function ($q) {
                $q->select("id", "name", "slug");
            },
            "accessors",
        ])->find($id);

        return view("pages.master.lsp.show", compact("record"));
    }

    public function create()
    {
        return view("pages.master.lsp.create");
    }

    public function edit($id)
    {
        $record = LSP::with([
            "certificationType" => function ($q) {
                $q->select("id", "name", "slug");
            },
            "accessors",
        ])->find($id);
        return view("pages.master.lsp.update", compact("record"));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "lsp_name" => "required|unique:lsp,lsp_name",
                "certification_type_id" => "required",
                "lsp_address" => "required",
            ]
        );

        $name = trim($request->lsp_name);

        try {
            $data = LSP::create([
                "lsp_name" => $name,
                "lsp_address" => $request->lsp_address,
                "certification_type_id" => $request->certification_type_id,
                "description" => $request->description ?? null,
                "lsp_phone" => $request->lsp_phone,
                "lsp_website" => $request->lsp_website,
                "lsp_email" => $request->lsp_email,
                "status" => true
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data LSP berhasil ditambahkan",
                "results" => $data,
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "lsp_name" => "required|unique:lsp,lsp_name," . $id,
                "certification_type_id" => "required",
                "lsp_address" => "required",
            ]
        );

        $name = trim($request->lsp_name);

        $data = LSP::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data LSP tidak ditemukan"
            ], 404);
        }

        try {

            $data->lsp_name =  $name;
            $data->certification_type_id =  $request->certification_type_id;
            $data->lsp_address =  $request->lsp_address;
            $data->description =  $request->description ?? $data->description;
            $data->lsp_email = $request->lsp_email;
            $data->lsp_phone = $request->lsp_phone;
            $data->lsp_website = $request->lsp_website;
            $data->save();


            return response()->json([
                "error" => false,
                "message" => "Data LSP berhasil diperbarui",
                "results" => $data,
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = LSP::find($request->id);
            $message = $data->status == false ? "dinonaktifkan" : "diaktifkan kembali";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "LSP {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function showExport()
    {
        $rowProperties = $this->getStaticLSPProperties();
        return view('pages.master.lsp.export_view', compact('rowProperties'));
    }
}
