<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\FileModule;
use App\Models\FileType;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\LearningModule;
use App\Traits\GlobalManager;

use Exception;
use Illuminate\Support\Facades\DB;

class LearningModuleController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $listExtension, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $checkExtension = collect($listExtension)->where("extension", $extension)->first();

        if ($extension) {
            $path = "modul_pembelajaran/{$unique}";
            $this->removeDirectory($path);
            Storage::putFileAs("public/{$path}", $file, $filename);
            return [
                "success" => true,
                "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
                "filename" => $filename,
                "composite" => json_encode($checkExtension)
            ];
        } else {
            return null;
        }
    }

    private function deleteItemFile($list)
    {
        foreach ($list as $value) {
            $moduleFile = FileModule::find($value);
            Storage::deleteDirectory("modul_pembelajaran/module_{$moduleFile->id}");
            $moduleFile->delete();
        }
    }


    public function index()
    {
        $filterBox = [
            [
                "id" => "module_name",
                "text" => "Nama Modul"
            ],
            [
                "id" => "category_module",
                "text" => "Kategori Modul"
            ],
            [
                "id" => "sub_category",
                "text" => "Sub Kategori Modul"
            ],
            [
                "id" => "type_module",
                "text" => "Tipe Modul"
            ],
        ];

        return view("pages.version.modul.index", compact("filterBox"));
    }

    public function getLearningModules()
    {
        try {
            $records = LearningModule::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("module_name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("module_name", "desc")->get(['id', DB::raw('module_name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function getAll()
    {
        $records = LearningModule::where('status', true)->get();
        return response()->json([
            "results" => $records,
        ]);
    }

    public function getLearningHasQuis()
    {
        $records = LearningModule::where('status', true)
            ->where('has_quiz', true)
            ->get();
        return response()->json([
            "results" => $records,
        ]);
    }

    public function show($id)
    {
        $record = LearningModule::with([
            "categoryModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "subCategoryModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "typeModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "fileModules"
        ])->find($id);

        return view('pages.version.modul.show', compact('record'));
    }

    public function create()
    {
        return view("pages.version.modul.create");
    }

    public function edit($id)
    {
        $record = LearningModule::with([
            "categoryModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "subCategoryModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "typeModule" => function ($q) {
                $q->select("id", "name", "description");
            },
            "fileModules"
        ])->find($id);
        return view('pages.version.modul.update', compact('record'));
    }

    public function find($id)
    {
        try {
            $data = LearningModule::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function store(Request $request)
    {
        $fileTypes = $this->getFileTypeCollection();

        $request->validate(
            [
                "module_name" => "required|unique:learning_modules,module_name",
                "category_module_id" => "required",
                "sub_category_module_id" => "required",
                "type_module_id" => "required",
                "can_download" => "required",
                "has_quiz" => "required",
            ],
            [
                "module_name.required" => "The :attribute field can not be blank value.",
                "category_module_id.required" => "The :attribute field can not be blank value.",
                "sub_category_module_id.required" => "The :attribute field can not be blank value.",
                "type_module_id.required" => "The :attribute field can not be blank value.",
                "can_download.required" => "The :attribute field can not be blank value.",
                "has_quiz.required" => "The :attribute field can not be blank value.",
            ]
        );

        $name = ucwords(trim($request->module_name));

        try {
            $data = LearningModule::create([
                "category_module_id" => (int) $request->category_module_id,
                "sub_category_module_id" => (int) $request->sub_category_module_id,
                "type_module_id" => (int) $request->type_module_id,
                "module_name" => $name,
                "module_desc" => $request->module_desc ?? null,
                "can_download" => (int) $request->can_download == 1 ? true : false,
                "has_quiz" => (int) $request->has_quiz == 1 ? true : false,
                "status" => true
            ]);

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal menambahkan modul pembelajaran baru"
                ]);
            }


            if ($request->type_mode_setting == 'video') {
                $failedPost = [];
                if ($request->sub_modules && count($request->sub_modules) > 0) {
                    foreach (collect($request->sub_modules) as $key => $value) {
                        $value = json_decode($value, true);

                        $moduleFile = FileModule::create([
                            "module_id" => $data->id,
                            "topic" => $value['topic'],
                            "description" => $value['description'],
                            "filepath" => $value['link']
                        ]);

                        if (!$moduleFile) {
                            array_push($failedPost, ["topic" => $value["topic"]]);
                        }
                    }
                }

                if (count($failedPost) > 0) {
                    return response()->json([
                        "error" => true,
                        "message" => "Modul pembelajaran berhasil ditambahkan. Namun beberapa topik gagal disimpan",
                        "results" => $failedPost,
                    ], 400);
                }
            } else {

                $failedUpload = [];

                if ($request->sub_modules && count($request->sub_modules) > 0) {
                    foreach (collect($request->sub_modules) as $key => $value) {
                        $value = json_decode($value, true);
                        $dataFile = null;

                        $moduleFile = FileModule::create([
                            "module_id" => $data->id,
                            "topic" => $value['topic'],
                            "description" => $value['description'],
                        ]);

                        if (!$moduleFile) {
                            array_push($failedUpload, ["filename" => $dataFile != null ? $dataFile["filename"] : "No file upload"]);
                        } else {
                            if ($request->hasFile("file_upload_{$key}")) {
                                $file = $request->file("file_upload_{$key}");
                                $dataFile = $this->uploadFile($file, $fileTypes, "module_{$moduleFile->id}");
                            }

                            $moduleFile->update([
                                "filename" => $dataFile != null ? $dataFile["filename"] : null,
                                "filepath" => $dataFile != null ? $dataFile["filepath"] : null,
                                "composite" => $dataFile != null ? $dataFile["composite"] : null
                            ]);
                        }
                    }
                }

                if (count($failedUpload) > 0) {
                    return response()->json([
                        "error" => true,
                        "message" => "Modul pembelajaran berhasil ditambahkan. Namun berkas modul pembelajaran gagal upload",
                        "results" => $failedUpload,
                    ], 400);
                }
            }

            return response()->json([
                "error" => false,
                "message" => "Modul pembelajaran berhasil ditambahkan"
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $fileTypes = $this->getFileTypeCollection();

        $request->validate(
            [
                "module_name" => "required|unique:learning_modules,module_name," . $id,
                "category_module_id" => "required",
                "sub_category_module_id" => "required",
                "type_module_id" => "required",
                "can_download" => "required",
                "has_quiz" => "required",
            ],
            [
                "module_name.required" => "The :attribute field can not be blank value.",
                "category_module_id.required" => "The :attribute field can not be blank value.",
                "sub_category_module_id.required" => "The :attribute field can not be blank value.",
                "type_module_id.required" => "The :attribute field can not be blank value.",
                "can_download.required" => "The :attribute field can not be blank value.",
                "has_quiz.required" => "The :attribute field can not be blank value.",
            ]
        );

        $name = ucwords(trim($request->module_name));

        $data = LearningModule::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data modul pembelajaran tidak ditemukan"
            ]);
        }

        $data->category_module_id = (int) $request->category_module_id ?? $data->category_module_id;
        $data->sub_category_module_id = (int) $request->sub_category_module_id ?? $data->sub_category_module_id;
        $data->type_module_id = (int) $request->type_module_id ??  $data->type_module_id;
        $data->module_name = $name;
        $data->module_desc = $request->module_desc ??  $data->module_desc;
        $data->can_download = (int) $request->can_download == 1 ? true : false;
        $data->has_quiz = (int) $request->has_quiz == 1 ? true : false;
        $data->save();

        if ($request->type_mode_setting == 'video') {
            $failedPost = [];
            if ($request->sub_modules && count($request->sub_modules) > 0) {
                $listId = [];
                foreach (collect($request->sub_modules) as $key => $value) {
                    $value = json_decode($value, true);

                    if ($value["id"] != '' && $value['deleted']) {
                        array_push($listId, $value["id"]);
                    } else if ($value["deleted"] == false) {

                        if ($value['id'] == '' || $value['id'] == null) {
                            $moduleFile = FileModule::create([
                                "module_id" => $data->id,
                                "topic" => $value['topic'],
                                "description" => $value['description'],
                                "filepath" => $value['files']
                            ]);

                            if (!$moduleFile) {
                                array_push($failedPost, ["topic" => $value["topic"]]);
                            }
                        }
                    }
                }

                if (count($listId) > 0) {
                    foreach ($listId as $value) {
                        $moduleFile = FileModule::find($value);
                        $moduleFile->delete();
                    }
                }
            }

            if (count($failedPost) > 0) {
                return response()->json([
                    "error" => true,
                    "message" => "Modul pembelajaran berhasil diperbarui. Namun berkas modul pembelajaran gagal disimpan",
                    "results" => $failedPost,
                ], 400);
            }
        } else {
            $failedUpload = [];

            if ($request->sub_modules && count($request->sub_modules) > 0) {
                $listId = [];
                foreach (collect($request->sub_modules) as $key => $value) {
                    $value = json_decode($value, true);
                    $dataFile = null;


                    if ($value["id"] != '' && $value['deleted']) {
                        array_push($listId, $value["id"]);
                    } else if ($value["deleted"] == false && $request->hasFile("file_upload_{$key}")) {
                        $file = $request->file("file_upload_{$key}");

                        $moduleFile = FileModule::create([
                            "module_id" => $data->id,
                            "topic" => $value['topic'],
                            "description" => $value['description'],
                        ]);

                        if (!$moduleFile) {
                            array_push($failedUpload, ["filename" => $dataFile != null ? $dataFile["filename"] : "No file upload"]);
                        } else {
                            if ($request->hasFile("file_upload_{$key}")) {
                                $file = $request->file("file_upload_{$key}");
                                $dataFile = $this->uploadFile($file, $fileTypes, "module_{$moduleFile->id}");
                            }

                            $moduleFile->update([
                                "filename" => $dataFile != null ? $dataFile["filename"] : null,
                                "filepath" => $dataFile != null ? $dataFile["filepath"] : null,
                                "composite" => $dataFile != null ? $dataFile["composite"] : null
                            ]);
                        }
                    }
                }

                if (count($listId) > 0) {
                    $this->deleteItemFile($listId);
                }
            }

            if (count($failedUpload) > 0) {
                return response()->json([
                    "error" => true,
                    "message" => "Modul pembelajaran berhasil disimpan. Namun berkas modul pembelajaran gagal upload",
                    "results" => $failedUpload,
                ], 400);
            }
        }


        return response()->json([
            "error" => false,
            "message" => "Modul pembelajaran berhasil diperbarui"
        ], 200);

        // try {

        // } catch (Exception $e) {
        //     return response()->json([
        //         "error" => true,
        //         "message" => $e->getMessage()
        //     ], $e->getCode());
        // }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = LearningModule::find($request->id);
            $message = $data->status == false ? "dinonaktifkan" : "diaktifkan kembali";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Module pembelajaran {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
