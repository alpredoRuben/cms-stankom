<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\Logo;
use App\Traits\GlobalManager;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class LogoController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "logo/{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return env("APP_URL") . "/storage/{$path}/{$filename}";
    }

    public function index()
    {
        $logo = Logo::orderBy('id', 'desc')->first();
        return view("pages.setting.logo.index", compact("logo"));
    }

    public function store(Request $request)
    {
        $filepath = null;
        $strUnique = time() . '' . Str::random(8);
        if ($request->hasFile('logo_app') && $request->file('logo_app')) {
            $file = $request->file('logo_app');
            $filepath = $this->uploadFile($file,  $strUnique);

            $logo = Logo::create([
                "logo_path" => $filepath,
                "str_unique" => $strUnique
            ]);

            if (!$logo) {
                return response()->json([
                    "error" => true,
                    "message" => "Tambah data logo baru gagal"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Logo berhasil ditambahkan"
            ], 201);
        } else {
            return response()->json([
                "error" => true,
                "message" => "Silahkan input gambar/logo"
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "content" => "required",
        ]);


        $logo = Logo::find($id);
        if (!$logo) {
            return response()->json([
                "error" => true,
                "message" => "Logo tidak ditemukan"
            ], 404);
        }

        $filepath = null;
        if ($request->hasFile('logo_app') && $request->file('logo_app')) {
            $file = $request->file('logo_app');
            $filepath = $this->uploadFile($file,  $logo->str_unique);

            $logo->logo_path = $filepath;
            $logo->save();

            if (!$logo) {
                return response()->json([
                    "error" => true,
                    "message" => "Ubah data logo baru gagal"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Logo berhasil diperbarui"
            ], 200);
        } else {
            return response()->json([
                "error" => true,
                "message" => "Silahkan input gambar/logo"
            ], 400);
        }
    }
}
