<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "nik", "text" => "NIK"],
            ["id" => "name", "text" => "Nama Lengkap"],
            ["id" => "email", "text" => "Email"],
            ["id" => "phone_number", "text" => "No. Ponsel"],
        ];
        return view('pages.master.users.members.index', compact('filterBox'));
    }
}
