<?php

namespace App\Http\Controllers\LMS;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsTag;
use App\Models\NewsTagList;
use App\Traits\GlobalManager;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";

        $path = "news/{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return [
            "success" => true,
            "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
            "filename" => $filename,
        ];
    }

    public function index()
    {
        return view("pages.master.news.index");
    }

    public function create()
    {
        return view("pages.master.news.create");
    }


    public function getTags()
    {
        try {
            $records = NewsTag::query();

            if (request()->input("term")) {
                $term = Str::lower(trim(request()->input("term")));
                $records = $records->where("tag_name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("tag_name", "desc")->get(['id', DB::raw('tag_name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function edit($id)
    {
        $record = News::with(["newsTagList"])->find($id);
        $tags = "";

        if (count($record->newsTagList) > 0) {
            foreach ($record->newsTagList as $key => $value) {
                $tags .= $value->tag->tag_name;

                if ($key < count($record->newsTagList) - 1) {
                    $tags .= ", ";
                }
            }
        } else {
            $tags = "";
        }

        return view("pages.master.news.update", compact('record', 'tags'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "news_title" => "required|unique:news,news_title",
                "news_photo" => "mimes:png,jpg,jpeg",
                "news_content"  => "required",
                "news_tags" => "required"
            ]
        );

        $title = trim($request->news_title) ?? "";

        try {
            $news = News::create([
                "news_title" => $title,
                "news_date" => Carbon::parse(Carbon::now())->format("Y-m-d"),
                "news_content" => $request->news_content,
            ]);

            if (!$news) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal menambahkan berita baru"
                ], 400);
            }

            if ($request->has('news_photo')) {
                if ($request->hasFile("news_photo")) {
                    $file = $request->file("news_photo");
                    $dataFile = $this->uploadFile($file, "news_{$news->id}");

                    if ($dataFile && $dataFile["success"] == true) {
                        $news = News::find($news->id);
                        $news->news_photo = $dataFile["filepath"];
                        $news->timestamps = false;
                        $news->save();
                    }
                }
            }


            $splitTags = $request->news_tags ? explode(",", trim($request->news_tags)) : [];
            if (count($splitTags) > 0) {

                foreach ($splitTags as $value) {
                    $tagSlug = Str::slug(Str::lower(trim($value)));
                    $tag = NewsTag::where('tag_slug', $tagSlug)->first();

                    if (!$tag) {
                        $tag = NewsTag::create([
                            "tag_name" => Str::lower(trim($value)),
                            "tag_slug" => $tagSlug
                        ]);
                    }

                    $findTagList = NewsTagList::where("news_id", $news->id)
                        ->where("tag_id", $tag->id)
                        ->first();

                    if (!$findTagList) {
                        NewsTagList::create([
                            "news_id" => $news->id,
                            "tag_id" => $tag->id
                        ]);
                    }
                }
            }

            return response()->json([
                "error" => false,
                "message" => "Berita baru berhasil ditambahkan",
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "news_title" => "required|unique:news,news_title," . $id,
                "news_photo" => "mimes:png,jpg,jpeg",
                "news_content"  => "required",
                "news_tags" => "required"
            ]
        );

        $news = News::find($id);
        if (!$news) {
            return response()->json([
                "error" => true,
                "message" => "Data berita tidak ditemukan"
            ], 404);
        }

        $title = trim($request->news_title) ?? "";


        try {

            $news->news_title = $title;
            $news->news_content = $request->news_content;
            $news->save();

            if (!$news) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal mengubah data berita"
                ], 400);
            }

            if ($request->has('news_photo')) {

                if ($request->hasFile("news_photo")) {
                    $file = $request->file("news_photo");
                    $dataFile = $this->uploadFile($file, "news_{$news->id}");

                    if ($dataFile && $dataFile["success"] == true) {
                        $news = News::find($news->id);
                        $news->news_photo = $dataFile["filepath"];
                        $news->timestamps = false;
                        $news->save();
                    }
                }
            }

            $splitTags = $request->news_tags ? explode(",", trim($request->news_tags)) : [];
            if (count($splitTags) > 0) {
                $tagId = [];
                foreach ($splitTags as $value) {
                    $tagSlug = Str::slug(Str::lower(trim($value)));
                    $tag = NewsTag::where('tag_slug', $tagSlug)->first();

                    if (!$tag) {
                        $tag = NewsTag::create([
                            "tag_name" => Str::lower(trim($value)),
                            "tag_slug" => $tagSlug
                        ]);
                    }

                    $listTag = NewsTagList::where("news_id", $news->id)
                        ->where("tag_id", $tag->id)
                        ->first();

                    if (!$listTag) {
                        NewsTagList::create([
                            "news_id" => $news->id,
                            "tag_id" => $tag->id
                        ]);
                    }

                    array_push($tagId, $tag->id);
                }

                NewsTagList::where("news_id", $news->id)->whereNotIn("tag_id", $tagId)->delete();
            }

            return response()->json([
                "error" => false,
                "message" => "Berita baru berhasil diperbarui"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = News::find($request->id);
            $message = $data->status == false ? "dinonaktifkan" : "diaktifkan kembali";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data berita {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function setHeadline($id)
    {
        $news = News::find($id);
        $news->is_headline = true;
        $news->save();
        if (!$news) {
            return response()->json([
                "error" => true,
                "message" => "Gagal menetapkan headline"
            ], 400);
        }
        return response()->json([
            "error" => true,
            "message" => "Berhasil menetapkan headline"
        ], 200);
    }

    public function disabledHeadline($id)
    {
        $news = News::find($id);
        $news->is_headline = false;
        $news->save();

        if (!$news) {
            return response()->json([
                "error" => true,
                "message" => "Nonaktifkan headline gagal"
            ], 400);
        }
        return response()->json([
            "error" => true,
            "message" => "Nonaktifkan headline berhasil"
        ], 200);
    }
}
