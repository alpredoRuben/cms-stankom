<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserProfile;
use App\Traits\GlobalManager;
use Exception;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ProfileController extends Controller
{
    use GlobalManager;

    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "avatar/user{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return [
            "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
            "filename" => $filename,
        ];
    }

    public function preview()
    {
        $roles = Role::get();
        $user = User::with(["userProfile"])->find(request()->user()->id);
        return view("pages.profile.preview", compact("user", "roles"));
    }


    public function uploadAvatar(Request $request)
    {
        $user = User::with(['userProfile'])->find($request->user()->id);

        if (!$user) {
            return response()->json([
                "message" => "Data pengguna tidak ditemukan",
            ], 404);
        }

        if ($request->hasFile("avatar")) {
            $file = $request->file('avatar');
            $data = $this->uploadFile($file, $user->id);

            $userProfile = UserProfile::where('user_id', $user->id)->first();

            if (!$userProfile) {
                $userProfile = UserProfile::create([
                    "user_id" => $user->id,
                    "photo" => $data["filepath"]
                ]);
            } else {
                $userProfile->update([
                    "photo" => $data["filepath"]
                ]);
            }

            return response()->json([
                "error" => false,
                "message" => "Upload avatar berhasil",
                "data" => $userProfile
            ], 200);
        } else {
            return response()->json([
                "error" => true,
                "message" => "File yang diupload tidak ditemukan"
            ], 404);
        }
    }

    public function edit()
    {
        $user = User::with([
            'userProfile' => function ($q) {
                $q->with(["ward", "education", "job_profession"]);
            },

        ])->find(request()->user()->id);

        $genders = [
            ["id" => "pria", "text" => "Pria"],
            ["id" => "wanita", "text" => "Wanita"]
        ];
        return view("pages.profile.update", compact("user", "genders"));
    }

    public function update(Request $request)
    {
        $user = User::find($request->user()->id);
        if (!$user) {
            return response()->json([
                "error" => false,
                "message" => "Data pengguna tidak ditemukan"
            ], 404);
        }

        $request->validate([
            "name" => "required",
            "email" => "required|email|unique:users,email," . $user->id,
            "gender" => "required",
        ]);


        try {

            $user->name = $request->name ?? $user->name;
            $user->email = $request->email ?? $user->email;

            if ($user->save()) {
                $saveProfile = UserProfile::where('user_id', $user->id)->first();

                if (!$saveProfile) {
                    $saveProfile = UserProfile::create([
                        "user_id" => $user->id,
                        "gender" => $request->gender ?? null,
                        "birth_date" => $request->birth_date ?? null,
                        "phone_number" => $request->phone ?? null,
                        "address" => $request->address ?? null,
                        "date_joined" => $request->date_joined ?? null,
                        "ward_id" => $request->ward_id ?? null,
                        "education_id" => $request->education_id ?? null,
                        "job_profession_id" => $request->job_profession_id ?? null,
                    ]);
                } else {
                    $saveProfile->update([
                        "gender" => $request->gender ?? $saveProfile->gender,
                        "birth_date" => $request->birth_date ?? $saveProfile->birth_date,
                        "phone_number" => $request->phone ?? $saveProfile->phone_number,
                        "address" => $request->address ??  $saveProfile->address,
                        "date_joined" => $request->date_joined ??  $saveProfile->date_joined,
                        "ward_id" => $request->ward_id ??  $saveProfile->ward_id,
                        "education_id" => $request->education_id ??  $saveProfilee->education_id,
                        "job_profession_id" => $request->job_profession_id ??  $saveProfile->job_profession_id,
                    ]);
                }


                return response()->json([
                    "error" => false,
                    "message" => "Data pengguna berhasil diperbarui",
                ], 200);
            }

            return response()->json([
                "error" => true,
                "message" => "Update profil pengguna gagal"
            ], 400);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }

        return response()->json([
            "error" => false,
            "message" => "Data profil berhasil diperbarui",
            "data" => $saveProfile
        ], 200);
    }

    public function editPassword()
    {
        return view("pages.profile.change_password");
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            "old_password" => "required",
            "new_password" => "required|required_with:confirm_new_password|same:confirm_new_password",
            "confirm_new_password" => "required"
        ]);

        $user = User::find($request->user()->id);

        if (!$user) {
            return response()->json([
                "message" => "Data pengguna tidak ditemukan"
            ], 404);
        }

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json(["message" => "Password lama tidak sesuai"], 400);
        }


        $user->password = bcrypt($request->new_password);
        $user->save();

        return response()->json([
            "message" => "Ubah password berhasil. Akun anda akan keluar secara otomatis"
        ], 200);
    }
}
