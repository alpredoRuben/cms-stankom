<?php

namespace App\Http\Controllers\LMS;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Province;
use Exception;
use Illuminate\Support\Facades\DB;

class ProvinceController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Propinsi"],
        ];
        return view('pages.master.locations.province.index', compact('filterBox'));
    }

    public function getProvinces()
    {
        try {
            $records = Province::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")
                ->get(['id', DB::raw('name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** FIND */
    public function find($id)
    {
        try {
            $data = Province::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|unique:provinces,name'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );


        try {

            $name = Str::lower(trim($request->name));
            $slug = Str::slug($name);

            $data = Province::create([
                "name" => ucwords($name),
                "slug" => $slug,
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data propinsi berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    /** UPDATE */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required|unique:provinces,name,' . $id
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        $data = Province::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data propinsi tidak ditemukan"
            ], 404);
        }


        try {
            $name = Str::lower(trim($request->name));
            $slug = Str::slug($name);

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data propinsi berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = Province::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data propinsi '{$name}' berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function restore($id)
    {
        try {
            $data = Province::withTrashed()->find($id);
            $name = $data->name;
            $data->restore();

            return response()->json([
                "error" => false,
                "message" => "Data {$name} berhasil dipulihkan kembali",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
