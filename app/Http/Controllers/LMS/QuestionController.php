<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\QuestionList;
use App\Models\Quiz;
use App\Models\QuizType;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        return view('pages.master.question.index');
    }

    public function create($quizID)
    {
        $quiz = Quiz::find($quizID);
        $quizType = QuizType::get();
        return view('pages.master.question.create', compact('quiz', 'quizType'));
    }
}
