<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Exception;

class QuizController extends Controller
{
    public function index()
    {
        return view("pages.master.quiz.index");
    }

    public function getAll()
    {
        $records = Quiz::where('status', true);

        if (request()->get('learning_id') && !empty(request()->get('learning_id'))) {
            $records = $records->where('learning_id', request()->get('learning_id'));
        }

        if (request()->get('webinar_id') && !empty(request()->get('webinar_id'))) {
            $records = $records->where('webinar_id', request()->get('webinar_id'));
        }

        $records = $records->orderBy('updated_at', 'desc')->get();

        return response()->json([
            "results" => $records,
            "learning_id" => request()->get('learning_id'),
            "webinar_id" => request()->get('webinar_id')
        ]);
    }

    public function create()
    {
        return view("pages.master.quiz.create");
    }

    public function show($id)
    {
        $record = Quiz::with([
            "classWebinar",
            "learningModule"
        ])->find($id);
        return view("pages.master.quiz.show", compact("record"));
    }

    public function edit($id)
    {
        $record = Quiz::with([
            "classWebinar",
            "learningModule"
        ])->find($id);
        return view("pages.master.quiz.update", compact("record"));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "name" => "required|unique:quiz,name",
                "webinar_id" => "required",
                "has_sertificated" => "required",
            ]
        );

        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));
            $code = "QUIZ_" . Str::random(20);

            $data = Quiz::create([
                "name" => $name,
                "sluq" => $slug,
                "code" => $code,
                "webinar_id" => $request->webinar_id,
                "learning_id" => $request->learning_id && empty($request->learning_id) ? null : $request->learning_id,
                "status" => true,
                "type" => $request->learning_id && empty($request->learning_id) ? "kelas" : "all",
                "has_sertificated"  => $request->has_sertificated == '1' ? true : false,
                "created_by" => $request->user()->id
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data quiz berhasil ditambahkan",
                "results" => $data,
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "name" => "required|unique:quiz,name," . $id,
                "webinar_id" => "required",
                "has_sertificated" => "required",
            ]
        );


        $data = Quiz::find($id);

        if (!$data) {
            return response()->json([
                "message" => "Data kuis tidak ditemukan."
            ], 404);
        }


        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data->name = $name;
            $data->sluq = $slug;
            $data->webinar_id = $request->webinar_id;
            $data->learning_id = $request->learning_id && empty($request->learning_id) ? null : $request->learning_id;
            $data->type = $request->learning_id && empty($request->learning_id) ? "kelas" : "all";
            $data->has_sertificated = $request->has_sertificated == '1' ? true : false;
            $data->updated_by =  $request->user()->id;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data quiz berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        $data = Quiz::find($request->id);
        $status = $data->status;
        $data->status = !$data->status;
        $data->updated_by = $request->user()->id;
        $data->save();

        return response()->json([
            "error" => false,
            "message" => $status == true ? "Berhasil menonaktifkan data kuis" : "Berhasil mengaktifkan kembali data kuis"
        ]);
    }
}
