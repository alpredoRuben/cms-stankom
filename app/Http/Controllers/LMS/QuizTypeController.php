<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\QuizType;
use Illuminate\Http\Request;

class QuizTypeController extends Controller
{
    public function getAll()
    {
        $records = QuizType::get();
        return response()->json([
            "results" => $records
        ]);
    }
}
