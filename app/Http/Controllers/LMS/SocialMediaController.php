<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\MedsosPage;
use App\Models\SocialMedia;
use Exception;
use Illuminate\Http\Request;

class SocialMediaController extends Controller
{
    public function index()
    {
        $sosmed = SocialMedia::with("medsos")->where('status', true)->get();
        $medsosPage = MedsosPage::with(['socialMedia'])->orderBy('updated_at', 'desc')
            ->paginate(5);
        return view("pages.master.social_media.index", compact("medsosPage", "sosmed"));
    }

    public function fetchMedsos(Request $request)
    {
        $medsosPage = MedsosPage::with(['socialMedia'])
            ->orderBy('updated_at', 'desc')
            ->paginate(5);

        return view("pages.master.social_media.sosmed", compact("medsosPage"));
    }




    public function show($id)
    {
        $medsosPage = MedsosPage::with(['socialMedia'])->find($id);
        return response()->json([
            "result" => $medsosPage
        ]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "medsos_id" => "required|unique:medsos_pages,medsos_id",
                "url" => "required"
            ]
        );

        try {
            $data = MedsosPage::create([
                "medsos_id" => $request->medsos_id,
                "url" => $request->url
            ]);

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal menambahkan media sosial"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Berhasil menambahkan media sosial",
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "medsos_id" => "required|unique:medsos_pages,medsos_id," . $id,
                "url" => "required"
            ]
        );

        $data = MedsosPage::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data media sosial tidak ditemukan"
            ], 404);
        }

        try {

            $data->medsos_id = $request->medsos_id;
            $data->url = $request->url;
            $data->save();

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal mengubah data media sosial"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Berhasil mengubah data media sosial"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function changeStatus(Request $request)
    {
        $data = MedsosPage::with(['socialMedia'])->find($request->id);
        $status = $data->status;


        if ($data->socialMedia->status == false) {
            return response()->json([
                "message" => "Status item icon media sosial tidak aktif. Anda tidak dapat mengkases perintah ini"
            ], 400);
        }


        $data->status = !$data->status;
        $data->save();

        return response()->json([
            "error" => false,
            "message" => $status == true ? "Berhasil menonaktifkan media sosial" : "Berhasil mengaktifkan kembali media sosial"
        ]);
    }
}
