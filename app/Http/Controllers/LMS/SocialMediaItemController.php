<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\MedsosPage;
use App\Models\SocialMedia;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SocialMediaItemController extends Controller
{
    public function index()
    {
        $status = true;

        if (request()->has('status') && request()->query('status') != '1') {
            $status = false;
        }

        $sosmed = SocialMedia::where('status', $status);

        if (request()->has('search') && request()->query('search') != '') {
            $search = request()->get('search');
            $sosmed = $sosmed->where("medsos_name", "ilike", "%${search}%");
        }

        $sosmed = $sosmed->orderBy('updated_at', 'desc')->paginate(8);

        if (request()->ajax()) {
            return view('pages.master.icon_sosmed.sosmed_paginate', compact('sosmed'))->render();
        }

        return view('pages.master.icon_sosmed.index', compact('sosmed'));
    }

    public function fetchMedsos()
    {
        $sosmed = SocialMedia::with("medsos")->where('status', true)->get();
        return response()->json(["result" => $sosmed]);
    }

    public function show($id)
    {
        $sosmed = SocialMedia::find($id);
        return response()->json([
            "result" => $sosmed
        ]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "medsos_name" => "required|unique:social_media,medsos_name",
                "medsos_icon" => "required",
            ]
        );

        try {
            $data = SocialMedia::create([
                "medsos_name" => Str::lower($request->medsos_name),
                "medsos_icon" => $request->medsos_icon
            ]);

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal menambahkan item media sosial"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Berhasil menambahkan item media sosial",
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                "medsos_name" => "required|unique:social_media,medsos_name," . $id,
                "medsos_icon" => "required",
            ]
        );

        $data = SocialMedia::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data item media sosial tidak ditemukan"
            ], 404);
        }

        try {

            $data->medsos_name = $request->medsos_name;
            $data->medsos_icon = $request->medsos_icon;
            $data->save();

            if (!$data) {
                return response()->json([
                    "error" => true,
                    "message" => "Gagal mengubah data item media sosial"
                ], 400);
            }

            return response()->json([
                "error" => false,
                "message" => "Berhasil mengubah data item media sosial"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }


    public function changeStatus(Request $request)
    {
        $data = SocialMedia::with(["medsos"])->find($request->id);
        $status = $data->status;
        $data->status = !$data->status;
        $data->save();

        if ($data->medsos) {
            $medsos = MedsosPage::find($data->medsos->id);
            $medsos->status = !$medsos->status;
            $medsos->save();
        }

        return response()->json([
            "error" => false,
            "message" => $status == true ? "Berhasil menonaktifkan icon dan akun media sosial" : "Berhasil mengaktifkan kembali icon dan akun media sosial"
        ]);
    }
}
