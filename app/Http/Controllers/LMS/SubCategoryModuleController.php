<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\SubCategoryModule;
use Exception;
use Illuminate\Support\Facades\DB;

class SubCategoryModuleController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Sub Kategori"],
            ["id" => "description", "text" => "Keterangan"],
        ];
        return view("pages.master.modul.sub_category.index", compact('filterBox'));
    }

    public function getSubCategoryModules()
    {
        try {

            $records = SubCategoryModule::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")->get(['id', DB::raw('name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function find($id)
    {
        try {
            $data = SubCategoryModule::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:sub_category_modules,name'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }

        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data = SubCategoryModule::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => $request->description ?? null,
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data sub kategori modul berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** UPDATE */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:sub_category_modules,name,' . $id
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }


        $data = SubCategoryModule::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data sub kategori modul tidak ditemukan"
            ], 404);
        }


        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->description = $request->description ?? $data->description;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data sub kategori modul berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = SubCategoryModule::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data sub kategori modul '{$name}', berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** RESTORE */
    public function restore($id)
    {
        try {
            SubCategoryModule::withTrashed()->find($id)->restore();

            return response()->json([
                "error" => false,
                "message" => "Data sub kategori modul berhasil dipulihkan",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
