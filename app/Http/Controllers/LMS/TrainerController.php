<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrainerController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "nik", "text" => "NIK"],
            ["id" => "name", "text" => "Nama Lengkap"],
            ["id" => "email", "text" => "Email"],
            ["id" => "phone_number", "text" => "No. Ponsel"],
        ];
        return view('pages.master.users.trainers.index', compact('filterBox'));
    }

    public function getTrainers()
    {
        try {

            $records = User::whereHas("roles", function ($query) {
                $query->where("name", "pengajar");
            })->where("status", true);

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")->get(['id', DB::raw('name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
