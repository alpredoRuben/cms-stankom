<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\TrainingRule;
use Exception;

class TrainingRuleController extends Controller
{
    public function index()
    {
        return view("pages.setting.training_rules.index");
    }

    public function create()
    {
        return view("pages.setting.training_rules.create");
    }

    public function edit($id)
    {
        $record = TrainingRule::find($id);
        return view("pages.setting.training_rules.update", compact('record'));
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "required|unique:training_rules,title",
            "description" => "required",
            "order_number" => "required",
        ]);

        $findOrderNumber = TrainingRule::where('order_number', $request->order_number)->first();

        if ($findOrderNumber) {
            return response()->json([
                "error" => true,
                "message" => "Maaf nomor urut {$request->order_number} sudah terdaftar"
            ], 400);
        }

        try {

            $title = trim($request->title);
            $data = TrainingRule::create([
                'title' => $title,
                'description' => $request->description,
                'order_number' => $request->order_number,
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data aturan pelatihan berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title" => "required|unique:training_rules,title, " . $id,
            "description" => "required",
            "order_number" => "required",
        ]);

        $title = trim($request->title);

        $findOrderNumber = TrainingRule::where('order_number', $request->order_number)
            ->where("id", "!=", $id)
            ->first();

        if ($findOrderNumber) {
            return response()->json([
                "error" => true,
                "message" => "Maaf nomor urut {$request->order_number} sudah terdaftar"
            ], 400);
        }

        $data = TrainingRule::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Aturan pelatihan tidak ditemukan"
            ], 404);
        }



        try {

            $data->title = $title;
            $data->description = $request->description;
            $data->order_number = $request->order_number;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data aturan pelatihan berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
