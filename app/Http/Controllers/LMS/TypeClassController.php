<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\TypeClass;
use Exception;
use Illuminate\Support\Facades\DB;

class TypeClassController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Tipe Kelas"],
            ["id" => "description", "text" => "Keterangan"],
        ];
        return view("pages.master.classes.type.index", compact('filterBox'));
    }

    public function getClassTypes()
    {
        try {

            $records = TypeClass::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")->get(['id', DB::raw('name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function find($id)
    {
        try {
            $data = TypeClass::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:type_classes,name'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }

        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data = TypeClass::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => $request->description ?? null,
                "status" => true
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data tipe kelas berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** UPDATE */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:type_classes,name,' . $id
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }


        $data = TypeClass::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data tipe kelas tidak ditemukan"
            ], 404);
        }


        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->description = $request->description ?? $data->description;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data tipe kelas berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = TypeClass::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data tipe kelas '{$name}', berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** CHANGE ACTIVATION */
    public function changeStatus(Request $request)
    {
        try {
            $data = TypeClass::find($request->id);
            $message = $data->status == true ? "berhasil dinonaktifkan" : "berhasil diaktifkan";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Tipe kelas {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** RESTORE */
    public function restore($id)
    {
        try {
            TypeClass::withTrashed()->find($id)->restore();

            return response()->json([
                "error" => false,
                "message" => "Data tipe kelas berhasil dipulihkan",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
