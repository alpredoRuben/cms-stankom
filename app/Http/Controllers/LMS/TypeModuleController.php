<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\TypeModule;
use Exception;
use Illuminate\Support\Facades\DB;

class TypeModuleController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Tipe Modul"],
            ["id" => "description", "text" => "Keterangan"],
        ];
        return view("pages.master.modul.type.index", compact("filterBox"));
    }

    public function getTypeModules()
    {
        try {

            $records = TypeModule::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("name", "desc")->get(['id', DB::raw('name as text')]);
            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function find($id)
    {
        try {
            $data = TypeModule::find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** STORE */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:type_modules,name'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }

        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));

            $data = TypeModule::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => $request->description ?? null,
            ]);

            return response()->json([
                "error" => false,
                "message" => "Data tipe modul berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** UPDATE */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:type_modules,name,' . $id
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validator->errors()->first()
            ], 422);
        }


        $data = TypeModule::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data tipe modul tidak ditemukan"
            ], 404);
        }


        try {
            $name = trim($request->name);
            $slug = Str::slug(Str::lower($name));


            $data->name = ucwords($name);
            $data->slug = $slug;
            $data->description = $request->description ?? $data->description;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Data tipe modul berhasil diperbarui"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** DELETE */
    public function delete($id)
    {
        $data = TypeModule::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data tipe modul '{$name}', berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /** RESTORE */
    public function restore($id)
    {
        try {
            TypeModule::withTrashed()->find($id)->restore();

            return response()->json([
                "error" => false,
                "message" => "Data tipe modul berhasil dipulihkan",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
