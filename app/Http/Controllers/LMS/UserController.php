<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\Mail\ActivationAcountEmail;
use App\Models\MailSchedule;
use App\Models\User;
use App\Traits\GlobalManager;
use App\Traits\StaticProperties;
use Spatie\Permission\Models\Role;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    use StaticProperties, GlobalManager;

    public function index()
    {
        $filterBox = [
            ["id" => "nik", "text" => "NIK"],
            ["id" => "name", "text" => "Nama Lengkap"],
            ["id" => "email", "text" => "Email"],
            ["id" => "role", "text" => "Kelompok Pengguna"],
            ["id" => "phone_number", "text" => "No. Ponsel"],
        ];

        $roles = Role::get();

        return view('pages.master.users.all_user.index', compact('filterBox', 'roles'));
    }

    public function create()
    {
        $genders = [
            ["id" => "pria", "text" => "Pria"],
            ["id" => "wanita", "text" => "Wanita"]
        ];
        $roles = Role::get();
        return view('pages.master.users.all_user.create_user', compact("roles", "genders"));
    }

    public function edit($id)
    {
        $user = User::with([
            'userProfile' => function ($q) {
                $q->with(["ward", "education", "job_profession"]);
            },

        ])->find($id);
        $roles = Role::get();
        $genders = [
            ["id" => "pria", "text" => "Pria"],
            ["id" => "wanita", "text" => "Wanita"]
        ];
        return view('pages.master.users.all_user.update_user', compact("roles", "user", "genders"));
    }

    // STORE
    public function store(Request $request)
    {
        $request->validate([
            "nik" => "required|unique:user_profiles,nik",
            "name" => "required|string",
            "email" => "required|email|unique:users,email",
            "gender" => "required",
            "role_id" => "required"
        ]);

        $password = Str::random(10);
        $token = Str::random(60);

        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($password),
            "status" => false
        ]);

        if (!$user) {
            return response()->json([
                "message" => "Gagal menyimpan data pengguna"
            ], 400);
        }

        $filepath = "";

        if ($request->hasFile("avatar")) {
            $file = $request->file('avatar');
            $data = $this->uploadFile($file, $user->id);
            $filepath = $data["filepath"];
        }


        if (!$user->userProfile) {
            $user->userProfile()->create([
                "nik" => $request->nik,
                "gender" => $request->gender,
                "phone_number" => $request->phone ?? null,
                "birth_date" => $request->birth_date ?? null,
                "address" => $request->address ?? null,
                "ward_id" => $request->ward_id ?? null,
                "education_id" => $request->education_id ?? null,
                "date_joined" => Carbon::now()->format("Y-m-d"),
                "job_profession_id" => $request->job_profession_id ?? null,
                "photo" => $filepath != "" ? $filepath : null
            ]);
        }

        $role = Role::find($request->role_id);

        if ($role) {
            $user->assignRole([$role->id]);
        }

        if (!$user->verifyUser) {
            $user->verifyUser()->create([
                "token" => $token,
            ]);
        }

        MailSchedule::create([
            "type" => "activation",
            "data" => json_encode([
                "token" => $token,
                "password" => null,
                "email" => $user->email,
                "name" => $user->name,
            ])
        ]);


        return response()->json([
            "error" => false,
            "message" => "Data pengguna berhasil disimpan",
        ], 201);

        // try {

        // } catch (Exception $e) {
        //     return response()->json([
        //         "error" => true,
        //         "message" => $e->getMessage()
        //     ], $e->getCode());
        // }
    }

    // UPDATE
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return response()->json([
                "error" => false,
                "message" => "Data pengguna tidak ditemukan"
            ], 404);
        }

        $request->validate([
            "nik" => "required|unique:user_profiles,nik," . $user->userProfile->id,
            "name" => "required",
            "email" => "required|email|unique:users,email," . $user->id,
            "gender" => "required",
            "role_id" => "required"
        ]);

        try {

            $user->name  = $request->name;
            $user->email = $request->email;

            if ($user->save()) {

                $filepath = "";

                if ($request->hasFile("avatar")) {
                    $file = $request->file('avatar');
                    $data = $this->uploadFile($file, $user->id);
                    $filepath = $data["filepath"];
                }

                $user->userProfile()->update([
                    "nik" => $request->nik,
                    "gender" => $request->gender,
                    "phone_number" => $request->phone ?? null,
                    "birth_date" => $request->birth_date ?? null,
                    "address" => $request->address ?? null,
                    "ward_id" => $request->ward_id ?? null,
                    "education_id" => $request->education_id ?? null,
                    "job_profession_id" => $request->job_profession_id ?? null,
                    "photo" => $filepath
                ]);

                $role = Role::find($request->role_id);

                if ($role) {
                    $user->syncRoles([$role->name]);
                }

                return response()->json([
                    "error" => false,
                    "message" => "Data pengguna berhasil diperbarui",
                ], 200);
            }

            return response()->json([
                "error" => true,
                "message" => "Gagal menyimpan data pengguna"
            ], 400);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    // CHANGE ACTIVATION
    public function changeStatus(Request $request)
    {
        try {
            $data = User::find($request->id);
            $message = $data->status == true ? "berhasil dinonaktifkan" : "berhasil diaktifkan";

            $data->status = !$data->status;
            $data->save();

            return response()->json([
                "error" => false,
                "message" => "Akun {$data->name} {$message}",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function showExport($type)
    {
        $rolename = 'all';
        if ($type != 'all') {
            $rolename = Role::find($type)->name;
        }

        $userProperties = $this->getStaticUserProperties();
        return view("pages.master.users.export_view", compact('userProperties', 'rolename'));
    }


    public function uploadFile($file, $unique)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = Str::random(20) . "_" . time() . "_{$unique}.{$extension}";
        $path = "avatar/user{$unique}";
        $this->removeDirectory($path);
        Storage::putFileAs("public/{$path}", $file, $filename);
        return [
            "filepath" => env("APP_URL") . "/storage/{$path}/{$filename}",
            "filename" => $filename,
        ];
    }

    public function uploadPhoto(Request $request)
    {
        if (!$request->user_id) {
            return response()->json([
                "error" => true,
                "message" => "ID user kosong"
            ], 400);
        }

        $user = User::find($request->user_id);

        if (!$user) {
            return response()->json([
                "error" => true,
                "message" => "ID User tidak ditemukan"
            ], 404);
        }

        if ($request->hasFile("avatar")) {
            $file = $request->file('avatar');
            $data = $this->uploadFile($file, $user->id);

            $user->userProfile()->update([
                "photo" => $data["filepath"]
            ]);

            return response()->json([
                "error" => false,
                "message" => "Upload avatar berhasil",
                "data" => $user->userProfile->photo
            ], 200);
        }

        return response()->json([
            "error" => true,
            "message" => "File yang diupload tidak ditemukan"
        ], 404);
    }
}
