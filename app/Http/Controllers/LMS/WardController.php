<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\PostalCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\Ward;
use Exception;
use Illuminate\Support\Facades\DB;

class WardController extends Controller
{
    public function index()
    {
        $filterBox = [
            ["id" => "name", "text" => "Nama Kelurahan"],
            ["id" => "district_name", "text" => "Nama Kecamatan"],
            ["id" => "postal_code", "text" => "Kode Pos"],
        ];
        return view('pages.master.locations.ward.index', compact('filterBox'));
    }

    public function getWards()
    {
        try {
            $records = Ward::query();

            if (request()->input("term")) {
                $term = trim(request()->input("term"));
                $records = $records->where("name", "ilike", "%{$term}%");
            }

            $records = $records->orderBy("id", "asc")
                ->get(['id', DB::raw('name as text')]);

            return response()->json([
                "results" => $records,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function find($id)
    {
        try {
            $data = Ward::with([
                "district",
                "postalCodes"
            ])->find($id);

            return response()->json([
                "error" => false,
                "message" => "Pencarian data berhasil",
                "result" => $data
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function getWardDetail($id)
    {
        $data = Ward::find($id);

        return response()->json([
            "result" => [
                "id" => $data->id,
                "ward_name" => $data->name,
                "postal_code" => $data->postalCodes->code ?? "",
                "district_name" => $data->district->name ?? "",
                "city_name" => $data->district->city->name ?? "",
                "province_name" => $data->district->city->province->name ?? ""
            ]
        ], 200);
    }

    // STORE
    public function store(Request $request)
    {
        $request->validate(
            [
                'district' => 'required',
                'name' => 'required|unique:wards,name',
            ],
            [
                'district.required' => 'The :attribute field can not be blank value.',
                'name.required' => 'The :attribute field can not be blank value.'
            ]
        );

        $name = Str::lower(trim($request->name));
        $slug = Str::slug($name);
        $code = Ward::max('code');

        try {

            $data = Ward::create([
                "district_id" => $request->district,
                "name" => ucwords($name),
                "slug" => $slug,
                "code" => (int) $code + 1
            ]);

            if ($request->postal_code) {
                $postalCode = PostalCode::where('code', $request->postal_code)->first();

                if ($postalCode) {
                    if ($postalCode->ward_id != $data->id) {
                        return response()->json([
                            "error" => true,
                            "message" => "Opps. Maaf Kode pos sudah pernah terdaftar"
                        ], 400);
                    }
                } else {
                    $data->postalCodes()->create(["code" => $request->postal_code]);
                }
            }

            return response()->json([
                "error" => false,
                "message" => "Data kelurahan berhasil ditambahkan",
                "results" => $data
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    // UPDATE
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required|unique:wards,name,' . $id,
                'district' => 'required'
            ],
            [
                'name.required' => 'The :attribute field can not be blank value.',
                'district.required' => 'The :attribute field can not be blank value.'
            ]
        );


        $data = Ward::find($id);

        if (!$data) {
            return response()->json([
                "error" => true,
                "message" => "Data kelurahan tidak ditemukan"
            ], 404);
        }

        $name = Str::lower(trim($request->name));
        $slug = Str::slug($name);

        $data->name = ucwords($name);
        $data->slug = $slug;
        $data->district_id = $request->district == 'same' ? $data->district_id : $request->district;
        $data->save();

        if ($request->postal_code) {
            $postalCode = PostalCode::where('code', $request->postal_code)->first();

            if ($postalCode) {
                if ($postalCode->ward_id != $data->id) {
                    return response()->json([
                        "error" => true,
                        "message" => "Opps. Maaf Kode pos sudah pernah terdaftar"
                    ], 400);
                }
            } else {
                $postalCode = PostalCode::create([
                    'ward_id' => $data->id,
                    'code' => $request->postal_code
                ]);
            }
        }

        return response()->json([
            "error" => false,
            "message" => "Data kelurahan berhasil diperbarui"
        ], 200);

        // try {

        // } catch (Exception $e) {
        //     return response()->json([
        //         "error" => true,
        //         "message" => $e->getMessage()
        //     ], $e->getCode());
        // }
    }

    // DELETE
    public function delete($id)
    {
        $data = Ward::find($id);

        try {
            $name = $data->name;
            $data->delete();

            return response()->json([
                "error" => false,
                "message" => "Data kelurahan '{$name}' berhasil dihapus",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function restore($id)
    {
        try {
            $data = Ward::withTrashed()->find($id);
            $name = $data->name;
            $data->restore();

            return response()->json([
                "error" => false,
                "message" => "Data {$name} berhasil dipulihkan kembali",
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
