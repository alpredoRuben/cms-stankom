<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Contact;
use App\Models\TrainingRule;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;

class WebDatatableController extends Controller
{
    public function getContactUs()
    {
        $data = Contact::orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->editColumn('address', function ($item) {
                $address = $item->address ? strip_tags($item->address) : "";
                return $address;
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                $str .= '<button type="button" class="btn btn-dark btn-rounded mx-2 py-2 px-4" onclick="eventEditData(' . $item->id . ')"><i class="fas fa-pencil"></i> Ubah</button>&nbsp;';

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'address'])
            ->make(true);
    }

    public function getAboutUs()
    {
        $data = AboutUs::orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->editColumn('content', function ($item) {
                $newsContent = $item->content ? strip_tags($item->content) : "";
                return strlen($newsContent) > 300 ? substr($newsContent, 0, 300) . "....(more)" : $newsContent;
            })
            ->editColumn('filepath', function ($item) {
                if ($item->filepath) {
                    return "<a href='{$item->filepath}' class='text text-primary' target='_blank'>Preview File</a>";
                }
                return "";
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                $str .= '<a href="' . route('setting.about_us.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'filepath', 'content'])
            ->make(true);
    }

    public function getBanners()
    {
        $data = Banner::orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->editColumn('banner_content', function ($item) {
                $newsContent = $item->banner_content ? strip_tags($item->banner_content) : "";
                return strlen($newsContent) > 300 ? substr($newsContent, 0, 300) . "....(more)" : $newsContent;
            })
            ->addColumn('banner_link', function ($item) {
                if ($item->link) {
                    return "<div class='d-grid'><a href='{$item->link}' class='badge bg-primary badge-default' target='_blank'>Link</a></div>";
                }
                return "";
            })
            ->addColumn('images', function ($item) {
                if ($item->banner_image) {
                    return "<div class='d-grid'><a href='{$item->banner_image}' class='badge bg-primary badge-default' target='_blank'>Gambar</a></div>";
                }
                return "";
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                $str .= '<a href="' . route('setting.banner.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                return $str;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'banner_link', 'banner_content', 'images'])
            ->make(true);
    }

    public function getTrainingRules()
    {
        $data = TrainingRule::orderBy('updated_at', 'desc');

        return DataTables::of($data)
            ->editColumn('order_number', function ($item) {
                return $item->order_number ? "<span class='badge bg-success'>{$item->order_number}</span>" : "";
            })
            ->addColumn('action', function ($item) {
                $str  = '';

                $str .= '<a href="' . route('setting.training_rule.edit', $item->id) . '" class="btn btn-dark btn-rounded mx-2 py-2 px-4"><i class="fas fa-pencil"></i> Ubah</a>&nbsp;';

                return $str;
            })
            ->editColumn('description', function ($item) {
                $description = $item->description ? strip_tags($item->description) : "";
                return strlen($description) > 200 ? substr($description, 0, 200) . "....(more)" : $description;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'order_number', 'description'])
            ->make(true);
    }
}
