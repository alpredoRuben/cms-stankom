<?php

namespace App\Http\Controllers\LMS;

use App\Http\Controllers\Controller;
use App\Models\WhatsAppWidget;
use Exception;
use Illuminate\Http\Request;

class WhatsappController extends Controller
{
    public function index()
    {
        $totalRow = WhatsAppWidget::count();
        $whatsapp = WhatsAppWidget::orderBy('updated_at', 'desc')->first();
        return view('pages.setting.whatsapp.index', compact('totalRow', 'whatsapp'));
    }

    public function find($id)
    {
        $whatsapp = WhatsAppWidget::find($id);
        return response()->json(["result" => $whatsapp]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "nomor_wa" => "required",
        ]);

        $whatsapp = WhatsAppWidget::create([
            "nomor_wa" => $request->nomor_wa
        ]);


        if (!$whatsapp) {
            return response()->json([
                "message" => "Gagal menyimpan nomor whatsapp"
            ], 400);
        }

        return response()->json([
            "message" => "Nomor whatsapp {$request->nomor_wa} berhasil disimpan"
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "nomor_wa" => "required|unique:whats_app_widgets,nomor_wa," . $id,
        ]);

        $whatsapp = WhatsAppWidget::find($id);

        if (!$whatsapp) {
            return response()->json([
                "message" => "Nomor whatsapp tidak ditemukan"
            ], 404);
        }


        try {
            $whatsapp->nomor_wa = $request->nomor_wa;
            $whatsapp->save();


            return response()->json([
                "message" => "Nomor whatsapp berhasil diubah"
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => true,
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
