<?php

namespace App\Http\Controllers\Major;

use App\Http\Controllers\Controller;
use App\Models\LearningModule;
use App\Models\User;
use App\Models\UserLearningModule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mockery\Mock;

class ModuleController extends Controller
{
    public function index()
    {
        return view('pages.major.modules.index');
    }

    public function paginateModule()
    {
        $user = request()->user();
        $modules = LearningModule::with([
            "moduleOfUsers",
            "fileModules",
            "typeModule",
            "subCategoryModule",
            "categoryModule"
        ])
            ->where('status', true)
            ->whereHas('moduleOfUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            });

        if (request()->query('category')) {
            $modules = $modules->where('category_module_id', request()->get('category'));
        }

        if (request()->query('sub_category')) {
            $modules = $modules->where('sub_category_module_id', request()->get('sub_category'));
        }

        if (request()->query('type')) {
            $modules = $modules->where('type_module_id', request()->get('type'));
        }

        $modules = $modules->paginate(6);

        $modules->getCollection = $modules->getCollection()->transform(function ($item) {
            $desc = $item->module_desc ? strip_tags($item->module_desc) : '';
            $item->strip_desc = $desc != '' ? Str::substr($desc, 0, 200) . '...(selengkapnya)' : '';
            return $item;
        });

        return response()->json($modules);
    }


    public function show($id)
    {
        $learningModule = LearningModule::with([
            "categoryModule",
            "subCategoryModule",
            "typeModule",
            "fileModules"
        ])->find($id);

        $user = User::with(['userProfile'])->find(request()->user()->id);

        $learnUser = UserLearningModule::where('user_id', $user->id)
            ->where('learning_module_id', $learningModule->id)->first();

        return view('pages.major.modules.show', compact('learningModule', 'user', 'learnUser'));
    }

    public function updateProfil($module_id)
    {
        $user = User::with([
            "userProfile",
        ])->find(request()->user()->id);

        return view('pages.major.profile', compact('user'));
    }

    public function postTicket(Request $request)
    {
        $ticket = UserLearningModule::create([
            "learning_module_id" => $request->learning_module_id,
            "user_id" => $request->user()->id,
            "ticked_code" => Str::random(20) . '-' . $request->learning_module_id . '-' . $request->user()->id
        ]);

        if (!$ticket) {
            return response()->json([
                "error" => true,
                "message" => "Registrasi modul pembelajaran gagal"
            ], 400);
        }

        return response()->json([
            "error" => false,
            "message" => "Registrasi modul pembelajaran berhasil"
        ]);
    }
}
