<?php

namespace App\Http\Controllers\Major;

use App\Http\Controllers\Controller;
use App\Models\ClassWebinar;
use App\Models\User;
use App\Models\UserWebinar;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WebinarController extends Controller
{

    public function index()
    {
        return view('pages.major.webinar.index');
    }


    public function paginateWebinar()
    {
        $user = request()->user();
        $webinars = ClassWebinar::with([
            "trainer",
            "typeClass",
            "userWebinars"
        ])
            ->where('status', true)
            ->whereHas('userWebinars', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            });


        if (request()->query('type_class')) {
            $modules = $modules->where('class_type_id', request()->get('type_class'));
        }

        $webinars = $webinars->paginate(6);

        $webinars->getCollection = $webinars->getCollection()->transform(function ($item) {
            $desc = $item->description ? strip_tags($item->description) : '';
            $item->strip_desc = $desc != '' ? Str::substr($desc, 0, 200) . '...(selengkapnya)' : '';
            return $item;
        });

        return response()->json($webinars);
    }

    public function show($id)
    {
        $webinar = ClassWebinar::with([
            "trainer",
            "typeClass",
            "userWebinars"
        ])->find($id);

        $user = User::with(['userProfile'])->find(request()->user()->id);

        $userWebinar = UserWebinar::where('user_id', $user->id)
            ->where('webinar_id', $webinar->id)->first();

        return view('pages.major.webinar.show', compact('webinar', 'user', 'userWebinar'));
    }


    public function updateProfil($webinar_id)
    {
        $user = User::with([
            "userProfile",
        ])->find(request()->user()->id);

        return view('pages.major.profile', compact('user'));
    }

    public function postTicket(Request $request)
    {
        $webinar = ClassWebinar::with("userWebinars")->find($request->webinar_id);

        if ($webinar->userWebinars()->count() >= $webinar->quota) {
            return response()->json([
                "error" => true,
                "message" => "Kuota sudah penuh"
            ], 400);
        }


        $ticket = UserWebinar::create([
            "webinar_id" => $request->webinar_id,
            "user_id" => $request->user()->id,
            "ticked_code" => Str::random(20) . '-' . $request->webinar_id . '-' . $request->user()->id
        ]);

        if (!$ticket) {
            return response()->json([
                "error" => true,
                "message" => "Registrasi modul pembelajaran gagal"
            ], 400);
        }

        return response()->json([
            "error" => false,
            "message" => "Registrasi modul pembelajaran berhasil"
        ]);
    }
}
