<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Contact;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function aboutUs()
    {
        $contactUs = Contact::orderBy('updated_at', 'desc')->first();
        $aboutUs = AboutUs::orderBy('updated_at', 'desc')->first();
        return view('pages.web.about_us.index', compact('aboutUs', 'contactUs'));
    }
}
