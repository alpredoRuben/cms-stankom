<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ArtikelController extends Controller
{
    public $rowLength = 10;

    public function articles()
    {
        $headlines = Article::where('is_headline', true)->first();
        $updateArticles = Article::where('is_headline', false)
            ->where('article_date', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->take(5)->get();


        //LATEST ARTICLES
        $latestArticles = Article::where('is_headline', false)
            ->where('article_date', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->paginate($this->rowLength);


        //OLDEST ARTICLES
        $oldestArticles = Article::where('is_headline', false)
            ->where('article_date', '<', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->paginate($this->rowLength);


        return view('pages.web.articles.index', compact('headlines', 'updateArticles', 'latestArticles', 'oldestArticles'));
    }

    public function latestArticles(Request $request)
    {

        $latestArticles = Article::where('is_headline', false)
            ->where('article_date', Carbon::now()->format('Y-m-d'));

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $latestArticles = $latestArticles->whereHas("articleTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }


        $latestArticles = $latestArticles->orderBy('created_at', 'desc')->paginate($this->rowLength);

        if ($request->ajax()) {
            return view('pages.web.articles.latest_articles', compact('latestArticles'))->render();
        }
    }

    public function oldestArticles(Request $request)
    {

        $oldestArticles = Article::where('is_headline', false)
            ->where('article_date', '<', Carbon::now()->format('Y-m-d'));

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $oldestArticles = $latestArticles->whereHas("articleTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }


        $oldestArticles = $oldestArticles->orderBy('created_at', 'desc')->paginate($this->rowLength);

        if ($request->ajax()) {
            return view('pages.web.articles.oldest_articles', compact('oldestArticles'))->render();
        }
    }

    public function showArticles($id)
    {
        $articles = Article::find($id);
        return view('pages.web.articles.show', compact('articles'));
    }
}
