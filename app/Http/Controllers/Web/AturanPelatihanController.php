<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\TrainingRule;
use Illuminate\Http\Request;

class AturanPelatihanController extends Controller
{
    public function index(Request $request)
    {
        $trainingRules = TrainingRule::orderBy('order_number', 'asc')->paginate(3);
        return view("pages.web.training_rules.index", compact('trainingRules'));
    }

    public function fetchRules(Request $request)
    {
        $trainingRules = TrainingRule::orderBy('order_number', 'asc')->paginate(3);
        return view('pages.web.training_rules.training_list', compact('trainingRules'));
    }
}
