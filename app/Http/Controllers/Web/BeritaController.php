<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Support\Carbon;

class BeritaController extends Controller
{
    public $rowLength = 10;

    public function news()
    {
        $headlines = News::where('is_headline', true)->first();
        $updateNews = News::where('is_headline', false)
            ->where('news_date', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->take(5)->get();


        //LATEST NEWS
        $latestNews = News::where('is_headline', false)
            ->where('news_date', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->paginate($this->rowLength);


        //OLDEST NEWS
        $oldestNews = News::where('is_headline', false)
            ->where('news_date', '<', Carbon::now()->format('Y-m-d'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->paginate($this->rowLength);


        return view('pages.web.news.index', compact('headlines', 'updateNews', 'latestNews', 'oldestNews'));
    }

    public function latestNews(Request $request)
    {

        $latestNews = News::where('is_headline', false)
            ->where('news_date', Carbon::now()->format('Y-m-d'));

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $latestNews = $latestNews->whereHas("newsTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }


        $latestNews = $latestNews->orderBy('created_at', 'desc')->paginate($this->rowLength);

        if ($request->ajax()) {
            return view('pages.web.news.latest_news', compact('latestNews'))->render();
        }
    }

    public function oldestNews(Request $request)
    {

        $oldestNews = News::where('is_headline', false)
            ->where('news_date', '<', Carbon::now()->format('Y-m-d'));

        if (request()->has('tags') && !empty(request()->get('tags'))) {
            $tags = request()->get('tags');
            $oldestNews = $latestNews->whereHas("newsTagList", function ($query) use ($tags) {
                $query->where("tag_id", $tags);
            });
        }


        $oldestNews = $oldestNews->orderBy('created_at', 'desc')->paginate($this->rowLength);

        if ($request->ajax()) {
            return view('pages.web.news.oldest_news', compact('oldestNews'))->render();
        }
    }

    public function showNews($id)
    {
        $news = News::find($id);
        return view('pages.web.news.show', compact('news'));
    }
}
