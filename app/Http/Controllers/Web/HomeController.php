<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Banner;
use App\Models\LearningModule;
use App\Models\News;
use App\Models\TrainingRule;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{

    public function index()
    {
        $banners = Banner::orderBy("banner_order", "asc")->get();
        $bidangCategory = LearningModule::with(["categoryModule", "subCategoryModule"])->get()->groupBy("categoryModule.name");
        $trainingRules = TrainingRule::orderBy('order_number', 'asc')->get();

        $latestNews = News::where('is_headline', false)
            ->whereMonth('news_date', '=', Carbon::now()->format('m'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->take(3)->get();

        $latestArticles = Article::where('is_headline', false)
            ->whereMonth('article_date', Carbon::now()->format('m'))
            ->where('status', true)
            ->orderBy('created_at', 'desc')
            ->take(3)->get();


        return view('pages.web.home.index', compact('banners', 'bidangCategory', 'trainingRules', 'latestNews', 'latestArticles'));
    }

    public function fetchRules(Request $request)
    {
        $trainingRules = TrainingRule::orderBy('order_number', 'asc')->paginate(3);
        return view('pages.web.training_rules.training_list', compact('trainingRules'));
    }
}
