<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ClassWebinar;
use App\Models\TypeClass;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class KelasWebinarController extends Controller
{
    public function index(Request $request)
    {
        $webinar = ClassWebinar::with([
            "trainer",
            "typeClass",
            "userWebinars"
        ])->where('status', true);

        if ($request->has('class_name')) {
            $search = $request->query('class_name');
            $webinar = $webinar->where("class_name", "ilike", "%{$search}%");
        }

        if ($request->has('sertificated')) {
            if ($request->query('sertificated') == '1') {
                $webinar = $webinar->where('has_sertificated', true);
            } else {
                $webinar = $webinar->where('has_sertificated', false);
            }
        }

        if ($request->has('type_class')) {
            $webinar = $webinar->where('class_type_id', $request->query('type_class'));
        }



        if ($request->has('period')) {
            $webinar = $webinar->whereMonth('open_date', Carbon::parse($request->query('period'))->format('m'));
        }

        $webinar = $webinar->paginate(6);

        if ($request->ajax()) {
            return view('pages.web.webinar.webinar', compact('webinar'))->render();
        }

        return view('pages.web.webinar.index', compact('webinar'));
    }

    public function show($id)
    {
        $webinar = ClassWebinar::with([
            "trainer",
            "typeClass",
            "userWebinars"
        ])->find($id);

        $user = Auth::user();
        return view("pages.web.webinar.show-webinar", compact("webinar", "user"));
    }

    public function getSertificatedStatus()
    {
        return response()->json([
            "data" => [
                ["id" => 1, "name" => "Bersertifikat"],
                ["id" => 0, "name" => "Tidak Bersertifikat"]
            ]
        ]);
    }

    public function getTypeClass()
    {
        $data = TypeClass::get();
        return response()->json([
            "data" => $data
        ]);
    }
}
