<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CertificationType;
use App\Models\LSP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class WebLSPController extends Controller
{
    public function index()
    {
        return view("pages.web.data_lsp.index");
    }

    public function getCertificationTypes()
    {
        $records = CertificationType::get();
        return response()->json([
            "results" => $records,
        ]);
    }

    public function getLSP()
    {
        $sertification = request()->get('sertification');
        $data = LSP::with(['certificationType'])
            ->where('status',  true);


        if (request()->has('sertification') && request()->get('sertification') != '-') {
            $sertification = request()->get('sertification');
            $data = $data->where("certification_type_id", $sertification);
        }

        $data = $data->orderBy("updated_at", "desc");

        return DataTables::of($data)
            ->addColumn('certification_name', function ($item) {
                return $item->certificationType ? $item->certificationType->name : "";
            })
            ->addColumn("status", function ($item) {
                return $item->status == true ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">NON AKTIF</span>';
            })
            ->addIndexColumn()
            ->rawColumns([
                "certification_name",
                "status",
            ])
            ->make(true);
    }
}
