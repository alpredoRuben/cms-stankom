<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CategoryModule;
use App\Models\LearningModule;
use App\Models\SubCategoryModule;
use App\Models\TypeModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebModuleController extends Controller
{
    public function index(Request $request)
    {
        $learningModule = LearningModule::with([
            "categoryModule",
            "subCategoryModule",
            "typeModule",
            "fileModules",
        ])->where('status', true);

        if ($request->has('module_name')) {
            $search = $request->query('module_name');
            $learningModule = $learningModule->where("module_name", "ilike", "%{$search}%");
        }

        if ($request->query('category')) {
            $learningModule = $learningModule->where('category_module_id', $request->query('category'));
        }

        if ($request->query('sub_category')) {
            $learningModule = $learningModule->where('sub_category_module_id', $request->query('sub_category'));
        }

        if ($request->query('type')) {
            $learningModule = $learningModule->where('type_module_id', $request->query('type'));
        }

        $learningModule = $learningModule->paginate(6);

        if ($request->ajax()) {
            return view('pages.web.module.modul_pagination', compact('learningModule'))->render();
        }

        return view("pages.web.module.index", compact("learningModule"));
    }

    public function show($id)
    {
        $learningModule = LearningModule::with([
            "categoryModule",
            "subCategoryModule",
            "typeModule",
            "fileModules",
        ])->find($id);

        $user = Auth::user();
        return view("pages.web.module.show-module", compact("learningModule", "user"));
    }

    public function getTypeModule()
    {
        $data = TypeModule::get();
        return response()->json([
            "data" => $data
        ]);
    }


    public function getSubCategory()
    {
        $data = SubCategoryModule::get();
        return response()->json([
            "data" => $data
        ]);
    }

    public function getCategoryModule()
    {
        $data = CategoryModule::get();
        return response()->json([
            "data" => $data
        ]);
    }
}
