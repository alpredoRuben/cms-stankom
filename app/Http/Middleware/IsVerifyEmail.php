<?php

namespace App\Http\Middleware;

use App\Mail\ActivationAcountEmail;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class IsVerifyEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if ($user && !$user->email_verified_at && $user->status != 'active') {
            Auth::logout();
            return Redirect::route('login')
                ->with(
                    'messageSuccess',
                    'Akun anda belum dikonfirmasi. Silahkan cek email dan verifikasi akun anda'
                );
        }

        return $next($request);
    }
}
