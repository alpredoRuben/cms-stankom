<?php

namespace App\Mail;

use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;


class ActivationAcountEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $name;
    public $type;
    public $password;
    public $token;
    public $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $password = null, $token, $type = 'activation')
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;
        $this->token = $token;
        $this->company = $this->getCompany();
    }

    public function getCompany()
    {
        $contact = Contact::orderBy('id', 'desc')->first();
        if (!$contact) {
            return [
                "phone" => "",
                "email" => ""
            ];
        }

        // $address = $contact->address ? strip_tags($item->address) : "";

        return [
            "phone" => $contact->phone ?? "",
            "email" => $contact->email ?? "",
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (Str::lower($this->type) == 'activation') {
            return $this->view('emails.activation_account');
        } else {
            return $this->view('emails.verification_email');
        }
    }
}
