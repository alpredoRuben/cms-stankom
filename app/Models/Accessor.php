<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accessor extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "nik",
        "name",
        "email",
        "phone_number",
        "description",
        "lsp_id"
    ];

    /** Relation To LSP */
    public function lsp()
    {
        return $this->belongsTo(LSP::class, 'lsp_id', 'id');
    }
}
