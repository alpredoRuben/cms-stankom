<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPSTORM_META\map;

class AnswerQuestion extends Model
{
    use HasFactory;
    protected $fillable = [
        "question_list_id",
        "answer_option_number",
        "answer_option_char",
        "answer_text",
        "answer_filepath",
        "is_image_answer",
        "is_true_answer",
        "created_by",
        "updated_by"
    ];
}
