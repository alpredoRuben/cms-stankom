<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "article_title",
        "article_date",
        "article_photo",
        "article_content",
        "status",
        "is_headline"
    ];

    /** Relation To Article Tag List */
    public function articleTagList()
    {
        return $this->hasMany(ArticleTagList::class, 'article_id', 'id')
            ->with(["tag"]);
    }
}
