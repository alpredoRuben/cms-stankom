<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleTagList extends Model
{
    use HasFactory;
    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        "article_id",
        "tag_id"
    ];

    /** Relation to News */
    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    /** Relation to Tag */
    public function tag()
    {
        return $this->belongsTo(ArticleTag::class, 'tag_id', 'id');
    }
}
