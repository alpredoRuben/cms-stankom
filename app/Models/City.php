<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "province_id",
        "name",
        "slug",
        "code"
    ];

    /** Relation To Province */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /** Relation To Districts */
    public function districts()
    {
        return $this->hasMany(Distric::class, 'city_id', 'id');
    }
}
