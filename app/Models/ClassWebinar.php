<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassWebinar extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "class_name",
        "class_type_id",
        "user_id",
        "open_date",
        "close_date",
        "class_time",
        "duration",
        "quota",
        "link",
        "description",
        "filepath",
        "has_quiz",
        "has_sertificated",
        "status"
    ];

    /** Relation to Trainer */
    public function trainer()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    /** Relation To ClassType  */
    public function typeClass()
    {
        return $this->belongsTo(TypeClass::class, 'class_type_id', 'id');
    }

    /** Relation to UserWebinars */
    public function userWebinars()
    {
        return $this->hasMany(UserWebinar::class, 'webinar_id', 'id');
    }
}
