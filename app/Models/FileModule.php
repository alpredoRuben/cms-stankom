<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileModule extends Model
{
    use HasFactory;
    protected $fillable = [
        "module_id",
        "topic",
        "description",
        "filename",
        "filepath",
        "composite"
    ];

    /** Relation to LearningModule */
    public function learningModule()
    {
        return $this->belongsTo(LearningModule::class, 'module_id', 'id');
    }

    public function getCompositeAttribute($value)
    {
        if (!empty($value)) {
            return json_decode($value, true);
        }

        return $value;
    }
}
