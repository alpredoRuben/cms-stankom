<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LSP extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'lsp';

    protected $fillable = [
        "lsp_name",
        "lsp_address",
        "certification_type_id",
        "description",
        "status",
        "lsp_phone",
        "lsp_website",
        "lsp_email"
    ];

    /** Relation To Certification Type */
    public function certificationType()
    {
        return $this->belongsTo(CertificationType::class, 'certification_type_id', 'id');
    }

    /** Relation To Accessor */
    public function accessors()
    {
        return $this->hasMany(Accessor::class, 'lsp_id', 'id');
    }
}
