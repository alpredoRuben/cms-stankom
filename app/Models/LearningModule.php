<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LearningModule extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "category_module_id",
        "sub_category_module_id",
        "type_module_id",
        "module_name",
        "module_desc",
        "can_download",
        "has_quiz",
        "status"
    ];

    /** Relation to CategoryModule */
    public function categoryModule()
    {
        return $this->belongsTo(CategoryModule::class, 'category_module_id', 'id');
    }

    /** Relation to SubCategoryModule */
    public function subCategoryModule()
    {
        return $this->belongsTo(SubCategoryModule::class, 'sub_category_module_id', 'id');
    }

    /** Relation to TypeModule */
    public function typeModule()
    {
        return $this->belongsTo(TypeModule::class, 'type_module_id', 'id');
    }

    /** Relation To FileModule */
    public function fileModules()
    {
        return $this->hasMany(FileModule::class, 'module_id', 'id');
    }

    /** Relation to UserLearningModule */
    public function moduleOfUsers()
    {
        return $this->hasMany(UserLearningModule::class, 'learning_module_id', 'id');
    }
}
