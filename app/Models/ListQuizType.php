<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListQuizType extends Model
{
    use HasFactory;
    protected $fillable = [
        "quiz_type_id",
        "quiz_id",
        "duration_minutes"
    ];


    /** Relation to Quiz */
    public function quiz()
    {
        return $this->belongsTo(Quiz::class, 'quiz_id', 'id');
    }

    /** Relation to Quiz */
    public function quizType()
    {
        return $this->belongsTo(QuizType::class, 'quiz_type_id', 'id');
    }
}
