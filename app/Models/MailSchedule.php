<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MailSchedule extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "type",
        "data",
        "send",
        "send_at"
    ];

    public function getDataAttribute($value)
    {
        return $value ? json_decode($value, true) : null;
    }
}
