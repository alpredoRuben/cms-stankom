<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedsosPage extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "medsos_id",
        "url",
        "status"
    ];

    /** Relation To Social Media */
    public function socialMedia()
    {
        return $this->belongsTo(SocialMedia::class, 'medsos_id', 'id');
    }
}
