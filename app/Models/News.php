<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "news_title",
        "news_date",
        "news_photo",
        "news_content",
        "status",
        "is_headline"
    ];


    /** Relation To News Tag List */
    public function newsTagList()
    {
        return $this->hasMany(NewsTagList::class, 'news_id', 'id')->with(["tag"]);
    }
}
