<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsTagList extends Model
{
    use HasFactory;
    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        "news_id",
        "tag_id"
    ];

    /** Relation to News */
    public function news()
    {
        return $this->belongsTo(News::class, 'news_id', 'id');
    }

    /** Relation to Tag */
    public function tag()
    {
        return $this->belongsTo(NewsTag::class, 'tag_id', 'id');
    }
}
