<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionList extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "question",
        "quiz_id",
        "scored",
        "status",
        "is_type_image",
        "filepath_question",
        "created_by",
        "updated_by"
    ];


    /** Relation Quiz */
    public function quiz()
    {
        return $this->belongsTo(Quiz::class, 'quiz_id', 'id');
    }

    /** Relation User */
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
