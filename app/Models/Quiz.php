<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quiz extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'quiz';
    protected $fillable = [
        "name",
        "sluq",
        "code",
        "min_scored",
        "webinar_id",
        "learning_id",
        "type",
        "status",
        "has_sertificated",
        "created_by",
        "updated_by"
    ];

    /** Relation to ClassWebinar */
    public function classWebinar()
    {
        return $this->belongsTo(ClassWebinar::class, 'webinar_id', 'id');
    }

    /** Relation to LearningModule */
    public function learningModule()
    {
        return $this->belongsTo(LearningModule::class, 'learning_id', 'id');
    }


    /** Relation User */
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
