<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMedia extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "medsos_name",
        "medsos_icon",
        "status"
    ];

    /** Relation to MedsosPage */
    public function medsos()
    {
        return $this->hasOne(MedsosPage::class, 'medsos_id', 'id');
    }
}
