<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeClass extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "name",
        "slug",
        "description",
        "status"
    ];

    /** Relation To ClassType  */
    public function webinars()
    {
        return $this->hasMany(ClassWebinar::class, 'class_type_id', 'id');
    }
}
