<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLearningModule extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "learning_module_id",
        "user_id",
        "ticked_code"
    ];

    /** Relation to Learning Module */
    public function learningModule()
    {
        return $this->belongsTo(LearningModule::class, 'learning_module_id', 'id');
    }

    /** Relation to User */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
