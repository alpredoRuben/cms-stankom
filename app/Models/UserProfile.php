<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;
    protected $fillable = [
        "user_id",
        "nik",
        "gender",
        "birth_date",
        "phone_number",
        "another_email",
        "address",
        "date_joined",
        "ward_id",
        "education_id",
        "job_profession_id",
        "photo"
    ];

    /** Relation To Ward (Kelurahan) */
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id', 'id');
    }

    /** Relation To User  */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /** Relation To JobProfession  */
    public function job_profession()
    {
        return $this->belongsTo(JobProfession::class, 'job_profession_id', 'id');
    }


    /** Relation To Education  */
    public function education()
    {
        return $this->belongsTo(Education::class, 'education_id', 'id');
    }
}
