<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWebinar extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "webinar_id",
        "user_id",
        "ticked_code"
    ];

    /** Relation To User */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /** Relation To ClassWebinar */
    public function webinar()
    {
        return $this->belongsTo(ClassWebinar::class, 'webinar_id', 'id');
    }
}
