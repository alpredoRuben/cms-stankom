<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ward extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        "district_id",
        "name",
        "slug",
        "code"
    ];

    /** Relation To District */
    public function district()
    {
        return $this->belongsTo(Distric::class, 'district_id', 'id');
    }

    /** Relation To PostalCode */
    public function postalCodes()
    {
        return $this->hasOne(PostalCode::class, 'ward_id', 'id');
    }
}
