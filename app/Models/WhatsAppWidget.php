<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsAppWidget extends Model
{
    use HasFactory;
    protected $fillable = [
        'nomor_wa'
    ];
}
