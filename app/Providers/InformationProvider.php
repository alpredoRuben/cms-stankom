<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Logo;
use App\Models\MedsosPage;
use App\Models\WhatsAppWidget;
use Illuminate\Support\ServiceProvider;

class InformationProvider extends ServiceProvider
{
    private function getContactInformation()
    {
        $info = Contact::orderBy('created_at', 'desc')->first();
        return $info;
    }

    private function getMediaSocial()
    {
        $mediaSocial = MedsosPage::with(["socialMedia"])
            ->where('status', true)->get();
        return $mediaSocial;
    }

    private function getLogo()
    {
        $logo = Logo::orderBy('id', 'desc')->first();
        return $logo;
    }

    private function getWhatsapp()
    {
        $whatsapp = WhatsAppWidget::orderBy('updated_at', 'desc')->first();
        $whatsappUrl = "";
        if ($whatsapp) {
            $whatsappUrl = "https://wa.me/{$whatsapp->nomor_wa}?text=%22Halo+LMS+Stankom%22";
        }
        return $whatsappUrl;
    }


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts/guest/index', function ($view) {
            $contact = $this->getContactInformation();
            $socialMedia = $this->getMediaSocial();
            $logo = $this->getLogo();
            $whatsapp = $this->getWhatsapp();

            $view->with("contact", $contact);
            $view->with("socialMedia", $socialMedia);
            $view->with("logo", $logo);
            $view->with("whatsapp", $whatsapp);
        });

        view()->composer('layouts/lms/app', function ($view) {
            $logo = $this->getLogo();
            $view->with("logo", $logo);
        });
    }
}
