<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

trait FileManager
{

    /** Read and Share File */
    public function getFileFromTemplates($filename)
    {
        $file = public_path() . "/templates/{$filename}";
        $headers = array(
            'Content-Type: text/csv',
        );

        return collect([
            "file" => $file,
            "headers" => $headers,
            "filename" => $filename
        ]);
    }


    public function extractCSVToArray($file, $delimiter = ',')
    {
        if (!file_exists($file) || !is_readable($file))
            return false;

        $header = null;
        $data = array();

        if (($handle = fopen($file, "r")) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (array(null) !== $row) {
                    if (!$header) {
                        if (count($row) > 0) {
                            for ($i = 0; $i < count($row); $i++) {
                                $replaceString = str_replace(" ", "_", trim($row[$i]));
                                $replaceString = str_replace('"', "", $replaceString);
                                $replaceString = preg_replace('/\W/', '', $replaceString);
                                $row[$i] =  Str::lower(trim($replaceString));
                            }
                        }
                        $header = $row;
                    } else {
                        $data[] = array_combine($header, $row);
                    }
                }
            }
        }

        return $data;
    }
}
