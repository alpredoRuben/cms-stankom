<?php

namespace App\Traits;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

trait GlobalManager
{
    public function getFileTypeCollection()
    {
        return collect([
            [
                "mode" => "video/mp4",
                "extension" => "mp4",
                "type" => "video",
            ],
            [
                "mode" => "video/mov",
                "extension" => "mov",
                "type" => "video",
            ],
            [
                "mode" => "video/wmv",
                "extension" => "wmv",
                "type" => "video",
            ],
            [
                "mode" => "video/avi",
                "extension" => "avi",
                "type" => "video",
            ],
            [
                "mode" => "video/flv",
                "extension" => "flv",
                "type" => "video",
            ],
            [
                "mode" => "video/webm",
                "extension" => "webm",
                "type" => "video",
            ],
            [
                "mode" => "video/mkv",
                "extension" => "mkv",
                "type" => "video",
            ],
            [
                "mode" => "application/pdf",
                "extension" => "pdf",
                "type" => "document"
            ],
            [
                "mode" => "document/docx",
                "extension" => "docx",
                "type" => "document"
            ],
            [
                "mode" => "document/doc",
                "extension" => "doc",
                "type" => "document"
            ],
            [
                "mode" => "document/pptx",
                "extension" => "pptx",
                "type" => "document"
            ],
        ]);
    }

    public function removeDirectory($path)
    {
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path, 0777, true, true);
        } else {
            Storage::deleteDirectory($path);
        }
    }
}
