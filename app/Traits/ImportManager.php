<?php

namespace App\Traits;

use App\Models\Accessor;
use App\Models\LSP;
use App\Models\MailSchedule;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

trait ImportManager
{

    public function storeImportUser($data)
    {
        $duplicated = 0;
        $totalInsert = 0;
        $results = [];

        foreach ($data as $value) {
            $user = User::with(['userProfile'])
                ->whereHas('userProfile', function ($q) use ($value) {
                    $q->where('nik', $value['nik']);
                })
                ->orWhere('email', $value['email'])->first();

            $password = Str::random(10);
            $token = Str::random(60);

            if (!$user) {

                $user = User::create([
                    'name' => $value["nama_lengkap"],
                    'email' => $value["email"],
                    'password' => bcrypt($password),
                    'status' => true
                ]);

                if (!$user->verifyUser) {
                    $user->verifyUser()->create([
                        "token" => $token,
                    ]);
                }

                MailSchedule::create([
                    "type" => "activation",
                    "data" => json_encode([
                        "token" => $token,
                        "password" => null,
                        "email" => $user->email,
                        "name" => $user->name,
                    ])
                ]);

                $totalInsert += 1;
            } else {
                $duplicated += 1;
            }

            if (!$user->userProfile()->first()) {
                $user->userProfile()->create([
                    "user_id",
                    "nik" => $value["nik"] ?? null,
                    "gender" => $value["jenis_kelamin"] ?? null,
                    "birth_date" => $value["tanggal_lahir"] ?? null,
                    "phone_number" => $value["no_ponsel"] ?? null,
                    "address" => $value["alamat"] ?? null,
                    "date_joined" => $value['tanggal_bergabung'] ? Carbon::parse($value['tanggal_bergabung'])->format("Y-m-d") : null,
                ]);
            }

            $roleName = $value["kelompok_pengguna"] ? Str::lower(trim($value["kelompok_pengguna"])) : "";
            $role = Role::where("name", $roleName)->first();

            if ($role) {
                $user->assignRole([$role->id]);
            }

            array_push($results, $user);
        }

        return [
            "duplicated" => $duplicated,
            "totalInsert" => $totalInsert,
        ];
    }


    public function storeImportAccessor($data)
    {
        $invalid = 0;
        $duplicated = 0;
        $totalInsert = 0;
        $failed = 0;
        $results = [];

        foreach ($data as $value) {
            if (isset($value["nama_lsp"]) && !empty($value["nama_lsp"])) {
                $lsp_name = $value["nama_lsp"];
                $lsp = LSP::where("lsp_name", "ilike", "%{$lsp_name}%")->first();

                if (!$lsp) {
                    $invalid += 1;
                } else {
                    if (isset($value["nik"]) && !empty($value["nik"])) {
                        $accessor = Accessor::where("nik", $value["nik"])->first();

                        if (!$accessor) {
                            $accessor = Accessor::create([
                                "nik" => $value["nik"],
                                "name" => $value["nama_lengkap"] ?? null,
                                "email" => $value["email"] ?? null,
                                "phone_number" => $value["no_ponsel"] ?? null,
                                "description" => $value["keterangan"] ?? null,
                                "lsp_id" => $lsp->id
                            ]);

                            if ($accessor) {
                                $totalInsert += 1;
                            } else {
                                $failed += 1;
                            }
                        } else {
                            $accessor->name = $value["nama_lengkap"] ?? $accessor->name;
                            $accessor->email =  $value["email"] ?? $accessor->email;
                            $accessor->phone_number = $value["no_ponsel"] ?? $accessor->phone_number;
                            $accessor->description = $value["keterangan"] ?? $accessor->description;
                            $accessor->lsp_id = $lsp->id;
                            $accessor->save();
                            $duplicated += 1;
                        }

                        array_push($results, $accessor);
                    } else {
                        $invalid += 1;
                    }
                }
            } else {
                $invalid += 1;
            }
        }

        return [
            "duplicated" => $duplicated,
            "totalInsert" => $totalInsert,
            "invalid" => $invalid,
            "failed" => $failed
        ];
    }
}
