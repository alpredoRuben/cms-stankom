<?php

namespace App\Traits;


trait StaticProperties
{
    public function getStaticUserProperties()
    {
        return  collect([
            ["nik", "NIK"],
            ["name", "Nama Lengkap"],
            ["email", "Email"],
            ["gender", "Jenis Kelamin"],
            ["birthdate", "Tanggal Lahir"],
            ["phone", "No. Ponsel"],
            ["address", "Alamat"],
            ["join", "Tanggal Join"],
            ["ward", "Kelurahan"],
            ["district", "Kecamatan"],
            ["city", "Kabupaten"],
            ["province", "Propinsi"],
            ["postal_code", "Kode Pos"],
            ["education", "Pendidikan"],
            ["profession", "Pekerjaan"],
            ["role", "Kelompok Pengguna"],
        ]);
    }

    public function getStaticAccessorProperties()
    {
        return  collect([
            ["nik", "NIK"],
            ["name", "Nama Lengkap"],
            ["email", "Email"],
            ["phone_number", "No. Ponsel"],
            ["description", "Keterangan"]
        ]);
    }

    public function getStaticLSPProperties()
    {
        return  collect([
            ["lsp_name", "Nama LSP"],
            ["lsp_address", "Alamat LSP"],
            ["certification_type_id", "Bidang Sertifikasi"],
            ["description", "Keterangan"],
            ["lsp_phone", "No. Telepon"],
            ["lsp_email", "Email LSP"],
            ["lsp_website", "Website LSP"]
        ]);
    }

    public function excelModuleStaticProperties()
    {
        return [
            ["column" => "module_name",             "desc" => "Nama Modul"],
            ["column" => "category_module_id",      "desc" => "Kategori Modul"],
            ["column" => "sub_category_module_id",  "desc" => "Sub Kategori Modul"],
            ["column" => "type_module_id",          "desc" => "Tipe Modul"],
            ["column" => "module_desc",             "desc" => "Keterangan"],
            ["column" => "can_download",            "desc" => "Ada Download"],
            ["column" => "has_quiz",                "desc" => "Ada Kuis"],
        ];
    }

    public function getStaticWebinar()
    {
        return  collect([
            ["class_name", "Nama Kelas"],
            ["class_type", "Tipe Kelas"],
            ["trainer", "Pengajar"],
            ["open_date", "Tanggal Buka"],
            ["close_date", "Tanggal Tutup"],
            ["class_time", "Jam Kelas"],
            ["duration", "Durasi (Menit)"],
            ["quota", "Kuota (Orang)"],
            ["total_user", "Jumlah Peserta (Orang)"],
            ["link", "Link"],
            ["has_quiz", "Kuis"],
            ["has_sertificated", "Sertifikat"],
            ["status", "Status"]
        ]);
    }
}
