<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id")->nullable()->default(null);
            $table->string("nik")->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->string('birth_date')->nullable()->default(null);
            $table->string('phone_number')->nullable()->default(null);
            $table->string('another_email')->nullable()->default(null);
            $table->longText("address")->nullable()->default(null);
            $table->date('date_joined')->nullable()->default(null);

            // Kelurahan
            $table->unsignedBigInteger('ward_id')->nullable()->default(null);

            // Pendidikan
            $table->unsignedBigInteger("education_id")->nullable()->default(null);

            // Pekerjaan
            $table->unsignedBigInteger("job_profession_id")->nullable()->default(null);


            // Foto/Avatar
            $table->longText('photo')->nullable()->default(null);

            $table->timestamps();


            //Foreign Key
            $table->foreign('user_id')->references('id')->on('users')->nullOnDelete();

            $table->foreign('ward_id')->references('id')->on('wards')->nullOnDelete();

            $table->foreign('education_id')->references('id')->on('education')->nullOnDelete();

            $table->foreign('job_profession_id')->references('id')->on('job_professions')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
