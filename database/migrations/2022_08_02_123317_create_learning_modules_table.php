<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLearningModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learning_modules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_module_id')->constrained('category_modules')->nullOnDelete();
            $table->foreignId('sub_category_module_id')->constrained('sub_category_modules')->nullOnDelete();
            $table->foreignId('type_module_id')->constrained('type_modules')->nullOnDelete();
            $table->string("module_name")->nullable()->default(null);
            $table->text("module_desc")->nullable()->default(null);
            $table->boolean("can_download")->nullable()->default(false);
            $table->boolean("has_quiz")->nullable()->default(false);
            $table->boolean("status")->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learning_modules');
    }
}
