<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_modules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('module_id')->constrained('learning_modules')->onUpdate('cascade')->onDelete('cascade');
            $table->string('topic')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->string('filename')->nullable()->default(null);
            $table->longText('filepath')->nullable()->default(null);
            $table->longText('composite')->nullable()->default(null);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_modules');
    }
}
