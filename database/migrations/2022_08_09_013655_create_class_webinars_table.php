<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_webinars', function (Blueprint $table) {
            $table->id();
            $table->string("class_name")->nullable()->default(null);
            $table->foreignId("class_type_id")->constrained('type_classes')->nullOnDelete();
            $table->foreignId("user_id")->constrained('users')->nullOnDelete();
            $table->date("open_date")->nullable()->default(null);
            $table->date("close_date")->nullable()->default(null);
            $table->time("class_time")->nullable()->default(null);
            $table->bigInteger('duration')->nullable()->default(0);
            $table->bigInteger('quota')->nullable()->default(0);
            $table->text('link')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->longText('filepath')->nullable()->default(null);
            $table->boolean('has_quiz')->nullable()->default(false);
            $table->boolean('has_sertificated')->nullable()->default(false);
            $table->boolean('status')->nullable()->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_webinars');
    }
}
