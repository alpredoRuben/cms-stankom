<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLSPSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lsp', function (Blueprint $table) {
            $table->id();
            $table->string('lsp_name')->nullable()->default(null);
            $table->longText('lsp_address')->nullable()->default(null);
            $table->foreignId("certification_type_id")->constrained('certification_types')->nullOnDelete();
            $table->text('description')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(true);
            $table->string('lsp_phone')->nullable()->default(null);
            $table->text('lsp_website')->nullable()->default(null);
            $table->string('lsp_email')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lsp');
    }
}
