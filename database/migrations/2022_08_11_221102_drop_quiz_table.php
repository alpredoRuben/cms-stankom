<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('answer_questions');
        Schema::dropIfExists('list_quiz_types');
        Schema::dropIfExists('question_lists');
        Schema::dropIfExists('quiz');
        Schema::dropIfExists('quiz_types');
        Schema::dropIfExists('question_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
