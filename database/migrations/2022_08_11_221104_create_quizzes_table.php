<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->default(null);
            $table->string('sluq')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);

            $table->unsignedBigInteger("webinar_id")->nullable()->default(null);
            $table->unsignedBigInteger("learning_id")->nullable()->default(null);

            $table->string('type')->nullable()->default('kelas');
            $table->boolean('status')->nullable()->default(true);

            $table->foreign('webinar_id')->references('id')->on('class_webinars')->onDelete('set null');
            $table->foreign('learning_id')->references('id')->on('learning_modules')->onDelete('set null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz');
    }
}
