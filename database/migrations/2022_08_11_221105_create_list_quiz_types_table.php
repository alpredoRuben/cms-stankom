<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListQuizTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_quiz_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("quiz_type_id")->nullable()->default(null);
            $table->unsignedBigInteger("quiz_id")->nullable()->default(null);
            $table->bigInteger('duration_minutes')->nullable()->default(null);
            $table->bigInteger('min_scored')->nullable()->default(null);
            $table->timestamps();
            $table->foreign('quiz_type_id')->references('id')->on('quiz_types')->onDelete('cascade');
            $table->foreign('quiz_id')->references('id')->on('quiz')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_quiz_types');
    }
}
