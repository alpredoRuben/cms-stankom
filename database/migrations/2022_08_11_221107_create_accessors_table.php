<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessors', function (Blueprint $table) {
            $table->id();
            $table->string("nik")->unique();
            $table->string("name")->nullable()->default(null);
            $table->string("email")->unique();
            $table->string("phone_number")->nullable()->default(null);
            $table->text("description")->nullable()->default(null);
            $table->foreignId("lsp_id")->constrained('lsp')->nullOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessors');
    }
}
