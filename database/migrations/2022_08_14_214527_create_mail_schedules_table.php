<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable()->default(null);
            $table->longText('data')->nullable()->default(null);
            $table->boolean('send')->nullable()->default(false);
            $table->timestamp('send_at')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_schedules');
    }
}
