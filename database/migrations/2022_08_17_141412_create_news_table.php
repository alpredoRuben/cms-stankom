<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('news_title')->nullable()->default(null);
            $table->date('news_date')->nullable()->default(null);
            $table->longText('news_photo')->nullable()->default(null);
            $table->longText('news_content')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(true);
            $table->boolean('is_headline')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
