<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('banner_title')->nullable()->default(null);
            $table->string('banner_slug')->nullable()->default(null);
            $table->integer('banner_order')->nullable()->default(null);
            $table->longText('banner_content')->nullable()->default(null);
            $table->text('banner_image')->nullable()->default(null);
            $table->text('str_unique')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
