<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('article_title')->nullable()->default(null);
            $table->date('article_date')->nullable()->default(null);
            $table->longText('article_photo')->nullable()->default(null);
            $table->longText('article_content')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(true);
            $table->boolean('is_headline')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
