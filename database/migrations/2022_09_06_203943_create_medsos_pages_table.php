<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedsosPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medsos_pages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('medsos_id')->nullable()->default(null);
            $table->text('url')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(true);

            $table->foreign('medsos_id')->references('id')->on('social_media')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medsos_pages');
    }
}
