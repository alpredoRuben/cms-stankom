<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_webinars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('webinar_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('ticked_code')->nullable()->default(null);

            $table->foreign('webinar_id')->references('id')->on('class_webinars')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_webinars');
    }
}
