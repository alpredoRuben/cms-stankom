<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('question_list_id')->nullable()->default(null);
            $table->integer('answer_option_number')->nullable()->default(null)->comment('Berisi iterasi angka 1, 2, 3');
            $table->string('answer_option_char')->nullable()->default(null)->comment('Berisi huruf abjad A, B, C dst');
            $table->longText('answer_text')->nullable()->default(null);
            $table->longText('answer_filepath')->nullable()->default(null);
            $table->boolean('is_image_answer')->nullable()->default(false);
            $table->boolean('is_true_answer')->nullable()->default(false);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('updated_by')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('question_list_id')
                ->references('id')
                ->on('question_lists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_questions');
    }
}
