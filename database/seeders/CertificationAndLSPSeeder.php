<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\CertificationType;
use App\Models\LSP;

class CertificationAndLSPSeeder extends Seeder
{
    private function getCertificationAndLsp()
    {
        return collect([
            [
                "name" => "Ekonomi Kreatif",
                "description" =>  "EKRAF",
                "data_lsp" => [
                    [
                        "lsp_name" => "LSP Mode Indonesia",
                        "lsp_address" => "Balai Besar Tekstil, Jln. Ahmad Yani No. 390, Kebonwaru, Kec. Batununggal, Kota Bandung, Jawa Barat, 40272",
                        "lsp_phone" => "0821-2041 8909",
                        "lsp_website" => "",
                        "lsp_email" => "lspmodeindonesia@gmail.com
                        "
                    ],
                    [
                        "lsp_name" => "LSP Musik Indonesia",
                        "lsp_address" => "Jalan Kramat Raya 16 Jakarta Pusat",
                        "lsp_phone" => "0812 192 7753",
                        "lsp_website" => "",
                        "lsp_email" => "lspmusik@gmail.com"
                    ],
                    [
                        "lsp_name" => "LSP Animedia",
                        "lsp_address" => "Gedung UMKM Center Lt. 2, Jl. Setiabudi No. 192, Banyumanik, Semarang, Jawa Tengah",
                        "lsp_phone" => "0813 9000 9716",
                        "lsp_website" => "www.lsp-animedia.co.id",
                        "lsp_email" => "lsp.animedia@gmail.com"
                    ],
                    [
                        "lsp_name" => "LSP Batik",
                        "lsp_address" => "Tambakaji Rava No 1 Ngaliyan, Semarang, Jawa Tengah, 50158",
                        "certification_type_id" => "",
                        "description" => "",
                        "status" => "",
                        "lsp_phone" => "0813 2758 5995 ",
                        "lsp_website" => "www.lspbatik.org",
                        "lsp_email" => "lspbatik@gmail.com"
                    ],
                    [
                        "lsp_name" => "LSP Fotografi Indonesia",
                        "lsp_address" => "Jl. Tebet Barat VIII No.1, Jakarta Selatan-12810",
                        "certification_type_id" => "",
                        "description" => "",
                        "status" => "",
                        "lsp_phone" => "0815 889 8915",
                        "lsp_website" => "www.lspfotografi.id",
                        "lsp_email" => "info@lspfotografi.id"
                    ],
                ]
            ],
            [
                "name" => "Pariwisata",
                "description" =>  "Pariwisata",
                "data_lsp" => []
            ],
        ]);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getCertificationAndLsp() as $c) {
            $name = trim($c["name"]);
            $slug = Str::slug(Str::lower($name));

            $certification = CertificationType::create([
                "name" => $name,
                "slug" => $slug,
            ]);

            if ($certification) {
                if (count($c["data_lsp"]) > 0) {
                    foreach ($c["data_lsp"] as $item) {
                        LSP::create([
                            "lsp_name" => $item["lsp_name"],
                            "lsp_address" => $item["lsp_address"],
                            "certification_type_id" => $certification->id,
                            "status" => true,
                            "lsp_phone" => $item["lsp_phone"],
                            "lsp_website" => $item["lsp_website"],
                            "lsp_email" => $item["lsp_email"]
                        ]);
                    }
                }
            }
        }
    }
}
