<?php

namespace Database\Seeders;

use App\Models\SocialMedia;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            EducationSeeder::class,
            ProfessionSeeder::class,
            FileTypeSeeder::class,
            ModuleSeeder::class,
            TypeClassSeeder::class,
            QuestionTypeSeeder::class,
            CertificationAndLSPSeeder::class,
            SocialMediaSeeder::class,
            PropinsiSeeder::class,
            KabupatenSeeder::class,
            KecamtanSeeder::class,
            KelurahanSeeder::class,
            KodePosSeeder::class
        ]);
    }
}
