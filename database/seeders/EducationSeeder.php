<?php

namespace Database\Seeders;

use App\Models\Education;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EducationSeeder extends Seeder
{
    private function getEducations()
    {
        return collect([
            "Tidak Sekolah",
            "SD/Sederajat",
            "SMP/Sederajat",
            "SMA/Sederajat",
            "D1",
            "D2",
            "D3",
            "D4",
            "S1",
            "S2",
            "S3",
        ]);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getEducations() as $value) {
            $name = trim($value);
            $slug = Str::slug(Str::lower($name));

            Education::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => null,
            ]);
        }
    }
}
