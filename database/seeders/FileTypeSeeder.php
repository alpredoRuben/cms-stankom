<?php

namespace Database\Seeders;

use App\Models\FileType;
use Illuminate\Database\Seeder;

class FileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                "name" => "video/mp4",
                "extension" => "mp4",
            ],
            [
                "name" => "video/mov",
                "extension" => "mov",
            ],
            [
                "name" => "video/wmv",
                "extension" => "wmv",
            ],
            [
                "name" => "video/avi",
                "extension" => "avi",
            ],
            [
                "name" => "video/flv",
                "extension" => "flv",
            ],
            [
                "name" => "video/webm",
                "extension" => "webm",
            ],
            [
                "name" => "video/mkv",
                "extension" => "mkv",
            ],
            [
                "name" => "application/pdf",
                "extension" => "pdf",
            ],
            [
                "name" => "document/docx",
                "extension" => "docx",
            ],
            [
                "name" => "document/doc",
                "extension" => "doc",
            ],
            [
                "name" => "document/pptx",
                "extension" => "pptx",
            ],

        ];

        foreach ($types as $value) {
            FileType::create($value);
        }
    }
}
