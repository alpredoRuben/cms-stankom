<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Province;
use App\Models\City;
use Illuminate\Support\Facades\Schema;


class KabupatenSeeder extends Seeder
{
    public function getKabupaten()
    {
        $dataKabupaten = file_get_contents(public_path() . '/CSV/kabupaten.json');
        $dataKabupaten = json_decode($dataKabupaten, true);
        return collect($dataKabupaten)->sortBy('city_id');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        City::truncate();
        Schema::enableForeignKeyConstraints();

        $groupCity = $this->getKabupaten()->groupBy('prov_id');

        foreach ($groupCity as $key => $cities) {
            $list = [];
            foreach ($cities as $item) {
                array_push($list, [
                    "name" => ucwords(Str::lower(trim($item["city_name"]))),
                    "slug" => Str::slug(trim($item["city_name"])),
                    "code" => $item["city_id"]
                ]);
            }

            $province = Province::with(["cities"])->where('code', $key)->first();

            if ($province) {
                $province->cities()->createMany($list);
            }
        }
    }
}
