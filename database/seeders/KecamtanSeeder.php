<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\City;
use App\Models\Distric;
use Illuminate\Support\Facades\Schema;

class KecamtanSeeder extends Seeder
{
    public function getKecamatan()
    {
        $dataKecamatan = file_get_contents(public_path() . '/CSV/kecamatan.json');
        $dataKecamatan = json_decode($dataKecamatan, true);
        return collect($dataKecamatan)->sortBy('dis_id');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Distric::truncate();
        Schema::enableForeignKeyConstraints();

        $groupKecamatan = $this->getKecamatan()->groupBy('city_id');


        foreach ($groupKecamatan as $key => $kecamatan) {
            $list = [];
            foreach ($kecamatan as $value) {
                array_push($list, [
                    "name" => ucwords(Str::lower(trim($value["dis_name"]))),
                    "slug" => Str::slug(Str::lower(trim($value["dis_name"]))),
                    "code" => $value["dis_id"]
                ]);
            }

            $city = City::with(['districts', 'province'])->where('code', $key)->first();

            if ($city) {
                $city->districts()->createMany($list);
            }
        }
    }
}
