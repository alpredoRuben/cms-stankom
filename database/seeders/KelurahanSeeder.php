<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Distric;
use App\Models\Ward;
use Illuminate\Support\Facades\Schema;

class KelurahanSeeder extends Seeder
{
    public function getKelurahan()
    {
        $dataKelurahan = file_get_contents(public_path() . '/CSV/kelurahan.json');
        $dataKelurahan = json_decode($dataKelurahan, true);

        return collect($dataKelurahan)->sortBy('subdis_id');
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Ward::truncate();
        Schema::enableForeignKeyConstraints();

        $groupKelurahan = $this->getKelurahan()->groupBy('dis_id');

        foreach ($groupKelurahan as $key => $kelurahan) {
            $list = [];
            foreach ($kelurahan as $value) {
                array_push($list, [
                    "name" => ucwords(Str::lower(trim($value["subdis_name"]))),
                    "slug" => Str::slug(Str::lower(trim($value["subdis_name"]))),
                    "code" => $value["subdis_id"]
                ]);
            }

            $district = Distric::with(["wards"])->where('code', $key)->first();

            if ($district) {
                $district->wards()->createMany($list);
            }
        }
    }
}
