<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PostalCode;
use App\Models\Ward;
use Illuminate\Support\Facades\Schema;

class KodePosSeeder extends Seeder
{
    public function getPostalCodes()
    {
        $dataKodePos = file_get_contents(public_path() . '/CSV/kode_pos.json');
        $dataKodePos = json_decode($dataKodePos, true);
        return collect($dataKodePos)->sortBy('postal_id');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        PostalCode::truncate();
        Schema::enableForeignKeyConstraints();

        $groupPostalCode = $this->getPostalCodes()->groupBy('subdis_id');

        foreach ($groupPostalCode as $key => $postalCode) {
            $ward = Ward::with(["postalCodes"])->where('code', $key)->first();

            if ($ward) {
                foreach ($postalCode as $value) {
                    $ward->postalCodes()->create([
                        "code" => $value["postal_code"]
                    ]);
                }
            }
        }
    }
}
