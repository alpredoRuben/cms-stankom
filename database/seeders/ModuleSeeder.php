<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\CategoryModule;
use App\Models\SubCategoryModule;
use App\Models\TypeModule;

class ModuleSeeder extends Seeder
{
    private function getCategoryModules()
    {
        return collect([
            ["name" => "Pariwisata"],
            ["name" => "Ekraf"],
        ]);
    }

    private function getSubCategoryModules()
    {
        return collect([
            ["name" => "SKKNI"],
            ["name" => "KKNI"],
            ["name" => "Skema Okupasi"],
        ]);
    }

    private function getTypeModules()
    {
        return collect([
            ["name" => "e-Book"],
            ["name" => "Video"],
            ["name" => "Handout"],
            ["name" => "Tutorial"]
        ]);
    }

    private function createSlug($str)
    {
        return Str::lower(trim($str));
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getCategoryModules() as $value) {
            CategoryModule::create([
                "name" => trim($value["name"]),
                "slug" => $this->createSlug($value["name"])
            ]);
        }

        foreach ($this->getSubCategoryModules() as $value) {
            SubCategoryModule::create([
                "name" => trim($value["name"]),
                "slug" => $this->createSlug($value["name"])
            ]);
        }

        foreach ($this->getTypeModules() as $value) {
            TypeModule::create([
                "name" => trim($value["name"]),
                "slug" => $this->createSlug($value["name"])
            ]);
        }
    }
}
