<?php

namespace Database\Seeders;

use App\Models\JobProfession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProfessionSeeder extends Seeder
{
    private function getJobProfession()
    {
        return collect([
            "Pegawai Negeri",
            "Pegawai Swasta",
            "TNI/Polri",
            "Profesional",
            "Wiraswasta",
            "Tidak Bekerja",
        ]);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getJobProfession() as $value) {
            $name = trim($value);
            $slug = Str::slug(Str::lower($name));

            JobProfession::create([
                "name" => ucwords($name),
                "slug" => $slug,
                "description" => null
            ]);
        }
    }
}
