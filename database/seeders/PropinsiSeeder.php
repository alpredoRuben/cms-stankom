<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Province;
use Illuminate\Support\Facades\Schema;

class PropinsiSeeder extends Seeder
{
    public function getProvinces()
    {
        $dataPropinsi = file_get_contents(public_path() . '/CSV/province.json');
        $dataPropinsi = json_decode($dataPropinsi, true);
        return collect($dataPropinsi)->sortBy("prov_id");
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Province::truncate();
        Schema::enableForeignKeyConstraints();


        // Fill Provinces
        foreach ($this->getProvinces() as $value) {
            $name = Str::lower(trim($value["prov_name"]));
            $slug = Str::slug($name);

            $province = Province::where('slug', $slug)->first();

            if (!$province) {
                $province = Province::create([
                    "name" => ucwords($name),
                    "slug" => $slug,
                    "code" => $value["prov_id"]
                ]);
            }
        }
    }
}
