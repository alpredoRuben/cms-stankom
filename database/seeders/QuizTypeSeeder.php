<?php

namespace Database\Seeders;

use App\Models\QuizType;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class QuizTypeSeeder extends Seeder
{
    private function getQuizType()
    {
        return collect([
            ["name" => "Pilihan Berganda"],
            ["name" => "Essay"],
            ["name" => "Pilihan Berganda Kompleks"]
        ]);
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getQuizType() as $item) {
            $slug = Str::slug($item['name']);

            QuizType::create([
                "name" => $item['name'],
                "slug" => $slug
            ]);
        }
    }
}
