<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Province;
use App\Models\City;
use App\Models\Distric;
use App\Models\PostalCode;
use App\Models\Ward;

class RegionSeeder extends Seeder
{

    private function loopKodePos($data, $id)
    {
        foreach ($data as $value) {
            $code = $value["postal_code"];

            $postalCode = PostalCode::where('code', $code)->first();

            if (!$postalCode) {
                PostalCode::create([
                    "ward_id" => $id,
                    "code" => $code
                ]);
            }
        }
    }


    private function loopKelurahan($data, $id)
    {
        $dataKodePos = file_get_contents(public_path() . '/CSV/kode_pos.json');
        $dataKodePos = json_decode($dataKodePos, true);

        foreach ($data as $value) {
            $name = Str::lower(trim($value["subdis_name"]));
            $slug = Str::slug($name);

            $ward = Ward::where('slug', $slug)->first();

            if (!$ward) {
                $ward = Ward::create([
                    "district_id" => $id,
                    "name" => ucwords($name),
                    "slug" => $slug,
                ]);
            }


            $collection = collect($dataKodePos)->filter(function ($x) use ($value) {
                return $x["subdis_id"] == $value["subdis_id"];
            });

            $this->loopKodePos($collection, $ward->id);
        }
    }

    private function loopKecamatan($data, $id)
    {
        $dataKelurahan = file_get_contents(public_path() . '/CSV/kelurahan.json');
        $dataKelurahan = json_decode($dataKelurahan, true);

        foreach ($data as $value) {

            $name = Str::lower(trim($value["dis_name"]));
            $slug = Str::slug($name);


            $district = Distric::where('slug', $slug)->first();

            if (!$district) {
                $district = Distric::create([
                    "city_id" => $id,
                    "name" => ucwords($name),
                    "slug" => $slug,

                ]);
            }


            $collection = collect($dataKelurahan)->filter(function ($x) use ($value) {
                return $x["dis_id"] == $value["dis_id"];
            });

            $this->loopKelurahan($collection, $district->id);
        }
    }

    private function loopKabupaten($data, $id)
    {
        $dataKecamatan = file_get_contents(public_path() . '/CSV/kecamatan.json');
        $dataKecamatan = json_decode($dataKecamatan, true);

        foreach ($data as $value) {

            $name = Str::lower(trim($value["city_name"]));
            $slug = Str::slug($name);

            $city = City::where('slug', $slug)->first();

            if (!$city) {
                $city = City::create([
                    "province_id" => $id,
                    "name" => ucwords($name),
                    "slug" => $slug,
                ]);
            }

            $collection = collect($dataKecamatan)->filter(function ($x) use ($value) {
                return $x["city_id"] == $value["city_id"];
            });

            $this->loopKecamatan($collection, $city->id);
        }
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataPropinsi = file_get_contents(public_path() . '/CSV/province.json');
        $dataPropinsi = json_decode($dataPropinsi, true);

        $dataKabupaten = file_get_contents(public_path() . '/CSV/kabupaten.json');
        $dataKabupaten = json_decode($dataKabupaten, true);


        //Fill Provinces
        foreach ($dataPropinsi as $value) {
            $name = Str::lower(trim($value["prov_name"]));
            $slug = Str::slug($name);

            $province = Province::where('slug', $slug)->first();

            if (!$province) {
                $province = Province::create([
                    "name" => ucwords($name),
                    "slug" => $slug,
                ]);
            }

            $collection = collect($dataKabupaten)->filter(function ($x) use ($value) {
                return $x['prov_id'] == $value["prov_id"];
            });

            $this->loopKabupaten($collection, $province->id);
        }
    }
}
