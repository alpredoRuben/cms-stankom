<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    private function getRoles()
    {
        return collect([
            "superadmin",
            "content writer",
            "pengajar",
            "peserta"
        ]);
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getRoles() as $value) {
            Role::create([
                "name" => $value,
            ]);
        }
    }
}
