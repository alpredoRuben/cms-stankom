<?php

namespace Database\Seeders;

use App\Models\SocialMedia;
use Illuminate\Database\Seeder;

class SocialMediaSeeder extends Seeder
{
    private function getSocialMedia()
    {
        return collect([
            [
                'medsos_name' => 'twitter',
                'medsos_icon' => '<i class="fab fa-twitter"></i>'
            ],
            [
                'medsos_name' => 'facebook',
                'medsos_icon' => '<i class="fab fa-facebook-f"></i>'
            ],
            [
                'medsos_name' => 'youtube',
                'medsos_icon' => '<i class="fab fa-youtube"></i>'
            ],
            [
                'medsos_name' => 'linkedin',
                'medsos_icon' => '<i class="fab fa-linkedin-in"></i>'
            ],
            [
                'medsos_name' => 'instagram',
                'medsos_icon' => '<i class="fab fa-instagram"></i>'
            ],
            [
                'medsos_name' => 'whatsapp',
                'medsos_icon' => '<i class="fab fa-whatsapp"></i>'
            ],
            [
                'medsos_name' => 'telegram',
                'medsos_icon' => '<i class="fab fa-telegram"></i>'
            ],
            [
                'medsos_name' => 'email',
                'medsos_icon' => '<i class="fas fa-envelope"></i>'
            ],
            [
                'medsos_name' => 'tiktok',
                'medsos_icon' => '<i class="fab fa-tiktok"></i>'
            ],
        ]);
    }

    public function run()
    {
        foreach ($this->getSocialMedia() as $value) {
            SocialMedia::create($value);
        }
    }
}
