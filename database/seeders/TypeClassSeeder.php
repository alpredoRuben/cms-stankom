<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\TypeClass;

class TypeClassSeeder extends Seeder
{
    private function getTypeClasses()
    {
        return collect([
            "Kelas Online",
            "Kelas Webinar"
        ]);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getTypeClasses() as $value) {
            $slug = Str::slug(Str::lower(trim($value)));

            TypeClass::create([
                "name" => $value,
                "slug" => $slug,
                "status" => true
            ]);
        }
    }
}
