<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    private function getUsers()
    {
        return collect([
            [
                'name' => 'Super Admin',
                'email' => 'superadmin@stankom.id',
                'password' => 'password',
                'remember_token' => Str::random(10),
                'status' => true,
                'gender' => 'pria',
                'rolename' => 'superadmin'
            ],
            [
                'name' => 'George Bryan',
                'email' => 'george.bryan@gmail.com',
                'password' => 'password',
                'remember_token' => Str::random(10),
                'status' => true,
                'gender' => 'pria',
                'rolename' => 'pengajar'
            ]
        ]);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getUsers() as $value) {
            $user = User::where('email', $value['email'])->first();

            if (!$user) {
                $user = User::create([
                    'name' => $value["name"],
                    'email' => $value["email"],
                    'email_verified_at' => now(),
                    'password' => bcrypt($value["password"]),
                    'remember_token' => Str::random(10),
                    'status' => $value['status']
                ]);
            }

            if (!$user->userProfile()->first()) {
                $user->userProfile()->create([
                    "gender" => $value['gender']
                ]);
            }


            $role = Role::where('name', Str::lower($value["rolename"]))->first();

            if ($role) {
                $user->assignRole([$role->id]);
            }
        }
    }
}
