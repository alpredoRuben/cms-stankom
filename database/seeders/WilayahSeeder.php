<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Province;
use App\Models\City;
use App\Models\Distric;
use App\Models\PostalCode;
use App\Models\Ward;

class WilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataPropinsi = file_get_contents(public_path() . '/CSV/province.json');
        $dataPropinsi = json_decode($dataPropinsi, true);

        $dataKabupaten = file_get_contents(public_path() . '/CSV/kabupaten.json');
        $dataKabupaten = json_decode($dataKabupaten, true);

        $dataKecamatan = file_get_contents(public_path() . '/CSV/kecamatan.json');
        $dataKecamatan = json_decode($dataKecamatan, true);

        $dataKelurahan = file_get_contents(public_path() . '/CSV/kelurahan.json');
        $dataKelurahan = json_decode($dataKelurahan, true);

        $dataKodePos = file_get_contents(public_path() . '/CSV/kode_pos.json');
        $dataKodePos = json_decode($dataKodePos, true);



        // Fill Provinces
        foreach ($dataPropinsi as $value) {
            $name = Str::lower(trim($value["prov_name"]));
            $slug = Str::slug($name);

            $province = Province::where('slug', $slug)->first();

            if (!$province) {
                $province = Province::create([
                    "name" => ucwords($name),
                    "slug" => $slug,
                    "code" => $value["prov_id"]
                ]);
            }
        }

        // Fill Kabupaten
        foreach ($dataKabupaten as $value) {

            $province = Province::where('code', $value["prov_id"])->first();

            $name = Str::lower(trim($value["city_name"]));
            $slug = Str::slug($name);

            $city = City::where('slug', $slug)->first();

            if (!$city) {
                $city = City::create([
                    "province_id" => $province ? $province->id : null,
                    "name" => ucwords($name),
                    "slug" => $slug,
                ]);
            }
        }

        // Fill Kecamatan
        foreach ($dataKecamatan as $value) {
            $city = City::where('code', $value["city_id"])->first();
            $name = Str::lower(trim($value["dis_name"]));
            $slug = Str::slug($name);

            $district = Distric::where('slug', $slug)->first();

            if (!$district) {
                $district = Distric::create([
                    "city_id" => $city ? $city->id : null,
                    "name" => ucwords($name),
                    "slug" => $slug,

                ]);
            }
        }

        // Fill Kelurahan
        foreach ($dataKelurahan as $value) {
            $district = Distric::where('code', $value["dis_id"])->first();

            $name = Str::lower(trim($value["subdis_name"]));
            $slug = Str::slug($name);

            $ward = Ward::where('slug', $slug)->first();

            if (!$ward) {
                $ward = Ward::create([
                    "district_id" => $district ? $district->id : null,
                    "name" => ucwords($name),
                    "slug" => $slug,
                ]);
            }
        }

        // Fill Kode Pos
        foreach ($dataKodePos as $value) {
            $ward = Ward::where('code', $value["subdis_id"])->first();
            $code = $value["postal_code"];

            $postalCode = PostalCode::where('code', $code)->first();

            if (!$postalCode) {
                PostalCode::create([
                    "ward_id" => $ward ? $ward->id : null,
                    "code" => $code
                ]);
            }
        }
    }
}
