"use strict";

function removeEmptyElement(data) {
    Object.keys(data).forEach((key) => {
      if (data[key] === null || data[key] === '') {
        delete data[key];
      }
    });

    return data;
  }


function showErrorPopup(error) {
    const { status, data } = error.response;
    let errorMessage = status == 500 ? error.response?.statusText : data.message;

    Swal.fire({
        icon: "error",
        title: "GAGAL",
        text: errorMessage,
    });
}

function initSelectBoxWithData(data, element, selected = null, placheloder ='') {
    element.select2({
        theme: "bootstrap-5",
        placheloder: placheloder,
        data
    });

    if (selected != null) {
        element.val(selected).change();
    }
}

function initialSelect2(url, element, propertyName = 'name', propertyId = 'id') {
    element.select2({
        theme: "bootstrap-5",
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        tags: true,
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(term) {
                return {
                    term: term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.items, function(item) {
                        return {
                            text: item[propertyName],
                            id: item[propertyId]
                        }
                    })
                };
            }
        }
    });
}

function initJQSelect2Paginate(element, url, placheloder)
{
    element.select2({
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder: placheloder,
        allowClear: false,
        ajax: {
            url: url,
            dataType: 'json',
            data: function(params) {
                let input = {
                    term: params.term || '',
                    page: params.page || 1,
                }
                return input

            },
            processResults: function(data) {
                return {
                    results: data.results,
                    pagination: data.pagination
                };
            },
            cache: true
        },
    });
}

function initJQSelect2(element, url, placheloder) {
    element.select2({
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder,
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(params) {
                let input = {
                    term: params.term || '',
                }
                return input

            },
            processResults: function(data) {
                return {
                    results: data.results,
                };
            },
        }
    });
}


function initJQSelect2Selection(element, url, placheloder, selected = null) {
    axios.get(url).then(response => {
        $.each(response.data.results, function(index, value) {
            element.append($("<option></option>").attr("value", value.id).text(value.text));

            if (selected != null && selected == value.id) {
                element.val(value.id).change();
            }
        });
    }).catch(error => {
        console.log(error);
    });


    element.select2({
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder: placheloder
    });
}

//Dipakai
function select2WithSelection(element, url, placheloder, selected = null, maxLength = false) {

    if(maxLength == true) {
        element.select2({
            theme: "bootstrap-5",
            placheloder,
            minimumInputLength: 1,
            ajax: {
                url: url,
                dataType: "json",
                type: "GET",
                data: function(params) {
                    let input = {
                        term: params.term || '',
                        page: params.page || 1
                    }
                    return input

                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    console.log(data.total_count);
                    return {
                        results: data.results,
                        pagination: {
                            more:   (params.page * 30) < data.total_count
                        }
                    };
                },
            },
            templateResult: function (item) {
                return item.text;
            },
            templateSelection: function (item) {
                if(item.id == '' && selected != null) {
                    item.id = selected.id;
                    item.text = selected.name;
                }

                return item.text;
            }
        });

    }
    else {
        element.select2({
            theme: "bootstrap-5",
            placheloder,
            ajax: {
                url: url,
                dataType: "json",
                type: "GET",
                data: function(params) {
                    let input = {
                        term: params.term || '',
                        page: params.page || 1
                    }
                    return input

                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    console.log(data.total_count);
                    return {
                        results: data.results,
                        pagination: {
                            more:  (params.page * 30) < data.total_count
                        }
                    };
                },
            },
            templateResult: function (item) {
                return item.text;
            },
            templateSelection: function (item) {
                if(item.id == '' && selected != null) {
                    item.id = selected.id;
                    item.text = selected.name;
                }

                return item.text;
            }
        });

    }



}

function initJQSelect2InModalPaginate(element, url, placheloder, modal)
{
    element.select2({
        dropdownParent:modal,
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder: placheloder,
        allowClear: false,
        ajax: {
            url: url,
            dataType: 'json',
            data: function(params) {
                let input = {
                    term: params.term || '',
                    page: params.page || 1,
                }
                return input

            },
            processResults: function(data) {
                return {
                    results: data.results,
                    pagination: data.pagination
                };
            },
            cache: true
        },
    });
}

function initSelect2InModal(element, url, placheloder, modal)
{
    element.select2({
        dropdownParent:modal,
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder,
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(params) {
                let input = {
                    term: params.term || '',
                }
                return input

            },
            processResults: function(data) {
                return {
                    results: data.results,
                };
            },
        },
        allowClear: true
    });
}

//Dipakai
function initSelect2InModalWithSelection(element, modal, url, placheloder, selected = null) {
    element.select2({
        dropdownParent:modal,
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder,
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(params) {
                let input = {
                    term: params.term || '',
                    page: params.page || 1
                }
                return input

            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: data.results,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
        },
        templateResult: function (item) {
            return item.text;
        },
        templateSelection: function (item) {
            if(item.id == '' && selected != null) {
                item.id = selected.id;
                item.text = selected.name;
            }

            return item.text;
        }
    });

}


function initSelect2InModalPaginateWithSelection(element, modal, url, placheloder, selected = null) {
    element.select2({
        dropdownParent:modal,
        theme: "bootstrap-5",
        minimumInputLength: 2,
        placheloder,
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(params) {
                let input = {
                    term: params.term || '',
                    page: params.page || 1
                }
                return input

            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: data.results,
                    pagination: data.pagination
                };
            },
        },
        templateResult: function (item) {
            return item.text;
        },
        templateSelection: function (item) {
            if(item.id == '' && selected != null) {
                item.id = selected.id;
                item.text = selected.name;
            }

            return item.text;
        }
    });

}
