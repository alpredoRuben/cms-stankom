@section('title')
    REGISTER
@endsection

@section('metadata')
    <meta content="Register" name="keywords" />
    <meta content="LMS Stankom - Halaman Register" name="description" />
@endsection

<x-guest-layout>
    <div class="container-fluid bg-backdrop py-6 my-2 mt-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center">
            <h1 class="display-4 text-white animated slideInDown mb-4">
                SELAMAT DATANG DI LMS STANKOM
            </h1>
        </div>
    </div>

    <div class="container-xxl courses my-0 py-6 pb-0">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500">
                <h1 class="display-6 mb-4">Pendaftaran Akun Baru</h1>
            </div>

            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-4 col-lg-6 wow fadeInUp" data-wow-delay="0.1s">

                    @if ($message = Session::get('messageSuccess'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @endif

                    @if ($message = Session::get('messageError'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @endif

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <!-- FIRST NAME -->
                        <div class="mb-3">
                            <x-label for="nik" class="form-label" :value="__('NIK')" />

                            <x-input id="nik" class="form-control" type="text" name="nik"
                                :value="old('nik')" />

                            @if ($errors->has('nik'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('nik') }}
                                </div>
                            @endif
                        </div>


                        <!-- FIRST NAME -->
                        <div class="mb-3">
                            <x-label for="first_name" class="form-label" :value="__('Nama Depan')" />

                            <x-input id="first_name" class="form-control" type="text" name="first_name"
                                :value="old('first_name')" />

                            @if ($errors->has('first_name'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('first_name') }}
                                </div>
                            @endif
                        </div>


                        <!-- LAST NAME -->
                        <div class="mb-3">
                            <x-label for="last_name" class="form-label" :value="__('Nama Belakang')" />

                            <x-input id="last_name" class="form-control" type="text" name="last_name"
                                :value="old('last_name')" />

                            @if ($errors->has('last_name'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('last_name') }}
                                </div>
                            @endif
                        </div>


                        <!-- EMAIL -->
                        <div class="mb-3">
                            <x-label for="email" class="form-label" :value="__('Email')" />

                            <x-input id="email" class="form-control" type="text" name="email"
                                :value="old('email')" />

                            @if ($errors->has('email'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>

                        <!-- PASSWORD -->
                        <div class="mb-3">
                            <x-label for="password" class="form-label" :value="__('Password')" />
                            <x-input id="password" class="form-control" type="password" name="password"
                                :value="old('password')" autocomplete="new-password" />

                            @if ($errors->has('password'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>

                        <!-- CONFIRM PASSWORD -->
                        <div class="mb-3">
                            <x-label for="password_confirmation" class="form-label" :value="__('Confirm Password')" />

                            <x-input id="password_confirmation" class="form-control" type="password"
                                name="password_confirmation" />
                        </div>

                        <div class="mb-3">
                            <a class="text-sm" href="{{ route('login') }}">
                                {{ __('Already registered?') }}
                            </a>
                        </div>


                        <div class="mb-3">
                            <div class="d-grid gap-2">
                                <x-button class="btn btn-dark">
                                    <i class="fas fa-user-plus"></i>
                                    {{ __('DAFTAR') }}
                                </x-button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

</x-guest-layout>
