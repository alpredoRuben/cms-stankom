@section('title')
    RESET PASSWORD
@endsection

@section('metadata')
    <meta content="Lupa Password" name="keywords" />
    <meta content="LMS Stankom - Halaman Lupa Password" name="description" />
@endsection

<x-guest-layout>

    <div class="container-fluid bg-backdrop py-6 my-2 mt-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center">
            <h1 class="display-4 text-white animated slideInDown mb-4">
                SELAMAT DATANG DI LMS STANKOM
            </h1>
        </div>
    </div>


    <div class="container-xxl courses my-0 py-6 pb-0">
        <div class="container">
            <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500">
                <h1 class="display-6 mb-4">Reset Password</h1>
            </div>

            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-4 col-lg-6 wow fadeInUp" data-wow-delay="0.1s">

                    @if ($message = Session::get('messageSuccess'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @endif

                    @if ($message = Session::get('messageError'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @endif

                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />



                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $request->route('token') }}">

                        <div class="mb-3">
                            <!-- EMAIL -->
                            <x-label for="email" class="form-label" :value="__('Email')" />

                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email', $request->email)"
                                autofocus readonly />

                            @if ($errors->has('email'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>

                        <div class="mb-3">
                            <!-- PASSWORD -->
                            <x-label for="password" class="form-label" :value="__('Password')" />
                            <x-input id="password" class="form-control" type="password" name="password"
                                :value="old('password')" autocomplete="current-password" />

                            @if ($errors->has('password'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>


                        <div class="mb-3">
                            <!-- PASSWORD -->
                            <x-label for="password_confirmation" class="form-label" :value="__('Confirm Password')" />
                            <x-input id="password_confirmation" class="form-control" type="password"
                                name="password_confirmation" required />


                            @if ($errors->has('password'))
                                <div class="text-danger text-sm fst-italic">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>


                        <div class="mb-3">
                            <div class="d-grid gap-2">
                                <x-button class="btn btn-dark">
                                    <i class="fas fa-sign-in-alt"></i>
                                    {{ __('Reset Password') }}
                                </x-button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

</x-guest-layout>
