<div class="row mb-3">
    <div class="col-sm-12">
        <div class="home-tab">
            <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link  {{ Route::is('master.location.province.list') ? 'active' : '' }}"
                            href="{{ route('master.location.province.list') }}">Propinsi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{ Route::is('master.location.city.list') ? 'active' : '' }}"
                            href="{{ route('master.location.city.list') }}">Kabupaten / Kota</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{ Route::is('master.location.district.list') ? 'active' : '' }}"
                            href="{{ route('master.location.district.list') }}">Kecamatan</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link  {{ Route::is('master.location.ward.list') ? 'active' : '' }}"
                            href="{{ route('master.location.ward.list') }}">Kelurahan</a>
                    </li>

                </ul>

            </div>
        </div>
    </div>
</div>

{{ $slot }}
