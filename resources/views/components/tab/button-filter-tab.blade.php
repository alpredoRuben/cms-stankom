<div class="row mb-3">
    <div class="col-12">
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <select id="selectionFilter" class="form-select form-select-lg"></select>
            </div>
            <div class="col-auto">
                <input type="text" id="inputTextSearch" class="form-control form-control-lg" />
            </div>
            <div class="col-auto">
                <div class="d-grid gap-2">
                    <button type="button" class="btn btn-warning" onclick="eventSearchData()">
                        <i class="fas fa-search"></i> &nbsp;
                        Search
                    </button>
                </div>
            </div>

            {{ $slot }}
        </div>
    </div>

</div>
