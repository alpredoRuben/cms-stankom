<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title>VERIFIKASI EMAIL AKUN</title>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }
    </style>
    <style media="screen and (min-width:480px)">
        .moz-text-html .mj-column-per-100 {
            width: 100% !important;
            max-width: 100%;
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.mj-full-width-mobile {
                width: 100% !important;
            }

            td.mj-full-width-mobile {
                width: auto !important;
            }
        }
    </style>
    <style type="text/css">
        @media (max-width: 479px) {
            .hide-on-mobile {
                display: none !important;
            }
        }

        .gr-mlimage-jvawtf img {
            box-sizing: border-box;
        }

        @media (min-width: 480px) {
            .gr-mlimage-xpkewp {
                height: 0px !important;
            }
        }

        .gr-mltext-kcexjp a,
        .gr-mltext-kcexjp a:visited {
            text-decoration: none;
        }

        .gr-mltext-kpkwlm a,
        .gr-mltext-kpkwlm a:visited {
            text-decoration: none;
        }

        .gr-mltext-ahorel a,
        .gr-mltext-ahorel a:visited {
            text-decoration: none;
        }

        .gr-mlbutton-payugj p {
            direction: ltr;
        }

        .gr-footer-dtmtee a,
        .gr-footer-dtmtee a:visited {
            color: #00BAFF;
            text-decoration: underline;
        }
    </style>
    <link
        href="https://fonts.googleapis.com/css?display=swap&family=Lato:400,400i,700,700i&subset=cyrillic,greek,latin-ext,vietnamese"
        rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?display=swap&family=Lato:400,400i,700,700i&subset=cyrillic,greek,latin-ext,vietnamese);
    </style>
</head>

<body style="word-spacing:normal;background-color:#FFFFFF;">
    <div style="background-color:#FFFFFF;">

        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="width:100%;">
                <tbody>
                    <tr>
                        <td
                            style="border-bottom:0 none #000000;border-left:0 none #000000;border-right:0 none #000000;border-top:0 none #000000;direction:ltr;font-size:0px;padding:10px 5px 20px 5px;text-align:center;">

                            <div class="mj-column-per-100 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    width="100%">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="background-color:transparent;border-bottom:none;border-left:none;border-right:none;border-top:none;vertical-align:top;padding:0;">
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center"
                                                                class="gr-mlimage-jvawtf gr-mlimage-xpkewp"
                                                                style="font-size:0px;padding:5px;word-break:break-word;">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                    role="presentation"
                                                                    style="border-collapse:collapse;border-spacing:0px;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width:100px;">
                                                                                <img alt="" height="auto"
                                                                                    src="{{ asset('images/logo.jpg') }}"
                                                                                    width="100">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="width:100%;">
                <tbody>
                    <tr>
                        <td
                            style="border-bottom:0 none #000000;border-left:0 none #000000;border-right:0 none #000000;border-top:0 none #000000;direction:ltr;font-size:0px;padding:20px 0;text-align:center;">

                            <div class="mj-column-per-100 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    width="100%">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="background-color:transparent;border-bottom:none;border-left:none;border-right:none;border-top:none;vertical-align:top;padding:0;">
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" class="gr-mltext-dumori gr-mltext-kcexjp"
                                                                style="font-size:0px;padding:10px;word-break:break-word;">
                                                                <div
                                                                    style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.4;text-align:left;color:#000000;">
                                                                    <p
                                                                        style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;">
                                                                        <span
                                                                            style="color: #A1A9AD; font-size: 34px; font-family: Times New Roman; font-weight: 700;">
                                                                            <strong>
                                                                                Terima Kasih
                                                                            </strong>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" class="gr-mltext-dumori gr-mltext-kpkwlm"
                                                                style="font-size:0px;padding:10px;word-break:break-word;">
                                                                <div
                                                                    style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.4;text-align:left;color:#000000;">
                                                                    <p
                                                                        style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;">
                                                                        <strong>
                                                                            <span
                                                                                style="font-family: Lato, Arial, sans-serif">
                                                                                Verifikasi dan Aktivasi Email Peserta
                                                                                LMS STANKOM
                                                                                Kemenparekraf
                                                                            </span>
                                                                        </strong>
                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" class="gr-mltext-dumori gr-mltext-ahorel"
                                                                style="font-size:0px;padding:10px;word-break:break-word;">
                                                                <div
                                                                    style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.4;text-align:left;color:#000000;">
                                                                    <p>
                                                                        <span
                                                                            style="font-family: Lato, Arial, sans-serif">Halo,
                                                                            {{ $name }}</span>
                                                                    </p>
                                                                    <p>
                                                                        Kredensial untuk login :
                                                                    </p>

                                                                    <p>
                                                                        Email : {{ $email }} <br />
                                                                        password: {{ $password }}
                                                                    </p>

                                                                    <p>
                                                                        <span
                                                                            style="font-family: Lato, Arial, sans-serif">Email
                                                                            anda berhasil diverifikasi dan akun sudah
                                                                            aktif dengan kredensial diatas. Terima kasih
                                                                            telah setia dengan
                                                                            STANKOM Direktorat Standardisasi Kompetensi
                                                                            Kementrian Pariwisata dan Ekonomi
                                                                            Kreatif.
                                                                        </span>
                                                                    </p>
                                                                    <p>
                                                                        <span
                                                                            style="font-family: Lato, Arial, sans-serif">
                                                                            Klik tombol &quot;Aktifkan
                                                                            Saya&quot; dibawah ini, untuk lanjut ke
                                                                            tahap selanjutnya</span>
                                                                    </p>
                                                                    <p>
                                                                        <span
                                                                            style="font-family: Lato, Arial, sans-serif">
                                                                            Selamat belajar!
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="width:100%;">
                <tbody>
                    <tr>
                        <td
                            style="border-bottom:0 none #000000;border-left:0 none #000000;border-right:0 none #000000;border-top:0 none #000000;direction:ltr;font-size:0px;padding:5px;text-align:center;">

                            <div class="mj-column-per-100 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    width="100%">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="background-color:transparent;border-bottom:none;border-left:none;border-right:none;border-top:none;vertical-align:top;padding:0;">
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" vertical-align="middle"
                                                                class="gr-mlbutton-yfekjm gr-mlbutton-payugj link-id-393c0f3484c5"
                                                                style="font-size:0px;padding:5px;word-break:break-word;">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                    role="presentation"
                                                                    style="border-collapse:separate;line-height:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center" bgcolor="#C5CBE3"
                                                                                role="presentation"
                                                                                style="border:none;border-bottom:0 none #000000;border-left:0 none #000000;border-radius:5px;border-right:0 none #000000;border-top:0 none #000000;cursor:auto;font-style:normal;mso-padding-alt:12px;background:#C5CBE3;word-break:break-word;"
                                                                                valign="middle">
                                                                                <a href="/email/account/activation/{{ $token }}"
                                                                                    style="display:inline-block;background:#C5CBE3;color:#4056A1;font-family:Lato, Arial, sans-serif;font-size:16px;font-style:normal;font-weight:bold;line-height:100%;margin:0;text-decoration:none;text-transform:none;padding:12px;mso-padding-alt:0px;border-radius:5px;">
                                                                                    Aktifkan</a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="width:100%;">
                <tbody>
                    <tr>
                        <td
                            style="border-bottom:0 none #000000;border-left:0 none #000000;border-right:0 none #000000;border-top:0 none #000000;direction:ltr;font-size:0px;padding:5px;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:590px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    width="100%">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="background-color:transparent;border-bottom:none;border-left:none;border-right:none;border-top:none;vertical-align:top;padding:0;">
                                                <table border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center"
                                                                class="gr-footer-ugsteh gr-footer-dtmtee"
                                                                style="font-size:0px;padding:10px;word-break:break-word;">
                                                                <div
                                                                    style="font-family:Arial;font-size:10px;font-style:normal;line-height:1;text-align:center;text-decoration:none;color:#000000;">
                                                                    <div>
                                                                        {{ $company['email'] }} -
                                                                        {{ $company['phone'] }}
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


    </div>
</body>

</html>
