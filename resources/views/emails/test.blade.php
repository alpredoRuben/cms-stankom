<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tegihan Terverifikasi</title>
    <style>
        * {
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
            line-height: 1.5;
        }
    </style>
</head>

<body style="max-width: 600px; margin: 30px auto;">
    <table style="width:100%">
        <tr>
            <td><img src="https://balailelang-info.d.logique.co.id/img/logo.png" style="width: 100px;" alt="logo">
            </td>
            <td align="right" style="font-size: 20px; font-weight: bold;">Universal Collection</td>
        </tr>
    </table>
    <p>Halo! {{ data . customer_name }},</p>
    <p>Selamat Anda telah memenangkan unit pada lelang {{ data . auction_name }} dengan rincian : </p>
    <p>
    <table>
        <tr>
            <td width="150px">Nomor Lot</td>
            <td>: {{ data . lot_number }}</td>
        </tr>
        <tr>
            <td>Jumlah Pembayaran</td>
            <td>: {{ data . total_payment }}</td>
        </tr>
    </table>
    </p>
    <p>
        Terima kasih dan sampai bertemu di lelang kami berikutnya
    </p>

    <br /><br />
    <p>Salam hangat<br />Universal Collection</p>
    <br /><br />
    <p style="font-size: 12px; color: #999; text-align: center;">Email dikirimkan otomatis melalui sistem, untuk
        informasi lebih lanjut silahkan menghubungi<br />
        {{ data . EMAIL_CS }} atau {{ data . PHONE_CS }}</p>
</body>

</html>
