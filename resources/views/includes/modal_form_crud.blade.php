<div id="modalFormDefault" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleDefault" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="modalAlertDefault" class="col-12"></div>
                    <div class="col-12">
                        <form>
                            <div class="form-floating mb-3">
                                <input type="text" id="modalInputDefaultName" class="form-control" />

                                <label for="modalInputDefaultName" class="form-label">{{ $labelDefaultName }}</label>
                            </div>

                            <div class="form-floating mb-3">
                                <textarea class="form-control form-textarea" id="modalTexareaDefaultDescription" style="min-height: 250px;"></textarea>
                                <label for="modalTexareaDefaultDescription"
                                    class="form-label">{{ $labelDefaultDescription }}</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonDefault" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
