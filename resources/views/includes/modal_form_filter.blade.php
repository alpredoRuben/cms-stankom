<div id="modalFilterBox" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Filter Pencarian</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-12">
                        <div id="modalFilterAlert"></div>
                        <form>
                            <div class="mb-3">
                                <label for="modalFilterSelection" class="form-label">
                                    Kategori Pencarian
                                </label>
                                <select id="modalFilterSelection" name="modalFilterSelection" class="form-select">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($filterBox as $item)
                                        <option value="{{ $item['id'] }}">{{ $item['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="modalFilterText" class="form-label">Pencarian</label>
                                <input type="text" id="modalFilterText" class="form-control form-control-lg" />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalFilterButton" type="button" class="btn btn-primary" data-id="">
                    Tampilkan Pencarian
                </button>
            </div>
        </div>
    </div>
</div>
