<div class="row mb-2 p-3">
    <div class="col-4">
        <div class="d-grid">
            <button type="button" class="btn btn-warning " onclick="eventShowFilter()">
                <i class="fas fa-filter"></i>
                Pencarian
            </button>
        </div>
    </div>

    <div class="col-4">
        <div class="d-grid">
            <button type="button" class="btn btn-success" onclick="eventRefreshData()">
                <i class="fas fa-refresh"></i>
                Refresh Data
            </button>
        </div>
    </div>

    <div class="col-4">
        <div class="row">
            <div class="col-6">
                <div class="d-grid dropdown">
                    <a class="btn btn-primary dropdown-toggle" href="#" role="button"
                        id="dropdownMenuOperasiData" data-bs-toggle="dropdown" aria-expanded="false">
                        Manage Data
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuOperasiData">
                        @if ($addAction == true)
                            @if ($addUrl != null)
                                <li>
                                    <a href="{{ $addUrl }}" class="dropdown-item">
                                        <i class="fas fa-plus"></i>
                                        {{ $addTitle }}
                                    </a>
                                </li>
                            @else
                                <li>
                                    <button type="button" class="dropdown-item" onclick="eventCreateData()">
                                        <i class="fas fa-plus"></i> &nbsp; {{ $addTitle }}
                                    </button>
                                </li>
                            @endif
                        @endif


                        @if ($exportUrl != null)
                            <li>
                                <a href="{{ $exportUrl }}" class="dropdown-item">
                                    <i class="fas fa-file-export"></i> &nbsp; Export File
                                </a>
                            </li>
                        @endif

                        @if (isset($urlDownload) && $urlDownload != null)
                            <li>
                                <a class="dropdown-item" href="{{ $urlDownload }}">
                                    <i class="fas fa-download"></i> &nbsp; Download File
                                </a>
                            </li>
                        @endif

                        @if (isset($import) && $import == true)
                            <li>
                                <button type="button" class="dropdown-item" onclick="eventImportData()">
                                    <i class="fas fa-file-import"></i> &nbsp; Import File
                                </button>
                            </li>
                        @endif

                    </ul>
                </div>
            </div>



            <div class="col-6">
                <div class="d-grid dropdown">
                    <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuPreviewData"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Preview Data
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuPreviewData">
                        <li>
                            <button type="button" class="dropdown-item" onclick="eventPreviewData()">
                                <i class="fas fa-table"></i>
                                &nbsp; Original Data
                            </button>
                        </li>

                        <li>
                            <button type="button" class="dropdown-item" onclick="eventPreviewHistory()">
                                <i class="fas fa-history"></i>
                                &nbsp; {{ isset($textHistory) ? $textHistory : 'Riwayat Data' }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
