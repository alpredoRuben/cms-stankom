<!-- Footer Start -->
<div class="container-fluid bg-dark text-light footer my-6 mb-0 py-6 wow fadeIn" data-wow-delay="0.1s">
    <div class="container">
        <div class="row g-5">
            <div class="col-lg-6 col-md-6">
                <div class="mb-2">
                    <div class="d-flex flex-row">
                        <div>
                            <i class="fa fa-book-reader text-white"></i>
                        </div>

                        <div class="text-primary ms-3 text-wrap fw-bold" style="width: 22rem; font-size: 14px;">
                            {!! $contact && $contact->company_name ? Str::upper($contact->company_name) : 'LMS STANKOM' !!}
                        </div>
                    </div>
                </div>

                <div class="mb-2">
                    <div class="d-flex flex-row">
                        <div>
                            <i class="fa fa-map-marker-alt"></i>
                        </div>
                        <div class="ms-3" style="width: 30rem; line-height: 0.5em; font-size: 14px;">
                            {!! $contact ? $contact->address : 'COMPANY ADDRESS' !!}
                        </div>
                    </div>
                </div>


                <div class="mb-2">
                    <div class="d-flex flex-row">
                        <div>
                            <i class="fa fa-phone-alt"></i>
                        </div>

                        <div class="ms-3">
                            {{ $contact ? $contact->phone : 'COMPANY PHONE' }}
                        </div>
                    </div>
                </div>

                <div class="mb-2">
                    <div class="d-flex flex-row">
                        <div>
                            <i class="fa fa-envelope"></i>
                        </div>

                        <div class="ms-3">
                            {!! $contact ? $contact->email : 'COMPANY EMAIL' !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <h4 class="text-light mb-4">Tautan Cepat</h4>
                <a class="btn btn-link" href="{{ route('web.tentang_kami') }}#aboutUs">Tentang Kami</a>
                <a class="btn btn-link" href="{{ route('web.data_lsp') }}">Daftar LSP</a>
                <a class="btn btn-link" href="{{ route('web.tentang_kami') }}#contactUs">Kontak Kami</a>
                <a class="btn btn-link" href="{{ route('web.aturan_pelatihan') }}">Aturan Pelatihan</a>
            </div>

            <div class="col-lg-3 col-md-6">
                <h6 class="text-white mt-4 mb-3">Sosial Media</h6>
                <div class="d-flex pt-2">
                    @if (count($socialMedia) > 0)
                        @foreach ($socialMedia as $item)
                            <a target="_blank" class="btn btn-square btn-outline-light me-1" href="{{ $item->url }}">
                                {!! $item->socialMedia->medsos_icon !!}
                            </a>
                        @endforeach
                    @else
                        <a target="_blank" class="btn btn-square btn-outline-light me-1" href="">
                            <i class="fas fa-ban"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->


<!-- Copyright Start -->
<div class="container-fluid copyright text-light py-4 wow fadeIn" data-wow-delay="0.1s">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                &copy; <a href="https://stankom.id">LMS Stankom</a>
            </div>
        </div>
    </div>
</div>
<!-- Copyright End -->
