<nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0">
    <a href="{{ route('web.home') }}" class="navbar-brand d-flex align-items-center border-end px-lg-5">
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="mt-3">
                <img src="{{ $logo ? $logo->logo_path : asset('images/Sertifikasi Kompetensi.png') }}"
                    style="width: 120px; height: 60px;" />
            </div>
            <div class="mt-4 ms-3 text-dark text-wrap fw-bold" style="width: 22rem; font-size: 14px;">
                {!! $contact && $contact->company_name ? Str::upper($contact->company_name) : 'LMS STANKOM' !!}
            </div>
        </div>
    </a>
    <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto p-4 p-lg-0">
            <a href="{{ route('web.home') }}"
                class="nav-item nav-link {{ Route::is('web.home') ? 'active' : '' }}">Home</a>

            <a href="{{ route('web.data_lsp') }}"
                class="nav-item nav-link {{ Route::is('web.data_lsp') ? 'active' : '' }}">Daftar LSP</a>

            <a href="{{ route('web.modul') }}"
                class="nav-item nav-link {{ Route::is('web.modul') ? 'active' : '' }}">Modul</a>

            <a href="{{ route('web.kelas_webinar') }}"
                class="nav-item nav-link {{ Route::is('web.kelas_webinar') ? 'active' : '' }}">Kelas / Webinar</a>

            <div class="nav-item dropdown">
                <a href="#"
                    class="nav-link dropdown-toggle {{ Route::is('web.berita') || Route::is('web.artikel') ? 'active' : '' }}"
                    data-bs-toggle="dropdown">Berita & Artikel</a>
                <div class="dropdown-menu bg-light m-0">

                    <a href="{{ route('web.berita') }}"
                        class="dropdown-item {{ Route::is('web.berita') ? 'active' : '' }}">
                        Berita
                    </a>
                    <a href="{{ route('web.artikel') }}"
                        class="dropdown-item {{ Route::is('web.artikel') ? 'active' : '' }}">
                        Artikel
                    </a>
                </div>
            </div>


            @auth
                <a href="{{ route('dashboard') }}" class="nav-item nav-link {{ Route::is('dashboard') ? 'active' : '' }}">
                    Dashboard
                </a>
            @else
                <a href={{ route('register') }} class="nav-item nav-link {{ Route::is('register') ? 'active' : '' }}">
                    Daftar
                </a>

                <a href={{ route('login') }} class="nav-item nav-link {{ Route::is('login') ? 'active' : '' }}">
                    Masuk
                </a>
            @endauth

        </div>

    </div>
</nav>
