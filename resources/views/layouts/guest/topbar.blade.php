<div class="container-fluid bg-backdrop text-light p-0" style="height: 60px;">
    <div class="row gx-0 d-none d-lg-flex">
        <div class="col-lg-7 px-5 text-start">
            <div class="row">
                <div class="col-4">
                    <div class="h-100 d-inline-flex align-items-center mt-2">
                        <small class="far fa-clock text-primary me-2"></small>
                        <small>{{ \Carbon\Carbon::now()->format('D, d F Y  H:m:s') }}</small>
                    </div>
                </div>
                <div class="col-8">
                    <div class="h-100 d-flex  flex-row align-items-center mt-2">
                        <div class="mx-2" style="width:250px;">
                            <select id="selectCategorySearch" class="form-control form-select border-none">
                                <option value="class_webinar">Kelas/Webinar</option>
                                <option value="learning_module">Modul Pembelajaran</option>
                                <option value="lsp">LSP</option>
                                <option value="news">Berita</option>
                                <option value="article">Artikel</option>
                            </select>
                        </div>

                        <div class="me-2" style="width: 400px;">
                            <input type="text" id="textSearchCagegory" class="form-control"
                                placeholder="Cari sesuai kategori" />
                        </div>

                        <div style="width: 100px;">
                            <button class="btn btn-primary" onclick="eventSearchCategoryPage()">
                                <i class="fas fa-search"></i> Cari
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 px-5 text-end">
            <div class="h-100 d-inline-flex align-items-center me-4 mt-2">
                <small class="fa fa-phone-alt text-primary me-2"></small>
                <small>{{ $contact ? $contact->phone : 'COMPANY PHONE' }}</small>
            </div>
            <div class="h-100 d-inline-flex align-items-center mx-n2">
                @if (count($socialMedia) > 0)
                    @foreach ($socialMedia as $item)
                        <a target="_blank"
                            class="btn btn-square btn-link rounded-0 border-0 border-end border-secondary"
                            href="{{ $item->url }}">
                            {!! $item->socialMedia->medsos_icon !!}
                        </a>
                    @endforeach
                @else
                    <a target="_blank" class="btn btn-square btn-link rounded-0 border-0 border-end border-secondary"
                        href="">
                        <i class="fas fa-ban"></i>
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
