@php
$routeName = Route::currentRouteName();
@endphp

<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">

        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="menu-icon mdi mdi-grid-large"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        @if (!auth()->user()->hasRole('peserta'))
            <li class="nav-item nav-category">MASTER</li>

            <!-- User -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#masterUser" aria-expanded="false"
                    aria-controls="masterUser">
                    <i class="menu-icon mdi mdi-account-multiple"></i>
                    <span class="menu-title">USER</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="masterUser">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.users.main.list') }}">
                                Pengguna
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- Pendidikan dan Profesi -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#educationProfession" aria-expanded="false"
                    aria-controls="educationProfession">
                    <i class="menu-icon mdi mdi-wallet-travel"></i>
                    <span class="menu-title">PENDIDIKAN & PROFESI</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="educationProfession">
                    <ul class="nav flex-column sub-menu">
                        <!-- Master Pendidikan -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.education.list') }}">
                                <span class="menu-title">Pendidikan</span>
                            </a>
                        </li>


                        <!-- Master Pekerjaan -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.profession.list') }}">
                                <span class="menu-title">Profesi</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- LSP dan Accessor -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#lspAccessor" aria-expanded="false"
                    aria-controls="lspAccessor">
                    <i class="menu-icon mdi mdi-account-star"></i>
                    <span class="menu-title">LSP & ASSESSOR</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="lspAccessor">
                    <ul class="nav flex-column sub-menu">
                        <!-- Sertifikasi -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.certification_types.list') }}">
                                <span class="menu-title">Bidang Sertifikasi</span>
                            </a>
                        </li>


                        <!-- LSP -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.lsp.list') }}">
                                <span class="menu-title">
                                    LSP
                                </span>
                            </a>
                        </li>

                        <!-- Master Accessor -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.accessor.list') }}">
                                <span class="menu-title">Assessor</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- Modul -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#masterModule" aria-expanded="false"
                    aria-controls="masterModule">
                    <i class="menu-icon mdi mdi-card-text-outline"></i>
                    <span class="menu-title">MODUL</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="masterModule">
                    <ul class="nav flex-column sub-menu">
                        <!-- Kategori Modul -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.module.category.list') }}">
                                Kategori Modul
                            </a>
                        </li>

                        <!-- Sub Kategori Modul -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.module.sub_category.list') }}">
                                Sub Kategori Modul
                            </a>
                        </li>

                        <!-- Tipe Modul -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.module.type.list') }}">
                                Tipe Modul
                            </a>
                        </li>

                        <!-- Modul Pembelajaran -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('learning_modules.list') }}">
                                <span class="menu-title">
                                    Modul Pembelajaran
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <!-- Kelas -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#masterClasses" aria-expanded="false"
                    aria-controls="masterClasses">
                    <i class="menu-icon mdi mdi-beats"></i>
                    <span class="menu-title">KELAS</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="masterClasses">
                    <ul class="nav flex-column sub-menu">
                        <!-- Master Tipe Kelas -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.classes.type.list') }}">
                                <span class="menu-title">Tipe Kelas</span>
                            </a>
                        </li>


                        <!-- Kelas Webinar -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('class_webinar.list') }}">
                                <span class="menu-title">
                                    Kelas / Webinar
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <!-- Kuis -->
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#masterQuiz" aria-expanded="false"
                    aria-controls="masterQuiz">
                    <i class="menu-icon mdi mdi-deskphone"></i>
                    <span class="menu-title">MASTER KUIS</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="masterQuiz">
                    <ul class="nav flex-column sub-menu">
                        <!-- Kuis -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.quiz.list') }}">
                                <span class="menu-title">Data Kuis</span>
                            </a>
                        </li>

                        <!-- Kelas Webinar -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.question.index') }}">
                                <span class="menu-title">
                                    Data Pertanyaan
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <!-- Lokasi -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('master.location.province.list') }}">
                    <i class="menu-icon mdi mdi-google-maps"></i>
                    <span class="menu-title">MASTER LOKASI</span>
                </a>
            </li>

            <!-- NEWS -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('master.news.list') }}">
                    <i class="menu-icon mdi mdi-newspaper"></i>
                    <span class="menu-title">
                        MASTER BERITA
                    </span>
                </a>
            </li>

            <!-- ARTICLES -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('master.articles.list') }}">
                    <i class="menu-icon mdi mdi-rss-box"></i>
                    <span class="menu-title">
                        MASTER ARTIKEL
                    </span>
                </a>
            </li>



            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#socialMedia" aria-expanded="false"
                    aria-controls="socialMedia">
                    <i class="menu-icon mdi mdi-grid-large"></i>
                    <span class="menu-title">MEDIA SOSIAL</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="socialMedia">
                    <ul class="nav flex-column sub-menu">

                        <!-- Akun Sosial Media -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.icon-social-media.list') }}">
                                Item
                            </a>
                        </li>


                        <!-- Akun Sosial Media -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('master.social-media.list') }}">
                                Link (Akun)
                            </a>
                        </li>

                    </ul>
                </div>
            </li>



            <li class="nav-item nav-category">PENGATURAN</li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="collapse" href="#companyProfile" aria-expanded="false"
                    aria-controls="companyProfile">
                    <i class="menu-icon mdi mdi-hospital-building"></i>
                    <span class="menu-title">PERUSAHAAN</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="companyProfile">
                    <ul class="nav flex-column sub-menu">

                        <!-- Kontak Kami -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.contact_us.list') }}">
                                Kontak Kami
                            </a>
                        </li>

                        <!-- Banner -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.banner.list') }}">
                                Banner
                            </a>
                        </li>

                        <!-- Logo -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.logo.list') }}">
                                Logo
                            </a>
                        </li>

                        <!-- Tentang Kami -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.about_us.list') }}">
                                Tentang Kami
                            </a>
                        </li>

                        <!-- Aturan Pelatihan -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.training_rule.list') }}">
                                Aturan Pelatihan
                            </a>
                        </li>

                        <!-- Whatsapp -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('setting.wa-widget.list') }}">
                                Whatsapp Widget
                            </a>
                        </li>


                    </ul>
                </div>
            </li>
        @endif


        @if (auth()->user()->hasRole('peserta'))
            <li class="nav-item nav-category">PROGRAM</li>
            <!-- Kuis -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('major.modules.list') }}">
                    <i class="menu-icon mdi mdi-card-text-outline"></i>
                    <span class="menu-title">
                        MODUL
                    </span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('major.webinar.list') }}">
                    <i class="menu-icon mdi mdi-beats"></i>
                    <span class="menu-title">
                        KELAS
                    </span>
                </a>
            </li>
        @endif



        <li class="nav-item nav-category">BANTUAN</li>

        <!-- LANDING PAGE -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('web.home') }}">
                <i class="menu-icon mdi mdi-webhook"></i>
                <span class="menu-title">
                    LANDING PAGE
                </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="menu-icon mdi mdi-logout-variant"></i>
                <span class="menu-title">LOGOUT</span>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
