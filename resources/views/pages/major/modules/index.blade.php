@section('title')
    MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Module Pembelajaran, Learning Modules" name="keywords" />
    <meta content="LMS Stankom - Halaman Module Pembelajaran" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        const filterSelection = {
            category: '',
            sub_category: '',
            type: '',
        }

        var formState = {
            category: $("#selectionCategoryModule"),
            sub_category: $("#selectionSubCategoryModule"),
            type: $("#selectionTypeModule"),
        }

        function initialCategoryModule() {
            const url = "{!! route('master.module.category.all') !!}"
            initJQSelect2(formState.category, url, "Pilih Kategori Modul")
        }

        function initialSubCategoryModule() {
            const url = "{!! route('master.module.sub_category.all') !!}"
            initJQSelect2(formState.sub_category, url, "Pilih Sub Kategori Modul")
        }

        function initialTypeModule() {
            const url = "{!! route('master.module.type.all') !!}"
            initJQSelect2(formState.type, url, "Pilih Tipe Modul")
        }

        function echoToElement(data) {
            let str = '';
            data.forEach(item => {
                let url = "{!! url('major/modules/show') !!}/" + item.id
                str += `
                <div class="col-lg-4 col-md-6 mt-5">
                    <div class="d-flex flex-column overflow-hidden h-100 border border-primary">
                        <div class="d-inline-block bg-primary text-white fs-6 fw-bold mb-2">&nbsp;</div>

                        <div class="text-center px-4 mb-4" style="height: 80px;">
                            <a href="${url}" class="fs-6 text-dark fw-bold">${item.module_name}</a>
                        </div>

                        <div class="px-4 mb-4" style="height: 80px;">${item.strip_desc}</div>

                        <div class="px-4 mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <ol class="breadcrumb text-success justify-content-center  mb-2">
                                        <li class="breadcrumb-item small">
                                            ${item?.category_module?.name || 'Tidak Ada'}
                                        </li>
                                        <li class="breadcrumb-item small">
                                            ${item?.sub_category_module?.name || 'Tidak Ada'}
                                        </li>
                                        <li class="breadcrumb-item small">
                                            ${item?.type_module?.name || 'Tidak Ada'}
                                        </li>
                                    </ol>
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <a href="${url}" class="btn btn-primary">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `
            });

            return str;
        }

        function paginateElement(links) {
            let str = `
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
            `

            links.forEach(item => {
                let paramString = item.url != null ? item.url.split('?')[1] : null;
                if (item.url == null) {
                    str += `
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">${item.label}</a>
                        </li>
                    `
                } else {
                    const page = paramString.split('=')[1];
                    str +=
                        `<li class="page-item ${item.active ? 'active' : ''}"><button class="page-link" onclick="loadListContent(${page})">${item.label}</button></li>`
                }
            });
            str += `</ul></nav>`

            return str;

        }


        function loadListContent(page = 1) {
            let url = "{!! route('major.modules.paginate_module') !!}?page=" + page;
            let str = '';

            if (filterSelection.category != '') {
                str += `&category=${filterSelection.category}`
            }

            if (filterSelection.sub_category != '') {
                str += `&sub_category=${filterSelection.sub_category}`
            }

            if (filterSelection.type != '') {
                str += `&type=${filterSelection.type}`
            }

            if (str != '') {
                url += str;
            }

            axios.get(url, filterSelection).then(response => {
                const {
                    data
                } = response;
                let resultString = ''
                let paginateString = ''
                if (data.data.length > 0) {
                    resultString = echoToElement(data.data);
                }

                paginateString = paginateElement(data.links);

                $("#contentModel").empty();
                $("#contentModel").html(
                    `
                        <div class="row mb-4">${resultString}</div>
                        <div class="row mb-2">${paginateString}</div>
                    `
                )

            })
        }

        function resetContent() {
            filterSelection.category = '';
            filterSelection.sub_category = '';
            filterSelection.type = '';
            loadListContent();
        }

        $(document).ready(function() {
            initialCategoryModule();
            initialSubCategoryModule();
            initialTypeModule();
            loadListContent();

            formState.category.on('select2:select', function(e) {
                filterSelection.category = e.params.data.id
                loadListContent();
            });

            formState.sub_category.on('select2:select', function(e) {
                filterSelection.sub_category = e.params.data.id
                loadListContent();
            });

            formState.type.on('select2:select', function(e) {
                filterSelection.type = e.params.data.id
                loadListContent();
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row p-2 mb-2">
                        <div class="col-12">
                            <h4 class="card-title">MODUL PEMBELAJARAN</h4>
                        </div>

                        <div class="col-12">
                            <p>Data modul pembelajaran saya</p>
                        </div>
                    </div>

                    <div class="row p-2">
                        <div class="col-3">
                            <!-- CATEGORY MODUL -->
                            <div class="mb-1">
                                <label for="selectionCategoryModule" class="form-label fw-bold">
                                    Kategori Modul
                                </label>
                                <select id="selectionCategoryModule" class="form-control"
                                    data-placeholder="Kategori Modul"></select>
                            </div>
                        </div>

                        <div class="col-3">
                            <!-- SUB CATEGORY MODUL -->
                            <div class="mb-1">
                                <label for="selectionSubCategoryModule" class="form-label fw-bold">
                                    Sub Kategori Modul
                                </label>
                                <select id="selectionSubCategoryModule" class="form-control"
                                    data-placeholder="Sub Kategori Modul"></select>
                            </div>
                        </div>

                        <div class="col-3">
                            <!-- TIPE MODUL -->
                            <div class="mb-1">
                                <label for="selectionTypeModule" class="form-label fw-bold">Tipe Modul</label>
                                <select id="selectionTypeModule" class="form-control form-select"
                                    data-placeholder="Tipe Modul"></select>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="mb-1">
                                <label for="btnReset" class="form-label fw-bold">&nbsp;</label>
                                <div class="d-grid">
                                    <button type="button" class="btn btn-success" id="btnReset"
                                        onclick="resetContent()">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row p-2 mb-2">
                        <div id="contentModel" class="col-12"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
