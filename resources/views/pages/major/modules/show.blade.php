@section('title')
    MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Modul Pembelajaran" name="keywords" />
    <meta content="LMS Stankom - Halaman Module Pembelajaran" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const learningModule = @json($learningModule)

        function eventShowFile(id) {
            if (learningModule.type_module?.name.toLowerCase() == 'video') {
                const find = learningModule.file_modules.find(x => x.id == id);
                console.log("FIND", find);

                const toolbarEnabled = learningModule.can_download ? '#toolbar=1' : '#toolbar=0'
                let str = ''
                if (find && find.filepath != '') {
                    $("#embedViewer").empty();
                    str +=
                        `<iframe width="100%" height="500" src="${find.filepath}${toolbarEnabled}" title="${find.topic}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                    $("#embedViewer").html(str);
                }
            } else {
                const find = learningModule.file_modules.find(x => x.id == id);

                const toolbarEnabled = learningModule.can_download ? '#toolbar=1&navpanes=1' : '#toolbar=0&navpanes=1'
                let str = ''
                if (find && find.composite.extension == 'pdf') {
                    $("#embedViewer").empty();
                    str +=
                        `<iframe  src="${find.filepath}${toolbarEnabled}" width="100%" height="500" class="frameView"></iframe>`;

                    $("#embedViewer").html(str);
                }
            }


        }

        function getSupportTicket() {

            const url = "{!! route('major.modules.support_ticket') !!}";

            axios.post(url, {
                learning_module_id: learningModule.id
            }).then(response => {
                console.log(response);
                const {
                    data
                } = response;
                Swal.fire({
                    title: 'BERHASIL',
                    icon: 'success',
                    text: data.message,
                    confirmButtonText: 'OK',
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "{!! route('major.modules.show', $learningModule->id) !!}"
                    }
                });

            }).catch(error => {
                showErrorPopup(error)
            })
        }

        $(document).ready(function() {


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                MODUL PEMBELAJARAN
                            </h4>
                        </div>
                    </div>

                    @if ($user->userProfile && $user->userProfile->nik != null && !empty($user->userProfile->nik))
                        <div class="row p-3">
                            <div class="col-12">
                                <div class="d-flex justify-content-between">

                                    <div class="fst-italic">
                                        Selamat datang di modul pembelajaran <span class="fw-bold">
                                            {{ $learningModule->categoryModule->name }}
                                        </span>
                                    </div>

                                    @if (!$learnUser)
                                        <div class="fst-italic">
                                            Silahkan klik
                                            <button type="button" class="btn btn-sm btn-link fw-bold"
                                                onclick="getSupportTicket()">Daftar</button>
                                            untuk mendapatkan izin akses modul pembelajaran ini
                                        </div>
                                    @else
                                        {{-- <div class="fs-6">
                                            <input type="text" id="inputSupportTicket" class="form-control"
                                                value="{!! $learnUser->ticked_code !!}" readonly />
                                        </div> --}}
                                    @endif

                                </div>
                            </div>
                        </div>

                        @if ($learnUser)
                            <div class="row p-3">
                                <div class="col-12 mb-3">
                                    <h5 class="fs-5 fw-bold">{{ $learningModule->module_name }}</h5>
                                </div>

                                <div class="col-12">
                                    <div class="d-flex flex-row">
                                        <div class="form-floating mx-2">
                                            <input type="text" id="category_name"
                                                class="form-control text-primary fw-bold"
                                                value="{{ $learningModule->categoryModule->name }}" />
                                            <label for="category_name" class="form-label">Kategori Modul</label>
                                        </div>
                                        <div class="form-floating mx-2">
                                            <input type="text" id="sub_category_name"
                                                class="form-control text-primary fw-bold"
                                                value="{{ $learningModule->subCategoryModule->name }}" />
                                            <label for="category_name" class="form-label">Sub Kategori Modul</label>
                                        </div>
                                        <div class="form-floating mx-2">
                                            <input type="text" id="type_name"
                                                class="form-control text-primary fw-bold"
                                                value="{{ $learningModule->typeModule->name }}" />
                                            <label for="type_name" class="form-label">Tipe Modul</label>
                                        </div>

                                        <div class="form-floating mx-2">

                                            @if ($learningModule->can_download)
                                                <input class="form-control" value="TERSEDIA" readonly />
                                            @else
                                                <input class="form-control" value="TIDAK TERSEDIA" readonly />
                                            @endif
                                            <label for="type_name" class="form-label">Download</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if (count($learningModule->fileModules) > 0)
                                <div class="row p-3">
                                    <div class="col-12">
                                        <div class="d-flex flex-row mb-3">
                                            @foreach ($learningModule->fileModules as $item)
                                                <div class="p-2">
                                                    <div class="d-grid">
                                                        <button class="btn btn-primary text-right"
                                                            onclick="eventShowFile('{{ $item->id }}')">
                                                            <i class="fas fa-caret-right"></i> &nbsp;
                                                            {{ $item->topic }}
                                                        </button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div id="embedViewer" class="col-12"></div>
                                </div>
                            @endif


                        @endif
                    @else
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                Silahkan lengkapi profil (NIK, Alamat, Pendidikan dan Pekerjaan) anda terlebih dahulu.
                                &nbsp;

                                <a href="{{ route('major.modules.update_profile', $learningModule->id) }}"
                                    class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i> Lengkapi Profil
                                </a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
