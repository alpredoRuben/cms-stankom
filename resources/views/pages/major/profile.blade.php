@section('title')
    UPDATE PROFIL
@endsection

@section('metadata')
    <meta content="Update Profil" name="keywords" />
    <meta content="LMS Stankom - Halaman Update Profil" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection


@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var user = @json($user);

        var formEditState = {
            nik: $("#nik"),
            name: $("#full_name"),
            email: $("#email"),
            role_name: $("#role_name"),
            gender: $("#gender"),
            phone_number: $("#phone_number"),
            birthdate: $("#birthdate"),
            date_joined: $("#date_joined"),
            address: $("#address"),
            ward: $("#ward"),
            postal_code: $("#postal_code"),
            district: $("#district"),
            city: $("#city"),
            province: $("#province"),
            education: $("#education"),
            profession: $("#profession"),
            action: $("#btnUpdateAction")
        }

        function initWards() {
            const url = "{!! route('master.location.ward.all') !!}"
            formEditState.ward.select2({
                theme: "bootstrap-5",
                minimumInputLength: 2,
                placheloder: "Pilih Kelurahan",
                allowClear: false,
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function(params) {
                        let input = {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                        return input

                    },
                    processResults: function(data) {
                        return {
                            results: data.results,
                            pagination: data.pagination
                        };
                    },

                    cache: true
                },

            });
        }

        function initEducation(selected = null) {
            const url = "{!! route('master.education.all') !!}";
            axios.get(url).then(response => {
                $.each(response.data.results, function(index, value) {
                    formEditState.education
                        .append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.text));

                    if (selected != null && selected == value.id) {
                        formEditState.education.val(value.id).change();
                    }
                });
            }).catch(error => {
                console.log(error);
            });


            formEditState.education.select2({
                theme: "bootstrap-5",
                minimumInputLength: 2,
                placheloder: "Pilih Pendidikan",
                allowClear: false,
            });
        }

        function initProfession(selected = null) {
            const url = "{!! route('master.profession.all') !!}";
            axios.get(url).then(response => {
                $.each(response.data.results, function(index, value) {
                    formEditState.profession
                        .append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.text));

                    if (selected != null && selected == value.id) {
                        formEditState.profession.val(value.id).change();
                    }
                });
            }).catch(error => {
                console.log(error);
            });


            formEditState.profession.select2({
                theme: "bootstrap-5",
                minimumInputLength: 2,
                placheloder: "Pilih Profesi/Pekerjaan",
                allowClear: false,
            });
        }



        $(document).ready(function() {
            console.log(user?.user_profile?.education_id);
            initWards();
            initEducation(user?.user_profile?.education_id);
            initProfession(user?.user_profile?.job_profession_id)

            MCDatepicker.create({
                el: '#birthdate',
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            MCDatepicker.create({
                el: '#date_joined',
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            $("#infoWard").click(function(e) {
                e.preventDefault();
                $('#coverInfoWard').attr('hidden', true);
                $('#coverWard').attr('hidden', false);
            });

            formEditState.ward.on('select2:select', function(e) {
                // Do something
                const params = e.params.data;
                const url = "{{ url('master/location/ward/detail') }}/" + params.id
                axios.get(url).then(response => {
                    const {
                        status,
                        data: {
                            result
                        }
                    } = response

                    formEditState.city.val(result.city_name)
                    formEditState.district.val(result.district_name)
                    formEditState.province.val(result.province_name)
                    formEditState.postal_code.val(result.postal_code)

                }).catch(error => {
                    console.log(error)
                })
            });

            // formEditState.action.
            formEditState.action.click(function(e) {
                e.preventDefault();
                let wardId = user?.user_profile?.ward_id
                if (formEditState.ward.val() != null) {
                    wardId = formEditState.ward.val()
                }

                const data = {
                    nik: formEditState.nik.val(),
                    name: formEditState.name.val(),
                    email: formEditState.email.val(),
                    gender: formEditState.gender.val(),
                    birth_date: formEditState.birthdate.val(),
                    phone_number: formEditState.phone_number.val(),
                    address: formEditState.address.val(),
                    date_joined: formEditState.date_joined.val(),
                    ward_id: wardId,
                    education_id: formEditState.education.val(),
                    job_profession_id: formEditState.profession.val(),
                };

                const url = "{!! route('profile.update') !!}";
                axios.post(url, data).then(response => {
                    Swal.fire({
                        title: 'BERHASIL',
                        icon: 'success',
                        text: response.data.message,
                    });
                    window.location.href = nextDestination;
                }).catch(error => {
                    console.log(error)
                })
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                FORM UPDATE PROFIL
                            </h4>
                        </div>
                    </div>


                    <div class="row mb-3 justify-items-center">

                        <div class="col-12">
                            <div class="row">

                                <!-- NIK -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="nik" class="form-label">No. KTP (NIK)</label>
                                        <input id="nik" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->nik ?? '' }}" />
                                    </div>
                                </div>

                                <!-- Nama Lengkap -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="full_name" class="form-label">Nama Lengkap</label>
                                        <input id="full_name" class="form-control text-success" type="text"
                                            value="{{ $user->name }}" />
                                    </div>
                                </div>

                                <!-- Tanggal Lahir -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="birthdate" class="form-label">Tanggal Lahir</label>
                                        <input id="birthdate" class="form-control text-success" type="text"
                                            value="{{ \Illuminate\Support\Carbon::parse($user->userProfile->birth_date)->format('Y-m-d') ?? '' }}" />

                                    </div>
                                </div>

                                <!-- Gender -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="gender" class="form-label">Jenis Kelamin</label>
                                        <select id="gender" class="form-control form-select">
                                            <option value="pria"
                                                {{ $user->userProfile->gender == 'pria' ? 'selected' : '' }}>Pria
                                            </option>
                                            <option value="wanita"
                                                {{ $user->userProfile->gender == 'wanita' ? 'selected' : '' }}>Wanita
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <!-- Email -->
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="email" class="form-label">Email</label>
                                        <input id="email" class="form-control text-success" type="text"
                                            value="{{ $user->email }}" />
                                    </div>
                                </div>



                                <!-- No. Ponsel -->
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="phone_number" class="form-label">No. Ponsel / HP</label>
                                        <input id="phone_number" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->phone_number ?? '' }}" />

                                    </div>
                                </div>


                                <!-- Tanggal Bergabung -->
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="date_joined" class="form-label">Tanggal Bergabung</label>
                                        <input id="date_joined" class="form-control text-success" type="text"
                                            value="{{ \Illuminate\Support\Carbon::parse($user->userProfile->date_joined)->format('Y-m-d') ?? '' }}" />
                                    </div>
                                </div>

                                <!-- Alamat -->
                                <div class="col-12">
                                    <div class="mb-3">
                                        <label for="address" class="form-label">Alamat</label>
                                        <textarea id="address" class="form-control text-success" style="height: 85px;">{{ $user->userProfile->phone_number ?? '' }}</textarea>

                                    </div>
                                </div>


                                <!-- Kelurahan -->
                                <div class="col-5">
                                    <div class="mb-3">
                                        <label for="ward" class="form-label">Kelurahan</label>
                                        <div id="coverInfoWard">
                                            <input type="text" id="infoWard" class="form-control"
                                                value="{{ $user->userProfile->ward->name ?? '' }}" readonly />
                                            <span class="text-small">Klik textbox untuk ganti kelurahan</span>
                                        </div>
                                        <div id="coverWard" hidden="true">
                                            <select id="ward" class="form-control form-select"
                                                aria-placeholder="Pilih Kelurahan"></select>
                                        </div>

                                    </div>
                                </div>

                                <!-- Kode Pos -->
                                <div class="col-2">
                                    <div class="mb-3">
                                        <label for="postal_code" class="form-label">Kode Pos</label>
                                        <input id="postal_code" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->ward->postalCodes->code ?? '' }}"
                                            readonly />
                                    </div>
                                </div>



                                <!-- Kecamatan -->
                                <div class="col-5">
                                    <div class="mb-3">
                                        <label for="district" class="form-label">Kecamatan</label>
                                        <input id="district" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->ward->district->name ?? '' }}" readonly />
                                    </div>
                                </div>

                                <!-- Kabupaten -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="city" class="form-label">Kabupaten/Kota</label>
                                        <input id="city" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->ward->district->city->name ?? '' }}"
                                            readonly />
                                    </div>
                                </div>

                                <!-- Propinsi -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="province" class="form-label">Propinsi</label>
                                        <input id="province" class="form-control text-success" type="text"
                                            value="{{ $user->userProfile->ward->district->city->province->name ?? '' }}"
                                            readonly />
                                    </div>
                                </div>


                                <!-- Pendidikan -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="education" class="form-label">Pendidikan Terakhir</label>
                                        <select id="education" class="form-control form-select"></select>
                                    </div>
                                </div>

                                <!-- Pekerjaan -->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="profession" class="form-label">Pekerjaan</label>
                                        <select id="profession" class="form-control form-select"></select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button id="btnUpdateAction" type="button" class="btn btn-primary">
                                            <i class="fas fa-save"></i>
                                            Update
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('includes.modal_form_profile', [
        'user' => $user,
    ]);
</x-app-layout>
