<div class="row p-2 m-2 bg-light">
    <div class="col-lg-8">

        <div class="d-flex flex-column bd-highlight mb-3">
            <div class="p-2 bd-highlight">
                <h3 class="fs-3">{{ $webinar->class_name }}</h3>
            </div>
            <div class="p-2 bd-highlight">
                <p class="text-black ">
                    {!! $webinar->description !!}
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 mt-5">

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Tipe Kelas</h5>
            <p>{{ $webinar->typeClass->name }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Pengajar</h5>
            <p>{{ $webinar->trainer->name }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Tanggal Buka Kelas</h5>
            <p>{{ \Carbon\Carbon::parse($webinar->open_date)->format('d F Y') }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Tanggal Buka Tutup</h5>
            <p>{{ \Carbon\Carbon::parse($webinar->close_date)->format('d F Y') }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Jam Kelas</h5>
            <p>{{ $webinar->class_time }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Durasi</h5>
            <p>{{ $webinar->duration }} Menit</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center">
            <h5>Kuota</h5>
            <p>{{ $webinar->quota }} Orang</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Jumlah Peserta</h5>
            @if ($webinar->userWebinars()->count() > 0)
                <p>{{ $webinar->userWebinars()->count() }} Orang</p>
            @else
                <p>Kosong</p>
            @endif

        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Sertifikat</h5>
            <p>{{ $webinar->has_sertificated ? 'Bersertifikat' : 'Tidak Bersertifikat' }}</p>
        </div>

        <div class="course-info d-flex justify-content-between align-items-center mb-2">
            <h5>Kuis</h5>
            <p>{{ $webinar->has_quiz ? 'Tersedia' : 'Tidak Tersedia' }}</p>
        </div>

        <div class="course-info d-flex flex-column align-items-center mb-2">

            @if ($webinar->has_quiz)
                <div class="d-grid w-100 m-2">
                    <a href="#" class="btn btn-primary text-white rounded-pill">
                        Daftar Kuis
                    </a>
                </div>
            @endif


            @if ($webinar->has_sertificated)
                <div class="d-grid w-100 m-2">
                    <a href="#" class="btn btn-success text-white rounded-pill">
                        Lihat Sertifikat
                    </a>
                </div>
            @endif



        </div>

    </div>
</div>
