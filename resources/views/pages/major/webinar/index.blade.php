@section('title')
    KELAS
@endsection

@section('metadata')
    <meta content="Kelas Webinar" name="keywords" />
    <meta content="LMS Stankom - Halaman Kelas Webinar" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        const filterSelection = {
            type_class: ''
        }

        var formState = {
            type_class: $("#selectionClassType"),
        }

        function initialClassType() {
            const url = "{!! route('master.classes.type.all') !!}";
            initJQSelect2(formState.type_class, url, "Pilih Tipe Kelas")
        }

        function echoToElement(data) {
            let str = '';
            data.forEach(item => {
                let url = "{!! url('major/webinar/show') !!}/" + item.id
                str += `
                <div class="col-lg-4 col-md-6 mt-5">
                    <div class="d-flex flex-column overflow-hidden h-100 border border-primary">
                        <div class="d-inline-block bg-primary text-white fs-6 py-1 px-4 mb-4">
                            ${item.type_class.name}
                        </div>

                        <div class="text-center px-4 mb-4">
                            <a href="${url}" class="fs-6 text-dark fw-bold">${item.class_name}</a>
                        </div>


                        <div class="position-relative mt-auto">
                            <div class="position-relative mt-auto">
                                <img class="img-fluid" src="${item.filepath}" alt="" style="width:100% !important; height: 300px !important;">
                            </div>
                        </div>

                        <div class="px-4 mt-4 mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <ol class="breadcrumb text-success justify-content-center  mb-2">
                                        <li class="breadcrumb-item small">
                                            <i class="fas fa-balance-scale"></i>
                                            ${item.quota} orang
                                        </li>
                                        <li class="breadcrumb-item small">
                                            <i class="fas fa-clock"></i>
                                            ${item.duration} Menit
                                        </li>

                                        <li class="breadcrumb-item small">
                                            <i class="fas fa-certificate"></i>
                                            ${item.has_sertificated ? 'Bersertifikat' : 'Tidak Bersertifikat'}
                                        </li>
                                    </ol>
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <a href="${url}" class="btn btn-primary">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `
            });

            return str;
        }

        function paginateElement(links) {
            let str = `
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
            `

            links.forEach(item => {
                let paramString = item.url != null ? item.url.split('?')[1] : null;
                if (item.url == null) {
                    str += `
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">${item.label}</a>
                        </li>
                    `
                } else {
                    const page = paramString.split('=')[1];
                    str +=
                        `<li class="page-item ${item.active ? 'active' : ''}"><button class="page-link" onclick="loadListContent(${page})">${item.label}</button></li>`
                }
            });
            str += `</ul></nav>`

            return str;

        }


        function loadListContent(page = 1) {
            let url = "{!! route('major.webinar.paginate_webinar') !!}?page=" + page;
            let str = '';

            if (filterSelection.category != '') {
                str += `&category=${filterSelection.category}`
            }

            if (filterSelection.sub_category != '') {
                str += `&sub_category=${filterSelection.sub_category}`
            }

            if (filterSelection.type != '') {
                str += `&type=${filterSelection.type}`
            }

            if (str != '') {
                url += str;
            }

            axios.get(url, filterSelection).then(response => {
                const {
                    data
                } = response;
                let resultString = ''
                let paginateString = ''
                if (data.data.length > 0) {
                    resultString = echoToElement(data.data);
                }

                paginateString = paginateElement(data.links);

                $("#contentModel").empty();
                $("#contentModel").html(
                    `
                        <div class="row mb-4">${resultString}</div>
                        <div class="row mb-2">${paginateString}</div>
                    `
                )

            })
        }

        function resetContent() {
            filterSelection.type_class = '';
            loadListContent();
        }

        $(document).ready(function() {
            initialClassType();
            loadListContent();

            formState.type_class.on('select2:select', function(e) {
                filterSelection.type_class = e.params.data.id
                loadListContent();
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row p-2 mb-2">
                        <div class="col-12">
                            <h4 class="card-title">KELAS (WEBINAR)</h4>
                        </div>

                        <div class="col-12">
                            <p>Data kelas (webinar) saya</p>
                        </div>
                    </div>

                    <div class="row p-2">
                        <div class="col-3">

                            <div class="mb-1">
                                <label for="selectionClassType" class="form-label">Tipe Kelas (Webinar)</label>
                                <select id="selectionClassType" class="form-select"
                                    data-placeholder="Tipe Kelas (Webinar)"></select>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="mb-1">
                                <label for="btnReset" class="form-label fw-bold">&nbsp;</label>
                                <div class="d-grid">
                                    <button type="button" class="btn btn-success" id="btnReset"
                                        onclick="resetContent()">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row p-2 mb-2">
                        <div id="contentModel" class="col-12"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
