@section('title')
    MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Modul Pembelajaran" name="keywords" />
    <meta content="LMS Stankom - Halaman Module Pembelajaran" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const classWebinar = @json($webinar);

        function getSupportTicket() {
            Swal.fire({
                title: 'KONFIRMASI',
                icon: 'info',
                text: `Apakah anda yakin mengambil ${classWebinar.type_class.name} ini`,
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.isConfirmed) {
                    const url = "{!! route('major.webinar.support_ticket') !!}";

                    axios.post(url, {
                        webinar_id: classWebinar.id
                    }).then(response => {
                        console.log(response);
                        const {
                            data
                        } = response;
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('major.webinar.show', $webinar->id) !!}"
                            }
                        });

                    }).catch(error => {
                        showErrorPopup(error)
                    })
                }
            });
        }

        $(document).ready(function() {


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                KELAS
                            </h4>
                        </div>
                    </div>

                    @if ($user->userProfile && $user->userProfile->nik != null && !empty($user->userProfile->nik))
                        <div class="row p-3">
                            <div class="col-12">
                                <div class="d-flex justify-content-between">

                                    <div class="fst-italic">
                                        Selamat datang di <span class="fw-bold">
                                            {{ $webinar->typeClass->name }}
                                        </span>
                                    </div>

                                    @if (!$userWebinar)
                                        <div class="fst-italic">
                                            Silahkan klik
                                            <button type="button" class="btn btn-sm btn-link fw-bold"
                                                onclick="getSupportTicket()">Daftar</button>
                                            untuk mendapatkan izin akses modul pembelajaran ini
                                        </div>
                                    @else
                                        <div class="fs-6">
                                            Nomor Register
                                            <input type="text" id="inputSupportTicket" class="form-control"
                                                value="{!! $userWebinar->ticked_code !!}" readonly />
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>

                        @if ($userWebinar)
                            @include('pages.major.webinar.detail', compact('webinar'))
                        @endif
                    @else
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                Silahkan lengkapi profil (NIK, Alamat, Pendidikan dan Pekerjaan) anda terlebih dahulu.
                                &nbsp;

                                <a href="{{ route('major.webinar.update_profile', $webinar->id) }}"
                                    class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i> Lengkapi Profil
                                </a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
