@section('title')
    ASSESSOR
@endsection

@section('metadata')
    <meta content="Kategori, Modul, Assessor" name="keywords" />
    <meta content="LMS Stankom - Halaman Assessor" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.accessors') !!}",
            tableName: "tableAccessor",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "nik",
                    name: "nik",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "name",
                    name: "name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "email",
                    name: "email",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "phone_number",
                    name: "phone_number",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "lsp_name",
                    name: "lsp_name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "description",
                    name: "description",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },

                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }

            ]
        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }




        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        function eventImportData() {
            Swal.fire({
                title: "Upload File CSV",
                input: 'file',
                inputAttributes: {
                    'accept': '.csv',
                    'aria-label': 'Upload file csv anda'
                },
                showCancelButton: true,
                confirmButtonText: 'Upload',
            }).then((file) => {
                if (file.isConfirmed && file.value) {
                    var formData = new FormData();
                    formData.append('import_file', file.value);
                    axios.post("{!! route('file_manager.accessor.import.csv') !!}", formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).then(response => {
                        // console.log(response)
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: response.data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                eventRefreshData();
                            }
                        });
                    }).catch(error => {
                        console.log("ERROR", error.response)
                        showErrorPopup(error)
                    })

                }
            })
        }


        // RESTORE DATA
        function eventRestoreData(id) {
            const url = "{!! url('/master/accessor/restore') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan mengaktifkan kembali data ini ?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Aktifkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.get(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventPreviewHistory();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        // DELETE DATA
        function eventDeleteData(id) {
            const url = "{!! url('/master/accessor/delete') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin ingin menonaktifkan data ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Nonaktifkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventRefreshData();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }


        $(document).ready(function() {

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 3
                }

            });

            dTables.on("xhr", function(e, settings, json) {});

            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER DATA ASSESSOR</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => route('master.accessor.show.export'),
                        'urlDownload' => route('file_manager.accessor.download.csv'),
                        'addUrl' => route('master.accessor.create'),
                        'addTitle' => 'Assessor',
                        'addAction' => true,
                        'textHistory' => 'Riwayat Nonaktif',
                        'import' => true,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableAccessor" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">NIK</th>
                                            <th class="bg-success text-white">NAMA</th>
                                            <th>EMAIL</th>
                                            <th>NO. PONSEL</th>
                                            <th>NAMA LSP</th>
                                            <th>KETERANGAN</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
