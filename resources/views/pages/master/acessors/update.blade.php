@section('title')
    UBAH ASSESSOR
@endsection

@section('metadata')
    <meta content="Ubah Assesor, Update Accessor" name="keywords" />
    <meta content="LMS Stankom - Halaman Ubah Assesor" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var accessor = @json($accessor)

        function initLSP(selected) {
            const url = "{!! route('master.lsp.all') !!}"
            initJQSelect2Selection($("#selectionLSP"), url, "Pilih LSP", selected)
        }

        $(document).ready(function() {
            initLSP(accessor.lsp_id);

            $("#buttonUpdateAction").click(function(e) {
                e.preventDefault();
                var data = {
                    nik: $("#textNIK").val(),
                    name: $("#textName").val(),
                    email: $("#textEmail").val(),
                    phone_number: $("#textPhone").val(),
                    lsp_id: $("#selectionLSP").val(),
                    description: $("#textDesc").val(),
                };

                console.log("UPDATE DATA", data)

                const url = "{!! route('master.accessor.update', $accessor->id) !!}"

                axios.put(url, data).then(response => {
                    console.log(response);
                    const {
                        status,
                        data
                    } = response;

                    Swal.fire({
                        title: 'BERHASIL',
                        icon: 'success',
                        text: data.message,
                        confirmButtonText: 'OK',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = "{!! route('master.accessor.list') !!}";
                        }
                    });

                }).catch(error => {
                    console.log(error.response);
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('nik')) {
                                $('.invalid-nik').text(data.errors.nik[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('name')) {
                                $('.invalid-name').text(data.errors.name[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('email')) {
                                $('.invalid-email').text(data.errors.email[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('phone_number')) {
                                $('.invalid-phone_number').text(data.errors.phone_number[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }
                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-sm-12">
            <div class="home-tab">
                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                    <div>
                        <div class="btn-wrapper">
                            <a href="{{ route('master.accessor.list') }}" class="btn btn-primary text-white me-0">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                                &nbsp; Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-12">
                            <h4 class="card-title">
                                <i class="fas fa-plus-circle fa-sm"></i> &nbsp;
                                FORM UBAH ASSESSOR
                            </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">

                            <!-- NIK -->
                            <div class="mb-3">
                                <label for="textNIK" class="form-label">NIK</label>
                                <input id="textNIK" class="form-control" type="text"
                                    value="{{ $accessor->nik }}" />
                                <div class="text-danger text-sm fst-italic invalid-nik"></div>
                            </div>

                            <!-- FULL NAME -->
                            <div class="mb-3">
                                <label for="textName" class="form-label">
                                    Nama Lengkap
                                </label>
                                <input id="textName" class="form-control" type="text"
                                    value="{{ $accessor->name }}" />
                                <div class="text-danger text-sm fst-italic invalid-name"></div>
                            </div>

                            <!-- EMAIL -->
                            <div class="mb-3">
                                <label for="textEmail" class="form-label">Email</label>
                                <input id="textEmail" class="form-control" type="text"
                                    value="{{ $accessor->email }}" />
                                <div class="text-danger text-sm fst-italic invalid-email"></div>
                            </div>

                            <!-- NO. PONSEL -->
                            <div class="mb-3">
                                <label for="textPhone" class="form-label">No. Ponsel</label>
                                <input id="textPhone" class="form-control" type="text"
                                    value="{{ $accessor->phone_number }}" />
                                <div class="text-danger text-sm fst-italic invalid-phone_number"></div>
                            </div>

                            <!-- LSP -->
                            <div class="mb-3">
                                <label for="selectionLSP" class="form-label">Nama LSP</label>
                                <select id="selectionLSP" class="form-control form-select"></select>
                                <div class="text-danger text-sm fst-italic invalid-lsp"></div>
                            </div>

                            <!-- KETERANGAN-->
                            <div class=" mb-3">
                                <label for="textDesc" class="form-label">Keterangan</label>
                                <textarea id="textDesc" class="form-control form-textarea" style="min-height: 150px;">{{ $accessor->description }}</textarea>
                            </div>

                            <div class="mb-3">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-dark" id="buttonUpdateAction">
                                        <i class="fas fa-save"></i> &nbsp;
                                        UPDATE
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
