@section('title')
    TAMBAH ARTIKEL
@endsection

@section('metadata')
    <meta content="Tambah Artikel" name="keywords" />
    <meta content="LMS Stankom - Halaman Tambah Artikel" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg=="
        crossorigin="anonymous" />
    <style type="text/css">
        .bootstrap-tagsinput {
            width: 100%;
        }

        .label-info {
            background-color: #17a2b8;

        }

        .label {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
            transition: color .15s ease-in-out, background-color .15s ease-in-out,
                border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }
    </style>
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"
        integrity="sha512-VvWznBcyBJK71YKEKDMpZ0pCVxjNuKwApp4zLF3ul+CiflQi6aIJR+aZCP/qWsoFBA28avL5T5HA+RE+zrGQYg=="
        crossorigin="anonymous"></script>
    {{-- <script src="{{ asset('ckeditor/ckeditor.js') }}"></script> --}}
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var formArticleState = {
            title: "textArticleTitle",
            file: "textUploadFile",
            tags: "textTag",
            content: "contentArticle",
            actionUploadFile: "buttonUploadFile",
            action: "buttonSaveAction",
        }
        var fileTemporary = null;

        function refreshErrorTagMessage() {
            $('.invalid-article_title').text("")
            $('.invalid-article_tags').text("")
            $('.invalid-article_content').text("")
        }

        $(document).ready(function() {
            refreshErrorTagMessage();

            CKEDITOR.replace(`${formArticleState.content}`, {
                extraPlugins: 'editorplaceholder,colorbutton,colordialog,justify',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Isi artikel di sini...',
                removeButtons: 'PasteFromWord'
            });

            $(`#${formArticleState.actionUploadFile}`).click(function(e) {
                e.preventDefault();
                $(`#${formArticleState.file}`).trigger('click');
            });

            /** Event On Choose File */
            $(`#${formArticleState.file}`).change(function(e) {
                fileTemporary = e.target.files[0];
                $("#infoFile").val(fileTemporary.name)
            });

            $(`#${formArticleState.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                var formData = new FormData();
                formData.append("article_title", $(`#${formArticleState.title}`).val());
                formData.append("article_content", CKEDITOR.instances?.[`${formArticleState.content}`]
                    .getData());

                if (fileTemporary != null) {
                    formData.append("article_photo", fileTemporary);
                }

                formData.append("article_tags", $(`#${formArticleState.tags}`).val());

                const url = "{!! route('master.articles.store') !!}"

                axios.post(url, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    if (response.status == 201 || response.status == 200) {
                        const {
                            data
                        } = response;
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('master.articles.list') !!}";
                            }
                        });
                    }


                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('article_title')) {
                                $('.invalid-article_title').text(data.errors
                                        .article_title[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('article_content')) {
                                $('.invalid-article_content').text(data.errors
                                        .article_content[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('article_tags')) {
                                $('.invalid-article_tags').text(data.errors.article_tags[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });


            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM TAMBAH ARTIKEL
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('master.articles.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <!-- NAMA KUIS -->
                            <div class="mb-3">
                                <label for="textArticleTitle" class="form-label">Judul Artikel</label>
                                <input id="textArticleTitle" class="form-control " type="text"
                                    placeholder="Judul Artikel" />
                                <div class="text-danger text-sm fst-italic invalid-article_title"></div>
                            </div>

                            <!-- KELAS WEBINAR -->
                            <div class="mb-3">
                                <label for="textTag" class="form-label">Tag</label>
                                <div class="input-group">
                                    <input type="text" data-role="tagsinput" id="textTag" name="tags"
                                        class="form-control">
                                    <div class="text-danger text-sm fst-italic invalid-article_tags"></div>
                                </div>
                            </div>

                            <!-- KELAS WEBINAR -->
                            <div class="mb-3">
                                <label for="textUploadFile" class="form-label">Upload File</label>
                                <div class="input-group">
                                    <input class="form-control" type="file" name="textUploadFile" id="textUploadFile"
                                        accept=".jpg,.png,.jpeg" hidden />
                                    <button type="button" class="btn btn-primary" id="buttonUploadFile">
                                        <i class="fas fa-upload"></i> &nbsp;
                                        Upload Gambar
                                    </button>

                                    <input type="text" id="infoFile" class="form-control form-control-lg"
                                        readonly />
                                </div>
                            </div>

                            <!-- MODULE PEMBELAJARAN -->
                            <div class="mb-3">
                                <label for="contentArticle" class="form-label">Isi Artikel</label>
                                <textarea cols="80" id="contentArticle" name="contentArticle" class="form-control" rows="10"
                                    data-sample-short></textarea>
                                <div class="text-danger text-sm fst-italic invalid-article_content"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-primary" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
