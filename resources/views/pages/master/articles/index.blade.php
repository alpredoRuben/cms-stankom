@section('title')
    ARTIKEL
@endsection

@section('metadata')
    <meta content="Artikel, article" name="keywords" />
    <meta content="LMS Stankom - Halaman Artikel" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>s
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.articles') !!}",
            tableName: "tableArticles",
            filters: {
                search: '',
                tags: '',
                start_date: '',
                end_date: ''
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "article_title",
                    name: "article_title",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "article_date",
                    name: "article_date",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "tags",
                    name: "tags",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "article_content",
                    name: "article_content",
                    orderable: false,
                    className: "align-middle text-wrap",
                },
                {
                    data: "headlines",
                    name: "headlines",
                    orderable: false,
                    className: "align-middle text-center",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }

            ]
        }

        var stateFilter = {
            tag: $("#selectionTags"),
            startDate: "startDate",
            endDate: "endDate",
            search: $("#textSearch")
        }

        function initialTags() {
            const url = "{!! route('master.articles.tags') !!}"
            initJQSelect2(stateFilter.tag, url, "Pilih Tag")
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.tags = '';
            tableState.filters.start_date = '';
            tableState.filters.end_date = '';
            reloadDatatable();
        }

        function eventFilterData() {
            tableState.filters.start_date = $(`#${stateFilter.startDate}`).val();
            tableState.filters.end_date = $(`#${stateFilter.endDate}`).val();
            tableState.filters.search = stateFilter.search.val();
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.articles.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        function setHeadline(id) {
            const url = "{!! url('/master/articles/headline') !!}/" + id
            axios.get(url).then(response => {
                Swal.fire({
                    title: 'BERHASIL',
                    text: response.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                }).then((r) => {
                    if (r.isConfirmed) {
                        reloadDatatable();
                    }
                });
            })
        }

        function disableHeadline(id) {
            const url = "{!! url('/master/articles/disabled-headline') !!}/" + id
            axios.get(url).then(response => {
                Swal.fire({
                    title: 'BERHASIL',
                    text: response.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                }).then((r) => {
                    if (r.isConfirmed) {
                        reloadDatatable();
                    }
                });
            })
        }

        $(document).ready(function() {
            initialTags();

            MCDatepicker.create({
                el: `#${stateFilter.startDate}`,
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            MCDatepicker.create({
                el: `#${stateFilter.endDate}`,
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });


            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.search = tableState.filters.search
                        d.tags = tableState.filters.tags
                        d.start_date = tableState.filters.start_date
                        d.end_date = tableState.filters.end_date
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }
            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
            });

            stateFilter.tag.change(function(e) {
                e.preventDefault();
                tableState.filters.tags = $(this).val();
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER ARTIKEL</h4>
                        </div>
                    </div>

                    @include('pages.master.articles.articles_filter')


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableArticles" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">JUDUL ARTIKEL</th>
                                            <th>TANGGAL ARTIKEL</th>
                                            <th>TAG</th>
                                            <th>STATUS</th>
                                            <th>ISI ARTIKEL</th>
                                            <th>HEADLINE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
