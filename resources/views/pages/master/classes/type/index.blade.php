@section('title')
    TIPE KELAS
@endsection

@section('metadata')
    <meta content="Tipe Kelas, Kelas" name="keywords" />
    <meta content="LMS Stankom - Halaman Tipe Kelas" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.type.classes') !!}",
            tableName: "tableTypeClasses",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "name",
                    name: "name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "description",
                    name: "description",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }

            ]
        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        var formModuleState = {
            modal: 'modalFormDefault',
            title: 'modalTitleDefault',
            alert: 'modalAlertDefault',
            name: 'modalInputDefaultName',
            description: 'modalTexareaDefaultDescription',
            action: 'modalButtonDefault'
        };


        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        // CREATE
        function eventCreateData(define = 'show') {
            $(`#${formModuleState.title}`).text("FORM TAMBAH TIPE MODUL");
            $(`#${formModuleState.action}`).text("SIMPAN");
            $(`#${formModuleState.alert}`).text("");
            $(`#${formModuleState.action}`).attr("data-id", "");
            $(`#${formModuleState.name}`).val("");
            $(`#${formModuleState.description}`).val("");
            $(`#${formModuleState.modal}`).modal(define);
        }


        // EDIT DATA
        function eventEditData(id) {
            const url = "{!! url('master/classes/type/find') !!}/" + id
            axios.get(url).then(response => {
                const {
                    data
                } = response
                $(`#${formModuleState.title}`).text("FORM UBAH TIPE MODUL");
                $(`#${formModuleState.action}`).text("UPDATE");
                $(`#${formModuleState.alert}`).text("");

                $(`#${formModuleState.action}`).attr("data-id", id);

                $(`#${formModuleState.name}`).val(data?.result?.name);
                $(`#${formModuleState.description}`).val(data?.result?.description);
                $(`#${formModuleState.modal}`).modal("show");
            }).catch(err => {
                showErrorPopup(err);
            })
        }

        // RESTORE DATA
        function eventRestoreData(id) {
            const url = "{!! url('/master/classes/type/restore') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan memulihkan kembali data ini ?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Pulihkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.get(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventPreviewHistory();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        // DELETE DATA
        function eventDeleteData(id) {
            const url = "{!! url('/master/classes/type/delete') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan menghapus data ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventRefreshData();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        // CHANGE STATUS
        function eventChangeStatus(id, status) {
            const url = "{!! route('master.classes.type.change_status') !!}";
            const msgAlert = status ? "menonaktifkan tipe kelas ini" : "mengaktifkan tipe kelas ini"


            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        $(document).ready(function() {
            console.log(formModuleState);

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
            });

            dTables.on("xhr", function(e, settings, json) {});

            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });


            $(`#${formModuleState.action}`).click(function(e) {
                e.preventDefault();
                const id = $(this).attr('data-id');
                const data = {
                    name: $(`#${formModuleState.name}`).val(),
                    description: $(`#${formModuleState.description}`).val()
                }

                if (id != '') {
                    // Update
                    const url = "{!! url('master/classes/type/update') !!}/" + id;

                    axios.put(url, data).then(response => {
                        console.log("RESPONSE", response);
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }


                    }).catch(error => {
                        const errorMessage = explorePopupError(error);

                        $(`#${formModuleState.alert}`).append(`
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="text-dark">
                                    <i>${errorMessage}</i>
                                </span>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        `);
                    });

                } else {
                    // Store
                    const url = "{!! route('master.classes.type.store') !!}"
                    axios.post(url, data).then(response => {
                        console.log("RESPONSE", response);
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }


                    }).catch(error => {
                        const errorMessage = explorePopupError(error);

                        $(`#${formModuleState.alert}`).append(`
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="text-dark">
                                    <i>${errorMessage}</i>
                                </span>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        `);
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER DATA TIPE KELAS</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => null,
                        'urlDownload' => null,
                        'addUrl' => null,
                        'addTitle' => 'Tipe Kelas',
                        'addAction' => true,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableTypeClasses" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>NO</th>
                                            <th>TIPE KELAS</th>
                                            <th>KETERANGAN</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL FORM CRUD -->
    @include('includes.modal_form_crud', [
        'labelDefaultName' => 'Tipe Kelas',
        'labelDefaultDescription' => 'Keterangan',
    ]);

    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
