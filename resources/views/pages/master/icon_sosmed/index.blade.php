@section('title')
    ITEM MEDIA SOSIAL
@endsection

@section('metadata')
    <meta content="Icon Media Sosial" name="keywords" />
    <meta content="LMS Stankom - Halaman Icon Media Sosial" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var filterSelection = {
            search: '',
            status: '1'
        }

        var formModal = {
            medsos_name: "modalSosmedName",
            medsos_icon: "modalIconSosmed",
            title: "modalTitleMedsos",
            action: "modalButtonAction",
            modal: "modalFormMedsos"
        }

        var formFilter = {
            search: $("#textSearch"),
            status: $("#selectStatus")

        };

        function refreshErrorTagMessage() {
            $('.invalid-medsos_name').text("")
            $('.invalid-medsos_icon').text("")
        }

        function fetchSosmed(page = 1) {
            let url = "{!! route('master.icon-social-media.list') !!}?page=" + page;
            let str = '';
            if (filterSelection.search != '') {
                str += `&search=${filterSelection.search}`
            }

            str += `&status=${filterSelection.status}`


            if (str != '') {
                url += str;
            }


            $.ajax({
                url: url,
                method: 'get',
                success: function(data) {
                    $('#coverSosmed').html(data);
                }
            });
        }

        function eventSearch() {
            filterSelection.search = formFilter.search.val();
            fetchSosmed();
        }

        function eventChangeSelection(e) {
            filterSelection.status = e.value;
            fetchSosmed();
        }

        function eventReset() {
            formFilter.search.val("")
            formFilter.status.val("1")
            filterSelection = {
                search: '',
                status: '1'
            }

            fetchSosmed();
        }

        function eventNonActive(id) {
            const url = "{!! route('master.icon-social-media.change_status') !!}";
            Swal.fire({
                title: 'PERINGATAN',
                text: 'Apakah anda yakin ingin menonaktifkan data ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Non Aktifkan'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventReset();
                        });
                    }).catch((error) => {});
                }
            });
        }


        function eventActive(id) {
            const url = "{!! route('master.icon-social-media.change_status') !!}";
            const msgAlert = "mengaktifkan data ini"
            Swal.fire({
                title: 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Aktifkan'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventReset();
                        });
                    }).catch((error) => {});
                }
            });
        }


        function eventCreate() {
            refreshErrorTagMessage()
            $(`#${formModal.medsos_name}`).val("")
            $(`#${formModal.medsos_icon}`).val("");
            $(`#${formModal.title}`).text(`FORM TAMBAH ITEM MEDIA SOSIAL`);
            $(`#${formModal.action}`).text("SIMPAN");
            $(`#${formModal.action}`).attr("data-id", "");
            $(`#${formModal.modal}`).modal("show");
        }

        function eventEdit(id) {
            refreshErrorTagMessage()
            const url = "{!! url('master/icon-social-media/show') !!}/" + id
            axios.get(url).then(response => {
                const {
                    data
                } = response;

                $(`#${formModal.medsos_name}`).val(data.result.medsos_name)
                $(`#${formModal.medsos_icon}`).val(data.result.medsos_icon);
                $(`#${formModal.title}`).text(`FORM UBAH ITEM MEDIA SOSIAL`);
                $(`#${formModal.action}`).text("UPDATE");
                $(`#${formModal.action}`).attr("data-id", id);
                $(`#${formModal.modal}`).modal("show");
            }).catch(error => {
                console.log(error);
            })
        }

        $(document).ready(function() {

            $(`#${formModal.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const id = $(this).attr('data-id');
                const data = {
                    medsos_name: $(`#${formModal.medsos_name}`).val(),
                    medsos_icon: $(`#${formModal.medsos_icon}`).val()
                }

                if (id != '') {
                    const url = "{!! url('master/icon-social-media/update') !!}/" + id;
                    axios.put(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    eventReset()
                                    $(`#${formModal.modal}`).modal("hide");
                                }
                            });
                        }

                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('medsos_name')) {
                                    $('.invalid-medsos_name').text(data.errors
                                            .medsos_name[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('medsos_icon')) {
                                    $('.invalid-medsos_icon').text(data.errors
                                            .medsos_icon[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('master.icon-social-media.store') !!}"
                    axios.post(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    eventReset()
                                    $(`#${formModal.modal}`).modal("hide");
                                }
                            });
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('medsos_name')) {
                                    $('.invalid-medsos_name').text(data.errors
                                            .medsos_name[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('medsos_icon')) {
                                    $('.invalid-medsos_icon').text(data.errors
                                            .medsos_icon[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER ITEM MEDIA SOSIAL</h4>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="selectStatus" class="form-label fw-bold">Status</label>
                                <select id="selectStatus" class="form-select form-control form-control-lg"
                                    onchange="eventChangeSelection(this)">
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row">
                                <div class="col-8">
                                    <div class="mb-3">
                                        <label for="textSearch" class="form-label fw-bold">Pencarian</label>
                                        <input id="textSearch" placeholder="Cari Kata Kunci..."
                                            class="form-control form-control-lg" type="text" />
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="d-grid mb-3" style="margin-top: 2rem;">
                                        <button type="button" type="button" class="btn btn-warning"
                                            onclick="eventSearch()">
                                            <i class="fas fa-search"></i>
                                            Cari
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-grid" style="margin-top: 2rem;">
                                        <button type="button" class="btn btn-info " onclick="eventReset()">
                                            <i class="fas fa-refresh"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="d-grid" style="margin-top: 2rem;">
                                        <button type="button" class="btn btn-primary " onclick="eventCreate()">
                                            <i class="fas fa-plus"></i>
                                            Media Sosial
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="coverSosmed" class="row mb-2 p-3">
                        @include('pages.master.icon_sosmed.sosmed_paginate', compact('sosmed'))
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.master.icon_sosmed.modal_sosmed')

</x-app-layout>
