<div id="modalFormMedsos" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleMedsos" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p class="fst-italic">
                            Icon media sosial dapat diperoleh dari link berikut.
                            <a href="https://fontawesome.com/v5/search" class="btn btn-info btn-sm" target="_blank">Klik
                                Icon</a>
                        </p>
                    </div>
                    <div class="col-12">
                        <form>
                            <div class="mb-3">
                                <label for="modalSosmedName" class="form-label">Nama Media Sosial</label>
                                <input type="text" id="modalSosmedName" class="form-control form-control-lg"
                                    placeholder="Contoh : Instagram">
                                <div class="text-danger text-sm fst-italic invalid-medsos_name"></div>
                            </div>

                            <div class="mb-3">
                                <label for="modalIconSosmed" class="form-label">Icon Media Sosial</label>
                                <input type="text" id="modalIconSosmed" class="form-control form-control-lg"
                                    placeholder="Contoh : <i class='fab fa-instagram'></i>">
                                <div class="text-danger text-sm fst-italic invalid-medsos_icon"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonAction" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
