@if ($sosmed && count($sosmed) > 0)
    @foreach ($sosmed as $item)
        <div class="col-3">
            <div class="card border border-dark shadow mb-3">
                <div class="d-flex flex-row justify-content-between">
                    <div class="py-2 ps-2">
                        @if ($item->medsos_icon)
                            {!! $item->medsos_icon !!}
                        @endif

                    </div>
                    <div class="py-3 ps-2">
                        <span class="fs-4 fw-bold">
                            {{ $item->medsos_name }}
                        </span>
                    </div>

                    <div class="py-3 px-2">
                        @if ($item->status)
                            <button type="button" class="btn btn-danger btn-sm"
                                onclick="eventNonActive({!! $item->id !!})">
                                <i class="fas fa-times"></i>
                            </button>
                        @else
                            <button type="button" class="btn btn-success btn-sm"
                                onclick="eventActive({!! $item->id !!})">
                                <i class="fas fa-check"></i>
                            </button>
                        @endif
                        <button type="button" class="btn btn-dark btn-sm" onclick="eventEdit({!! $item->id !!})">
                            <i class="fas fa-edit"></i>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
            @if ($sosmed->onFirstPage())
                <li class="page-item disabled">
                    <button class="page-link" tabindex="-1" aria-disabled="true">Previous</button>
                </li>
            @else
                <li class="page-item">
                    <button class="page-link"
                        onclick="fetchSosmed('{{ $sosmed->currentPage() - 1 }}')">Previous</button>
                </li>
            @endif

            @if ($sosmed->hasMorePages())
                <li class="page-item">
                    <button class="page-link" onclick="fetchSosmed('{{ $sosmed->currentPage() + 1 }}')">Next</button>
                </li>
            @else
                <li class="page-item disabled">
                    <button class="page-link" tabindex="-1" aria-disabled="true">Next</button>
                </li>
            @endif

        </ul>
    </nav>

@endif
