@section('title')
    MASTER KECAMATAN
@endsection

@section('metadata')
    <meta content="Master Kecamatan" name="keywords" />
    <meta content="LMS Stankom - Halaman Master Kecamatan" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('master.datatables.districts') !!}",
            tableName: "tableDistricts",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    className: "text-center align-middle"
                },
                {
                    data: "name",
                    name: "name",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "city_name",
                    name: "city_name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }

            ]
        }

        var formDistrictState = {
            modal: "modalForm",
            title: "modalFormTitle",
            alert: "modalFormAlert",
            selection: "modalFormSelection",
            textInput: "modalFormTextInput",
            action: "modalFormButtonSave"
        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        function refreshErrorTagMessage() {
            $('.invalid-name').text("")
            $('.invalid-city').text("")
        }

        function initialCity(selected = null) {
            const url = "{!! route('master.location.city.all') !!}"
            if (selected == null) {
                initSelect2InModal($(`#${formDistrictState.selection}`), url, "Pilih Kabupaten/Kota", $(
                    `#${formDistrictState.modal}`))
                $(`#${formDistrictState.selection}`).select2("val", "");
            } else {
                initSelect2InModalWithSelection(
                    $(`#${formDistrictState.selection}`),
                    $(`#${formDistrictState.modal}`),
                    url,
                    "Pilih Kabupaten/Kota",
                    selected
                );
            }
        }


        // CREATE
        function eventCreateData(define = 'show') {
            $(`#${formDistrictState.title}`).text("FORM TAMBAH KECAMATAN");
            $(`#${formDistrictState.action}`).text("SIMPAN");
            $(`#${formDistrictState.action}`).attr("data-id", "");
            $(`#${formDistrictState.alert}`).text("");
            $(`#${formDistrictState.textInput}`).val("");
            initialCity();
            $(`#${formDistrictState.modal}`).modal(define);
        }

        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        // EDIT DATA
        function eventEditData(id) {
            const url = "{!! url('master/location/district/find') !!}/" + id
            axios.get(url).then(response => {
                const {
                    data,
                    status
                } = response;
                console.log(data);
                $(`#${formDistrictState.title}`).text("FORM EDIT KECAMATAN");
                $(`#${formDistrictState.action}`).text("UPDATE");
                $(`#${formDistrictState.action}`).attr("data-id", id);

                $(`#${formDistrictState.alert}`).text("");
                $(`#${formDistrictState.textInput}`).val(data?.result?.name);
                initialCity(data?.result?.city);
                $(`#${formDistrictState.modal}`).modal("show");
            }).catch(err => {
                showErrorPopup(err);
            })
        }

        // DELETE DATA
        function eventDeleteData(id) {
            const url = "{!! url('master/location/district/delete') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan menghapus data ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventRefreshData();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        // RESTORE DATA
        function eventRestoreData(id) {
            const url = "{!! url('/master/location/district/restore') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan memulihkan kembali data ini ?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Pulihkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.get(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventPreviewHistory();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        $(document).ready(function() {
            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: false,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
            });

            dTables.on("xhr", function(e, settings, json) {
                // console.log(json);
            });


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

            $(`#${formDistrictState.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();
                const id = $(this).attr('data-id');
                let cityId = $(`#${formDistrictState.selection}`).val();

                if (cityId == null || cityId == '') {
                    cityId = 'same';
                }

                const data = {
                    city: cityId,
                    name: $(`#${formDistrictState.textInput}`).val(),
                }

                if (id != '') {
                    // Update
                    const url = "{!! url('master/location/district/update') !!}/" + id;
                    axios.put(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('city')) {
                                    $('.invalid-city').text(data.errors
                                            .city[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('name')) {
                                    $('.invalid-name').text(data.errors.name[0])
                                        .css('font-size', '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('master.location.district.store') !!}"
                    axios.post(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }

                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('city')) {
                                    $('.invalid-city').text(data.errors
                                            .city[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('name')) {
                                    $('.invalid-name').text(data.errors.name[0])
                                        .css('font-size', '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <x-banner-page>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                <h4 class="card-title">MASTER DATA KECAMATAN</h4>
                            </div>
                        </div>

                        @include('includes.nav_button', [
                            'exportUrl' => null,
                            'addUrl' => null,
                            'addTitle' => 'Kecamatan',
                            'addAction' => true,
                        ])


                        <div class="row mb-2 p-3">
                            <div class="col">
                                <div class="table-responsive">
                                    <table id="tableDistricts" class="table table-striped table-hover" width="100%">
                                        <thead>
                                            <tr class="bg-primary text-white">
                                                <th>NO</th>
                                                <th>KECAMATAN</th>
                                                <th>KABUPATEN/KOTA</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-banner-page>
    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])

    @include('pages.master.locations.district.district_modal')
</x-app-layout>
