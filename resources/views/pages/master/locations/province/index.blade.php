@section('title')
    MASTER PROPINSI
@endsection

@section('metadata')
    <meta content="Master Propinsi" name="keywords" />
    <meta content="LMS Stankom - Halaman Master Propinsi" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('master.datatables.provinces') !!}",
            tableName: "tableProvinces",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    className: "text-center align-middle"
                },
                {
                    data: "name",
                    name: "name",
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }

            ]
        }

        var formProvinceState = {
            modal: "modalForm",
            title: "modalFormTitle",
            alert: "modalFormAlert",
            textInput: "modalFormTextInput",
            action: "modalFormButtonSave"
        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        function refreshErrorTagMessage() {
            $('.invalid-name').text("")
        }

        // CREATE
        function eventCreateData(define = 'show') {
            $(`#${formProvinceState.title}`).text("FORM TAMBAH PROPINSI");
            $(`#${formProvinceState.action}`).text("SIMPAN");
            $(`#${formProvinceState.action}`).attr("data-id", "");

            $(`#${formProvinceState.alert}`).text("");
            $(`#${formProvinceState.textInput}`).val("");
            $(`#${formProvinceState.modal}`).modal(define);
        }

        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        // EDIT DATA
        function eventEditData(id) {
            const url = "{!! url('master/location/province/find') !!}/" + id
            console.log("URL", url)
            axios.get(url).then(response => {
                const {
                    data,
                    status
                } = response;
                $(`#${formProvinceState.title}`).text("FORM EDIT PROPINSI");
                $(`#${formProvinceState.action}`).text("UPDATE");
                $(`#${formProvinceState.action}`).attr("data-id", id);

                $(`#${formProvinceState.alert}`).text("");
                $(`#${formProvinceState.textInput}`).val(data?.result?.name);
                $(`#${formProvinceState.modal}`).modal("show");
            }).catch(err => {
                showErrorPopup(err);
            })
        }

        // DELETE DATA
        function eventDeleteData(id) {
            const url = "{!! url('master/location/province/delete') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan menghapus data ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventRefreshData();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        // RESTORE DATA
        function eventRestoreData(id) {
            const url = "{!! url('/master/location/province/restore') !!}/" + id
            Swal.fire({
                title: 'PERINGATAN!',
                text: "Apakah anda yakin akan memulihkan kembali data ini ?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Pulihkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.get(url).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            if (r.isConfirmed) {
                                eventPreviewHistory();
                            }
                        });
                    }).catch((error) => {
                        showErrorPopup(error);
                    });
                }
            });
        }

        $(document).ready(function() {
            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: false,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
            });

            dTables.on("xhr", function(e, settings, json) {
                // console.log(json);
            });


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

            $(`#${formProvinceState.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const id = $(this).attr('data-id');
                const data = {
                    name: $(`#${formProvinceState.textInput}`).val(),
                }

                if (id != '') {
                    // Update
                    const url = "{!! url('master/location/province/update') !!}/" + id;

                    axios.put(url, data).then(response => {
                        console.log("RESPONSE", response);
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:

                                if (data.errors.hasOwnProperty('name')) {
                                    $('.invalid-name').text(data.errors.name[0])
                                        .css('font-size', '0.9rem');
                                }

                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('master.location.province.store') !!}"
                    axios.post(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateData('hide')
                        }

                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:

                                if (data.errors.hasOwnProperty('name')) {
                                    $('.invalid-name').text(data.errors.name[0])
                                        .css('font-size', '0.9rem');
                                }

                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <x-banner-page>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                <h4 class="card-title">MASTER DATA PROPINSI</h4>
                            </div>
                        </div>

                        @include('includes.nav_button', [
                            'exportUrl' => null,
                            'addUrl' => null,
                            'addTitle' => 'Propinsi',
                            'addAction' => true,
                        ])


                        <div class="row mb-2 p-3">
                            <div class="col">
                                <div class="table-responsive">
                                    <table id="tableProvinces" class="table table-striped table-hover" width="100%">
                                        <thead>
                                            <tr class="bg-primary text-white">
                                                <th>NO</th>
                                                <th>NAMA PROPINSI</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-banner-page>
    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])

    @include('pages.master.locations.province.province_modal')
</x-app-layout>
