<div id="modalForm" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalFormTitle" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="modalFormAlert" class="col-12"></div>
                    <div class="col-12">
                        <form>
                            <div class="mb-3">
                                <label for="modalFormTextInput" value="Nama Propinsi" class="form-label">
                                    Nama Propinsi
                                </label>
                                <input type="text" id="modalFormTextInput" class="form-control  form-control-lg" />
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalFormButtonSave" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
