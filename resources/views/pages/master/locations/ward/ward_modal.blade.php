<div id="modalForm" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalFormTitle" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">

                        <div class="mb-3">
                            <label for="modalFormSelection" class="form-label">
                                Nama Kecamatan
                            </label>
                            <select id="modalFormSelection" class="form-control"
                                data-placeholder="Pilih Kecamatan"></select>
                            <div class="text-danger text-sm fst-italic invalid-district"></div>
                        </div>


                        <div class="mb-3">
                            <label for="modalFormTextInput" class="form-label">
                                Nama Kelurahan
                            </label>
                            <input type="text" id="modalFormTextInput" class="form-control  form-control-lg" />
                            <div class="text-danger text-sm fst-italic invalid-name"></div>
                        </div>

                        <div class="mb-3">
                            <label for="modalFormNumberInput" class="form-label">
                                Kode Pos
                            </label>
                            <input type="number" id="modalFormNumberInput" class="form-control  form-control-lg" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalFormButtonSave" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
