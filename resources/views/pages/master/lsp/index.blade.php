@section('title')
    MASTER LSP
@endsection

@section('metadata')
    <meta content="Master LSP" name="keywords" />
    <meta content="LMS Stankom - Halaman Master LSP" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.lsp') !!}",
            tableName: "tableLSP",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "lsp_name",
                    name: "lsp_name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "lsp_email",
                    name: "lsp_email",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "lsp_phone",
                    name: "lsp_phone",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "certification_name",
                    name: "certification_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "lsp_address",
                    name: "lsp_address",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "lsp_website",
                    name: "lsp_website",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "description",
                    name: "description",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "total_accessors",
                    name: "total_accessors",
                    orderable: true,
                    className: "align-middle text-nowrap text-center fw-bold",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle",
                }

            ]

        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.lsp.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        $(document).ready(function() {

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }

            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
            });


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER LSP</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => route('master.lsp.show.export'),
                        'addUrl' => route('master.lsp.create'),
                        'addTitle' => 'LSP',
                        'addAction' => true,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableLSP" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">NAMA LSP</th>
                                            <th>EMAIL</th>
                                            <th>NO. TELEPON/HP</th>
                                            <th>BIDANG SERTIFIKAT</th>
                                            <th>ALAMAT LSP</th>
                                            <th>WEBSITE</th>
                                            <th>KETERANGAN</th>
                                            <th>TOTAL ASSESSOR</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
