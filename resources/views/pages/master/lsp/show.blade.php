@section('title')
    DETAIL LSP
@endsection

@section('metadata')
    <meta content="Detail LSP, LSP" name="keywords" />
    <meta content="LMS Stankom - Halaman Detail LSP" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var dataLSP = <?php echo json_encode($record); ?>;

        $(document).ready(function() {
            console.log(dataLSP)
        });
    </script>
@endsection

<x-app-layout>
    <div class="row mb-2">
        <div class="col-sm-12">
            <div class="home-tab">
                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                    <div>
                        <div class="btn-wrapper">
                            <a href="{{ route('master.lsp.list') }}" class="btn btn-primary text-white me-0">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                                &nbsp; Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-12">
                            <h4 class="card-title">
                                <i class="fas fa-bullseye"></i> &nbsp;
                                DETAIL LEMBAGA SERTIFIKASI PROFESI (LSP)
                            </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-floating mb-3">
                                <input type="text" id="nameLSP" class="form-control text-primary"
                                    value="{{ $record->lsp_name }}" />
                                <label for="nameLSP" class="form-label">Nama LSP</label>
                            </div>

                            <div class="form-floating mb-3">
                                <input type="text" id="emailLSP" class="form-control text-primary"
                                    value="{{ $record->lsp_email }}" />
                                <label for="emailLSP" class="form-label">Email</label>
                            </div>


                            <div class="form-floating mb-3">
                                <input type="text" id="phoneLSP" class="form-control text-primary"
                                    value="{{ $record->lsp_phone }}" />
                                <label for="phoneLSP" class="form-label">No. Telepon/HP</label>
                            </div>

                            <div class="form-floating mb-3">
                                <input type="text" id="certificationType" class="form-control text-primary"
                                    value="{{ $record->certificationType->name }}" />
                                <label for="certificationType" class="form-label">Bidang Sertifikasi</label>
                            </div>

                            <div class="form-floating mb-3">
                                <textarea type="text" id="addressLSP" class="form-control text-primary" style="height: 100px;">{{ $record->lsp_address }}</textarea>
                                <label for="addressLSP" class="form-label">Alamat LSP</label>
                            </div>

                            <div class="form-floating mb-3">
                                <input type="text" id="websiteLSP" class="form-control text-primary"
                                    value="{{ $record->lsp_website }}" />
                                <label for="websiteLSP" class="form-label">Website LSP</label>
                            </div>

                            <div class="form-floating mb-3">
                                <textarea type="text" id="description" class="form-control text-primary" style="height: 100px;">{{ $record->description }}</textarea>
                                <label for="description" class="form-label">Keterangan</label>
                            </div>

                            <div class="mb-3">
                                <label for="type_name" class="form-label">Status</label>
                                @if ($record->status)
                                    <label class="badge bg-success badge-success text-white">ACTIVE</label>
                                @else
                                    <label class="badge bg-danger badge-danger text-white">NON ACTIVE</label>
                                @endif
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="mb-3">
                                <h4 class="text-success">List Assessor</h4>
                                <ul class="list-group">
                                    @foreach ($record->accessors as $value)
                                        <li class="list-group-item">
                                            <i class="fas fa-caret-right"></i> &nbsp;
                                            <span class="fs-6 fw-bold">{{ $value->name }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
