@section('title')
    UPDATE LSP
@endsection

@section('metadata')
    <meta content="Master Update LSP" name="keywords" />
    <meta content="LMS Stankom - Halaman Master Update LSP" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var dataLSP = @json($record)

        var formState = {
            lsp_name: $("#textLSPName"),
            certification: $("#selectionCertificationType"),
            lsp_address: $("#textareaLSPAddress"),
            description: $("#textareaDescription"),
            lsp_email: $("#textLSPEmail"),
            lsp_phone: $("#textLSPPhone"),
            lsp_website: $("#textLSPWebsite"),
            actionSave: $("#buttonSaveAction"),
        }

        function initialCertificationType(selected = null) {
            const url = "{!! route('master.certification_types.all') !!}"

            axios.get(url).then(response => {
                $.each(response.data.results, function(index, value) {

                    formState.certification
                        .append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.text));

                    if (selected != null && selected == value.id) {
                        formState.certification.val(value.id).change();
                    }
                });
            }).catch(error => {
                console.log(error);
            });


            formState.certification.select2({
                theme: "bootstrap-5",
                placheloder: "Pilih Bidang Sertifikasi"
            });
        }

        function initialAccessor() {

            const url = "{!! route('master.accessor.all') !!}"

            axios.get(url).then(response => {
                $.each(response.data.items, function(index, value) {
                    formState.accessor
                        .append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.name));

                    if (selected != null && selected == value.id) {
                        formState.accessor.val(value.id).change();
                    }
                });
            }).catch(error => {
                console.log(error);
            });


            formState.accessor.select2({
                theme: "bootstrap-5",
                placheloder: "Pilih  Nama Assesor"
            });


        }

        function refreshErrorTagMessage() {
            $('.invalid-lsp_name').text("")
            $('.invalid-certification_type_id').text("")
            $('.invalid-accessor_id').text("")
            $('.invalid-lsp_address').text("")
        }

        $(document).ready(function() {
            console.log(dataLSP.certification_type_id);
            initialCertificationType(dataLSP.certification_type_id);

            formState.actionSave.click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const data = {
                    lsp_name: formState.lsp_name.val(),
                    certification_type_id: formState.certification.val(),
                    lsp_address: formState.lsp_address.val(),
                    description: formState.description.val(),
                    lsp_email: formState.lsp_email.val(),
                    lsp_phone: formState.lsp_phone.val(),
                    lsp_website: formState.lsp_website.val()
                };

                const url = "{!! route('master.lsp.update', $record->id) !!}";

                axios.put(url, data).then(response => {
                    if (response.status == 201 || response.status == 200) {
                        const {
                            data
                        } = response;

                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('master.lsp.list') !!}";
                            }
                        });
                    }

                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('lsp_name')) {
                                $('.invalid-lsp_name').text(data.errors
                                        .lsp_name[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('certification_type_id')) {
                                $('.invalid-certification_type_id').text(data.errors
                                        .certification_type_id[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('accessor_id')) {
                                $('.invalid-accessor_id').text(data.errors
                                        .accessor_id[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }


                            if (data.errors.hasOwnProperty('lsp_address')) {
                                $('.invalid-lsp_address').text(data.errors
                                        .lsp_address[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }


                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">


        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM UPDATE LEMBAGA SERTIFIKASI PROFESI (LSP)
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('master.lsp.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <!--  NAMA LSP -->
                            <div class="mb-3">
                                <label for="textLSPName" class="form-label fw-bold">Nama LSP</label>
                                <input id="textLSPName" class="form-control form-control-lg" type="text"
                                    value="{{ $record->lsp_name }}" placeholder="Nama Kelas (Webinar)" />
                                <div class="text-danger text-sm fst-italic invalid-lsp_name"></div>
                            </div>

                            <!--  NOMOR TELEPON LSP -->
                            <div class="mb-3">
                                <label for="textLSPPhone" class="form-label fw-bold">No. Telepon/HP LSP</label>
                                <input id="textLSPPhone" class="form-control form-control-lg" type="text"
                                    value="{{ $record->lsp_phone }}" placeholder="Nomor Telepone/HP LSP" />
                            </div>

                            <!-- EMAIL LSP -->
                            <div class="mb-3">
                                <label for="textLSPEmail" class="form-label fw-bold">Email LSP</label>
                                <input id="textLSPEmail" class="form-control form-control-lg" type="email"
                                    value="{{ $record->lsp_email }}" placeholder="Email LSP" />
                            </div>


                            <!-- BIDANG SERTIFIKASI -->
                            <div class="mb-3">
                                <label for="selectionCertificationType" class="form-label fw-bold">
                                    Bidang Sertifikasi
                                </label>
                                <select id="selectionCertificationType" class="form-select form-select-lg"
                                    data-placeholder="Bidang Sertifikasi"></select>
                                <div class="text-danger text-sm fst-italic invalid-certification_type_id"></div>
                            </div>


                            <!-- ALAMAT LSP -->
                            <div class="mb-3">
                                <label for="textareaLSPAddress" class="form-label fw-bold">
                                    Alamat LSP
                                </label>
                                <textarea type="text" id="textareaLSPAddress" class="form-control" style="height: 100px;">{{ $record->lsp_address }}</textarea>
                                <div class="text-danger text-sm fst-italic invalid-lsp_address"></div>
                            </div>

                            <!-- WEBSITE LSP -->
                            <div class="mb-3">
                                <label for="textLSPWebsite" class="form-label fw-bold">Website LSP</label>
                                <input id="textLSPWebsite" class="form-control form-control-lg" type="text"
                                    value="{{ $record->lsp_website }}" placeholder="Website LSP" />
                            </div>


                            <div class="mb-3">
                                <label for="textareaDescription" class="form-label fw-bold">
                                    Keterangan
                                </label>
                                <textarea type="text" id="textareaDescription" class="form-control" style="height: 100px;">{{ $record->description }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-dark" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
