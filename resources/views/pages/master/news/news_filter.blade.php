<div class="row mb-2">
    <div class="col-3">
        <div class="mb-3">
            <label for="selectionTags" class="form-label">Tags</label>
            <select id="selectionTags" class="form-control" data-placeholder="Tags"></select>
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="startDate" class="form-label">Dari Tanggal</label>
            <input type="text" id="startDate" class="form-control">
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="endDate" class="form-label">Sampai Tanggal</label>
            <input type="text" id="endDate" class="form-control">
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="textSearch" class="form-label">Pencarian</label>
            <input id="textSearch" class="form-control " type="text" placeholder="Kata kunci pencarian..." />
        </div>
    </div>
</div>

<div class="row mb-2">
    <div class="col-4">
        <div class="d-grid">
            <button type="button" class="btn btn-warning " onclick="eventFilterData()">
                <i class="fas fa-filter"></i>
                Pencarian
            </button>
        </div>
    </div>

    <div class="col-4">
        <div class="d-grid">
            <button type="button" class="btn btn-success" onclick="eventRefreshData()">
                <i class="fas fa-refresh"></i>
                Refresh
            </button>
        </div>
    </div>

    <div class="col-4">

        <div class="d-grid">
            <div class="btn-group">
                <button type="button" class="btn btn-dark">Operasi Data</button>
                <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('master.news.create') }}" class="dropdown-item">
                            <i class="fas fa-plus"></i> &nbsp; Berita
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
