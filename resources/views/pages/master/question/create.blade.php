@section('title')
    TAMBAH PERTANYAAN
@endsection

@section('metadata')
    <meta content="TAMBAH PERTANYAAN" name="keywords" />
    <meta content="LMS Stankom - Halaman TAMBAH PERTANYAAN" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var dataQuiz = @json($quiz);
        var dataQuizType = @json($quizType);
        var categoryState = [];

        var formModalCategory = {
            modal: "modalFormCategory",
            title: "modalTitleCategory",
            quiz_type: "modalSelectQuizType",
            duration: "modalInputDuration",
            min_score: "modalInputScored",
            action: "modalButtonCategory",

        }

        function showListCategory(categories) {
            var str = '';
            $(`#showListCategory`).empty();
            if (categories.length > 0) {
                categories.forEach(element => {
                    const name_type = dataQuizType.find(x => x.id == element.quiz_type_id)
                    str += `
                        <div class="px-0 col-4">
                            <div class="d-grid">
                                <button class="btn btn-default border-success mt-4">
                                    ${name_type.name || ""}
                                </button>
                            </div>
                        </div>
                    `
                });

                console.log(str)
                $(`#showListCategory`).html(str);
            }

            return str;
        }

        function eventShowCategoryModal() {
            $(`#${formModalCategory.title}`).text("FORM TAMBAH KATEGORI KUIS");
            $(`#${formModalCategory.quiz_type}`).val("").change();
            $(`#${formModalCategory.duration}`).val("");
            $(`#${formModalCategory.min_score}`).val("");
            $(`#${formModalCategory.modal}`).modal("show");
        }


        $(document).ready(function() {
            $(`#${formModalCategory.quiz_type}`).select2({
                dropdownParent: $(`#${formModalCategory.modal}`),
                theme: "bootstrap-5",
                placeholder: "Pilih Modul Pembelajaran",
                allowClear: true
            });

            $(`#${formModalCategory.action}`).click(function(e) {
                e.preventDefault();
                const quiz_type_id = $(`#${formModalCategory.quiz_type}`).val()
                if (categoryState.length > 0) {
                    const find = categoryState.find(x => x.quiz_type_id == quiz_type_id);
                    if (find) {
                        Swal.fire({
                            title: 'PERINGATAN',
                            icon: 'warning',
                            text: 'Kategori kuis sudah ditambahkan',
                        });
                    }
                    $(`#${formModalCategory.modal}`).modal("hide");
                    return
                }

                categoryState.push({
                    quiz_id: dataQuiz.id,
                    quiz_type_id: $(`#${formModalCategory.quiz_type}`).val(),
                    duration_minutes: $(`#${formModalCategory.duration}`).val(),
                    min_scored: $(`#${formModalCategory.min_score}`).val(),
                });

                console.log(categoryState)
                showListCategory(categoryState);

                $(`#${formModalCategory.modal}`).modal("hide");
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3 p-2">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                LIST PERTANYAAN
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-2">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('master.quiz.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3 p-2">
                        <div class="col-9">
                            <h5 class="fs-4 fw-bold">Kuis {{ ucwords($quiz->name) }}</h5>
                        </div>
                        <div class="col-3 justify-end">
                            <span class="alert alert-success text-dark" disabled>{{ $quiz->code }}</span>
                        </div>
                    </div>

                    <div class="row mb-3 p-2">
                        <div class="col-2">
                            <div class="px-0 d-grid">
                                <button class="btn btn-primary mt-4" onclick="eventShowCategoryModal()">
                                    <i class="fas fa-plus"></i> Kategori Soal
                                </button>
                            </div>
                        </div>

                        <div class="col-8">
                            <div id="showListCategory" class="row"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-dark" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.master.question.modals.quiz_category')
</x-app-layout>
