@section('title')
    MASTER PERTANYAAN
@endsection

@section('metadata')
    <meta content="MASTER PERTANYAAN" name="keywords" />
    <meta content="LMS Stankom - Halaman MASTER PERTANYAAN" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.quiz') !!}",
            tableName: "tableQuestion",
            filters: {
                status: "active",
                webinar_id: "",
                learning_id: "",
                search: "",
                sertificated: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "name",
                    name: "name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "code",
                    name: "code",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "min_scored",
                    name: "min_scored",
                    orderable: true,
                    className: "align-middle text-center",
                },
                {
                    data: "webinar_name",
                    name: "webinar_name",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "learning_name",
                    name: "learning_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "has_sertificated",
                    name: "has_sertificated",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle",
                }

            ]

        }


        const formFilterState = {
            learningModule: "selectionModule",
            classWebinar: "selectionClassWebinar",
            status: "selectionStatus",
            quiz: "selectionQuizName",
            search: "textSearch",
        }

        const formModalChoose = {
            selection: "modalSelectionQuizName",
            modal: "modalFormDefault",
            title: "modalTitleDefault",
            action: "modalButtonDefault"
        }


        function initialClassWebinar() {
            const url = "{!! route('class_webinar.webinar_quiz') !!}";

            axios.get(url).then(response => {
                    const {
                        data
                    } = response;
                    $(`#${formFilterState.classWebinar}`).empty();
                    if (data.results.length > 0) {
                        $(`#${formFilterState.classWebinar}`).append(new Option('Pilih Kelas/Webinar', ''));
                        data.results.forEach(item => {
                            $(`#${formFilterState.classWebinar}`).append(new Option(item.class_name, item.id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formFilterState.classWebinar}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Kelas/Webinar",
                allowClear: true
            });
        }

        function initialLearningModule() {
            const url = "{!! route('learning_modules.learning_quiz') !!}";
            axios.get(url).then(response => {
                    const {
                        data
                    } = response;

                    if (data.results.length > 0) {
                        $(`#${formFilterState.learningModule}`).empty();
                        $(`#${formFilterState.learningModule}`).append(new Option('Pilih Modul Pembelajaran', ''));
                        data.results.forEach(item => {
                            $(`#${formFilterState.learningModule}`).append(new Option(item.module_name, item
                                .id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formFilterState.learningModule}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Modul Pembelajaran",
                allowClear: true
            });

        }

        function initialQuiz() {

            const params = {
                learning_id: $(`#${formFilterState.learningModule}`).val(),
                webinar_id: $(`#${formFilterState.classWebinar}`).val(),
            }

            const url = "{!! route('quiz.full') !!}";
            axios.get(url, {
                    params: params
                }).then(response => {
                    console.log(response)
                    const {
                        data
                    } = response;

                    $(`#${formFilterState.quiz}`).empty();

                    if (data.results.length > 0) {
                        $(`#${formFilterState.quiz}`).append(new Option('Pilih Nama Kuis', ''));
                        data.results.forEach(item => {
                            $(`#${formFilterState.quiz}`).append(new Option(item.name, item.id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formFilterState.quiz}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Nama Kuis",
                allowClear: true
            });

        }



        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters = {
                status: "active",
                webinar_id: "",
                learning_id: "",
                search: "",
                sertificated: ""
            }
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.quiz.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        function eventShowChooseQuiz() {
            const url = "{!! route('quiz.full') !!}";
            axios.get(url).then(response => {
                    const {
                        data
                    } = response;

                    if (data.results.length > 0) {
                        $(`#${formModalChoose.selection}`).empty();
                        $(`#${formModalChoose.selection}`).append(new Option('Pilih Nama Kuis', ''));
                        data.results.forEach(item => {
                            $(`#${formModalChoose.selection}`).append(new Option(item.name, item.id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formModalChoose.selection}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Nama Kuis",
                allowClear: true
            });

            $(`#${formModalChoose.title}`).text("FORM PILIH KUIS");
            $(`#${formModalChoose.action}`).text("TAMBAH PERTANYAAN");
            $(`#${formModalChoose.action}`).attr("data-id", "");
            $(`#${formModalChoose.modal}`).modal('show');
        }

        $(document).ready(function() {
            initialClassWebinar();
            initialLearningModule();
            initialQuiz();

            $(`#${formFilterState.learningModule}`).change(function(e) {
                e.preventDefault();
                tableState.filters.learning_id = e.target.value;
                initialQuiz();
                // reloadDatatable();
            });

            $(`#${formFilterState.classWebinar}`).change(function(e) {
                e.preventDefault();
                tableState.filters.webinar_id = e.target.value;
                initialQuiz();
                // reloadDatatable();
            });

            $(`#${formFilterState.status}`).change(function(e) {
                e.preventDefault();
                tableState.filters.status = e.target.value
                // reloadDatatable();
            });


            $(`#${formModalChoose.action}`).click(function(e) {
                e.preventDefault();
                var quizId = $(`#${formModalChoose.selection}`).val();

                if (quizId && quizId != '') {
                    window.location.href = "{!! url('master/question/quiz') !!}/" + quizId;
                } else {
                    Swal.fire({
                        title: 'PERINGATAN',
                        icon: 'warning',
                        text: 'Silahkan pilih nama kuis',
                        confirmButtonText: 'OK',
                    })
                }
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER PERTANYAAN DARI KUIS</h4>
                        </div>
                    </div>

                    @include('pages.master.question.question-filter')


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableQuestion" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">PERTANYAAN</th>
                                            <th>TIPE PERTANYAAN</th>
                                            <th>NAMA KUIS</th>
                                            <th>SKOR</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.master.question.modal-choose')

</x-app-layout>
