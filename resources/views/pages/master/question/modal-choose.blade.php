<div id="modalFormDefault" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleDefault" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <form>
                            <div class="mb-3">
                                <label for="modalSelectionQuizName" class="form-label">Pilih Nama Kuis</label>
                                <select id="modalSelectionQuizName" class="form-control form-select"
                                    data-placeholder="Pilih Nama Kuis"></select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonDefault" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
