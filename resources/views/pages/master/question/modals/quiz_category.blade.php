<div id="modalFormCategory" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleCategory" class="modal-title">Kategori Kuis</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <form>
                            <div class="mb-3">
                                <label for="modalSelectQuizType" class="form-label">Kategori Kuis</label>
                                <select id="modalSelectQuizType"
                                    class="form-control form-select form-control-lg text-primary"
                                    data-placeholder="Kategori/Kuis">
                                    @foreach ($quizType as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="modalInputDuration" class="form-label">Durasi Waktu (Menit)</label>
                                <input type="number" id="modalInputDuration" class="form-control form-control-lg" />
                            </div>

                            <div class="mb-3">
                                <label for="modalInputScored" class="form-label">Nilai Minimal (Min Score)</label>
                                <input type="number" id="modalInputScored" class="form-control form-control-lg" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonCategory" type="button" class="btn btn-primary" data-id="">
                    Tambahkan
                </button>
            </div>
        </div>
    </div>
</div>
