<div class="row mb-4">
    <div class="col-3">
        <div class="mb-3">
            <label for="selectionModule" class="form-label">Modul Pembelajaran</label>
            <select id="selectionModule" class="form-control form-select" data-placeholder="Module Pembelajaran"></select>
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="selectionClassWebinar" class="form-label">Kelas/Webinar</label>
            <select id="selectionClassWebinar" class="form-control form-select"
                data-placeholder="Kelas/Webinar"></select>
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="selectionQuizName" class="form-label">Nama Kuis</label>
            <select id="selectionQuizName" class="form-control form-select" data-placeholder="Nama Kuis"></select>
        </div>
    </div>

    <div class="col-3">
        <div class="mb-3">
            <label for="selectionStatus" class="form-label">Status Kuis</label>
            <select id="selectionStatus" class="form-control form-select">
                <option value="active" selected>Aktif</option>
                <option value="non-active">Tidak Aktif</option>
            </select>
        </div>
    </div>
</div>

<div class="row mb-4">

    <div class="col-6">
        <div class="row">
            <div class="col-8">
                <div class="mb-3">
                    <input id="textSearch" class="form-control form-control-lg" type="text"
                        placeholder="Kata kunci pencarian..." />
                </div>
            </div>

            <div class="col-4">
                <div class="d-grid">
                    <button type="button" class="btn btn-warning " onclick="eventFilterData()">
                        <i class="fas fa-filter"></i>
                        Cari
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <div class="d-grid">
                    <button type="button" class="btn btn-success" onclick="eventRefreshData()">
                        <i class="fas fa-refresh"></i>
                        Refresh
                    </button>
                </div>
            </div>

            <div class="col-6">
                <div class="d-grid">
                    <div class="btn-group">
                        <button type="button" class="btn btn-dark">Operasi Data</button>
                        <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="visually-hidden">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <button type="button" class="dropdown-item" onclick="eventShowChooseQuiz()">
                                    <i class="fas fa-plus"></i> &nbsp; Pertanyaan Baru
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
