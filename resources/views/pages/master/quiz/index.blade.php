@section('title')
    MASTER KUIS
@endsection

@section('metadata')
    <meta content="MASTER KUIS" name="keywords" />
    <meta content="LMS Stankom - Halaman MASTER KUIS" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.quiz') !!}",
            tableName: "tableQuiz",
            filters: {
                status: "active",
                webinar_id: "",
                learning_id: "",
                search: "",
                sertificated: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "name",
                    name: "name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "code",
                    name: "code",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "webinar_name",
                    name: "webinar_name",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "learning_name",
                    name: "learning_name",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "has_sertificated",
                    name: "has_sertificated",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle",
                }

            ]

        }

        const formFilterState = {
            learningModule: "selectionModule",
            classWebinar: "selectionClassWebinar",
            status: "selectionStatus",
            sertificated: "selectionHasSertificated",
            search: "textSearch",
        }

        function initialClassWebinar() {
            const url = "{!! route('class_webinar.webinar_quiz') !!}";

            axios.get(url).then(response => {
                    const {
                        data
                    } = response;
                    $(`#${formFilterState.classWebinar}`).empty();
                    if (data.results.length > 0) {
                        $(`#${formFilterState.classWebinar}`).append(new Option('Pilih Kelas/Webinar', ''));
                        data.results.forEach(item => {
                            $(`#${formFilterState.classWebinar}`).append(new Option(item.class_name, item.id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formFilterState.classWebinar}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Kelas/Webinar",
                allowClear: true
            });
        }

        function initialLearningModule() {
            const url = "{!! route('learning_modules.learning_quiz') !!}";
            axios.get(url).then(response => {
                    const {
                        data
                    } = response;

                    $(`#${formFilterState.learningModule}`).empty();
                    if (data.results.length > 0) {
                        $(`#${formFilterState.learningModule}`).append(new Option('Pilih Modul Pembelajaran', ''));
                        data.results.forEach(item => {
                            $(`#${formFilterState.learningModule}`).append(new Option(item.module_name, item
                                .id));
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formFilterState.learningModule}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Modul Pembelajaran",
                allowClear: true
            });

        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters = {
                status: "active",
                webinar_id: "",
                learning_id: "",
                search: "",
                sertificated: ""
            }
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.quiz.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        $(document).ready(function() {
            initialClassWebinar();
            initialLearningModule();

            $(`#${formFilterState.status}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Tipe Kuis",
            });

            $(`#${formFilterState.sertificated}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Tipe Kuis",
            });

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.status = tableState.filters.status;
                        d.search = tableState.filters.search;
                        d.webinar_id = tableState.filters.webinar_id;
                        d.learning_id = tableState.filters.learning_id;
                        d.sertificated = tableState.filters.sertificated;
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }

            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
            });



            $(`#${formFilterState.learningModule}`).change(function(e) {
                e.preventDefault();
                tableState.filters.learning_id = e.target.value;
                reloadDatatable();
            });

            $(`#${formFilterState.classWebinar}`).change(function(e) {
                e.preventDefault();
                tableState.filters.webinar_id = e.target.value;
                reloadDatatable();
            });


            $(`#${formFilterState.status}`).change(function(e) {
                e.preventDefault();
                tableState.filters.status = e.target.value
                reloadDatatable();
            });

            $(`#${formFilterState.sertificated}`).change(function(e) {
                e.preventDefault();
                tableState.filters.sertificated = e.target.value
                console.log(tableState.filters)
                reloadDatatable();
            });


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER KUIS</h4>
                        </div>
                    </div>

                    @include('pages.master.quiz.quiz_filter')


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableQuiz" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">NAMA QUIZ</th>
                                            <th>KODE KUIS</th>
                                            <th>KELAS/WEBINAR</th>
                                            <th>MODUL PEMBELAJARAN</th>
                                            <th>SERTIFIKAT</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
