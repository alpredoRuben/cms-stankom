<div class="row mb-4">
    <div class="col-4">
        <div class="mb-3">
            <label for="selectionModule" class="form-label">Modul Pembelajaran</label>
            <select id="selectionModule" class="form-control form-select" data-placeholder="Module Pembelajaran"></select>
        </div>
    </div>

    <div class="col-4">
        <div class="mb-3">
            <label for="selectionClassWebinar" class="form-label">Kelas/Webinar</label>
            <select id="selectionClassWebinar" class="form-control" data-placeholder="Kelas/Webinar"></select>
        </div>
    </div>

    <div class="col-2">
        <div class="mb-3">
            <label for="selectionStatus" class="form-label">Status Kuis</label>
            <select id="selectionStatus" class="form-control form-select">
                <option value="active" selected>Aktif</option>
                <option value="non-active">Tidak Aktif</option>
            </select>
        </div>
    </div>

    <div class="col-2">
        <div class="mb-3">
            <label for="selectionHasSertificated" class="form-label">Status Sertifikat</label>
            <select id="selectionHasSertificated" class="form-control form-select">
                <option value="">Pilih Status Sertifikat</option>
                <option value="1">Bersertifikat</option>
                <option value="2">Tidak Bersertifikat</option>
            </select>
        </div>
    </div>
</div>

<div class="row mb-4">

    <div class="col-6">
        <div class="row">
            <div class="col-8">
                <div class="mb-3">
                    <input id="textSearch" class="form-control form-control-lg" type="text"
                        placeholder="Kata kunci pencarian..." />
                </div>
            </div>

            <div class="col-4">
                <div class="d-grid">
                    <button type="button" class="btn btn-warning " onclick="eventFilterData()">
                        <i class="fas fa-filter"></i>
                        Cari
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <div class="d-grid">
                    <button type="button" class="btn btn-success" onclick="eventRefreshData()">
                        <i class="fas fa-refresh"></i>
                        Refresh
                    </button>
                </div>
            </div>

            <div class="col-6">
                <div class="d-grid">
                    <div class="btn-group">
                        <button type="button" class="btn btn-dark">Operasi Data</button>
                        <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="visually-hidden">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('master.quiz.create') }}" class="dropdown-item">
                                    <i class="fas fa-plus"></i> &nbsp; Kuis Baru
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('master.quiz.export') }}" class="dropdown-item">
                                    <i class="fas fa-file-export"></i> &nbsp; Export Kuis
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
