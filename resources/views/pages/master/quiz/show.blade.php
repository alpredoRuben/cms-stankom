@section('title')
    PREVIEW KUIS
@endsection

@section('metadata')
    <meta content="Preview Kuis" name="keywords" />
    <meta content="LMS Stankom - Preview Kuis" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script></script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-sm-12">
            <div class="home-tab">
                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                    <div>
                        <div class="btn-wrapper">
                            <a href="{{ route('master.quiz.list') }}" class="btn btn-primary text-white me-0">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                                &nbsp; Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-bullseye"></i> &nbsp;
                                DETAIL KUIS
                            </h4>
                        </div>

                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">

                                    <div class="d-grid">
                                        <a href="" class="btn btn-dark btn-sm">
                                            <i class="fas fa-key"></i>
                                            Pertanyaan
                                        </a>
                                    </div>

                                    <div class="d-grid">
                                        <a href="{{ route('master.quiz.list') }}"
                                            class="btn btn-primary text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <!-- NAMA KUIS -->
                        <div class="col-12">
                            <div class="form-floating mb-3">
                                <input id="quizName" class="form-control" value="{{ $record->quiz_name }}"
                                    type="text" readonly />
                                <label for="quizName" class="form-label">Nama Kuis</label>
                            </div>

                            <!-- KELAS WEBINAR -->
                            <div class="form-floating mb-3">
                                <input id="webinarName" class="form-control"
                                    value="{{ $record->classWebinar->class_name }}" type="text" readonly />
                                <label for="webinarName" class="form-label">Kelas/Webinar</label>
                            </div>

                            <!-- MODULE PEMBELAJARAN -->
                            <div class="form-floating mb-3">
                                <input id="moduleName" class="form-control"
                                    value="{{ $record->learningModule->module_name }}" type="text" readonly />
                                <label for="moduleName" class="form-label">Modul Pembelajaran</label>
                            </div>

                            <!-- STATUS SERTIFIKAT -->
                            <div class="form-floating mb-3">
                                <input id="sertificated" class="form-control"
                                    value="{{ $record->has_sertificated ? 'Bersertifikat' : 'Tidak Bersertifikat' }}"
                                    type="text" readonly />
                                <label for="sertificated" class="form-label">Status Sertifikat</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
