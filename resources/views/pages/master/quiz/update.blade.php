@section('title')
    TAMBAH KUIS
@endsection

@section('metadata')
    <meta content="Tambah Kuis" name="keywords" />
    <meta content="LMS Stankom - Halaman Kuis" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var dataQuiz = @json($record);

        const formQuizState = {
            quizName: "textQuizName",
            classWebinar: "selectionClassWebinar",
            learningModule: "selectionModule",
            hasSertificated: 'radioSertificated',
            actionStore: "buttonSaveAction"
        }

        function refreshErrorTagMessage() {
            $('.invalid-name').text("")
            $('.invalid-webinar_id').text("")
        }


        function initialClassWebinar(selected = null) {
            const url = "{!! route('class_webinar.full') !!}";

            axios.get(url).then(response => {
                    const {
                        data
                    } = response;
                    $(`#${formQuizState.classWebinar}`).empty();
                    if (data.results.length > 0) {

                        $(`#${formQuizState.classWebinar}`).append(new Option('Pilih Kelas/Webinar', ''));
                        data.results.forEach(item => {
                            $(`#${formQuizState.classWebinar}`).append(new Option(item.class_name, item.id));
                            if (selected == item.id) {
                                $(`#${formQuizState.classWebinar}`).val(selected).trigger('change')
                            }
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })


            $(`#${formQuizState.classWebinar}`).select2({
                theme: "bootstrap-5",
                placeholder: 'Pilih Kelas/Webinar',
                allowClear: true
            });




        }

        function initialLearningModule(selected = null) {
            const url = "{!! route('learning_modules.full') !!}";
            axios.get(url).then(response => {
                    const {
                        data
                    } = response;

                    $(`#${formQuizState.learningModule}`).empty();
                    if (data.results.length > 0) {
                        $(`#${formQuizState.learningModule}`).append(new Option('Pilih Modul Pembelajaran', ''));
                        data.results.forEach(item => {
                            $(`#${formQuizState.learningModule}`).append(new Option(item.module_name, item.id));
                            if (selected == item.id) {
                                $(`#${formQuizState.learningModule}`).val(selected).trigger('change')
                            }
                        });
                    }
                })
                .catch(error => {
                    console.log(error)
                })

            $(`#${formQuizState.learningModule}`).select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Modul Pembelajaran",
                allowClear: true
            });

        }

        $(document).ready(function() {


            initialClassWebinar(dataQuiz.webinar_id);
            initialLearningModule(dataQuiz.learning_id);

            if (dataQuiz.has_sertificated) {
                $(`input:radio[name=${formQuizState.hasSertificated}]`).filter('[value=1]').prop('checked', true);
            } else {
                $(`input:radio[name=${formQuizState.hasSertificated}]`).filter('[value=0]').prop('checked', true);
            }

            $(`#${formQuizState.actionStore}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const data = {
                    name: $(`#${formQuizState.quizName}`).val(),
                    webinar_id: $(`#${formQuizState.classWebinar}`).val(),
                    learning_id: $(`#${formQuizState.learningModule}`).val(),
                    has_sertificated: $(`input[name="${formQuizState.hasSertificated}"]:checked`).val(),
                };



                const url = "{!! route('master.quiz.update', $record->id) !!}";

                axios.put(url, data).then(response => {
                    if (response.status == 201 || response.status == 200) {
                        const {
                            data
                        } = response;

                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('master.quiz.list') !!}"
                            }
                        });
                    }
                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('name')) {
                                $('.invalid-name').text(data.errors.name[0]).css('font-size',
                                    '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('webinar_id')) {
                                $('.invalid-webinar_id').text(data.errors.webinar_id[0])
                                    .css('font-size', '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('min_scored')) {
                                $('.invalid-min_scored').text(data.errors.min_scored[0]).css(
                                    'font-size', '0.8rem');
                            }

                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3 p-2">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM TAMBAH KUIS
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-2">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('master.quiz.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <!-- NAMA KUIS -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="textQuizName" class="form-label">Nama Kuis</label>
                                <input id="textQuizName" class="form-control form-control-lg" type="text"
                                    placeholder="Nama Kuis" value="{{ $record->name }}" />
                                <div class="text-danger text-sm fst-italic invalid-name"></div>
                            </div>
                        </div>

                        <!-- KELAS WEBINAR -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="selectionClassWebinar" class="form-label">
                                    Nama Kelas / Webinar
                                </label>
                                <select id="selectionClassWebinar" name="selectionClassWebinar"
                                    class="form-control form-control-lg form-select"
                                    data-placeholder="Kelas/Webinar"></select>
                                <div class="text-danger text-sm fst-italic invalid-webinar_id"></div>
                            </div>
                        </div>

                        <!-- MODUL PEMBELAJARAN -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="selectionModule" class="form-label">
                                    Nama Modul Pembelajaran
                                </label>
                                <select id="selectionModule" class="form-control form-control-lg form-select"
                                    data-placeholder="Modul Pembelajaran"></select>
                            </div>
                        </div>

                        <!-- BERSERTIFIKAT -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="radioSertificated" class="form-label">Apakah kuis ini bertifikat ?</label>
                                <div id="radioSertificated">
                                    <input class="form-check-input" type="radio" name="radioSertificated"
                                        id="radioSertificated1" value="1" checked>
                                    <label class="form-check-label" for="radioSertificate1">
                                        Ya
                                    </label>
                                    &nbsp;
                                    <input class="form-check-input" type="radio" name="radioSertificated"
                                        id="radioSertificated2" value="0">
                                    <label class="form-check-label" for="radioSertificated2">
                                        Tidak
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-dark" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    UPDATE
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
