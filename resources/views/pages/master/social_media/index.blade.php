@section('title')
    LINK MEDIA SOSIAL
@endsection

@section('metadata')
    <meta content="LINK MEDIA SOSIAL" name="keywords" />
    <meta content="LMS Stankom - Halaman LINK MEDIA SOSIAL" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var titlePage = "MEDIA SOSIAL";
        var formModalMedsos = {
            modal: 'modalFormMedsos',
            title: 'modalTitleMedsos',
            selection: 'modalSelectMedia',
            input: 'modalInputURL',
            action: 'modalButtonAction',
        };

        function refreshErrorTagMessage() {
            $('.invalid-medsos_id').text("")
        }


        function fetchSosmed(page = 1) {
            let url = "{!! route('master.social-media.fetchMedsos') !!}?page=" + page

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverMedsos').html(data);
                }
            });
        }

        function eventCreateData() {
            refreshErrorTagMessage()
            initSelection();
            $(`#${formModalMedsos.input}`).val("");
            $(`#${formModalMedsos.title}`).text(`FORM TAMBAH LINK ${titlePage}`);
            $(`#${formModalMedsos.action}`).text("SIMPAN");
            $(`#${formModalMedsos.action}`).attr("data-id", "");
            $(`#${formModalMedsos.modal}`).modal("show");
        }

        function eventEditData(id) {
            refreshErrorTagMessage()


            const url = "{!! url('master/social-media/show') !!}/" + id
            axios.get(url).then(response => {
                const {
                    data
                } = response;
                initSelection(data.result.medsos_id);
                // $(`#${formModalMedsos.selection}`).val(data.result.medsos_id).change();
                $(`#${formModalMedsos.input}`).val(data.result.url);
                $(`#${formModalMedsos.title}`).text(`FORM UBAH LINK ${titlePage}`);
                $(`#${formModalMedsos.action}`).text("UPDATE");
                $(`#${formModalMedsos.action}`).attr("data-id", id);
                $(`#${formModalMedsos.modal}`).modal("show");
            }).catch(error => {
                showErrorPopup(error)
            })

        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.social-media.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            refreshPage();
                        });
                    }).catch((error) => {
                        showErrorPopup(error)
                    });
                }
            });
        }

        function refreshPage() {
            fetchSosmed(1);
        }

        function initSelection(selected = null) {
            const url = "{!! route('master.icon-social-media.fetch') !!}";
            axios.get(url).then(response => {
                    const {
                        data
                    } = response

                    $(`#${formModalMedsos.selection}`).empty();
                    if (data.result.length > 0) {

                        data.result.forEach(item => {
                            if (selected == null) {
                                if (!item.medsos) {
                                    $(`#${formModalMedsos.selection}`).append(new Option(item.medsos_name, item
                                        .id))
                                }
                            } else {
                                $(`#${formModalMedsos.selection}`).append(new Option(item.medsos_name, item.id))
                            }

                        });


                        if (selected != null) {
                            $("#modalSelectMedia > [value=" + selected + "]").attr("selected", "true");
                        }

                    }

                })
                .catch(error => {
                    showErrorPopup(error)
                })

        }


        $(document).ready(function() {

            $(`#${formModalMedsos.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const id = $(this).attr('data-id');
                const data = {
                    medsos_id: $(`#${formModalMedsos.selection}`).val(),
                    url: $(`#${formModalMedsos.input}`).val()
                }

                if (id != '') {
                    // Update
                    const url = "{!! url('master/social-media/update') !!}/" + id;
                    axios.put(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    refreshPage()
                                    $(`#${formModalMedsos.modal}`).modal("hide");
                                }
                            });
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('medsos_id')) {
                                    $('.invalid-medsos_id').text(data.errors
                                            .medsos_id[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('master.social-media.store') !!}"
                    axios.post(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    refreshPage()
                                    $(`#${formModalMedsos.modal}`).modal("hide");
                                }
                            });
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('medsos_id')) {
                                    $('.invalid-medsos_id').text(data.errors
                                            .medsos_id[0])
                                        .css(
                                            'font-size',
                                            '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER LINK MEDIA SOSIAL</h4>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-3">
                            <div class="d-grid">
                                <button type="button" class="btn btn-primary " onclick="eventCreateData()">
                                    <i class="fas fa-plus"></i>
                                    Akun Media Sosial
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="coverMedsos" class="row">
                        @include('pages.master.social_media.sosmed', compact('medsosPage'))
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.master.social_media.modal_sosmed', compact('sosmed'));
</x-app-layout>
