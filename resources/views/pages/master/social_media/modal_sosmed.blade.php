<div id="modalFormMedsos" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleMedsos" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <form>
                            <div class="mb-3">
                                <label for="modalSelectMedia" class="form-label">Sosial Media</label>
                                <select class="form-select" id="modalSelectMedia"></select>
                                <div class="text-danger text-sm fst-italic invalid-medsos_id"></div>
                            </div>

                            <div class="mb-3">
                                <label for="modalInputURL" class="form-label">URL Akun Sosial Media</label>
                                <input type="text" id="modalInputURL" class="form-control" />

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonAction" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
