@foreach ($medsosPage as $item)
    <div class="col-12 mb-4">
        <div class="card border border-success">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex flex-row">
                                    <div class="mx-2 my-2">{!! $item->socialMedia->medsos_icon !!}</div>
                                    <div class="fw-bold fs-5 my-1 ms-4 text-uppercase">
                                        {{ $item->socialMedia->medsos_name }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="d-flex flex-column">
                                    <div class="card-text mb-2 ms-5">
                                        URL <a href="{{ $item->url }}">{!! $item->url !!}</a>
                                    </div>
                                    <div class="card-text ms-5">
                                        Status
                                        @if ($item->status)
                                            <span class="badge bg-success">Aktif</span>
                                        @else
                                            <span class="badge bg-danger">Tidak Aktif</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="d-grid gap-2">
                            <button class="btn btn-dark" type="button" onclick="eventEditData('{{ $item->id }}')">
                                <i class="fas fa-edit"></i> Ubah
                            </button>
                            <button class="btn {{ $item->status ? 'btn-warning' : 'btn-primary' }}" type="button"
                                onclick="eventChangeStatus('{{ $item->id }}')">
                                @if ($item->status)
                                    <i class="fas fa-times"></i> Nonaktifkan
                                @else
                                    <i class="fas fa-check"></i> Aktifkan
                                @endif


                            </button>
                        </div>
                    </div>
                </div>





            </div>
        </div>
    </div>
@endforeach


<div class="col-12 my-2">
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">

            @if ($medsosPage->onFirstPage())
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
            @else
                <li class="page-item">
                    <button type="button" class="page-link" tabindex="-1"
                        onclick="fetchSosmed('{{ $medsosPage->currentPage() - 1 }}')">Previous</button>
                </li>
            @endif


            @if ($medsosPage->hasMorePages())
                <li class="page-item">
                    <button type="button" class="page-link"
                        onclick="fetchSosmed('{{ $medsosPage->currentPage() + 1 }}')">Next</button>
                </li>
            @else
                <li class="page-item disabled">
                    <button type="button" class="page-link" aria-disabled="true">Next</button>
                </li>
            @endif
        </ul>
    </nav>
</div>
