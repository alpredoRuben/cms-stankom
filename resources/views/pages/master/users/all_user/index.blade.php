@section('title')
    PENGGUNA
@endsection

@section('metadata')
    <meta content="Pengguna" name="keywords" />
    <meta content="LMS Stankom - Halaman Pengguna" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.users') !!}",
            tableName: "tableUsers",
            filters: {
                role: '',
                status: '',
                search: '',
            },
            columns: [{
                    orderable: false,
                    searchable: false,
                },
                {
                    data: "nik",
                    name: "nik",
                },
                {
                    data: "name",
                    name: "name",
                },
                {
                    data: "email",
                    name: "email",
                },
                {
                    data: "rolename",
                    name: "rolename",

                },
                {
                    data: "gender",
                    name: "gender",
                },
                {
                    data: "birthdate",
                    name: "birthdate",
                },

                {
                    data: "phone_number",
                    name: "phone_number",
                },
                {
                    data: "education",
                    name: "education",
                },
                {
                    data: "job_desc",
                    name: "job_desc",
                },
                {
                    data: "alamat",
                    name: "alamat",
                    orderable: false,
                },
                {
                    data: "kode_pos",
                    name: "kode_pos",
                },
                {
                    data: "kelurahan",
                    name: "kelurahan",
                    orderable: false,
                },
                {
                    data: "kecamatan",
                    name: "kecamatan",
                    orderable: false,
                },
                {
                    data: "kabupaten",
                    name: "kabupaten",
                    orderable: false,
                },
                {
                    data: "propinsi",
                    name: "propinsi",
                    orderable: false,
                },
                {
                    data: "activation",
                    name: "activation",
                    orderable: false
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                },
                {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                    className: "align-middle text-center text-nowrap",
                }

            ]
        }

        var filterFormState = {
            roles: "filterSelectRoles",
            status: "filterStatusUser",
            search: "filterSearchUser"
        }

        var formModuleState = {
            modal: 'modalFormDefault',
            title: 'modalTitleDefault',
            alert: 'modalAlertDefault',
            name: 'modalInputDefaultName',
            description: 'modalTexareaDefaultDescription',
            action: 'modalButtonDefault'
        };

        function eventShowFilter() {
            tableState.filters.search = $(`#${filterFormState.search}`).val();
            reloadDatatable();
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.role = '';
            tableState.filters.status = '';
            tableState.filters.search = '';
            reloadDatatable();
        }

        function eventImportData() {
            Swal.fire({
                title: "Pilih file CSV",
                input: 'file',
                inputAttributes: {
                    'accept': '.csv',
                    'aria-label': 'Upload file csv anda'
                },
                showCancelButton: true,
                confirmButtonText: 'Upload',
            }).then((file) => {
                if (file.isConfirmed && file.value) {
                    var formData = new FormData();
                    formData.append('import_file', file.value);

                    axios.post("{!! route('file_manager.users.import.csv') !!}", formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).then(response => {
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: response.data.message,
                        });
                        eventRefreshData();
                    }).catch(error => {
                        showErrorPopup(error)
                    })

                }
            })

        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.users.main.change_status') !!}";
            const msgAlert = status ? "menonaktifkan akun ini" : "mengaktifkan akun ini"


            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id,
                        status: status
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData()
                        });
                    }).catch((error) => {});
                }
            });
        }

        function eventUploadImage(id, status) {
            console.log(id);

            Swal.fire({
                title: 'Upload Foto',
                input: 'file',
                inputAttributes: {
                    'accept': '.jpg,.png,.jpeg',
                    'aria-label': 'Upload foto profil'
                }
            }).then(res => {
                if (res.isConfirmed) {
                    const url = "{!! route('master.users.main.upload_photo') !!}"
                    var formData = new FormData();
                    formData.append('user_id', id);
                    formData.append('avatar', res.value);

                    axios.post(url, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(response => {
                        if (response.status == 201 || response.status == 200) {
                            const {
                                data
                            } = response;

                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    eventRefreshData();
                                }
                            });
                        }

                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        showErrorPopup(error)
                    });

                }
            })

        }

        function eventExportExcel() {
            let types = tableState.filters.role == '' ? 'all' : tableState.filters.role
            window.location.href = "{!! url('master/users/main/show/export') !!}/" + types;
        }

        $(document).ready(function() {

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history;
                        d.status = tableState.filters.status;
                        d.role = tableState.filters.role;
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 3
                }

            });

            dTables.on("xhr", function(e, settings, json) {});

            $(`#${filterFormState.roles}`).change(function(e) {
                e.preventDefault();
                tableState.filters.role = $(this).val();
            });

            $(`#${filterFormState.status}`).change(function(e) {
                e.preventDefault();
                tableState.filters.status = $(this).val();
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER DATA PENGGUNA</h4>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-4">
                            <div class=" mb-3">
                                <label for="filterSelectRoles" class="form-label">Kelompok Pengguna</label>
                                <select id="filterSelectRoles" class="form-control form-control-lg form-select">
                                    <option value="" selected>Semuanya</option>
                                    @foreach ($roles as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class=" mb-3">
                                <label for="filterStatusUser" class="form-label">Status Pengguna</label>
                                <select id="filterStatusUser" class="form-control form-control-lg form-select">
                                    <option value="" selected>Semuanya</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class=" mb-3">
                                <label for="filterSearchUser" class="form-label">Cari Kata Kunci</label>
                                <input type="text" id="filterSearchUser" class="form-control form-control-lg"
                                    placeholder="Kata Kunci : Nama Lengkap, Email, Tgl Lahir" />
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="d-grid">
                                <button type="button" class="btn btn-warning " onclick="eventShowFilter()">
                                    <i class="fas fa-filter"></i>
                                    Pencarian
                                </button>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="d-grid">
                                <button type="button" class="btn btn-success" onclick="eventRefreshData()">
                                    <i class="fas fa-refresh"></i>
                                    Refresh Data
                                </button>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="d-grid">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary">Operasi Data</button>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('master.users.main.create') }}" class="dropdown-item">
                                                <i class="fas fa-plus"></i>
                                                &nbsp; Pengguna
                                            </a>
                                        </li>
                                        <li>
                                            <button type="button" class="dropdown-item" onclick="eventExportExcel()">
                                                <i class="fas fa-file-export"></i>
                                                &nbsp; Export File
                                                </a>
                                            </button>
                                        </li>

                                        <li>
                                            <a class="dropdown-item"
                                                href="{{ route('file_manager.users.download.csv') }}">
                                                <i class="fas fa-download"></i>
                                                &nbsp; Download File
                                            </a>
                                        </li>

                                        <li>
                                            <button type="button" class="dropdown-item" onclick="eventImportData()">
                                                <i class="fas fa-file-import"></i>
                                                &nbsp; Import File
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableUsers"
                                    class="table  table-bordered-less table-striped table-condensed  table-hover"
                                    width="100%">
                                    <thead class="bg-primary text-white">
                                        <tr>
                                            <th class="bg-success">NO</th>
                                            <th class="bg-success">NIK</th>
                                            <th class="bg-success">NAMA LENGKAP</th>
                                            <th>EMAIL</th>
                                            <th>KELOMPOK PENGGUNA</th>
                                            <th>JENIS KELAMIN</th>
                                            <th>TGL. LAHIR</th>
                                            <th>NO. PONSEL</th>
                                            <th>PENDIDIKAN</th>
                                            <th>PEKERJAAN</th>
                                            <th>ALAMAT</th>
                                            <th>KODE POS</th>
                                            <th>KELURAHAN</th>
                                            <th>KECAMATAN</th>
                                            <th>KABUPATEN</th>
                                            <th>PROPINSI</th>
                                            <th>AKTIVASI AKUN</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
