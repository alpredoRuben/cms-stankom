@section('title')
    PROFIL
@endsection

@section('metadata')
    <meta content="Profil" name="keywords" />
    <meta content="LMS Stankom - Halaman Profil" name="description" />
@endsection

@section('stylesheets')
    <style>
        .avatar {
            vertical-align: middle;
            width: 250px;
            height: 350px;
            border-radius: 10%;
            border: 1px solid #ccc
        }
    </style>
@endsection


@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var dataUser = @json($user);
        var defaultImage = "{!! asset('images/default_user.png') !!}";

        $(document).ready(function() {
            console.log(dataUser)

            if (dataUser.user_profile != null && dataUser.user_profile.photo != null) {
                $("#imgAvatar").attr("src", dataUser.user_profile.photo)
            } else {
                $("#imgAvatar").attr("src", defaultImage)
            }

            $("#btnUploadFile").click(function(e) {
                e.preventDefault();
                $("#inputAvatar").trigger('click')
            });

            $("#inputAvatar").change(function(e) {
                e.preventDefault();
                const listFile = e.target.files;
                const formData = new FormData();
                formData.append("avatar", listFile[0])
                const url = "{!! route('profile.avatar.upload') !!}"
                axios.post(url, formData).then(response => {
                    console.log(response);

                    const {
                        data
                    } = response

                    Swal.fire({
                        title: 'BERHASIL',
                        icon: 'success',
                        text: data.message,
                        confirmButtonText: 'OK',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $("#imgAvatar").attr("src", data.data.photo)
                            $(".avatarIconUser").attr("src", data.data.photo)
                        }
                    });
                }).catch(error => {
                    showErrorPopup(error)
                })

            });


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                PROFIL
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('profile.edit') }}" class="btn btn-primary btn-sm">
                                            <i class="fas fa-edit"></i>
                                            Ubah
                                        </a>
                                    </div>

                                    <div class="d-grid">
                                        <a href="{{ route('profile.edit_password_view') }}" class="btn btn-dark btn-sm">
                                            <i class="fas fa-key"></i>
                                            Ganti Password
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row mb-3 justify-items-center">
                        <!-- AVATAR -->
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <img id="imgAvatar" src="" class="m-2 avatar" />

                                    </div>
                                </div>

                                <div class="col-12">
                                    <input type="file" id="inputAvatar" name="inputAvatar" class="form-control"
                                        accept=".png,.jpg,.jpeg" hidden>
                                    <div class="d-grid">
                                        <button id="btnUploadFile" type="button" type="button"
                                            class="btn btn-primary btn-sm">
                                            <i class="fas fa-upload"></i>
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INFO -->
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <div class="row">

                                <!-- NIK -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="nik" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->nik ?? '' }}" readonly />
                                        <label for="nik" class="form-label">No. KTP (NIK)</label>
                                    </div>
                                </div>

                                <!-- Nama Lengkap -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="full_name" class="form-control text-primary" type="text"
                                            value="{{ $user->name }}" readonly />
                                        <label for="full_name" class="form-label">Nama Lengkap</label>
                                    </div>
                                </div>

                                <!-- Email -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="email" class="form-control text-primary" type="text"
                                            value="{{ $user->email }}" readonly />
                                        <label for="email" class="form-label">Email</label>
                                    </div>
                                </div>

                                <!-- Kelompok Pengguna -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">

                                        <input id="role_name" class="form-control text-primary" type="text"
                                            value="{{ $user->roles[0]->name }}" readonly />
                                        <label for="role_name" class="form-label">Kelompok Pengguna</label>
                                    </div>
                                </div>

                                <!-- Gender -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="gender" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile && $user->userProfile->gender ? ucwords($user->userProfile->gender) : '' }}"
                                            readonly />
                                        <label for="gender" class="form-label">Jenis Kelamin</label>
                                    </div>
                                </div>

                                <!-- No. Ponsel -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="phone_number" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile && $user->userProfile->phone_number ? $user->userProfile->phone_number : '' }}"
                                            readonly />
                                        <label for="phone_number" class="form-label">No. Ponsel / HP</label>
                                    </div>
                                </div>

                                <!-- Tanggal Lahir -->
                                <div class="col-12">
                                    <div class="form-floating mb-3">
                                        <input id="birthdate" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile && $user->userProfile->birth_date ? \Illuminate\Support\Carbon::parse($user->userProfile->birth_date)->format('d F Y') : '' }}"
                                            readonly />
                                        <label for="birthdate" class="form-label">Tanggal Lahir</label>
                                    </div>
                                </div>


                                <!-- Tanggal Bergabung -->
                                <div class="col-12">
                                    <div class="form-floating mb-3">
                                        <input id="date_joined" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile && $user->userProfile->date_joined ? \Illuminate\Support\Carbon::parse($user->userProfile->date_joined)->format('d F Y') : '' }}"
                                            readonly />
                                        <label for="date_joined" class="form-label">Tanggal Bergabung</label>
                                    </div>
                                </div>

                                <!-- Alamat -->
                                <div class="col-12">
                                    <div class="form-floating mb-3">
                                        <textarea id="address" class="form-control text-primary" style="height: 85px;" disabled>{{ $user->userProfile->phone_number ?? '' }}</textarea>
                                        <label for="address" class="form-label">Alamat</label>
                                    </div>
                                </div>


                                <!-- Kode Pos -->
                                <div class="col-2">
                                    <div class="form-floating mb-3">
                                        <input id="postal_code" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->ward->postalCodes->code ?? '' }}"
                                            readonly />
                                        <label for="postal_code" class="form-label">Kode Pos</label>
                                    </div>
                                </div>

                                <!-- Kelurahan -->
                                <div class="col-5">
                                    <div class="form-floating mb-3">
                                        <input id="ward" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->ward->name ?? '' }}" readonly />
                                        <label for="ward" class="form-label">Kelurahan</label>
                                    </div>
                                </div>

                                <!-- Kecamatan -->
                                <div class="col-5">
                                    <div class="form-floating mb-3">
                                        <input id="district" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->ward->district->name ?? '' }}" readonly />
                                        <label for="district" class="form-label">Kecamatan</label>
                                    </div>
                                </div>

                                <!-- Kabupaten -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="city" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->ward->district->city->name ?? '' }}"
                                            readonly />
                                        <label for="city" class="form-label">Kabupaten/Kota</label>
                                    </div>
                                </div>

                                <!-- Propinsi -->
                                <div class="col-6">
                                    <div class="form-floating mb-3">
                                        <input id="province" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->ward->district->city->province->name ?? '' }}"
                                            readonly />
                                        <label for="province" class="form-label">Propinsi</label>
                                    </div>
                                </div>


                                <!-- Pendidikan -->
                                <div class="col-12">
                                    <div class="form-floating mb-3">
                                        <input id="education" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->education->name ?? '' }}" readonly />
                                        <label for="education" class="form-label">Pendidikan Terakhir</label>
                                    </div>
                                </div>

                                <!-- Pekerjaan -->
                                <div class="col-12">
                                    <div class="form-floating mb-3">
                                        <input id="profession" class="form-control text-primary" type="text"
                                            value="{{ $user->userProfile->job_profession->name ?? '' }}" readonly />
                                        <label for="profession" class="form-label">Pekerjaan</label>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
