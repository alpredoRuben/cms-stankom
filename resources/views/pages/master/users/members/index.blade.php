@section('title')
    PESERTA
@endsection

@section('metadata')
    <meta content="Peserta" name="keywords" />
    <meta content="LMS Stankom - Halaman Peserta" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.master.users') !!}",
            tableName: "tableTrainers",
            filters: {
                rolename: 'peserta',
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    searchable: false,
                },
                {
                    data: "nik",
                    name: "nik",
                },
                {
                    data: "name",
                    name: "name",
                },
                {
                    data: "email",
                    name: "email",
                },
                {
                    data: "rolename",
                    name: "rolename",

                },
                {
                    data: "gender",
                    name: "gender",
                },
                {
                    data: "birthdate",
                    name: "birthdate",
                },

                {
                    data: "phone_number",
                    name: "phone_number",
                },
                {
                    data: "education",
                    name: "education",
                },
                {
                    data: "job_desc",
                    name: "job_desc",
                },
                {
                    data: "alamat",
                    name: "alamat",
                    orderable: false,
                },
                {
                    data: "kode_pos",
                    name: "kode_pos",
                },
                {
                    data: "kelurahan",
                    name: "kelurahan",
                    orderable: false,
                },
                {
                    data: "kecamatan",
                    name: "kecamatan",
                    orderable: false,
                },
                {
                    data: "kabupaten",
                    name: "kabupaten",
                    orderable: false,
                },
                {
                    data: "propinsi",
                    name: "propinsi",
                    orderable: false,
                },
                {
                    data: "activation",
                    name: "activation",
                    orderable: false
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                },

            ]
        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        var formModuleState = {
            modal: 'modalFormDefault',
            title: 'modalTitleDefault',
            alert: 'modalAlertDefault',
            name: 'modalInputDefaultName',
            description: 'modalTexareaDefaultDescription',
            action: 'modalButtonDefault'
        };


        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('master.users.main.change_status') !!}";
            const msgAlert = status ? "menonaktifkan akun ini" : "mengaktifkan akun ini"


            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id,
                        status: status
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData()
                        });
                    }).catch((error) => {});
                }
            });
        }

        $(document).ready(function() {
            console.log(formModuleState);

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history;
                        d.rolename = tableState.filters.rolename;
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 3
                }

            });

            dTables.on("xhr", function(e, settings, json) {});

            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MASTER DATA PESERTA</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => route('master.users.main.show.export', 'peserta'),
                        'addUrl' => '',
                        'addTitle' => 'Peserta',
                        'addAction' => false,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableTrainers" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead class="bg-primary text-white">
                                        <tr>
                                            <th class="bg-success">NO</th>
                                            <th class="bg-success">NIK</th>
                                            <th class="bg-success">NAMA LENGKAP</th>
                                            <th>EMAIL</th>
                                            <th>KELOMPOK PENGGUNA</th>
                                            <th>JENIS KELAMIN</th>
                                            <th>TGL. LAHIR</th>
                                            <th>NO. PONSEL</th>
                                            <th>PENDIDIKAN</th>
                                            <th>PEKERJAAN</th>
                                            <th>ALAMAT</th>
                                            <th>KODE POS</th>
                                            <th>KELURAHAN</th>
                                            <th>KECAMATAN</th>
                                            <th>KABUPATEN</th>
                                            <th>PROPINSI</th>
                                            <th>AKTIVASI AKUN</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
