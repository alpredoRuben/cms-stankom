@section('title')
    UPDATE PROFIL
@endsection

@section('metadata')
    <meta content="Update Profil" name="keywords" />
    <meta content="LMS Stankom - Halaman Update Profil" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
    <style>
        a,
        a:hover {
            color: rgb(11, 4, 132)
        }

        .outline-eyes {
            padding-bottom: 16px;
        }
    </style>
@endsection


@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        function refreshErrorTagMessage() {
            $('.invalid-new_password').text("")
            $('.invalid-confirm_new_password').text("")
            $('.invalid-old_password').text("")
        }

        function eventShowPassword() {
            if ($("#show_old_password input").attr("type") == "text") {
                $('#show_old_password input').attr("type", "password");
            } else {
                $('#show_old_password input').attr("type", "text");
            }

            if ($("#show_new_password input").attr("type") == "text") {
                $('#show_new_password input').attr("type", "password");
            } else {
                $('#show_new_password input').attr("type", "text");
            }

            if ($("#show_confirm_password input").attr("type") == "text") {
                $('#show_confirm_password input').attr("type", "password");
            } else {
                $('#show_confirm_password input').attr("type", "text");
            }
        }

        function eventSaveNewPassword() {
            refreshErrorTagMessage();
            const url = "{!! route('profile.change_password') !!}"
            axios.post(url, {
                old_password: $("#inputOldPassword").val(),
                new_password: $("#inputNewPassword").val(),
                confirm_new_password: $("#inputConfirmNewPassword").val()
            }).then((response) => {
                console.log(response);
                Swal.fire({
                    title: 'BERHASIL',
                    text: response.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                }).then((r) => {
                    if (r.isConfirmed) {
                        document.getElementById('logout-form').submit()
                    }
                });
            }).catch((error) => {
                // showErrorPopup(error);
                console.log(error.response)
                const {
                    status,
                    data
                } = error.response;

                switch (status) {
                    case 422:
                        if (data.errors.hasOwnProperty('old_password')) {
                            $('.invalid-old_password').text(data.errors.new_password[0])
                                .css('font-size', '0.8rem');
                        }


                        if (data.errors.hasOwnProperty('new_password')) {
                            $('.invalid-new_password').text(data.errors.new_password[0])
                                .css('font-size', '0.8rem');
                        }

                        if (data.errors.hasOwnProperty('confirm_new_password')) {
                            $('.invalid-confirm_new_password').text(data.errors
                                    .confirm_new_password[0])
                                .css('font-size', '0.8rem');
                        }

                        break;

                    default:
                        showErrorPopup(error)
                        break;
                }
            });
        }
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                FORM UBAH PASSWORD
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('profile.preview') }}" class="btn btn-primary btn-sm">
                                            <i class="fas fa-eye"></i>&nbsp;
                                            Profil
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3 justify-content-center">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-12">
                                    <div id="show_old_password" class="mb-4">
                                        <label for="inputOldPassword" class="form-label fw-bold">
                                            Password Lama
                                        </label>
                                        <input type="password" id="inputOldPassword" placeholder="Password Lama"
                                            class="form-control form-control-lg" />
                                        <div class="text-danger text-sm fst-italic invalid-old_password"></div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div id="show_new_password" class="mb-4">
                                        <label for="inputNewPassword" class="form-label fw-bold">
                                            Password Baru
                                        </label>
                                        <input type="password" id="inputNewPassword" placeholder="Password Baru"
                                            class="form-control form-control-lg" />
                                        <div class="text-danger text-sm fst-italic invalid-new_password"></div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div id="show_confirm_password" class="mb-4">
                                        <label for="inputConfirmNewPassword" class="form-label fw-bold">
                                            Konfirmasi Password Baru
                                        </label>

                                        <input type="password" id="inputConfirmNewPassword"
                                            placeholder="Konfirmasi Password Baru"
                                            class="form-control form-control-lg" />
                                        <div class="text-danger text-sm fst-italic invalid-confirm_new_password">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="mb-4">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input p-2"
                                                onclick="eventShowPassword()">&nbsp;
                                            Perlihatkan Password
                                        </label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="button" class="btn btn-primary" onclick="eventSaveNewPassword()">
                                            <i class="fas fa-save"></i>
                                            Ubah Password
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

</x-app-layout>
