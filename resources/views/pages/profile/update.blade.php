@section('title')
    UPDATE PROFIL
@endsection

@section('metadata')
    <meta content="Update Profil" name="keywords" />
    <meta content="LMS Stankom - Halaman Update Profil" name="description" />
@endsection

@section('stylesheets')
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection


@section('javascripts')
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var dataUser = @json($user);
        var dataGenders = @json($genders);

        const formState = {
            nik: "inputNik",
            name: "inputName",
            email: "inputEmail",
            gender: "selectGender",
            phone: "inputPhone",
            birthdate: "inputBirthDate",
            address: "inputAddress",
            ward: "selectWard",
            postal_code: "inputPostalCode",
            district: "inputDistrict",
            city: "inputCity",
            province: "inputProvince",
            education: "selectEducation",
            job: "selectJob",
            action: "buttonUpdateAction",
            date_joined: "inputDateJoined"
        };

        function refreshErrorTagMessage() {
            $('.invalid-name').text("")
            $('.invalid-email').text("")
            $('.invalid-gender').text("")
        }

        function initialMCDatepicker(element, data) {
            MCDatepicker.create({
                el: `#${element}`,
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                closeOnBlur: true,
                selectedDate: new Date(data),
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });
        }

        function initialSetElement() {
            $(`#${formState.nik}`).val(dataUser?.user_profile?.nik)
            $(`#${formState.name}`).val(dataUser?.name)
            $(`#${formState.email}`).val(dataUser?.email)
            $(`#${formState.phone}`).val(dataUser?.user_profile?.phone_number)
            $(`#${formState.birthdate}`).val(dataUser?.user_profile?.birth_date)
            $(`#${formState.date_joined}`).val(dataUser?.user_profile?.date_joined)
            $(`#${formState.address}`).val(dataUser?.user_profile?.address)
        }

        function initWard(selected = null) {
            let url = "{!! route('master.location.ward.all') !!}";
            select2WithSelection($(`#${formState.ward}`), url, "Pilih Kelurahan", selected, true)
        }

        function initEducation(selected = null) {
            let url = "{!! route('master.education.all') !!}";
            select2WithSelection($(`#${formState.education}`), url, "Pilih Pendidikan", selected)
        }

        function initJob(selected = null) {
            let url = "{!! route('master.profession.all') !!}";
            select2WithSelection($(`#${formState.job}`), url, "Pilih Pekerjaan", selected)
        }

        function initLocationDescription(id) {
            const url = "{{ url('master/location/ward/detail') }}/" + id
            if (id && id != null) {
                axios.get(url).then(response => {
                    const {
                        status,
                        data: {
                            result
                        }
                    } = response
                    $(`#${formState.postal_code}`).val(result.postal_code)
                    $(`#${formState.district}`).val(result.district_name)
                    $(`#${formState.province}`).val(result.province_name)
                    $(`#${formState.city}`).val(result.city_name)
                }).catch(error => {
                    showErrorPopup(error)
                })
            }
        }



        $(document).ready(function() {
            initialSetElement();
            initSelectBoxWithData(dataGenders, $(`#${formState.gender}`), dataUser?.user_profile?.gender,
                'Jenis Kelamin');
            initialMCDatepicker(formState.birthdate, dataUser?.user_profile?.birth_date)
            initialMCDatepicker(formState.date_joined, dataUser?.user_profile?.date_joined)
            initWard(dataUser?.user_profile?.ward);
            initLocationDescription(dataUser?.user_profile?.ward_id)
            initEducation(dataUser?.user_profile?.education)
            initJob(dataUser?.user_profile?.job_profession)

            $(`#${formState.ward}`).on('select2:select', function(e) {
                const params = e.params.data;
                initLocationDescription(params.id)
            });


            /** Submit Update */
            $(`#${formState.action}`).click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                let data = {
                    name: $(`#${formState.name}`).val(),
                    email: $(`#${formState.email}`).val(),
                    gender: $(`#${formState.gender}`).val(),
                    phone: $(`#${formState.phone}`).val(),
                    birth_date: $(`#${formState.birthdate}`).val(),
                    date_joined: $(`#${formState.date_joined}`).val(),
                    address: $(`#${formState.address}`).val(),
                    ward_id: Number($(`#${formState.ward}`).val()) || null,
                    role_id: Number($(`#${formState.roles}`).val()) || null,
                    job_profession_id: Number($(`#${formState.job}`).val()) || null,
                    education_id: Number($(`#${formState.education}`).val()) || null,
                }

                if (dataUser?.user_profile?.ward_id != null && $(`#${formState.ward}`).val() == null) {
                    data.ward_id = dataUser?.user_profile?.ward_id;
                }

                if (dataUser?.user_profile?.job_profession_id != null && $(`#${formState.job}`).val() ==
                    null) {
                    data.job_profession_id = dataUser?.user_profile?.job_profession_id;
                }

                if (dataUser?.user_profile?.education_id != null && $(`#${formState.education}`).val() ==
                    null) {
                    data.education_id = dataUser?.user_profile?.education_id;
                }

                const url = "{!! route('profile.update') !!}";
                axios.post(url, data).then(response => {
                    Swal.fire({
                        title: 'BERHASIL',
                        icon: 'success',
                        text: response.data.message,
                        confirmButtonText: 'OK',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = "{!! route('profile.preview') !!}";
                        }
                    });
                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:

                            if (data.errors.hasOwnProperty('name')) {
                                $('.invalid-name').text(data.errors.name[0])
                                    .css('font-size', '0.9rem');
                            } else {
                                $('.invalid-name').text("")
                            }

                            if (data.errors.hasOwnProperty('email')) {
                                $('.invalid-email').text(data.errors.email[0])
                                    .css('font-size', '0.9rem');
                            } else {
                                $('.invalid-email').text("")
                            }

                            if (data.errors.hasOwnProperty('gender')) {
                                $('.invalid-gender').text(data.errors.gender[0])
                                    .css('font-size', '0.9rem');
                            } else {
                                $('.invalid-gender').text("")
                            }
                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                })

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                FORM EDIT PROFIL
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('profile.preview') }}" class="btn btn-primary btn-sm">
                                            <i class="fas fa-eye"></i>&nbsp;
                                            Profil
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- NIK, NAMA, EMAIL -->
                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <!-- FULL NAME -->
                                <label for="inputNik" class="form-label fw-bold">NIK</label>
                                <input id="inputNik" class="form-control form-control-lg" type="text"
                                    placeholder="NIK" value="" readonly />
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <!-- FULL NAME -->
                                <label for="inputName" class="form-label fw-bold">Nama Lengkap</label>
                                <input id="inputName" class="form-control p-4" type="text"
                                    placeholder="Nama Lengkap" />
                                <div class="text-danger text-sm fst-italic invalid-name"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <!-- EMAIL -->
                                <label for="inputEmail" class="form-label fw-bold">Email</label>
                                <input id="inputEmail" class="form-control p-4" type="text" placeholder="Email" />
                                <div class="text-danger text-sm fst-italic invalid-email"></div>
                            </div>
                        </div>
                    </div>

                    <!-- JENIS KELAMIN, TANGGAL LAHIR, NOMOR PONSEL/HP -->
                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <!-- GENDER -->
                                <label for="selectGender" class="form-label fw-bold">Jenis Kelamin</label>
                                <select id="selectGender" class="form-select form-select-lg"
                                    data-placeholder="Jenis Kelamin"></select>
                                <div class="text-danger text-sm fst-italic invalid-gender"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputBirthDate" class="form-label fw-bold">Tanggal Lahir</label>
                                <input id="inputBirthDate" class="form-control form-control-lg text-success"
                                    type="text" />
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputPhone" class="form-label fw-bold">No. Ponsel / HP</label>
                                <input id="inputPhone" class="form-control form-control-lg text-success"
                                    type="text" />
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputDateJoined" class="form-label fw-bold">Tanggal Gabung</label>
                                <input id="inputDateJoined" class="form-control form-control-lg text-success"
                                    type="text" />
                            </div>
                        </div>
                    </div>


                    <!-- ADDRESS -->
                    <div class="row mb-2 p-3">
                        <!-- Alamat -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputAddress" class="form-label fw-bold">Alamat</label>
                                <textarea id="inputAddress" class="form-control form-control-lg" style="height: 85px;"></textarea>

                            </div>
                        </div>

                        <!-- Kelurahan -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="selectWard" class="form-label fw-bold">Kelurahan</label>
                                <select id="selectWard" class="form-select form-select-lg"
                                    data-placeholder="Pilih Kelurahan"></select>

                            </div>
                        </div>

                        <!-- Kode Pos -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputPostalCode" class="form-label fw-bold">Kode Pos</label>
                                <input id="inputPostalCode" class="form-control form-control-lg text-success"
                                    type="text" readonly />
                            </div>
                        </div>


                        <!-- Kecamatan -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputDistrict" class="form-label fw-bold">Kecamatan</label>
                                <input id="inputDistrict" class="form-control form-control-lg text-success"
                                    type="text" readonly />
                            </div>
                        </div>

                        <!-- Kabupaten -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputCity" class="form-label fw-bold">Kabupaten/Kota</label>
                                <input id="inputCity" class="form-control form-control-lg text-success"
                                    type="text" readonly>
                            </div>
                        </div>

                        <!-- Propinsi -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputProvince" class="form-label fw-bold">Propinsi</label>
                                <input id="inputProvince" class="form-control form-control-lg text-success"
                                    type="text" readonly />
                            </div>
                        </div>
                    </div>


                    <!-- KELOMPOK PENGGUNA, PENDIDIKAN, PEKERJAAN -->
                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="selectEducation" class="form-label fw-bold">Pendidikan Terakhir</label>
                                <select id="selectEducation" class="form-select form-select-lg"
                                    data-placeholder="Pendidikan Terakhir"></select>
                            </div>
                        </div>

                        <!-- Pekerjaan -->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="selectJob" class="form-label fw-bold">Pekerjaan</label>
                                <select id="selectJob" class="form-select form-select-lg"
                                    data-placeholder="Pekerjaan"></select>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p3">
                        <div class="col-12">
                            <div class="mb-3">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-dark" id="buttonUpdateAction">
                                        <i class="fas fa-save"></i> &nbsp;
                                        UPDATE
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
