@section('title')
    TAMBAH KONTENT
@endsection

@section('metadata')
    <meta content="Tentang Kami" name="keywords" />
    <meta content="LMS Stankom - Halaman Tentang Kami" name="description" />
@endsection


@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var formAboutUsState = {
            photo: "inputPhoto",
            content: "inputContent",
            action: "buttonSaveAction",
        }

        $(document).ready(function() {
            CKEDITOR.replace(`${formAboutUsState.content}`, {
                extraPlugins: 'print,format,font,editorplaceholder,colorbutton,colordialog',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Isi kontent tentang kami di sini...',
                removeButtons: 'PasteFromWord'
            });

            $(`#${formAboutUsState.action}`).click(function(e) {
                e.preventDefault();
                console.log("RUNN CLICK");

                var formData = new FormData();
                formData.append('content', CKEDITOR.instances?.[`${formAboutUsState.content}`].getData())
                formData.append('photo', $(`#${formAboutUsState.photo}`).prop('files')[0])

                // Store
                const url = "{!! route('setting.about_us.store') !!}"
                axios.post(url, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log("RESPONSE", response);
                    const {
                        status,
                        data
                    } = response

                    if (status == 200 || status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'BERHASIL',
                            text: data.message,
                        });

                        window.location.href = "{!! route('setting.about_us.list') !!}"
                    }


                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('contact')) {
                                $('.invalid-contact').text(data.errors.contact[0])
                                    .css('font-size', '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('photo')) {
                                $('.invalid-photo').text(data.errors.photo[0])
                                    .css('font-size', '0.9rem');
                            }
                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM TAMBAH KONTEN TENTANG KAMI
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('setting.about_us.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="inputPhoto" class="form-label fw-bold">Foto</label>
                                <input type="file" id="inputPhoto" class="form-control form-control-lg" />
                                <div class="text-danger text-sm fst-italic invalid-photo"></div>
                            </div>

                            <div class="mb-3">
                                <label for="inputContent" class="form-label fw-bold">Isi Kontent</label>
                                <textarea cols="80" id="inputContent" name="inputContent" class="form-control" rows="10" data-sample-short></textarea>
                                <div class="text-danger text-sm fst-italic invalid-content"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-primary" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
