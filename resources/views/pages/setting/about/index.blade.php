@section('title')
    TENTANG KAMI
@endsection

@section('metadata')
    <meta content="Tentang Kami" name="keywords" />
    <meta content="LMS Stankom - Halaman Tentang Kami" name="description" />
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.web.about_us') !!}",
            tableName: "tableAboutUs",
            columns: [{
                    data: "content",
                    name: "content",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "filepath",
                    name: "filepath",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }
            ]
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            reloadDatatable();
        }

        function eventCreateAbout() {
            window.location.href = "{!! route('setting.about_us.create') !!}"
        }

        $(document).ready(function() {
            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                },
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                scrollX: true,
            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
                if (json.data.length > 0) {
                    $("#btnCreateAbout").attr('disabled', true)
                } else {
                    $("#btnCreateAbout").attr('disabled', false)
                }
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">DATA TENTANG KAMI</h4>
                        </div>
                    </div>

                    @if ($totalAboutUs <= 0)
                        <div class="row mb-2 p-3">
                            <div class="col-2">
                                <div class="d-grid">
                                    <button id="btnCreateAbout" type="button" class="btn btn-primary"
                                        onclick="eventCreateAbout()">
                                        <i class="fas fa-plus"></i>
                                        Konten Baru
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableAboutUs" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>ISI KONTEN</th>
                                            <th>FILEPATH</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.contact.modal_contact')
</x-app-layout>
