@section('title')
    BANNER
@endsection

@section('metadata')
    <meta content="Banner" name="keywords" />
    <meta content="LMS Stankom - Halaman Banner" name="description" />
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.web.banner') !!}",
            tableName: "tableBanner",
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "banner_title",
                    name: "banner_title",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "banner_order",
                    name: "banner_order",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "banner_content",
                    name: "banner_content",
                    orderable: false,
                    className: "align-middle text-wrap",
                },

                {
                    data: "banner_link",
                    name: "banner_link",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },

                {
                    data: "images",
                    name: "images",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },

                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }
            ]
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            reloadDatatable();
        }

        function eventCreateAbout() {
            window.location.href = "{!! route('setting.about_us.create') !!}"
        }

        $(document).ready(function() {
            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 3
                }
            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
                if (json.data.length > 0) {
                    $("#btnCreateAbout").attr('disabled', true)
                } else {
                    $("#btnCreateAbout").attr('disabled', false)
                }
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row p-3">
                        <div class="col-12">
                            <h4 class="card-title">BANNER</h4>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-2">
                            <div class="d-grid">
                                <a href="{{ route('setting.banner.create') }}" class="btn btn-primary">
                                    <i class="fas fa-plus"></i>
                                    Banner Baru
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableBanner" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">JUDUL BANNER</th>
                                            <th class="bg-success text-white">URUTAN</th>
                                            <th>DESKRIPSI</th>
                                            <th>LINK</th>
                                            <th>GAMBAR</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.contact.modal_contact')
</x-app-layout>
