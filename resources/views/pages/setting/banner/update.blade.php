@section('title')
    EDIT BANNER
@endsection

@section('metadata')
    <meta content="Edit Banner" name="keywords" />
    <meta content="LMS Stankom - Halaman Edit Banner" name="description" />
@endsection


@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var formBannerState = {
            title: "bannerTitle",
            photo: "bannerPhoto",
            order: "bannerOrder",
            content: "bannerContent",
            action: "buttonSaveAction",
        }

        function refreshErrorTagMessage() {
            $('.invalid-banner_title').text("")
            $('.invalid-banner_image').text("")
            $('.invalid-banner_order').text("")
            $('.invalid-banner_content').text("")
        }

        $(document).ready(function() {

            $(`#${formBannerState.action}`).click(function(e) {
                refreshErrorTagMessage()
                e.preventDefault();

                var formData = new FormData()

                formData.append('banner_title', $(`#${formBannerState.title}`).val())
                formData.append('banner_order', $(`#${formBannerState.order}`).val())
                formData.append('banner_content', $(`#${formBannerState.content}`).val())
                formData.append('banner_image', $(`#${formBannerState.photo}`).prop('files')[0])

                // Store
                const url = "{!! route('setting.banner.update', $record->id) !!}"
                axios.post(url, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    }
                }).then(response => {
                    const {
                        status,
                        data
                    } = response

                    if (status == 200 || status == 201) {
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('setting.banner.list') !!}"
                            }
                        });
                    }


                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('banner_title')) {
                                $('.invalid-banner_title').text(data.errors.banner_title[0])
                                    .css('font-size', '0.9rem');
                            }


                            if (data.errors.hasOwnProperty('banner_content')) {
                                $('.invalid-banner_content').text(data.errors.banner_content[0])
                                    .css('font-size', '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('banner_image')) {
                                $('.invalid-banner_image').text(data.errors.banner_image[0])
                                    .css('font-size', '0.9rem');
                            }


                            if (data.errors.hasOwnProperty('banner_order')) {
                                $('.invalid-banner_order').text(data.errors.banner_order[0])
                                    .css('font-size', '0.9rem');
                            }
                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM EDIT BANNER
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('setting.banner.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="bannerTitle" class="form-label fw-bold">Judul Banner</label>
                                <input type="text" id="bannerTitle" class="form-control form-control-lg"
                                    placeholder="Judul Banner..." value="{{ $record->banner_title }}" />
                                <div class="text-danger text-sm fst-italic invalid-banner_title"></div>
                            </div>

                            <div class="mb-3">
                                <label for="bannerContent" class="form-label fw-bold">CTA (Content)</label>
                                <textarea id="bannerContent" class="form-control form-textarea" style="height: 80px;">{{ $record->banner_content }}</textarea>
                                <div class="text-danger text-sm fst-italic invalid-banner_content"></div>
                            </div>

                            <div class="mb-3">
                                <label for="bannerOrder" class="form-label fw-bold">Urutan</label>
                                <input type="number" id="bannerOrder" class="form-control form-control-lg"
                                    value="{{ $record->banner_order }}" />
                                <div class="text-danger text-sm fst-italic invalid-banner_order"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="d-flex justify-content-start">
                                <div class="mb-3">
                                    <label for="infoPhoto" class="form-label fw-bold">Gambar Sebelumnya</label> <br />
                                    <a href="{{ $record->banner_image }}" class="btn btn-primary">Preview Gambar</a>
                                </div>
                                <div class="mb-3 mx-5">
                                    <label for="bannerPhoto" class="form-label fw-bold">Unggah Gambar</label>
                                    <input type="file" id="bannerPhoto" class="form-control form-control-lg"
                                        accept="images/*" />
                                    <div class="text-danger text-sm fst-italic invalid-banner_image">Proses unggah
                                        gambar, akan menambahkan data gambar baru dan menghapus yang lama</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-primary" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    UPDATE
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
