@section('title')
    KONTAK KAMI
@endsection

@section('metadata')
    <meta content="Kontak Kami" name="keywords" />
    <meta content="LMS Stankom - Halaman Kontak Kami" name="description" />
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.web.contact_us') !!}",
            tableName: "tableContactUs",
            columns: [{
                    data: "company_name",
                    name: "company_name",
                    orderable: true,
                    className: "align-middle text-wrap",
                }, {
                    data: "phone",
                    name: "phone",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "email",
                    name: "email",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "address",
                    name: "address",
                    orderable: false,
                    className: "align-middle text-wrap",
                },
                {
                    data: "website",
                    name: "website",
                    orderable: false,
                    className: "align-middle text-wrap",
                },

                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }
            ]
        }

        var formContactState = {
            company: "modalCompanyName",
            phone: "modalPhone",
            email: "modalEmail",
            address: "modalAddress",
            website: "modalWebsite",
            action: "modalButtonContact",
            title: "modalTitleContact",
            modal: "modalFormContact",
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            reloadDatatable();
        }

        function eventCreateContact(define = 'show') {
            console.log("HELLO")
            $(`#${formContactState.title}`).text("FORM TAMBAH KONTAK");
            $(`#${formContactState.action}`).text("SIMPAN");
            $(`#${formContactState.action}`).attr("data-id", "");

            $(`#${formContactState.company}`).val("");
            $(`#${formContactState.phone}`).val("");
            $(`#${formContactState.email}`).val("");


            $(`#${formContactState.website}`).val("");

            $(`#${formContactState.modal}`).modal(define)
        }

        function eventEditData(id) {
            const url = "{!! url('setting/contact-us/find') !!}/" + id
            axios.get(url).then(response => {
                const {
                    data
                } = response
                console.log(data)

                $(`#${formContactState.title}`).text("FORM EDIT KONTAK");
                $(`#${formContactState.action}`).text("UPDATE");
                $(`#${formContactState.action}`).attr("data-id", id);

                $(`#${formContactState.company}`).val(data.results.company_name);
                $(`#${formContactState.phone}`).val(data.results.phone);
                $(`#${formContactState.email}`).val(data.results.email);
                CKEDITOR.instances?.[`${formContactState.address}`].setData(data.results.address)
                $(`#${formContactState.website}`).val(data.results.website);

                $(`#${formContactState.modal}`).modal('show')
            }).catch(err => {
                showErrorPopup(err);
            })
        }

        $(document).ready(function() {

            CKEDITOR.replace(`${formContactState.address}`, {
                extraPlugins: 'font,editorplaceholder,colorbutton,colordialog,justify',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Alamat...',
                removeButtons: 'PasteFromWord'
            });


            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                },
                columnDefs: [{
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                scrollX: true,
                fixedColumns: {
                    left: 1
                }
            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
                if (json.data.length > 0) {
                    $("#btnCreateContact").attr('disabled', true)
                } else {
                    $("#btnCreateContact").attr('disabled', false)
                }
            });

            $(`#${formContactState.action}`).click(function(e) {
                e.preventDefault();
                const id = $(this).attr('data-id');
                const data = {
                    company_name: $(`#${formContactState.company}`).val(),
                    address: CKEDITOR.instances?.[`${formContactState.address}`].getData(),
                    phone: $(`#${formContactState.phone}`).val(),
                    email: $(`#${formContactState.email}`).val(),
                    website: $(`#${formContactState.website}`).val()
                }

                if (id != '') {
                    // Update
                    const url = "{!! url('setting/contact-us/update') !!}/" + id;

                    axios.put(url, data).then(response => {
                        console.log("RESPONSE", response);
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateContact('hide')
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('company_name')) {
                                    $('.invalid-company_name').text(data.errors.company_name[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('address')) {
                                    $('.invalid-address').text(data.errors.address[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('phone')) {
                                    $('.invalid-phone').text(data.errors.phone[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('email')) {
                                    $('.invalid-email').text(data.errors.email[0])
                                        .css('font-size', '0.9rem');
                                }


                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('setting.contact_us.store') !!}"
                    axios.post(url, data).then(response => {
                        console.log("RESPONSE", response);
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL',
                                text: data.message,
                            });

                            eventRefreshData();
                            eventCreateContact('hide')
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('company_name')) {
                                    $('.invalid-company_name').text(data.errors.company_name[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('address')) {
                                    $('.invalid-address').text(data.errors.address[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('phone')) {
                                    $('.invalid-phone').text(data.errors.phone[0])
                                        .css('font-size', '0.9rem');
                                }

                                if (data.errors.hasOwnProperty('email')) {
                                    $('.invalid-email').text(data.errors.email[0])
                                        .css('font-size', '0.9rem');
                                }

                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">DATA KONTAK KAMI</h4>
                        </div>
                    </div>

                    @if ($totalContact <= 0)
                        <div class="row mb-2 p-3">
                            <div class="col-2">
                                <div class="d-grid">
                                    <button id="btnCreateContact" type="button" class="btn btn-primary "
                                        onclick="eventCreateContact()">
                                        <i class="fas fa-plus"></i>
                                        Kontak Baru
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif



                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableContactUs" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">PERUSAHAAN</th>
                                            <th>PHONE</th>
                                            <th>EMAIL</th>
                                            <th>ALAMAT</th>
                                            <th>WEBSITE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.contact.modal_contact')
</x-app-layout>
