<div id="modalFormContact" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleContact" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="mb-1">
                            <label for="modalCompanyName" class="form-label">Perusahaan</label>
                            <input type="text" id="modalCompanyName" class="form-control form-control-lg" />
                            <div class="text-danger text-sm fst-italic invalid-company_name"></div>
                        </div>

                        <div class="mb-1">
                            <label for="modalPhone" class="form-label">No. Telepon/HP</label>
                            <input type="text" id="modalPhone" class="form-control form-control-lg" />
                            <div class="text-danger text-sm fst-italic invalid-phone"></div>
                        </div>

                        <div class="mb-1">
                            <label for="modalEmail" class="form-label">Email</label>
                            <input type="email" id="modalEmail" class="form-control form-control-lg" />
                            <div class="text-danger text-sm fst-italic invalid-email"></div>
                        </div>

                        <div class="mb-1">
                            <label for="modalAddress" class="form-label">Alamat</label>
                            <textarea cols="80" id="modalAddress" name="modalAddress" class="form-control" rows="10" data-sample-short></textarea>
                            <div class="text-danger text-sm fst-italic invalid-address"></div>
                        </div>

                        <div class="mb-1">
                            <label for="modalWebsite" class="form-label">Website</label>
                            <input type="text" id="modalWebsite" class="form-control form-control-lg" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonContact" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
