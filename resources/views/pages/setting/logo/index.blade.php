@section('title')
    LOGO
@endsection

@section('metadata')
    <meta content="Logo" name="keywords" />
    <meta content="LMS Stankom - Halaman Logo" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        function eventCreate() {
            Swal.fire({
                title: 'Upload Logo Aplikasi',
                input: 'file',
                inputAttributes: {
                    'accept': 'image/jpeg,image/png,image/jpg',
                    'aria-label': 'Silahkan upload gambar'
                }
            }).then(res => {
                if (res.isConfirmed) {
                    const url = "{!! route('setting.logo.store') !!}"
                    const formData = new FormData();
                    formData.append('logo_app', res.value);
                    axios.post(url, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(response => {
                        const {
                            data
                        } = response
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('setting.logo.list') !!}";
                            }
                        });
                    }).catch(error => {
                        console.log(error);
                        // showErrorPopup(error)
                    })
                }
            })
        }

        function eventEdit(id) {
            console.log(id)
            Swal.fire({
                title: 'Upload Logo Aplikasi',
                input: 'file',
                inputAttributes: {
                    'accept': 'image/jpeg,image/png,image/jpg',
                    'aria-label': 'Silahkan upload gambar'
                }
            }).then(res => {
                if (res.isConfirmed) {
                    const url = "{!! route('setting.logo.store') !!}"
                    const formData = new FormData();
                    formData.append('logo_app', res.value);
                    axios.post(url, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(response => {
                        const {
                            data
                        } = response
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('setting.logo.list') !!}";
                            }
                        });
                    }).catch(error => {
                        console.log(error);
                        // showErrorPopup(error)
                    })
                }
            })
        }


        $(document).ready(function() {

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">LOGO</h4>
                        </div>
                    </div>

                    @if (!$logo)
                        <div class="row mb-2 p-3">
                            <div class="col-2">
                                <div class="d-grid">
                                    <button id="btnCreateAbout" type="button" class="btn btn-primary"
                                        onclick="eventCreate()">
                                        <i class="fas fa-plus"></i>
                                        Logo Baru
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if ($logo)
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Logo Aplikasi</h5>
                                        <div>
                                            <div class="d-flex flex-row bd-highlight mb-3">
                                                <div class="p-2 bd-highlight">
                                                    <a href="{{ $logo->logo_path }}" target="_blank"
                                                        class="btn btn-success">
                                                        <i class="fas fa-eye"></i> &nbsp; Preview Logo
                                                    </a>
                                                </div>
                                                <div class="p-2 bd-highlight">
                                                    <button type="button" class="btn btn-primary"
                                                        onclick="eventEdit({!! $logo->id !!})">
                                                        <div class="fas fa-edit"></div> &nbsp; Ubah
                                                    </button>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.contact.modal_contact')
</x-app-layout>
