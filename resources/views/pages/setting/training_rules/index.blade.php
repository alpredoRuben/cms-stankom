@section('title')
    ATURAN PELATIHAN
@endsection

@section('metadata')
    <meta content="Aturan Pelatihan" name="keywords" />
    <meta content="LMS Stankom - Halaman Aturan Pelatihan" name="description" />
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.web.training_rule') !!}",
            tableName: "tableTrainingRules",
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "title",
                    name: "title",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "order_number",
                    name: "order_number",
                    orderable: false,
                    className: "align-middle text-nowrap text-center",
                },

                {
                    data: "description",
                    name: "description",
                    orderable: true,
                    className: "align-middle text-wrap",
                },

                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle text-nowrap",
                }
            ]
        }


        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            reloadDatatable();
        }


        $(document).ready(function() {
            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }
            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
                if (json.data.length > 0) {
                    $("#btnCreateAbout").attr('disabled', true)
                } else {
                    $("#btnCreateAbout").attr('disabled', false)
                }
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">ATURAN PELATIHAN</h4>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-3">
                            <div class="d-grid">
                                <a href="{{ route('setting.training_rule.create') }}" class="btn btn-primary">
                                    <i class="fas fa-plus"></i>
                                    Aturan Pelatihan Baru
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableTrainingRules" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">#</th>
                                            <th class="bg-success text-white">JUDUL</th>
                                            <th>NOMOR URUT</th>
                                            <th>KETERANGAN</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.contact.modal_contact')
</x-app-layout>
