@section('title')
    UBAH ATURAN PELATIHAN
@endsection

@section('metadata')
    <meta content="Ubah Aturan Pelatihan" name="keywords" />
    <meta content="LMS Stankom - Halaman Ubah Aturan Pelatihan" name="description" />
@endsection


@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"
        integrity="sha512-VvWznBcyBJK71YKEKDMpZ0pCVxjNuKwApp4zLF3ul+CiflQi6aIJR+aZCP/qWsoFBA28avL5T5HA+RE+zrGQYg=="
        crossorigin="anonymous"></script>
    {{-- <script src="{{ asset('ckeditor/ckeditor.js') }}"></script> --}}
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var trainingRules = @json($record);
        var formRuleState = {
            title: "ruleTitle",
            description: "ruleDescription",
            order: "ruleOrder",
            action: "buttonSaveAction",
        }

        function refreshErrorTagMessage() {
            $('.invalid-title').text("")
            $('.invalid-order_number').text("")
            $('.invalid-description').text("")
        }

        $(document).ready(function() {

            CKEDITOR.replace(`${formRuleState.description}`, {
                extraPlugins: 'editorplaceholder,colorbutton,colordialog',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Isi berita di sini...',
                removeButtons: 'PasteFromWord'
            });

            CKEDITOR.instances?.[`${formRuleState.description}`].setData(trainingRules.description)

            $(`#${formRuleState.action}`).click(function(e) {
                refreshErrorTagMessage()
                e.preventDefault();

                var data = {
                    title: $(`#${formRuleState.title}`).val(),
                    description: CKEDITOR.instances?.[`${formRuleState.description}`].getData(),
                    order_number: $(`#${formRuleState.order}`).val()
                };

                // Store
                const url = "{!! route('setting.training_rule.update', $record->id) !!}"
                axios.put(url, data, {
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    }
                }).then(response => {
                    const {
                        status,
                        data
                    } = response

                    if (status == 200 || status == 201) {
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('setting.training_rule.list') !!}"
                            }
                        });

                    }


                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('title')) {
                                $('.invalid-title').text(data.errors.title[0])
                                    .css('font-size', '0.9rem');
                            }


                            if (data.errors.hasOwnProperty('order_number')) {
                                $('.invalid-order_number').text(data.errors.order_number[0])
                                    .css('font-size', '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('description')) {
                                $('.invalid-description').text(data.errors.description[0])
                                    .css('font-size', '0.9rem');
                            }

                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM UBAH ATURAN PELATIHAN
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('setting.training_rule.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="ruleTitle" class="form-label fw-bold">
                                    Judul Aturan
                                </label>
                                <input type="text" id="ruleTitle" class="form-control form-control-lg"
                                    placeholder="Judul Aturan..." value="{{ $record->title }}" />
                                <div class="text-danger text-sm fst-italic invalid-title"></div>
                            </div>


                            <div class="mb-3">
                                <label for="ruleDescription" class="form-label">Deskripsi Aturan</label>
                                <textarea cols="80" id="ruleDescription" name="ruleDescription" class="form-control" rows="10"
                                    data-sample-short></textarea>
                                <div class="text-danger text-sm fst-italic invalid-description"></div>
                            </div>

                            <div class="mb-3">
                                <label for="ruleOrder" class="form-label fw-bold">Urutan</label>
                                <input type="number" id="ruleOrder" class="form-control form-control-lg"
                                    value="{{ $record->order_number }}" />
                                <div class="text-danger text-sm fst-italic invalid-order_number"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-primary" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    UPDATE
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
