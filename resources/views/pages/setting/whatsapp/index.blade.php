@section('title')
    WHATSAPP WIDGET
@endsection

@section('metadata')
    <meta content="Whatsapp Widget" name="keywords" />
    <meta content="LMS Stankom - Halaman Whatsapp Widget" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var formModalState = {
            modal: "modalFormCover",
            title: "modalTitle",
            input: "modalWhatsappNumber",
            action: "modalButtonAction"
        };

        function eventCreateData() {
            $(`#${formModalState.title}`).text("FORM TAMBAH NOMOR WHATSAPP");
            $(`#${formModalState.action}`).text("SIMPAN");
            $(`#${formModalState.action}`).attr("data-id", "");
            $(`#${formModalState.input}`).val("");
            $(`#${formModalState.modal}`).modal('show')
        }

        function eventEditData(id, nomorWa) {
            console.log(nomorWa)
            $(`#${formModalState.title}`).text("FORM UBAH NOMOR WHATSAPP");
            $(`#${formModalState.action}`).text("UPDATE");
            $(`#${formModalState.action}`).attr("data-id", id);
            $(`#${formModalState.input}`).val(`${nomorWa}`);
            $(`#${formModalState.modal}`).modal('show')
        }

        $(document).ready(function() {


            $(`#${formModalState.action}`).click(function(e) {
                e.preventDefault();
                const id = $(this).attr('data-id');
                const data = {
                    nomor_wa: $(`#${formModalState.input}`).val()
                };


                if (id != '') {
                    // Update
                    const url = "{!! url('setting/wa-widget/update') !!}/" + id;

                    axios.put(url, data).then(response => {

                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = "{!! route('setting.wa-widget.list') !!}";
                                }
                            });
                        }


                    }).catch(error => {
                        console.log(error.response)
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('nomor_wa')) {
                                    $('.invalid-nomor_wa').text(data.errors.nomor_wa[0])
                                        .css('font-size', '0.9rem');
                                }

                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });

                } else {
                    // Store
                    const url = "{!! route('setting.wa-widget.store') !!}";
                    axios.post(url, data).then(response => {
                        const {
                            status,
                            data
                        } = response

                        if (status == 200 || status == 201) {
                            Swal.fire({
                                title: 'BERHASIL',
                                icon: 'success',
                                text: data.message,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = "{!! route('setting.wa-widget.list') !!}";
                                }
                            });
                        }


                    }).catch(error => {
                        const {
                            status,
                            data
                        } = error.response;

                        switch (status) {
                            case 422:
                                if (data.errors.hasOwnProperty('nomor_wa')) {
                                    $('.invalid-nomor_wa').text(data.errors.nomor_wa[0])
                                        .css('font-size', '0.9rem');
                                }
                                break;

                            default:
                                showErrorPopup(error)
                                break;
                        }
                    });
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">DATA WHATSAPP WIDGET</h4>
                        </div>
                    </div>

                    @if ($totalRow <= 0)
                        <div class="row mb-2 p-3">
                            <div class="col-2">
                                <div class="d-grid">
                                    <button id="btnCreateData" type="button" class="btn btn-primary "
                                        onclick="eventCreateData()">
                                        <i class="fas fa-plus"></i>
                                        Whatsapp Widget
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif



                    @if ($whatsapp)
                        <div class="row mb-2 p-3">
                            <div class="col-12">
                                <div class="card mb-3" style="max-width: 540px;">
                                    <div class="row g-0">
                                        <div class="col-md-4">
                                            <img src="{{ asset('images/wa.png') }}" class="img-fluid rounded-start"
                                                alt="Whatsapp Logo">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">Nomor Whatsapp</h5>
                                                <p class="card-text">
                                                    {{ $whatsapp->nomor_wa }}
                                                </p>
                                            </div>
                                            <div class="card-footer bg-white border-0">
                                                <button type="button" class="btn btn-primary btn-sm mt-2"
                                                    onclick="eventEditData('{{ $whatsapp->id }}', '{{ $whatsapp->nomor_wa }}')">
                                                    <i class="fas fa-edit"></i> Ubah
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    @include('pages.setting.whatsapp.modal_wa')
</x-app-layout>
