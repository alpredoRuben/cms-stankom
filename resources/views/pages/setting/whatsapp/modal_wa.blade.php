<div id="modalFormCover" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitle" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="mb-1">
                            <label for="modalWhatsappNumber" class="form-label">Nomor Whatsapp</label>
                            <input type="text" id="modalWhatsappNumber" class="form-control form-control-lg" />
                            <div class="text-danger text-sm fst-italic invalid-nomor_wa"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonAction" type="button" class="btn btn-primary" data-id=""></button>
            </div>
        </div>
    </div>
</div>
