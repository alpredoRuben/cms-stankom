@section('title')
    TAMBAH KELAS (WEBINAR)
@endsection

@section('metadata')
    <meta content="Tambah Kelas (Webinar)" name="keywords" />
    <meta content="LMS Stankom - Halaman Tambah Kelas (Webinar)" name="description" />
@endsection

@section('stylesheets')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var formState = {
            class_name: $("#textClassWebinar"),
            class_type: $("#selectionClassType"),
            trainer: $("#selectionTrainer"),
            open_date: $("#inputOpenDate"),
            close_date: $("#inputCloseDate"),
            class_time: $("#inputClassTime"),
            duration: $("#inputDuration"),
            quota: $("#inputQuota"),
            link: $("#inputLink"),
            sertificated: "radioSertificated",
            quiz: "radioQuiz",
            images: $("#inputImages"),
            description: "inputDescription",

            actionSave: $("#buttonSaveAction")

        }

        function initialClassType() {
            const url = "{!! route('master.classes.type.all') !!}";
            initJQSelect2(formState.class_type, url, "Pilih Tipe Kelas")
        }

        function initialTrainer() {
            const url = "{!! route('master.users.trainers.all') !!}"
            initJQSelect2(formState.trainer, url, "Pilih Pengajar")
        }

        function refreshErrorTagMessage() {
            $('.invalid-trainer').text("")
            $('.invalid-class_name').text("")
            $('.invalid-class_type_id').text("")
            $('.invalid-open_date').text("")
            $('.invalid-close_date').text("")
            $('.invalid-quota').text("")
            $('.invalid-class_time').text("")
            $('.invalid-duration').text("")
        }

        $(document).ready(function() {
            CKEDITOR.replace(`${formState.description}`, {
                extraPlugins: 'justify,font,editorplaceholder,colorbutton,colordialog',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Isi keterangan...',
                removeButtons: 'PasteFromWord'
            });

            initialClassType();
            initialTrainer();

            MCDatepicker.create({
                el: '#inputOpenDate',
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            MCDatepicker.create({
                el: '#inputCloseDate',
                dateFormat: 'YYYY-MM-DD',
                bodyType: 'inline',
                theme: {
                    theme_color: '#38ada9',
                    display: {
                        foreground: 'rgba(255, 255, 255, 0.8)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            formState.class_time.timepicker({
                maxHours: 24,
                defaultTime: 'current',
                template: 'dropdown',
                showSeconds: true,
                showMeridian: false,
                defaultTime: false
            });


            formState.actionSave.click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                var formData = new FormData();
                formData.append('class_name', formState.class_name.val());
                formData.append('class_type_id', formState.class_type.val());
                formData.append('user_id', formState.trainer.val());
                formData.append('open_date', formState.open_date.val());
                formData.append('close_date', formState.close_date.val());
                formData.append('class_time', formState.class_time.val());
                formData.append('duration', formState.duration.val());
                formData.append('quota', formState.quota.val());
                formData.append('link', formState.link.val());
                formData.append('has_quiz', $(`input[name="${formState.quiz}"]:checked`).val());
                formData.append('has_sertificated', $(`input[name="${formState.sertificated}"]:checked`)
                    .val());
                formData.append('description', CKEDITOR.instances?.[`${formState.description}`].getData());
                formData.append('file_images', formState.images[0].files[0]);

                for (var pair of formData.entries()) {
                    console.log(pair[0], pair[1]);
                }

                const url = "{!! route('class_webinar.store') !!}";

                axios.post(url, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    if (response.status == 201 || response.status == 200) {
                        const {
                            data
                        } = response;

                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('class_webinar.list') !!}";
                            }
                        });
                    }

                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('class_name')) {
                                $('.invalid-class_name').text(data.errors
                                        .class_name[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('class_type_id')) {
                                $('.invalid-class_type_id').text(data.errors
                                        .class_type_id[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('user_id')) {
                                $('.invalid-trainer').text(data.errors
                                        .user_id[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }


                            if (data.errors.hasOwnProperty('open_date')) {
                                $('.invalid-open_date').text(data.errors
                                        .open_date[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('close_date')) {
                                $('.invalid-close_date').text(data.errors
                                        .close_date[0])
                                    .css(
                                        'font-size',
                                        '0.8rem');
                            }

                            if (data.errors.hasOwnProperty('quota')) {
                                $('.invalid-quota').text(data.errors.quota[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('class_time')) {
                                $('.invalid-class_time').text(data.errors
                                        .class_time[0])
                                    .css(
                                        'font-size',
                                        '0.7rem');
                            }

                            if (data.errors.hasOwnProperty('duration')) {
                                $('.invalid-duration').text(data.errors.duration[0])
                                    .css(
                                        'font-size',
                                        '0.7rem');
                            }

                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });

            });


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM TAMBAH KELAS (WEBINAR)
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-2">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('class_webinar.list') }}"
                                            class="btn btn-sm btn-primary text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row mb-2 p-3">
                        <!-- NAMA KELAS -->
                        <div class="col-4">
                            <div class="mb-2">
                                <label for="textClassWebinar" class="form-label">Nama Kelas/Webinar</label>
                                <input id="textClassWebinar" class="form-control" type="text"
                                    value="{{ old('class_name') }}" placeholder="Nama Kelas (Webinar)" />
                                <div class="text-danger text-sm fst-italic invalid-class_name"></div>
                            </div>
                        </div>

                        <!-- TIPE KELAS -->
                        <div class="col-4">
                            <div class="mb-2">
                                <label for="selectionClassType" class="form-label">Tipe Kelas/Webinar</label>
                                <select id="selectionClassType" class="form-select"
                                    data-placeholder="Tipe Kelas (Webinar)"></select>
                                <div class="text-danger text-sm fst-italic invalid-class_type_id"></div>
                            </div>
                        </div>

                        <!-- PENGAJAR -->
                        <div class="col-4">
                            <div class="mb-2">
                                <label for="selectionTrainer" class="form-label">Pengajar (Trainer)</label>
                                <select id="selectionTrainer" class="form-control"
                                    data-placeholder="Pengajar (Trainer)"></select>
                                <div class="text-danger text-sm fst-italic invalid-trainer"></div>
                            </div>
                        </div>
                    </div>


                    <!-- Tanggal Kelas Buka, Tutup, Jam Kelas, dan Duration -->
                    <div class="row mb-2 p-3">
                        <!-- TANGGAL KELAS BUKA -->
                        <div class="col-4">
                            <div class="mb-2">
                                <label for="inputOpenDate" class="form-label">Tanggal Kelas Buka</label>
                                <input type="text" id="inputOpenDate" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-open_date"></div>
                            </div>
                        </div>

                        <!-- TANGGAL KELAS TUTUP -->
                        <div class="col-4">
                            <div class="mb-2">
                                <label for="inputCloseDate" class="form-label">Tanggal Kelas Tutup</label>
                                <input type="text" id="inputCloseDate" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-close_date"></div>
                            </div>
                        </div>

                        <!-- JAM KELAS -->
                        <div class="col-2">
                            <div class="mb-2">
                                <label for="inputClassTime" class="form-label">Jam Kelas</label>
                                <input type="text" id="inputClassTime" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-class_time"></div>
                            </div>
                        </div>

                        <!-- DURASI  -->
                        <div class="col-2">
                            <div class="mb-2">
                                <label for="inputDuration" class="form-label">Durasi (Menit)</label>
                                <input type="number" id="inputDuration" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-duration"></div>
                            </div>
                        </div>
                    </div>

                    <!-- Quota dan Link -->
                    <div class="row mb-2 p-3">
                        <!-- QUOTA -->
                        <div class="col-2">
                            <div class="mb-2">
                                <label for="inputQuota" class="form-label">Kuota (Orang)</label>
                                <input type="number" id="inputQuota" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-quota"></div>
                            </div>
                        </div>

                        <!-- LINK -->
                        <div class="col-10">
                            <div class="mb-2">
                                <label for="inputLink" class="form-label">Link</label>
                                <input type="text" id="inputLink" class="form-control" />
                                <div class="text-danger text-sm fst-italic invalid-link"></div>
                            </div>
                        </div>
                    </div>

                    <!-- Quiz dan Sertifikat -->
                    <div class="row mb-2 p-3">
                        <!-- HAS QUIZ -->
                        <div class="col-3">
                            <div class="mb-2">
                                <label for="radioQuiz" class="form-label">Ada Kuis</label>
                                <div id="radioQuiz">
                                    <input class="form-check-input" type="radio" name="radioQuiz" id="radioQuiz1"
                                        value="1" checked>
                                    <label class="form-check-label" for="radioQuiz1">
                                        Ada
                                    </label>
                                    &nbsp;
                                    <input class="form-check-input" type="radio" name="radioQuiz" id="radioQuiz2"
                                        value="0">
                                    <label class="form-check-label" for="radioQuiz2">
                                        Tidak Ada
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- HAS SERTIFICATED -->
                        <div class="col-3">
                            <div class="mb-2">
                                <label for="radioSertificated" class="form-label">Ada Sertifikat</label>
                                <div id="radioSertificated">
                                    <input class="form-check-input" type="radio" name="radioSertificated"
                                        id="radioSertificated1" value="1" checked>
                                    <label class="form-check-label" for="radioSertificate1">
                                        Ada
                                    </label>
                                    &nbsp;
                                    <input class="form-check-input" type="radio" name="radioSertificated"
                                        id="radioSertificated2" value="0">
                                    <label class="form-check-label" for="radioSertificated2">
                                        Tidak Ada
                                    </label>
                                </div>
                            </div>
                        </div>


                        <!-- GAMBAR -->
                        <div class="col-3">
                            <div class="mb-2">
                                <label for="inputImages" class="form-label">Upload Gambar</label>
                                <input type="file" accept="image/png,image/jpg,image/jpeg" id="inputImages"
                                    class="form-control form-control-lg" />
                            </div>
                        </div>
                    </div>


                    <!-- Description -->
                    <div class="row mb-2 p-3">

                        <!-- LINK -->
                        <div class="col-12">
                            <div class="mb-2">
                                <textarea cols="80" id="inputDescription" name="inputDescription" class="form-control" rows="10"
                                    data-sample-short></textarea>
                            </div>


                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-dark" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
