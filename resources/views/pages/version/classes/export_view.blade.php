@section('title')
    EXPORT KELAS/WEBINAR
@endsection

@section('metadata')
    <meta content="EXPORT KELAS/WEBINAR" name="keywords" />
    <meta content="LMS Stankom - Halaman EXPORT KELAS/WEBINAR" name="description" />
    <style>
        .form-check-in-table {
            width: 20px;
            height: 20px;
        }
    </style>
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const webinarProperties = <?php echo json_encode($webinarProperties); ?>;

        var filterParameters = {
            type: 'limit',
            limit: 10,
            offset: 0,
        }

        function defaultElementExport(val = 'limit') {
            if (val == 'all') {
                $("#exportOffsetNumber").val("");
                $("#exportLimitNumber").val("");

                $("#exportOffsetNumber").attr('readonly', true);
                $("#exportLimitNumber").attr('readonly', true);
            } else {
                $("#exportOffsetNumber").val(filterParameters.offset);
                $("#exportLimitNumber").val(filterParameters.limit);

                $("#exportOffsetNumber").attr('readonly', false);
                $("#exportLimitNumber").attr('readonly', false);
            }
        }


        function updateDataTableSelectAllCtrl(table) {
            var $table = table.table().node();
            var $chkbox_all = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if ($chkbox_checked.length === 0) {
                chkbox_select_all.checked = false;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length) {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        $(document).ready(function() {
            defaultElementExport(val = 'limit');

            var rows_selected = [];

            var table = $('#tableProperty').DataTable({
                data: webinarProperties,
                lengthChange: false,
                searching: false,
                columnDefs: [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'width': '2%',
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return '<input type="checkbox" class="form-check-in-table">';
                    }
                }],
                order: [
                    [1, 'asc']
                ],
                'rowCallback': function(row, data, dataIndex) {
                    // Get row ID
                    var rowId = data[0];

                    // If row ID is in the list of selected row IDs
                    if ($.inArray(rowId, rows_selected) !== -1) {
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        // $(row).addClass('selected');
                    }
                }
            });

            $('#tableProperty tbody').on('click', 'input[type="checkbox"]', function(e) {
                var $row = $(this).closest('tr');

                // Get row data
                var data = table.row($row).data();

                // Get row ID
                var rowId = data[0];

                // Determine whether row ID is in the list of selected row IDs
                var index = $.inArray(rowId, rows_selected);

                // If checkbox is checked and row ID is not in list of selected row IDs
                if (this.checked && index === -1) {
                    rows_selected.push(rowId);

                    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
                } else if (!this.checked && index !== -1) {
                    rows_selected.splice(index, 1);
                }

                if (this.checked) {
                    // $row.addClass('selected');
                } else {
                    // $row.removeClass('selected');
                }

                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });

            // Handle click on table cells with checkboxes
            $('#tableProperty').on('click', 'tbody td, thead th:first-child', function(e) {
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });

            // Handle click on "Select all" control
            $('thead input[name="select_all"]', table.table().container()).on('click', function(e) {

                if (this.checked) {
                    $('#tableProperty tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('#tableProperty tbody input[type="checkbox"]:checked').trigger('click');
                }

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });

            // Handle table draw event
            table.on('draw', function() {
                updateDataTableSelectAllCtrl(table);
            });



            $("#exportFilterSelection").change(function(e) {
                e.preventDefault();
                const value = $(this).val();
                defaultElementExport(value);
            });


            $("#buttonExportData").click(function(e) {
                e.preventDefault();
                filterParameters.type = $("#exportFilterSelection").val();
                filterParameters.limit = $("#exportLimitNumber").val();
                filterParameters.offset = $("#exportOffsetNumber").val();

                axios({
                    url: "{!! route('file_manager.class-webinar.export.csv') !!}",
                    method: 'GET',
                    responseType: 'blob',
                    params: {
                        type: filterParameters.type,
                        limit: filterParameters.limit,
                        offset: filterParameters.offset,
                        selections: rows_selected,
                    }
                }).then((response) => {
                    console.log(response);
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', 'export_class_webinar.csv');
                    document.body.appendChild(link);
                    link.click();
                });

            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-sm-12">
            <div class="home-tab">
                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                    <div>
                        <div class="btn-wrapper">
                            <a href="{{ route('class_webinar.list') }}" class="btn btn-primary text-white me-0">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                                &nbsp; Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-12">
                            <h4 class="card-title">
                                <i class="fas fa-cog fa-sm"></i> &nbsp;
                                EXPORT KELAS / WEBINAR
                            </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="exportFilterSelection" value="Nama Propinsi" class="form-label">
                                    Pilih Kategori Baris
                                </label>
                                <select id="exportFilterSelection" class="form-control">
                                    <option value="limit" selected>Baris Tertentu</option>
                                    <option value="all">Seluruh Baris</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="exportOffsetNumber" value="Nama Propinsi" class="form-label">
                                    Mulai dari baris ke-
                                </label>
                                <input type="number" id="exportOffsetNumber" class="form-control  form-control-lg" />

                                <label for="exportLimitNumber" value="Nama Propinsi" class="form-label">
                                    Banyak baris / Jumlah baris
                                </label>
                                <input type="number" id="exportLimitNumber" class="form-control  form-control-lg" />
                            </div>

                            <div class="mb-3">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-success" id="buttonExportData">
                                        <i class="fas fa-file-export"></i> &nbsp;
                                        EXPORT DATA
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <p class="alert alert-info text-dark m-2">
                                Pilih nama field yang akan diekstrak ke file CSV
                            </p>
                            <div class="table-responsive">
                                <table id="tableProperty" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class="form-check-in-table" name="select_all" value="1"
                                                    type="checkbox" />
                                            </th>
                                            <th>Nama Field</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</x-app-layout>
