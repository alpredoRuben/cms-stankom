@section('title')
    KELAS/WEBINAR
@endsection

@section('metadata')
    <meta content="Kelas Webinar, Kelas, Webinar" name="keywords" />
    <meta content="LMS Stankom - Halaman Kelas/Webinar" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.complements.class_webinar') !!}",
            tableName: "tableClassWebinar",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "class_name",
                    name: "class_name",
                    orderable: true,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "type_class_name",
                    name: "type_class_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "trainer_name",
                    name: "trainer_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "open_date",
                    name: "open_date",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "close_date",
                    name: "close_date",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "class_time",
                    name: "class_time",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "duration",
                    name: "duration",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "quota",
                    name: "quota",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "link",
                    name: "link",
                    orderable: false,
                    className: "align-middle text-wrap",
                },
                {
                    data: "has_quiz",
                    name: "has_quiz",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "has_sertificated",
                    name: "has_sertificated",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle",
                }

            ]

        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('class_webinar.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        $(document).ready(function() {

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }

            });

            dTables.on("xhr", function(e, settings, json) {
                console.log(json)
            });


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">KELAS / WEBINAR</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => route('class_webinar.export_show'),
                        'urlDownload' => null,
                        'addUrl' => route('class_webinar.create'),
                        'addTitle' => 'Kelas (Webinar)',
                        'addAction' => true,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableClassWebinar" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">NAMA KELAS/WEBINAR</th>
                                            <th>TIPE KELAS/WEBINAR</th>
                                            <th>PENGAJAR</th>
                                            <th>TANGGAL BUKA</th>
                                            <th>TANGGAL TUTUP</th>
                                            <th>JAM KELAS</th>
                                            <th>DURASI</th>
                                            <th>KUOTA</th>
                                            <th>LINK</th>
                                            <th>QUIZ</th>
                                            <th>SERTIFIKAT</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
