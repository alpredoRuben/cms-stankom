@section('title')
    DETAIL KELAS/WEBINAR
@endsection

@section('metadata')
    <meta content="Detail Kelas/Webinar, Class/Webinar Detail" name="keywords" />
    <meta content="LMS Stankom - Halaman Detail Kelas/Webinar" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var classWebinar = <?php echo json_encode($record); ?>;

        $(document).ready(function() {
            console.log(classWebinar)
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-bullseye"></i> &nbsp;
                                DETAIL KELAS/WEBINAR
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('class_webinar.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row mb-2 p-3">

                        <div class="col-8">
                            <div class="d-flex flex-column mb-3">
                                <div class="p-2">
                                    <h3 class="fs-3 text-primary fw-bold">{{ $record->class_name }}</h3>
                                </div>
                                <div class="p-2">
                                    <p class="text-black ">
                                        {!! $record->description !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <h5>Tipe Kelas/Webinar</h5>
                                <p>{{ $record->typeClass->name }}</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Pengajar</h5>
                                <p>{{ $record->trainer->name }}</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Tanggal Kelas Buka</h5>
                                <p>{{ $record->open_date }}</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Tanggal Kelas Tutup</h5>
                                <p>{{ $record->close_date }}</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Jam Kelas</h5>
                                <p>{{ $record->class_time }}</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Durasi</h5>
                                <p>{{ $record->duration }} Menit</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Kuota</h5>
                                <p>{{ $record->quota }} Orang</p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Kuis</h5>
                                <p>
                                    @if ($record->has_quiz)
                                        <label class="badge bg-success badge-success text-white">ADA</label>
                                    @else
                                        <label class="badge bg-danger badge-danger text-white">TIDAK ADA</label>
                                    @endif
                                </p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Sertifikat</h5>
                                <p>
                                    @if ($record->has_sertificated)
                                        <label class="badge bg-success badge-success text-white">BERSERTIFIKAT</label>
                                    @else
                                        <label class="badge bg-danger badge-danger text-white">TIDAK
                                            BERSERTIFIKAT</label>
                                    @endif
                                </p>
                            </div>

                            <div class="d-flex justify-content-between align-items-center  mb-2">
                                <h5>Link</h5>
                                <a href="{{ $record->link }}" target="_blank" class="btn btn-dark btn-sm">
                                    <i class="fas fa-link"></i> &nbsp; Kunjungi
                                </a>
                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
