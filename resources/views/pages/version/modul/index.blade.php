@section('title')
    MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Module Pembelajaran, Learning Modules" name="keywords" />
    <meta content="LMS Stankom - Halaman Module Pembelajaran" name="description" />
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('datatables.complements.learning_module') !!}",
            tableName: "tableLearningModules",
            filters: {
                history: "",
                search: "",
                category: ""
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "module_name",
                    name: "module_name",
                    orderable: true,
                    className: "align-middle text-wrap",
                },
                {
                    data: "category_name",
                    name: "category_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "sub_category_name",
                    name: "sub_category_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "tipe_name",
                    name: "tipe_name",
                    orderable: true,
                    className: "align-middle",
                },
                {
                    data: "download",
                    name: "download",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "quiz",
                    name: "quiz",
                    orderable: false,
                    className: "align-middle",
                },
                {
                    data: "module_desc",
                    name: "module_desc",
                    orderable: false,
                    className: "align-middle text-wrap",
                },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "align-middle text-nowrap",
                },
                {
                    data: "action",
                    name: "action",
                    searchable: false,
                    className: "text-center align-middle",
                }

            ]

        }

        var formFilterState = {
            modal: 'modalFilterBox',
            selection: 'modalFilterSelection',
            search: 'modalFilterText',
            button: 'modalFilterButton',
            alert: 'modalFilterAlert',
        }

        function eventImportData() {
            Swal.fire({
                title: "Pilih file Excel",
                input: 'file',
                inputAttributes: {
                    'accept': '.xls,.xlsx',
                    'aria-label': 'Upload file excel anda'
                },
                showCancelButton: true,
                confirmButtonText: 'Upload',
            }).then((file) => {
                if (file.isConfirmed && file.value) {
                    var formData = new FormData();
                    formData.append('import_file', file.value);

                    axios.post("{!! route('excel-manager.learning-modules') !!}", formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).then(response => {
                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: response.data.message,
                        });
                        eventRefreshData();
                    }).catch(error => {
                        showErrorPopup(error)
                    })

                }
            })
        }

        function eventShowFilter(define = 'show') {
            $(`#${formFilterState.alert}`).empty();
            $(`#${formFilterState.selection}`).val("").change();
            $(`#${formFilterState.search}`).val("");
            $(`#${formFilterState.modal}`).modal(define);
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function eventRefreshData() {
            tableState.filters.search = '';
            tableState.filters.category = '';
            reloadDatatable();
        }

        function eventChangeStatus(id, status) {
            const url = "{!! route('learning_modules.change_status') !!}";
            const msgAlert = status == true ? "menonaktifkan data ini" : "mengaktifkan data ini"
            Swal.fire({
                title: status ? 'PERINGATAN' : 'INFORMASI',
                text: `Apakah anda yakin ingin ${msgAlert}?`,
                icon: status ? 'warning' : 'info',
                showCancelButton: true,
                confirmButtonColor: '#035e4e',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, ' + (status ? 'Non Aktifkan' : 'Aktifkan')
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post(url, {
                        id: id
                    }).then((response) => {
                        Swal.fire({
                            title: 'BERHASIL',
                            text: response.data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((r) => {
                            eventRefreshData();
                        });
                    }).catch((error) => {});
                }
            });
        }

        function eventPreviewHistory() {
            tableState.filters.history = "deleted";
            reloadDatatable();
        }

        function eventPreviewData() {
            tableState.filters.history = "";
            reloadDatatable();
        }

        $(document).ready(function() {

            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.category = tableState.filters.category;
                        d.search = tableState.filters.search;
                        d.history = tableState.filters.history
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                },
                scrollX: true,
                fixedColumns: {
                    left: 2
                }

            });

            dTables.on("xhr", function(e, settings, json) {});


            $(`#${formFilterState.button}`).click(function(e) {
                e.preventDefault();
                tableState.filters.category = $(`#${formFilterState.selection}`).val();
                tableState.filters.search = $(`#${formFilterState.search}`).val();

                if (tableState.filters.category != '') {
                    reloadDatatable();
                    eventShowFilter('hide')
                } else {
                    $(`#${formFilterState.alert}`).append(
                        `<div class="alert alert-danger text-dark">Silahkan pilih kategori pencarian</div>`
                    )
                }
            });

        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="card-title">MODUL PEMBELAJARAN</h4>
                        </div>
                    </div>

                    @include('includes.nav_button', [
                        'exportUrl' => null,
                        'urlDownload' => route('file_manager.learning-modules.download.excel'),
                        'addUrl' => route('learning_modules.create'),
                        'addTitle' => 'Modul Pembelajaran',
                        'addAction' => true,
                        'import' => true,
                    ])


                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="tableLearningModules" class="table table-striped table-condensed table-hover"
                                    width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th class="bg-success text-white">NO</th>
                                            <th class="bg-success text-white">MODUL</th>
                                            <th>KATEGORI</th>
                                            <th>SUB KATEGORI</th>
                                            <th>TIPE MODUL</th>
                                            <th>DOWNLOAD</th>
                                            <th>QUIZ</th>
                                            <th>KETERANGAN</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL FORM FILTER -->
    @include('includes.modal_form_filter', [
        'filterBox' => $filterBox,
    ])
</x-app-layout>
