<div id="modalFormBerkas" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modalTitleBerkas" class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-3">
                            <label for="modalTopic" class="form-label fw-bold">
                                Topik/Materi Pertemuan
                            </label>
                            <input id="modalTopic" class="form-control" type="text" placeholder="Topik/Materi" />
                        </div>

                        <div class="mb-3">
                            <label for="modalDescription" class="form-label fw-bold">
                                Keterangan
                            </label>
                            <textarea id="modalDescription" class="form-control" style="height:80px;" placeholder="Keterangan Topi/Materi"></textarea>
                        </div>

                        <div id="typeVideo" class="mb-3">
                            <label for="modalLinkVideo" class="form-label fw-bold">
                                URL (Link) Video
                            </label>
                            <input id="modalLinkVideo" class="form-control" type="text"
                                placeholder="Link video dari youtube" />
                            <span class="fs-6 text-success">Pastikan url/link share video youtube tidak dikunci dan
                                tidak
                                bersifat privasi</span>
                        </div>

                        <div id="typeNonVideo" class="m-3">
                            <label for="modalTypeFile" class="form-label fw-bold">
                                Upload berkas modul pembelajaran
                            </label>

                            <input class="form-control" type="file" name="modalTypeFile" id="modalTypeFile"
                                accept="application/pdf,.doc,.docx,." hidden />

                            <div class="m-1">
                                <button type="button" class="btn btn-primary" id="modalBrowseFile">
                                    Browse File
                                </button>
                            </div>
                        </div>

                        <div id="resultUpload" class="m-1"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="modalButtonSave" type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
