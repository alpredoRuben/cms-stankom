@section('title')
    DETAIL MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Detail Modul Pembelajaran, Learning Module Detail" name="keywords" />
    <meta content="LMS Stankom - Halaman Detail Modul Pembelajaran" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var learningModule = @json($record)


        function eventShowFile(id) {
            if (learningModule.type_module?.name.toLowerCase() == 'video') {
                const find = learningModule.file_modules.find(x => x.id == id);
                console.log("FIND", find);

                const toolbarEnabled = learningModule.can_download ? '#toolbar=1' : '#toolbar=0'
                let str = ''
                if (find && find.filepath != '') {
                    $("#embedViewer").empty();
                    str +=
                        `<iframe width="100%" height="500" src="${find.filepath}${toolbarEnabled}" title="${find.topic}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                    $("#embedViewer").html(str);
                }
            } else {
                const find = learningModule.file_modules.find(x => x.id == id);

                const toolbarEnabled = learningModule.can_download ? '#toolbar=1&navpanes=1' : '#toolbar=0&navpanes=1'
                let str = ''
                if (find && find.composite.extension == 'pdf') {
                    $("#embedViewer").empty();
                    str +=
                        `<iframe  src="${find.filepath}${toolbarEnabled}" width="100%" height="500" class="frameView"></iframe>`;

                    $("#embedViewer").html(str);
                }
            }


        }



        $(document).ready(function() {


        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title">
                                <i class="fas fa-bullseye"></i> &nbsp;
                                DETAIL MODUL PEMBELAJARAN
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('learning_modules.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">
                        <div class="col-4">
                            <div class="form-floating mb-3">
                                <input type="text" id="category_name" class="form-control text-primary fw-bold"
                                    value="{{ $record->categoryModule->name }}" />
                                <label for="category_name" class="form-label">Kategori</label>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-floating mb-3">
                                <input type="text" id="sub_category_name" class="form-control text-primary fw-bold"
                                    value="{{ $record->subCategoryModule->name }}" />
                                <label for="category_name" class="form-label">Sub Kategori</label>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-floating mb-3">
                                <input type="text" id="type_name" class="form-control text-primary fw-bold"
                                    value="{{ $record->typeModule->name }}" />
                                <label for="type_name" class="form-label">Tipe</label>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-floating mb-3">
                                <input type="text" id="modul_name" class="form-control text-primary fw-bold"
                                    value="{{ $record->module_name }}" />
                                <label for="modul_name" class="form-label">Modul Pembelajaran</label>
                            </div>
                        </div>

                        <div class="col-12">
                            @if ($record->module_desc && !empty($record->module_desc))
                                <div class="form-floating mb-3">
                                    <textarea type="text" id="modul_desc" class="form-control text-primary fw-bold" style="min-height: 200px;">{{ strip_tags($record->module_desc) }}</textarea>
                                    <label for="modul_desc" class="form-label">Keterangan</label>
                                </div>
                            @endif
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <label for="type_name" class="form-label">Status</label>
                                @if ($record->status)
                                    <label class="badge bg-success badge-success text-white">ACTIVE</label>
                                @else
                                    <label class="badge bg-success badge-danger text-white">NON ACTIVE</label>
                                @endif
                            </div>
                        </div>
                    </div>


                    @if (count($record->fileModules) > 0)
                        <div class="row p-3">
                            <div class="col-md-12">
                                <h5>Berkas Pembelajaran</h5>
                            </div>

                            <div class="col-3 py-2">
                                @foreach ($record->fileModules as $item)
                                    <div class="mb-2">
                                        <div class="d-grid">
                                            <button class="btn btn-primary text-right"
                                                onclick="eventShowFile('{{ $item->id }}')">
                                                <i class="fas fa-caret-right"></i> &nbsp;
                                                {{ $item->topic }}
                                            </button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div id="embedViewer" class="col-9 py-2"></div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
