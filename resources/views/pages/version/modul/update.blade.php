@section('title')
    EDIT MODUL PEMBELAJARAN
@endsection

@section('metadata')
    <meta content="Edit Modul Pembelajaran, Update Learning Module" name="keywords" />
    <meta content="LMS Stankom - Halaman Edit Modul Pembelajaran" name="description" />
@endsection

@section('stylesheets')
    <style>
        input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
    </style>
@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/4.19.1/standard-all/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var learningModule = @json($record);
        const extentionsType = {
            video: [
                'video/mp4', 'video/mov', 'video/wmv', 'video/avi', 'video/flv', 'video/webm', 'video/mkv'
            ],
            document: ['application/pdf', '.doc', '.docx', '.pptx', ]
        }

        var formModuleState = {
            modal: 'modalFormBerkas',
            title: 'modalTitleBerkas',
            topic: 'modalTopic',
            description: 'modalDescription',
            file: 'modalTypeFile',
            link: 'modalLinkVideo',
            actionBrowseFile: 'modalBrowseFile',
            coverUpload: 'resultUpload',
            actionSave: 'modalButtonSave'
        };

        var typeModeSetting = '';
        var temporaryFile = null;
        var temporaryList = [];

        var formState = {
            name: $("#textModulName"),
            description: "textModulDescription",
            category: $("#selectionCategoryModule"),
            sub_category: $("#selectionSubCategoryModule"),
            type: $("#selectionTypeModule"),
            download: "radioCanDownload",
            quiz: "radioHasQuiz"
        }

        function fillInfoFile(data) {
            $("#infoFile").empty();
            let strList = ''
            $.each(data, function(i, v) {
                const element = data[i];

                if (!element.deleted) {
                    strList += `
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <strong>${v.topic}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" onclick="removeItemList(${i})"></button>
                        </div>
                    `
                }

            });

            $("#infoFile").append(strList);
        }

        function eventAddFileModule(define = 'show') {
            if (typeModeSetting == '') {
                Swal.fire({
                    title: 'PERINGATAN',
                    icon: 'warning',
                    text: 'Silahkan pilih nama tipe modul',
                    confirmButtonText: 'OK',
                });
            } else {
                $(`#${formModuleState.title}`).text("FORM BERKAS MODUL PEMBELAJARAN");

                if (typeModeSetting == 'video') {
                    $(`#typeNonVideo`).hide();
                } else {
                    $(`#typeVideo`).hide();
                }

                $(`#${formModuleState.actionSave}`).text("UPDATE");
                $(`#${formModuleState.title}`).val("");
                $(`#${formModuleState.topic}`).val("");
                $(`#${formModuleState.description}`).val("");
                $(`#${formModuleState.file}`).val("");
                $(`#${formModuleState.coverUpload}`).empty();
                $(`#${formModuleState.modal}`).modal(define);
            }

        }

        function refreshErrorTagMessage() {
            $('.invalid-category_module_id').text("")
            $('.invalid-sub_category_module_id').text("")
            $('.invalid-type_module_id').text("")
            $('.invalid-module_name').text("")
        }

        /** INITIAL **/
        function initialAcceptExtension(selected = null) {
            let strExtention = extentionsType.video.concat(extentionsType.document).join(",")
            if (selected != null && selected.toLowerCase() == 'video') {
                strExtention = extentionsType.video.concat(extentionsType.video).join(",")
            }

            $(`#${formModuleState.file}`).attr("accept", strExtention);
        }

        function initialCategoryModule(selected = null) {
            const url = "{!! route('master.module.category.all') !!}";
            initJQSelect2Selection(formState.category, url, "Pilih Kategori Modul", selected)
        }

        function initialSubCategoryModule(selected = null) {
            const url = "{!! route('master.module.sub_category.all') !!}";
            initJQSelect2Selection(formState.sub_category, url, "Pilih Sub Kategori Modul", selected)
        }

        function initialTypeModule(selected = null) {
            const url = "{!! route('master.module.type.all') !!}"
            initJQSelect2Selection(formState.type, url, "Pilih Tipe Modul", selected)
        }

        function addFile() {
            $('#inputFileUpload').trigger('click');
        }

        function removeItemList(i) {
            temporaryList = temporaryList.map((value, index) => {
                if (index == i) {
                    value.deleted = true
                }
                return value;
            });
            console.log(temporaryList);
        }

        $(document).ready(function() {
            if (learningModule.type_module.name.toLowerCase() == 'video') {
                typeModeSetting = 'video';
            } else {
                typeModeSetting = 'non-video';
            }


            CKEDITOR.replace(`${formState.description}`, {
                extraPlugins: 'justify,font,editorplaceholder,colorbutton,colordialog',
                uiColor: '#CCEAEE',
                language: 'id',
                editorplaceholder: 'Isi keterangan..',
                removeButtons: 'PasteFromWord'
            });

            CKEDITOR.instances?.[`${formState.description}`].setData(learningModule.module_desc)

            /** Initial Select2 Category Modul **/
            initialCategoryModule(learningModule.category_module_id);
            initialSubCategoryModule(learningModule.sub_category_module_id);
            initialTypeModule(learningModule.type_module_id);
            initialAcceptExtension(learningModule.type_module.name);

            if (learningModule.can_download) {
                $(`input:radio[name=${formState.download}]`).filter('[value=1]').prop('checked', true);
            } else {
                $(`input:radio[name=${formState.download}]`).filter('[value=0]').prop('checked', true);
            }

            if (learningModule.has_quiz) {
                $(`input:radio[name=${formState.quiz}]`).filter('[value=1]').prop('checked', true);
            } else {
                $(`input:radio[name=${formState.quiz}]`).filter('[value=0]').prop('checked', true);
            }

            if (learningModule.file_modules?.length > 0) {
                for (let a = 0; a <= learningModule.file_modules.length - 1; a++) {
                    const item = learningModule.file_modules[a];
                    temporaryList.push({
                        id: item.id,
                        topic: item.topic,
                        description: item.description,
                        files: null,
                        deleted: false
                    });

                }
                fillInfoFile(temporaryList);
            }

            $(`#${formModuleState.actionBrowseFile}`).click(function(e) {
                e.preventDefault();
                $(`#${formModuleState.file}`).trigger('click');
            });

            /** Event On Choose File */
            $(`#${formModuleState.file}`).change(function(e) {
                temporaryFile = e.target.files[0];

                let strElement = '';
                if (e.target.files.length > 0) {
                    $.each(e.target.files, function(index, value) {
                        const filename = value.name
                        strElement +=
                            `<div class="badge badge-success bg-success badge-lg text-white mb-2">${filename}</div><br />`
                    });
                }

                $(`#${formModuleState.coverUpload}`).empty();
                $(`#${formModuleState.coverUpload}`).html(strElement);
            });

            $(`#${formModuleState.actionSave}`).click(function(e) {
                e.preventDefault();
                if (typeModeSetting == 'video') {
                    temporaryList.push({
                        id: '',
                        topic: $(`#${formModuleState.topic}`).val(),
                        description: $(`#${formModuleState.description}`).val(),
                        files: $(`#${formModuleState.link}`).val(),
                        deleted: false
                    });

                } else {
                    temporaryList.push({
                        id: '',
                        topic: $(`#${formModuleState.topic}`).val(),
                        description: $(`#${formModuleState.description}`).val(),
                        files: temporaryFile,
                        deleted: false
                    });
                }

                console.log(temporaryList)
                fillInfoFile(temporaryList);
                eventAddFileModule("hide");
            });


            formState.type.on('select2:select', function(e) {
                const value = e.params.data;
                if (value.text.toLowerCase() == 'video') {
                    $(`#${formModuleState.file}`).attr('accept', extentionsType.video.join(","))
                } else if (value.text.toLowerCase() == 'e-book') {
                    $(`#${formModuleState.file}`).attr('accept', extentionsType.document.join(","))
                } else {}
            });


            $("#buttonSaveAction").click(function(e) {
                e.preventDefault();
                refreshErrorTagMessage();

                const formData = new FormData();

                formData.append("module_name", formState.name.val());
                formData.append("module_desc", CKEDITOR.instances?.[`${formState.description}`].getData());
                formData.append("category_module_id", formState.category.val() != null ? formState.category
                    .val() : '');
                formData.append("sub_category_module_id", formState.sub_category.val() != null ? formState
                    .sub_category.val() : '');
                formData.append("type_module_id", formState.type.val() != null ? formState.type.val() : '');
                formData.append("can_download", $(`input[name="${formState.download}"]:checked`)
                    .val());
                formData.append("has_quiz", $(`input[name="${formState.quiz}"]:checked`)
                    .val());
                formData.append("type_mode_setting", typeModeSetting);


                if (temporaryList.length > 0) {

                    if (typeModeSetting == 'video') {
                        for (let i = 0; i < temporaryList.length; i++) {
                            formData.append('sub_modules[]', JSON.stringify(temporaryList[i]));
                        }
                    } else {
                        for (let i = 0; i < temporaryList.length; i++) {
                            formData.append('sub_modules[]', JSON.stringify(temporaryList[i]));
                            formData.append(`file_upload_${i}`, temporaryList[i].files);
                        }
                    }


                }


                const url = "{!! route('learning_modules.update', $record->id) !!}"

                axios.post(url, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {

                    if (response.status == 201 || response.status == 200) {
                        const {
                            data
                        } = response;

                        Swal.fire({
                            title: 'BERHASIL',
                            icon: 'success',
                            text: data.message,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{!! route('learning_modules.list') !!}";
                            }
                        });
                    }


                }).catch(error => {
                    const {
                        status,
                        data
                    } = error.response;

                    switch (status) {
                        case 422:
                            if (data.errors.hasOwnProperty('category_module_id')) {
                                $('.invalid-category_module_id').text(data.errors
                                        .category_module_id[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('sub_category_module_id')) {
                                $('.invalid-sub_category_module_id').text(data.errors
                                        .sub_category_module_id[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('type_module_id')) {
                                $('.invalid-type_module_id').text(data.errors.type_module_id[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }

                            if (data.errors.hasOwnProperty('module_name')) {
                                $('.invalid-module_name').text(data.errors.module_name[0])
                                    .css(
                                        'font-size',
                                        '0.9rem');
                            }
                            break;

                        default:
                            showErrorPopup(error)
                            break;
                    }
                });
            });
        });
    </script>
@endsection

<x-app-layout>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 p-3">
                        <div class="col-8">
                            <h4 class="card-title fw-bold">
                                <i class="fas fa-hdd"></i> &nbsp;
                                FORM UBAH MODUL PEMBELAJARAN
                            </h4>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="d-flex flex-row-reverse mb-3">
                                    <div class="d-grid mx-2">
                                        <a href="{{ route('learning_modules.list') }}"
                                            class="btn btn-primary btn-sm text-white me-0">
                                            <i class="fas fa-arrow-alt-circle-left"></i>
                                            &nbsp; Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2 p-3">

                        <div class="col-4">
                            <!-- CATEGORY MODUL -->
                            <div class="mb-3">
                                <label for="selectionCategoryModule" class="form-label fw-bold">
                                    Kategori Modul
                                </label>
                                <select id="selectionCategoryModule" class="form-select"
                                    data-placeholder="Kategori Modul"></select>
                                <div class="text-danger text-sm fst-italic invalid-category_module_id"></div>
                            </div>
                        </div>
                        <div class="col-4">
                            <!-- SUB CATEGORY MODUL -->
                            <div class="mb-3">
                                <label for="selectionSubCategoryModule" class="form-label fw-bold">
                                    Sub Kategori Modul
                                </label>
                                <select id="selectionSubCategoryModule" class="form-select"
                                    data-placeholder="Sub Kategori Modul"></select>

                                <div class="text-danger text-sm fst-italic invalid-sub_category_module_id"></div>
                            </div>
                        </div>
                        <div class="col-4">
                            <!-- TIPE MODUL -->
                            <div class="mb-3">
                                <label for="selectionTypeModule" class="form-label fw-bold">Tipe Modul</label>
                                <select id="selectionTypeModule" class="form-select"
                                    data-placeholder="Tipe Modul"></select>
                                <div class="text-danger text-sm fst-italic invalid-type_module_id"></div>
                            </div>
                        </div>


                        <div class="col-12">

                            <!-- MODUL NAME -->
                            <div class="mb-3">
                                <label for="textModulName" class="form-label fw-bold">Nama Modul Pembelajaran</label>
                                <input id="textModulName" class="form-control" type="text"
                                    placeholder="Nama Modul Pembelajaran..." value="{{ $record->module_name }}" />
                                <div class="text-danger text-sm fst-italic invalid-module_name"></div>
                            </div>

                            <!-- KETERANGAN MODUL -->
                            <div class="mb-3">
                                <label for="textModulDescription" class="form-label fw-bold">Keterangan</label>
                                <textarea cols="80" id="textModulDescription" name="textModulDescription" class="form-control" rows="10"
                                    data-sample-short></textarea>
                            </div>
                        </div>


                        <div class="col-6">
                            <!-- CAN DOWNLOAD -->
                            <div class="mb-3">
                                <label for="radioDownload" class="form-label fw-bold">
                                    Apakah modul pembelajaran ini dapat di download ?
                                </label>
                                <div id="radioDownload" class="d-flex justify-content-start">
                                    <div class="px-2">
                                        <input class="form-check-input" type="radio" name="radioCanDownload"
                                            id="radioDownload1" value="1" checked>
                                        <label class="form-check-label" for="radioDownload1">
                                            Ya
                                        </label>
                                    </div>

                                    <div class="px-2">
                                        <input class="form-check-input" type="radio" name="radioCanDownload"
                                            id="radioDownload2" value="0">
                                        <label class="form-check-label" for="radioDownload2">
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <!-- HAS QUIZ -->
                            <div class="mb-3">
                                <label for="radioQuiz" class="form-label fw-bold">
                                    Apakah terdapat kuis dalam modul pembelajaran ini ?
                                </label>
                                <div id="radioQuiz" class="d-flex justify-content-start">
                                    <div class="px-2">
                                        <input class="form-check-input" type="radio" name="radioHasQuiz"
                                            id="radioQuiz1" value="1" checked>
                                        <label class="form-check-label" for="radioQuiz1">
                                            Ya, Ada
                                        </label>
                                    </div>
                                    <div class="px-2">
                                        <input class="form-check-input" type="radio" name="radioHasQuiz"
                                            id="radioQuiz2" value="0">
                                        <label class="form-check-label" for="radioQuiz2">
                                            Tidak Ada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <!-- INSERT SUB MODULE -->
                            <div class="mb-3">
                                <button class="btn btn-dark btn-sm" onclick="eventAddFileModule()">
                                    <i class="fas fa-plus"></i>
                                    Berkas Modul Pembelajaran
                                </button>
                            </div>

                            <div id="infoFile" class="mb-3"></div>
                        </div>




                        <div class="col-12">
                            <div class="d-grid gap-2">
                                <button type="button" class="btn btn-primary" id="buttonSaveAction">
                                    <i class="fas fa-save"></i> &nbsp;
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.version.modul.modal_berkas')
</x-app-layout>
