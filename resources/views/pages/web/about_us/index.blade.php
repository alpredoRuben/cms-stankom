@section('title')
    TENTANG KAMI
@endsection


@section('metadata')
    <meta content="Tentang Kami" name="keywords" />
    <meta content="LMS Stankom - Halaman Tentang Kami" name="description" />
@endsection

@section('stylesheets')
    <style type="text/css">
        .wrimagecard {
            margin-top: 0;
            margin-bottom: 1.5rem;
            text-align: center;
            position: relative;
            box-shadow: 12px 15px 20px 0px rgba(46, 61, 73, 0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

        .big-icon {
            font-size: 50px;
        }

        .wrimagecard .fa {
            position: relative;
            font-size: 70px;
        }

        .wrimagecard-topimage_header {
            padding: 20px;
        }

        a.wrimagecard:hover,
        .wrimagecard-topimage:hover {
            box-shadow: 2px 4px 8px 0px rgba(46, 61, 73, 0.2);
        }

        .wrimagecard-topimage a {
            width: 100%;
            height: 100%;
            display: block;
        }

        .wrimagecard-topimage_title {
            padding: 20px 24px;
            height: 80px;
            padding-bottom: 0.75rem;
            position: relative;
        }

        .wrimagecard-topimage a {
            border-bottom: none;
            text-decoration: none;
            color: #525c65;
            transition: color 0.3s ease;
        }
    </style>
@endsection

<x-guest-layout>

    {{-- About Us --}}
    <div id="aboutUs" class="container-xxl py-6 my-4 mt-0 wow fadeIn" data-wow-delay="0.1s">
        @if ($aboutUs)
            <div class="container">
                <div class="row mb-2 g-5">
                    <div class="col-lg-12 wow fadeInUp" data-wow-delay="0.1s">
                        <img src="{{ $aboutUs->filepath ?? asset('images/e-learning.jpg') }}" alt=""
                            style="max-height: 500px; width:100%; object-fit: fill">
                    </div>
                    <div class="col-lg-12 wow fadeInUp" data-wow-delay="0.5s">
                        <div class="h-100 mt-5">
                            <h3 class="text-dark mb-2">Tentang Kami</h3>
                            <div>
                                {!! $aboutUs->content ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {{-- Contact Us --}}
    <div id="contactUs" class="container-xxl py-6 my-4 mt-0 wow fadeIn">
        @if ($contactUs)
            <div class="container">
                <div class="row mb-2 mt-5 g-5" data-wow-delay="0.1s">
                    <div class="col-12">
                        <h3>
                            Kontak {{ $contactUs->company_name ?? 'STANKOM' }}
                        </h3>
                    </div>
                    <div class="col-4">
                        <a href="#">
                            <div class="wrimagecard-topimage_header text-center">
                                <span class="big-icon">
                                    <i class="fas fa-map-marker" style="color:#16A085"></i>
                                </span>
                            </div>
                            <div class="wrimagecard-topimage_title text-center">
                                <p class="text-dark fs-5">
                                    {{ $contactUs->address ?? '' }}
                                    {{ $contactUs && $contactUs->ward ? ', ' . $contactUs->ward->name : '' }}
                                </p>
                            </div>
                        </a>
                    </div>

                    <div class="col-4">
                        <a href="#">
                            <div class="wrimagecard-topimage_header text-center">
                                <span class="big-icon">
                                    <i class="fas fa-phone" style="color:#083d99"></i>
                                </span>
                            </div>
                            <div class="wrimagecard-topimage_title text-center">
                                <p class="text-dark fs-5">
                                    {{ $contactUs->phone ?? '' }}
                                </p>
                            </div>
                        </a>
                    </div>

                    <div class="col-4">
                        <a href="#">
                            <div class="wrimagecard-topimage_header text-center">
                                <span class="big-icon">
                                    <i class="fas fa-envelope" style="color: #8b0bb3"></i>
                                </span>
                            </div>
                            <div class="wrimagecard-topimage_title text-center">
                                <p class="text-dark fs-5">
                                    {{ $contactUs->email ?? '' }}
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        @endif
    </div>

</x-guest-layout>
