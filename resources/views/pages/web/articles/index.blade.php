@section('title')
    ARTIKEL
@endsection


@section('metadata')
    <meta content="Artikel, Articles" name="keywords" />
    <meta content="LMS Stankom - Halaman Artikel" name="description" />
@endsection

@section('stylesheets')
    <style type="text/css">
        .stretch-card {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
            -webkit-box-pack: stretch;
            -ms-flex-pack: stretch;
            justify-content: stretch;
        }

        .position-relative {
            position: relative !important;
            box-shadow: 5px 10px #fff;
        }

        .stretch-card .card {
            width: 100%;
        }

        .grid-margin {
            margin-bottom: 2rem;
        }

        .border-bottom-blue {
            border-bottom: solid 1px #032a63;
        }


        .banner-content {
            width: 850px;
            position: absolute;
            background-color: #3a5057;
            padding: 20px 60px;
            left: 0px;
            bottom: 0px;
            opacity: .95;
        }

        @media (max-width: 999px) {
            .banner-content {
                padding: 12px 12px;
            }
        }

        .pr-3 {
            padding-right: 1rem !important;
        }

        .image-section {
            max-width: 100%;
            height: auto;
        }

        .image-lg {
            width: 350px;
        }


        .rotate-img {
            overflow: hidden;
        }

        .fs-12 {
            font-size: 12px;
        }

        .stan {
            font-size: 14px;
        }

        .image-headline {
            width: 850px;
            height: 500px;
        }

        .static-height: {
            height: 500px;
        }
    </style>
@endsection

@section('javascripts')
    <script>
        function fetchLatestArticles(page) {
            let url = "{!! route('guest.articles.latest') !!}?page=" + page

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverLatestArticle').html(data);
                }
            });
        }

        function fetchOldestArticles(page) {
            let url = "{!! route('guest.articles.latest') !!}?page=" + page

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverOldestArticle').html(data);
                }
            });
        }

        $(document).ready(function() {});
    </script>
@endsection

<x-guest-layout>
    <!-- Page Header Start -->
    <div class="container-fluid py-6 my-6 mt-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container">
            <div class="row mb-2 px-4">
                <div class="col-12">
                    <h1 class="display-6 mb-4">Artikel</h1>
                </div>
            </div>

            @if ($headlines)
                <div class="row mb-2 px-4">
                    <div class="col-xl-8 stretch-card grid-margin">
                        <div class="position-relative">
                            <a href="{{ route('guest.articles.show', $headlines->id) }}">
                                <img src="{{ $headlines->article_photo ?? asset('images/images.jpeg') }}" alt="banner"
                                    class="image-headline" />
                            </a>
                            <div class="banner-content">
                                <div class="badge bg-danger fs-12 mb-3">
                                    Headline Articles
                                </div>
                                <br />
                                <a href="{{ route('guest.articles.show', $headlines->id) }}"
                                    class="mb-0 fs-4 text-white">
                                    {{ $headlines->article_title }}
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 stretch-card grid-margin">
                        <div class="card bg-dark static-height overflow-auto overflow-y-hidden"
                            style="height: 500px; scroll-behavior: smooth; border: none;">
                            <div class="card-body">
                                <p class="text-primary fs-4 fw-bold">Artikel Terbaru</p>

                                @if (count($updateArticles) > 0)
                                    @foreach ($updateArticles as $item)
                                        <div
                                            class="d-flex border-bottom-blue pt-3 pb-4 align-items-center justify-content-between">
                                            <div class="pr-3">
                                                <a href="{{ route('guest.articles.show', $item->id) }}"
                                                    class="text-white stan fw-bold">
                                                    {{ $item->article_title }}
                                                </a>
                                                <div class="fs-12">
                                                    {{ $item->updated_at ? \Carbon\Carbon::parse($item->updated_at)->diffForHumans() : '' }}
                                                </div>
                                            </div>
                                            <div class="rotate-img">
                                                <img src="{{ $item->article_photo ?? asset('images/images.jpeg') }}"
                                                    alt="thumb" class="image-section image-lg" />
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            @endif


            @if (count($latestArticles->getCollection()) > 0)
                <div class="row mb-2 px-4" data-aos="fade-up">
                    <div class="col-lg-12">
                        <h5>Artikel Terbaru</h5>
                    </div>
                </div>

                <div id="coverLatestArticle" class="row mb-2 px-4">
                    @include('pages.web.articles.latest_articles', compact('latestArticles'))
                </div>
            @endif


            @if (count($oldestArticles->getCollection()) > 0)
                <div class="row mb-2 px-4" data-aos="fade-up">
                    <div class="col-lg-12">
                        <h5>{{ count($latestArticles->getCollection()) > 0 ? 'Artikel Sebelumnya' : 'Daftar Artikel' }}
                        </h5>
                    </div>
                </div>

                <div id="coverOldestArticle" class="row mb-2 px-4">
                    @include('pages.web.articles.oldest_articles', compact('oldestArticles'))
                </div>
            @endif

        </div>
    </div>
    <!-- Page Header End -->
</x-guest-layout>
