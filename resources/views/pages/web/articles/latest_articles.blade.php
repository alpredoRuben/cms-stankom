<div class="col-lg-12 stretch-card grid-margin">
    <div class="card">
        <div class="card-body">
            @foreach ($latestArticles->getCollection() as $item)
                <div class="row">
                    <div class="col-sm-4 grid-margin">
                        <div class="position-relative">
                            <a href="{{ route('guest.articles.show', $item->id) }}" class="rotate-img">
                                <img src="{{ $item->article_photo ?? asset('images/images.jpeg') }}" alt="thumb"
                                    class="img-fluid" />
                            </a>
                            <div class="badge-positioned">
                                <span class="badge badge-danger font-weight-bold">latest news</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8  grid-margin">
                        <a href="{{ route('guest.articles.show', $item->id) }}" class="text-dark mb-2 fw-bold fs-4">
                            {{ $item->article_title }}
                        </a>
                        <div class="fs-12 mb-2">
                            {{ $item->updated_at ? \Carbon\Carbon::parse($item->updated_at)->diffForHumans() : '' }}
                        </div>
                        <p class="mb-0">
                            @php
                                $newsContent = $item->article_content ? strip_tags($item->article_content) : '';
                                echo strlen($newsContent) > 400 ? substr($newsContent, 0, 400) . '....(selanjutnya)' : $newsContent;
                            @endphp
                        </p>
                        @if ($item->articleTagList && count($item->articleTagList) > 0)
                            <p class="mt-2">
                                @php
                                    $split = '';
                                    foreach ($item->articleTagList as $key => $value) {
                                        $split .= "<span class='badge bg-info'>{$value->tag->tag_name}</span>";

                                        if ($key < count($item->articleTagList) - 1) {
                                            $split .= ' ';
                                        }
                                    }

                                    echo $split;
                                @endphp
                            </p>
                        @endif

                    </div>
                </div>
            @endforeach


            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-end">

                            @if ($latestArticles->onFirstPage())
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                </li>
                            @else
                                <li class="page-item">
                                    <button type="button" class="page-link" tabindex="-1"
                                        onclick="fetchLatestArticles('{{ $latestArticles->currentPage() - 1 }}')">Previous</button>
                                </li>
                            @endif


                            @if ($latestArticles->hasMorePages())
                                <li class="page-item">
                                    <button type="button" class="page-link"
                                        onclick="fetchLatestArticles('{{ $latestArticles->currentPage() + 1 }}')">Next</button>
                                </li>
                            @else
                                <li class="page-item disabled">
                                    <button type="button" class="page-link" aria-disabled="true">Next</button>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>
