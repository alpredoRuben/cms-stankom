@section('title')
    BERITA
@endsection


@section('metadata')
    <meta content="Berita" name="keywords" />
    <meta content="LMS Stankom - Halaman Berita" name="description" />
@endsection

@section('stylesheets')
@endsection

@section('javascripts')
    <style>
        .image-section {
            width: 100%;
            height: 550px;
        }
    </style>
@endsection

<x-guest-layout>
    <!-- Page Header Start -->
    <div class="container-fluid py-6 my-6 mt-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container">
            <div class="row mb-2 px-4">
                <div class="col-12">
                    <p class="text-success mb-4">
                        DETAIL ARTIKEL
                    </p>
                </div>
            </div>

            <div class="row mb-4 px-4">
                <div class="col-12 stretch-card grid-margin">
                    <h3 class="display-6 mb-4">
                        {{ $articles->article_title }}
                    </h3>

                    <div class="position-relative">
                        <img src="{{ $articles->article_photo ?? asset('images/images.jpeg') }}" alt="banner"
                            class="image-section" />
                    </div>
                </div>
            </div>

            <div class="row mb-2 px-4">
                <div class="col-12">
                    {!! $articles->article_content !!}
                </div>
            </div>

        </div>
    </div>
    <!-- Page Header End -->
</x-guest-layout>
