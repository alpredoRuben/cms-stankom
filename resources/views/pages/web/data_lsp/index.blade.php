@section('title')
    DATA LSP
@endsection


@section('metadata')
    <meta content="Data LSP" name="keywords" />
    <meta content="LMS Stankom - Halaman Data LSP" name="description" />
@endsection

{{-- STYLESHEET --}}
@section('stylesheets')
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
    <style>
        .backgroundSlate {
            background-color: #eef1f2
        }

        .bg-flate {
            background-color: #f8f8ff
        }

        .bg-card-header {
            background-color: #034380;
            color: #eee;
        }

        .search_select_box {
            max-width: 400px;
            margin: 10px auto;
        }

        .search_select_box select {
            width: 100%;
        }

        .search_select_box button {
            background-color: #3c59ab !important;
            padding: 10px 20px;
            color: #fff !important
        }

        .dropdown-item.active,
        .dropdown-item:active {
            color: #fff;
            text-decoration: none;
            background-color: #3c59ab !important;
        }
    </style>
@endsection



@section('javascripts')
    <script src="{{ asset('js/globals.js') }}"></script>
    <script>
        var tableState = {
            url: "{!! route('guest.datatable_lsp') !!}",
            tableName: "tableLSP",
            filters: {
                sertification: "-"
            },
            columns: [{
                    orderable: false,
                    className: "text-center align-middle"
                },
                {
                    data: "lsp_name",
                    name: "lsp_name",
                    orderable: false,
                    className: "align-middle  text-nowrap",
                },
                {
                    data: "lsp_email",
                    name: "lsp_email",
                    orderable: false,
                    className: "align-middle  text-nowrap",
                },
                {
                    data: "lsp_phone",
                    name: "lsp_phone",
                    orderable: false,
                    className: "align-middle  text-nowrap",
                },
                {
                    data: "lsp_website",
                    name: "lsp_website",
                    orderable: false,
                    className: "align-middle  text-nowrap",
                }

            ]

        }

        function initSelected(element, url) {
            element.selectpicker();
            axios.get(url).then(res => {
                const {
                    data
                } = res;
                element.empty();
                element.append(`<option value="-" selected="true">Semuanya</option>`);
                $.each(data.results, function(index, value) {
                    element.append(`<option value="${value.id}" selected="true">${value.name}</option>`)
                });
                element.val('-')
                element.selectpicker('refresh')
            });
        }

        function reloadDatatable() {
            $(`#${tableState.tableName}`).DataTable().ajax.reload();
        }

        function getCertificationType() {
            const url = "{!! route('guest.certification_types') !!}";
            initSelected($("#selectCategorySertification"), url);
        }


        $(document).ready(function() {
            tableState.filters.sertification = "-";
            getCertificationType();

            const url = "{!! route('guest.certification_types') !!}";


            var dTables = $('#' + tableState.tableName).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                searching: false,
                orderable: true,
                lengthChange: false,
                pageLength: 10,
                ajax: {
                    url: tableState.url,
                    data: function(d) {
                        d.sertification = tableState.filters.sertification;
                    },
                },
                columnDefs: [{
                    orderable: false,
                    targets: 0
                }, {
                    defaultContent: "",
                    targets: "_all",
                }, ],
                columns: tableState.columns,
                fnRowCallback: function(nRow, aData, iDisplayIndex) {
                    $('td:eq(0)', nRow).html(aData.DT_RowIndex);
                    return nRow;
                }

            });

            dTables.on("xhr", function(e, settings, json) {});

            $("#selectCategorySertification").change(function(e) {
                e.preventDefault();
                tableState.filters.sertification = $(this).val();
                reloadDatatable()
            });
        });
    </script>
@endsection

<x-guest-layout>
    <div class="container-xxl py-6">
        <div class="container">
            <div class="row mb-2 wow fadeInUp" data-wow-delay="0.1s">
                <div class="col-12">
                    <h1 class="display-6 mb-4">Lembaga Sertifikasi Profesi (LSP)</h1>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-12">
                    <div class="d-flex flex-row mb-3">
                        <div class="p-3">
                            <label for="selectCategorySertification" class="form-label text-primary fs-5 fw-bold">
                                Kategori Bidang Sertifikasi
                            </label>
                        </div>

                        <div style="width: 30%">
                            <div class="search_select_box">
                                <select id="selectCategorySertification" class="w-100"
                                    data-live-search="true"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-12 mb-2">
                    <div class="table-responsive">
                        <table id="tableLSP" class="table table-hover" width="100%">
                            <thead class="bg-dark text-white">
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA LSP</th>
                                    <th>EMAIL</th>
                                    <th>NO. TELEPON/HP</th>
                                    <th>WEBSITE</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
