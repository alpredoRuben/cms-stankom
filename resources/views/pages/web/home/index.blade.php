@section('title')
    HOME
@endsection


@section('metadata')
    <meta content="Home" name="keywords" />
    <meta content="LMS Stankom - Halaman Home" name="description" />
@endsection

@section('stylesheets')
    <style>
        .text-black {
            color: #000;
        }

        .carousel-caption {
            top: auto;
            bottom: 0;
        }
    </style>
@endsection

<x-guest-layout>
    <div class="container-fluid p-0 wow fadeIn" data-wow-delay="0.1s">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                @if (count($banners) > 0)
                    @foreach ($banners as $key => $item)
                        <button type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"
                            aria-current="true" aria-label="Slide 1"></button>
                    @endforeach
                @endif
            </div>
            <div class="carousel-inner">
                @if (count($banners) > 0)
                    @foreach ($banners as $key => $item)
                        <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                            <img src="{{ $item->banner_image }}" class="d-block w-100" alt="image-{{ $key }}">

                            <div class="carousel-caption">
                                <div class="d-flex flex-column mb-2 px-5 py2">
                                    <div class="fs-3 fw-bold text-light animated slideInDown">
                                        {{ $item->banner_title }}
                                    </div>
                                    <div class="mb-3">
                                        <i>{{ $item->banner_content }}</i>
                                    </div>
                                    <div class="mb-5">
                                        <a href="{{ $item->link }}" target="_blank"
                                            class="btn btn-primary">KUNJUNGI</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>


    @if ($trainingRules && count($trainingRules) > 0)
        <div id="kategoriBidangUsaha" class="container-xxl py-6">
            <div class="container">
                <div class="row mb-4">
                    <h4 class="text-dark">Aturan Pelatihan</h4>
                </div>
                <div class="row">
                    @foreach ($trainingRules as $item)
                        <div class="col-12">
                            <div class="d-flex flex-row">
                                <div class="p-1 mt-2">
                                    <div class="btn-square bg-primary me-3">
                                        <i class="fa fa-check  text-white"></i>
                                    </div>
                                </div>
                                <div class="p-1 mt-0">
                                    <h5 class="mb-0">
                                        {{ $item->title }}
                                    </h5>
                                    <div>
                                        {!! $item->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    @endif

    <div id="kategoriBidangUsaha" class="container-xxl py-6">
        <div class="container">
            <div class="row mb-4">
                <h4 class="text-dark">Kategori Bidang Usaha</h4>
            </div>
            <div class="row">
                @if ($bidangCategory['Pariwisata'] && count($bidangCategory['Pariwisata']) > 0)
                    <div class="col-6">
                        <table class="table table-hover">
                            <thead>
                                <tr class="bg-dark text-white fs-4">
                                    <th>PARIWISATA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bidangCategory['Pariwisata'] as $item)
                                    <tr>
                                        <td class="text-black fs-5">{{ $item->module_name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif


                @if ($bidangCategory['Ekraf'] && count($bidangCategory['Ekraf']) > 0)
                    <div class="col-6">
                        <table class="table">
                            <thead>
                                <tr class="bg-dark text-white fs-4">
                                    <th>EKRAF</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bidangCategory['Ekraf'] as $item)
                                    <tr>
                                        <td class="text-black fs-5">{{ $item->module_name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>


    @if ($latestNews && count($latestNews) > 0)
        <div id="berita" class="container-xxl py-6">
            <div class="container">
                <div class="row mb-4">
                    <h4 class="text-dark">Berita</h4>
                </div>
                <div class="row">
                    <div class="card shadow border-none">
                        <div class="card-body">
                            @foreach ($latestNews as $item)
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="position-relative">
                                            <a href="{{ route('guest.news.show', $item->id) }}" class="rotate-img">
                                                <img src="{{ $item->news_photo ?? asset('images/images.jpeg') }}"
                                                    alt="thumb" class="img-fluid" />
                                            </a>
                                            <div class="badge-positioned">
                                                <span class="badge badge-danger font-weight-bold">latest news</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">

                                        <a href="{{ route('guest.news.show', $item->id) }}"
                                            class="text-dark mb-2 fw-bold fs-4">
                                            {{ $item->news_title }}
                                        </a>
                                        <div class="fs-12 mb-2">
                                            {{ $item->updated_at ? \Carbon\Carbon::parse($item->updated_at)->diffForHumans() : '' }}
                                        </div>
                                        <p class="mb-0">
                                            @php
                                                $newsContent = $item->news_content ? strip_tags($item->news_content) : '';
                                                echo strlen($newsContent) > 400 ? substr($newsContent, 0, 400) . '....(selanjutnya)' : $newsContent;
                                            @endphp
                                        </p>

                                        @if (count($item->newsTagList) > 0)
                                            <p class="mt-2">
                                                @php
                                                    $split = '';
                                                    foreach ($item->newsTagList as $key => $value) {
                                                        $split .= "<span class='badge bg-info'>{$value->tag->tag_name}</span>";

                                                        if ($key < count($item->newsTagList) - 1) {
                                                            $split .= ' ';
                                                        }
                                                    }

                                                    echo $split;
                                                @endphp
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('web.berita') }}" class="btn btn-primary">
                                        Lainnya
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if ($latestArticles && count($latestArticles) > 0)
        <div id="artikel" class="container-xxl py-6">
            <div class="container">
                <div class="row mb-4">
                    <h4 class="text-dark">Artikel</h4>
                </div>
                <div class="row">
                    <div class="card shadow border-none">
                        <div class="card-body">
                            @foreach ($latestArticles as $item)
                                <div class="row">
                                    <div class="col-sm-4 grid-margin">
                                        <div class="position-relative">
                                            <a href="{{ route('guest.articles.show', $item->id) }}" class="rotate-img">
                                                <img src="{{ $item->article_photo ?? asset('images/images.jpeg') }}"
                                                    alt="thumb" class="img-fluid" />
                                            </a>
                                            <div class="badge-positioned">
                                                <span class="badge badge-danger font-weight-bold">latest news</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8  grid-margin">
                                        <a href="{{ route('guest.articles.show', $item->id) }}"
                                            class="text-dark mb-2 fw-bold fs-4">
                                            {{ $item->article_title }}
                                        </a>
                                        <div class="fs-12 mb-2">
                                            {{ $item->updated_at ? \Carbon\Carbon::parse($item->updated_at)->diffForHumans() : '' }}
                                        </div>
                                        <p class="mb-0">
                                            @php
                                                $newsContent = $item->article_content ? strip_tags($item->article_content) : '';
                                                echo strlen($newsContent) > 400 ? substr($newsContent, 0, 400) . '....(selanjutnya)' : $newsContent;
                                            @endphp
                                        </p>
                                        @if ($item->articleTagList && count($item->articleTagList) > 0)
                                            <p class="mt-2">
                                                @php
                                                    $split = '';
                                                    foreach ($item->articleTagList as $key => $value) {
                                                        $split .= "<span class='badge bg-info'>{$value->tag->tag_name}</span>";

                                                        if ($key < count($item->articleTagList) - 1) {
                                                            $split .= ' ';
                                                        }
                                                    }

                                                    echo $split;
                                                @endphp
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('web.artikel') }}" class="btn btn-primary">
                                        Lainnya
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

</x-guest-layout>
