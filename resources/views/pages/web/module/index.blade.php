@section('title')
    MODUL PEMBELAJARAN
@endsection


@section('metadata')
    <meta content="Modul, Modul Pembelajaran" name="keywords" />
    <meta content="LMS Stankom - Halaman Modul" name="description" />
@endsection

{{-- STYLESHEET --}}
@section('stylesheets')
    <style>
        .backgroundSlate {
            background-color: #eef1f2
        }

        .bg-flate {
            background-color: #f8f8ff
        }

        .bg-card-header {
            background-color: #034380;
            color: #eee;
        }

        .search_select_box {
            max-width: 400px;
            margin: 10px auto;
        }

        .button_box {
            max-width: 400px;
            margin: 10px auto;
        }

        .search_select_box select {
            width: 100%;
        }

        .search_select_box button {
            background-color: #3c59ab !important;
            padding: 10px 20px;
            color: #fff !important
        }

        .dropdown-item.active,
        .dropdown-item:active {
            color: #fff;
            text-decoration: none;
            background-color: #3c59ab !important;
        }

        .carousel-control-prev {
            left: -50px;
            margin-top: 10px;
        }

        .carousel-control-next {
            right: -50px;
            margin-top: 10px;
        }

        .carousel-control-prev,
        .carousel-control-next {
            background-color: #0a4c97;
            width: 6vh;
            height: 6vh;
            border-radius: 50%;
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
@endsection

{{-- JAVASCRIPT --}}
@section('javascripts')
    <script>
        var stateModule = {
            category: $("#selectCategoryModule"),
            sub_category: $("#selectSubCategoryModule"),
            type: $("#selectTypeModule"),
        }

        var filterSelection = {
            category: '-',
            subCategory: '-',
            type: '-'
        }

        function initSelected(element, url) {
            element.selectpicker();
            axios.get(url).then(res => {
                const {
                    data
                } = res;
                element.empty();
                element.append(`<option value="-" selected="true">Semuanya</option>`);
                $.each(data.data, function(index, value) {
                    element.append(`<option value="${value.id}" selected="true">${value.name}</option>`)
                });
                element.val('-')
                element.selectpicker('refresh')
            });
        }

        function getCategoryModule() {
            const url = "{!! route('guest.category_modules') !!}";
            initSelected(stateModule.category, url);

        }

        function getSubCategoryModule() {
            const url = "{!! route('guest.sub_category_modules') !!}";
            initSelected(stateModule.sub_category, url)
        }

        function getTypeModule() {
            const url = "{!! route('guest.type_modules') !!}";
            initSelected(stateModule.type, url);
        }


        function fetchModule(page = 1) {
            let url = "{!! route('web.modul') !!}?page=" + page;
            let str = '';
            if (filterSelection.category != '-') {
                str += `&category=${filterSelection.category}`
            }

            if (filterSelection.subCategory != '-') {
                str += `&sub_category=${filterSelection.subCategory}`
            }

            if (filterSelection.type != '-') {
                str += `&type=${filterSelection.type}`
            }

            if (str != '') {
                url += str;
            }

            console.log(str);
            console.log(url)

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverModule').html(data);
                }
            });
        }

        function eventRefreshPage() {
            window.location.href = "{!! route('web.modul') !!}"
        }


        $(document).ready(function() {
            getCategoryModule();
            getSubCategoryModule();
            getTypeModule();

            stateModule.category.change(function(e) {
                e.preventDefault();
                filterSelection.category = $(this).val();
                fetchModule();
            });

            stateModule.sub_category.change(function(e) {
                e.preventDefault();
                filterSelection.subCategory = $(this).val()
                fetchModule()
            });

            stateModule.type.change(function(e) {
                e.preventDefault();
                filterSelection.type = $(this).val()
                fetchModule()
            });
        });
    </script>
@endsection

<x-guest-layout>
    <div class="container-xxl py-6">
        <div class="container">
            <div class="row mb-2">
                <div class="col-12">
                    <h1 class="display-6 mb-4">Modul Pembelajaran</h1>
                </div>
            </div>

            <div class="row backgroundSlate text-dark mb-2">
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="selectCategoryModule">Kategori Modul</label>
                        <div class="search_select_box">
                            <select id="selectCategoryModule" name="selectCategoryModule" class="w-100"
                                data-live-search="true"></select>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="selectSubCategoryModule">Sub Kategori Modul</label>
                        <div class="search_select_box">
                            <select id="selectSubCategoryModule" class="w-100" data-live-search="true"></select>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="selectTypeModule">Jenis Modul</label>
                        <div class="search_select_box">
                            <select id="selectTypeModule" class="w-100" data-live-search="true"></select>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="btnRefresh">&nbsp;</label>
                        <div class="button_box d-grid">
                            <button class="btn btn-info text-white p-2" onclick="eventRefreshPage()">
                                <i class="fas fa-refresh"></i>
                                Refresh
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div id="coverModule" class="col-12">
                    @include('pages.web.module.modul_pagination', compact('learningModule'))
                </div>
            </div>

        </div>
    </div>

</x-guest-layout>
