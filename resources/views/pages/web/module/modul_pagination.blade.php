<div id="carouselExampleControls" class="carousel d-none d-sm-block" data-bs-ride="false">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="row g-4 justify-content-center">
                @foreach ($learningModule as $key => $item)
                    <div class="col-lg-4 col-md-6 wow fadeInUp mt-5" data-wow-delay="0.1s">
                        <div
                            class="courses-item d-flex flex-column bg-white overflow-hidden h-100 border shadow rounded">
                            <div class="text-center p-2 mb-2">
                                <div class="row mb-2" style="height: 80px;">
                                    <div class="col-12">
                                        <a href="{{ route('guest.modul-show', $item->id) }}"
                                            class="fs-6 text-dark fw-bold"> {{ $item->module_name }}</a>
                                    </div>
                                </div>
                            </div>

                            <div class="p-2 mb-2">
                                <div class="row px-2 mb-4" style="height: 100px">
                                    <div class="col-12">
                                        @php
                                            $module_desc = $item->module_desc ? strip_tags($item->module_desc) : '';

                                        @endphp

                                        {{ $module_desc != '' ? Str::substr($module_desc, 0, 100) . '...' : '' }}
                                    </div>
                                </div>
                            </div>

                            <div class="p-2">
                                <div class="row">
                                    <div class="col-12">
                                        <ol class="breadcrumb text-success justify-content-center mb-0">
                                            <li class="breadcrumb-item small">
                                                {{ $item->categoryModule->name }}
                                            </li>
                                            <li class="breadcrumb-item small">
                                                {{ $item->subCategoryModule->name }}
                                            </li>

                                            <li class="breadcrumb-item small">
                                                {{ $item->typeModule->name }}
                                            </li>
                                        </ol>
                                    </div>

                                    <div class="col-12">
                                        <a href="{{ route('guest.modul-show', $item->id) }}"
                                            class="btn btn-success text-white rounded-pill w-100">
                                            Selengkapnya
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    @if ($learningModule->onFirstPage())
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev" disabled>
            <i class="fas fa-angle-left" aria-hidden="true"></i>
            <span class="visually-hidden">Previous</span>
        </button>
    @else
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev" onclick="fetchModule('{{ $learningModule->currentPage() - 1 }}')">
            <i class="fas fa-angle-left" aria-hidden="true"></i>
            <span class="visually-hidden">Previous</span>
        </button>
    @endif

    @if ($learningModule->hasMorePages())
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next" onclick="fetchModule('{{ $learningModule->currentPage() + 1 }}')">
            <i class="fas fa-angle-right" aria-hidden="true"></i>
            <span class="visually-hidden">Next</span>
        </button>
    @else
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next" disabled>
            <i class="fas fa-angle-right" aria-hidden="true"></i>
            <span class="visually-hidden">Next</span>
        </button>
    @endif

</div>
