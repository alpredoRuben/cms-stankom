@section('title')
    DETAIL MODUL PEMBELAJARAN
@endsection


@section('metadata')
    <meta content="Detail Modul Pembelajaran" name="keywords" />
    <meta content="LMS Stankom - Halaman Detail Modul Pembelajaran" name="description" />
@endsection

{{-- STYLESHEET --}}
@section('stylesheets')
    <style type="text/css">
        .text-black {
            color: black !important;
        }

        .course-details h3 {
            font-size: 24px;
            margin: 30px 0 15px 0;
            font-weight: 700;
            position: relative;
            padding-bottom: 10px;
        }

        .course-details h3:before {
            content: "";
            position: absolute;
            display: block;
            width: 100%;
            height: 1px;
            background: #eef0ef;
            bottom: 0;
            left: 0;
        }

        .course-details h3:after {
            content: "";
            position: absolute;
            display: block;
            width: 60px;
            height: 1px;
            background: #5fcf80;
            bottom: 0;
            left: 0;
        }

        .course-details .course-info {
            background: #fafbfa;
            padding: 10px 15px;
            margin-bottom: 15px;
        }

        .course-details .course-info h5 {
            font-weight: 400;
            font-size: 16px;
            margin: 0;
            font-family: "Poppins", sans-serif;
        }

        .course-details .course-info p {
            margin: 0;
            font-weight: 600;
            color: #077d77
        }

        .course-details .course-info a {
            color: #657a6d;
        }
    </style>
@endsection

{{-- JAVASCRIPT --}}
@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function showAlertInfo() {
            Swal.fire({
                title: 'INFORMASI AKSES DITOLAK',
                icon: 'warning',
                text: 'Mohon maaf, Anda harus mendaftar terlebih dahulu. Silahkan daftarkan akun anda',
                confirmButtonText: 'OK',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "{!! route('register') !!}";
                }
            });
        }
    </script>
@endsection

<x-guest-layout>
    <div class="container-xxl py-6">

        <section id="course-details" class="course-details">
            <div class="container" data-aos="fade-up">

                <div class="row">
                    <div class="col-lg-8">

                        <div class="d-flex flex-column bd-highlight mb-3">
                            <div class="p-2 bd-highlight">
                                <h3 class="fs-3">{{ $learningModule->module_name }}</h3>
                            </div>
                            <div class="p-2 bd-highlight">
                                <p class="text-black ">
                                    {!! $learningModule->module_desc !!}
                                </p>
                            </div>
                        </div>



                    </div>
                    <div class="col-lg-4 mt-5">

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Kategori</h5>
                            <p>{{ $learningModule->categoryModule->name }}</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Sub Kategori</h5>
                            <p>{{ $learningModule->subCategoryModule->name }}</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Tipe Modul</h5>
                            <p>{{ $learningModule->typeModule->name }}</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Download Materi</h5>
                            <p>{{ $learningModule->can_download ? 'Tersedia' : 'Tidak Tersedia' }}</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Kuis</h5>
                            <p>{{ $learningModule->has_quiz ? 'Tersedia' : 'Tidak Tersedia' }}</p>
                        </div>

                        <div class="course-info d-flex align-items-center">

                            @if ($user)
                                @if ($user->hasRole('peserta'))
                                    <div class="d-grid w-100">
                                        <a href="{{ route('major.modules.show', $learningModule->id) }}"
                                            class="btn btn-primary text-white rounded-pill">
                                            Masuk
                                        </a>
                                    </div>
                                @else
                                    <div class="d-grid w-100">
                                        <a href="{{ route('learning_modules.show', $learningModule->id) }}"
                                            class="btn btn-primary text-white rounded-pill">
                                            Masuk
                                        </a>
                                    </div>
                                @endif
                            @else
                                <div class="d-grid w-100">
                                    <button type="button" class="btn btn-primary text-white rounded-pill"
                                        onclick="showAlertInfo()">
                                        Daftar
                                    </button>
                                </div>

                            @endif



                        </div>

                    </div>
                </div>

            </div>
        </section>

    </div>

</x-guest-layout>
