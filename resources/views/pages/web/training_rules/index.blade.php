@section('title')
    ATURAN PELATIHAN
@endsection


@section('metadata')
    <meta content="Aturan Pelatihan" name="keywords" />
    <meta content="LMS Stankom - Halaman Aturan Pelatihan" name="description" />
@endsection

@section('javascripts')
    <script>
        function fetchTrainingRules(page) {
            let url = "{!! route('guest.training-rules') !!}?page=" + page

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverTrainingRules').html(data);
                }
            });
        }
    </script>
@endsection

<x-guest-layout>
    <div class="container-xxl py-6">
        <div class="container">
            <div class="row g-5">

                <div class="col-lg-12 wow fadeInUp" data-wow-delay="0.1s">
                    <h3 class="mb-4">Aturan Pelatihan</h3>
                    <div id="coverTrainingRules" class="row gy-5 gx-4">
                        @include('pages.web.training_rules.training_list', compact('trainingRules'))
                    </div>
                </div>


            </div>
        </div>
    </div>
</x-guest-layout>
