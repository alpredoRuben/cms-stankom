@if ($trainingRules && count($trainingRules) > 0)
    @foreach ($trainingRules as $item)
        <div class="col-sm-12 wow fadeIn" data-wow-delay="0.1s">
            <div class="d-flex flex-row">
                <div class="p-1 mt-2">
                    <div class="btn-square bg-primary me-3">
                        <i class="fa fa-check  text-white"></i>
                    </div>
                </div>
                <div class="p-1 mt-0">
                    <h5 class="mb-0">
                        {{ $item->title }}
                    </h5>
                    <div>
                        {!! $item->description !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="col-sm-12">
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">

                @if ($trainingRules->onFirstPage())
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                @else
                    <li class="page-item">
                        <button type="button" class="page-link" tabindex="-1"
                            onclick="fetchTrainingRules('{{ $trainingRules->currentPage() - 1 }}')">Previous</button>
                    </li>
                @endif


                @if ($trainingRules->hasMorePages())
                    <li class="page-item">
                        <button type="button" class="page-link"
                            onclick="fetchTrainingRules('{{ $trainingRules->currentPage() + 1 }}')">Next</button>
                    </li>
                @else
                    <li class="page-item disabled">
                        <button type="button" class="page-link" aria-disabled="true">Next</button>
                    </li>
                @endif
            </ul>
        </nav>
    </div>

@endif()
