@section('title')
    WEBINAR
@endsection


@section('metadata')
    <meta content="KELAS (WEBINAR)" name="keywords" />
    <meta content="LMS Stankom - Halaman Kelas (Webinar)" name="description" />
@endsection

{{-- STYLESHEET --}}
@section('stylesheets')
    <link href="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.css" rel="stylesheet" />
    <style>
        .backgroundSlate {
            background-color: #eef1f2
        }

        .bg-flate {
            background-color: #f8f8ff
        }

        .bg-card-header {
            background-color: #034380;
            color: #eee;
        }

        .search_select_box {
            max-width: 400px;
            margin: 10px auto;
        }

        .search_select_box select {
            width: 100%;
        }

        .button_box {
            max-width: 400px;
            margin: 10px auto;
        }


        .search_select_box button {
            background-color: #3c59ab !important;
            padding: 10px 20px;
            color: #fff !important
        }

        .dropdown-item.active,
        .dropdown-item:active {
            color: #fff;
            text-decoration: none;
            background-color: #3c59ab !important;
        }

        .carousel-control-prev {
            left: -50px;
            margin-top: 100px;
        }

        .carousel-control-next {
            right: -50px;
            margin-top: 100px;
        }

        .carousel-control-prev,
        .carousel-control-next {
            background-color: #0a4c97;
            width: 6vh;
            height: 6vh;
            border-radius: 50%;
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
@endsection

{{-- JAVASCRIPT --}}
@section('javascripts')
    <script src="https://cdn.jsdelivr.net/npm/mc-datepicker/dist/mc-calendar.min.js"></script>
    <script>
        var stateFilter = {
            tipe: $("#selectTipeKelas"),
            period: "inputOpenDate",
            sertificated: $("#selectSertificated")
        }

        var filterSelection = {
            tipe: '-',
            period: '',
            sertificated: '-'
        }

        function initSelected(element, url) {
            element.selectpicker();
            axios.get(url).then(res => {
                const {
                    data
                } = res;
                element.empty();
                element.append(`<option value="-" selected="true">Semuanya</option>`);
                $.each(data.data, function(index, value) {
                    element.append(`<option value="${value.id}" selected="true">${value.name}</option>`)
                });
                element.val('-')
                element.selectpicker('refresh')
            });
        }

        function getTypeClass() {
            const url = "{!! route('guest.type_class') !!}";
            initSelected(stateFilter.tipe, url);

        }

        function getSertificated() {
            const url = "{!! route('guest.sertificated') !!}";
            initSelected(stateFilter.sertificated, url);

        }



        function fetchWebinar(page = 1) {
            let url = "{!! route('web.kelas_webinar') !!}?page=" + page;
            let str = '';
            if (filterSelection.tipe != '-') {
                str += `&type_class=${filterSelection.tipe}`
            }

            if (filterSelection.sertificated != '-') {
                str += `&sertificated=${filterSelection.sertificated}`
            }

            if (filterSelection.period != '') {
                str += `&period=${filterSelection.period}`
            }

            if (str != '') {
                url += str;
            }

            console.log(url)
            console.log(filterSelection)

            $.ajax({
                url: url,
                success: function(data) {
                    $('#coverModule').html(data);
                }
            });
        }

        function eventRefreshPage() {
            window.location.href = "{!! route('web.kelas_webinar') !!}"
        }


        $(document).ready(function() {
            getTypeClass();
            getSertificated();

            stateFilter.tipe.change(function(e) {
                e.preventDefault();
                filterSelection.tipe = $(this).val();
                fetchWebinar();
            });

            stateFilter.sertificated.change(function(e) {
                e.preventDefault();
                filterSelection.sertificated = $(this).val();
                fetchWebinar();
            });

            const periodDate = MCDatepicker.create({
                el: `#${stateFilter.period}`,
                dateFormat: 'YYYY-MM',
                bodyType: 'inline',
                theme: {
                    theme_color: '#07615e',
                    display: {
                        foreground: 'rgba(223, 226, 230, 0.5)',
                        background: '#fff'
                    },
                    picker_header: {
                        active: '#084e87',
                        inactive: '#ad7007'
                    }
                }
            });

            periodDate.onSelect((date, formatedDate) => {
                filterSelection.period = formatedDate;
                fetchWebinar();
            });

        });
    </script>
@endsection

<x-guest-layout>
    <div class="container-fluid py-6">
        <div class="container">
            <div class="row mb-2">
                <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
                    <h5 class="text-dark text-uppercase mb-2">Program Kelas / Webinar</h5>
                </div>
            </div>


            <div class="row backgroundSlate text-dark mb-2">
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="selectTipeKelas">Tipe Kelas</label>
                        <div class="search_select_box">
                            <select id="selectTipeKelas" class="w-100" data-live-search="true"></select>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="selectSertificated">Sertifikat</label>
                        <div class="search_select_box">
                            <select id="selectSertificated" class="w-100" data-live-search="true"></select>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="inputOpenDate" class="form-label">Periode Kelas</label>
                        <div style="margin-top: 5px;">
                            <input type="text" id="inputOpenDate" class="form-control p-2" />
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 my-2">
                        <label for="btnRefresh">&nbsp;</label>
                        <div class="button_box d-grid">
                            <button class="btn btn-info text-white p-2" onclick="eventRefreshPage()">
                                <i class="fas fa-refresh"></i>
                                Refresh
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div id="coverModule" class="col-12">
                    @include('pages.web.webinar.webinar', compact('webinar'))
                </div>
            </div>
        </div>
    </div>

</x-guest-layout>
