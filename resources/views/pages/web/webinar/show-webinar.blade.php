@section('title')
    DETAIL WEBINAR
@endsection


@section('metadata')
    <meta content="Detail Webinar" name="keywords" />
    <meta content="LMS Stankom - Halaman Detail Webinar" name="description" />
@endsection

{{-- STYLESHEET --}}
@section('stylesheets')
    <style type="text/css">
        .text-black {
            color: black !important;
        }

        .course-details h3 {
            font-size: 24px;
            margin: 30px 0 15px 0;
            font-weight: 700;
            position: relative;
            padding-bottom: 10px;
        }

        .course-details h3:before {
            content: "";
            position: absolute;
            display: block;
            width: 100%;
            height: 1px;
            background: #eef0ef;
            bottom: 0;
            left: 0;
        }

        .course-details h3:after {
            content: "";
            position: absolute;
            display: block;
            width: 60px;
            height: 1px;
            background: #5fcf80;
            bottom: 0;
            left: 0;
        }

        .course-details .course-info {
            background: #fafbfa;
            padding: 10px 15px;
            margin-bottom: 15px;
        }

        .course-details .course-info h5 {
            font-weight: 400;
            font-size: 16px;
            margin: 0;
            font-family: "Poppins", sans-serif;
        }

        .course-details .course-info p {
            margin: 0;
            font-weight: 600;
            color: #077d77
        }

        .course-details .course-info a {
            color: #657a6d;
        }
    </style>
@endsection

{{-- JAVASCRIPT --}}
@section('javascripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function showAlertInfo() {
            Swal.fire({
                title: 'INFORMASI AKSES DITOLAK',
                icon: 'warning',
                text: 'Mohon maaf, Anda harus mendaftar terlebih dahulu. Silahkan daftarkan akun anda',
                confirmButtonText: 'OK',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "{!! route('register') !!}";
                }
            });
        }
    </script>
@endsection

<x-guest-layout>
    <div class="container-xxl py-6">

        <section id="course-details" class="course-details">
            <div class="container" data-aos="fade-up">

                <div class="row">
                    <div class="col-lg-8">

                        <div class="d-flex flex-column bd-highlight mb-3">
                            <div class="p-2 bd-highlight">
                                <h3 class="fs-3">{{ $webinar->class_name }}</h3>
                            </div>
                            <div class="p-2 bd-highlight">
                                <p class="text-black ">
                                    {!! $webinar->description !!}
                                </p>
                            </div>
                        </div>



                    </div>
                    <div class="col-lg-4 mt-5">

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Tipe Kelas</h5>
                            <p>{{ $webinar->typeClass->name }}</p>
                        </div>

                        @if ($user)
                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Pengajar</h5>
                                <p>{{ $webinar->trainer->name }}</p>
                            </div>

                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Tanggal Buka Kelas</h5>
                                <p>{{ \Carbon\Carbon::parse($webinar->open_date)->format('d F Y') }}</p>
                            </div>

                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Tanggal Buka Tutup</h5>
                                <p>{{ \Carbon\Carbon::parse($webinar->close_date)->format('d F Y') }}</p>
                            </div>

                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Jam Kelas</h5>
                                <p>{{ $webinar->class_time }}</p>
                            </div>
                        @endif

                        @if (!$user)
                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Periode Mulai</h5>
                                <p>{{ \Carbon\Carbon::parse($webinar->open_date)->format('F Y') }}</p>
                            </div>
                        @endif


                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Durasi</h5>
                            <p>{{ $webinar->duration }} Menit</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Kuota</h5>
                            <p>{{ $webinar->quota }} Orang</p>
                        </div>

                        @if ($user)
                            <div class="course-info d-flex justify-content-between align-items-center">
                                <h5>Jumlah Pendaftar</h5>
                                <p>{{ $webinar->userWebinars && $webinar->userWebinars()->count() > 0 ? $webinar->userWebinars()->count() . ' Orang' : 'Kosong' }}
                                </p>
                            </div>
                        @endif

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Sertifikat</h5>
                            <p>{{ $webinar->has_sertificated ? 'Bersertifikat' : 'Tidak Bersertifikat' }}</p>
                        </div>

                        <div class="course-info d-flex justify-content-between align-items-center">
                            <h5>Kuis</h5>
                            <p>{{ $webinar->has_quiz ? 'Tersedia' : 'Tidak Tersedia' }}</p>
                        </div>

                        <div class="course-info d-flex align-items-center">

                            @if ($user)
                                @if ($user->hasRole('peserta'))
                                    <div class="d-grid w-100">
                                        <a href="{{ route('major.webinar.show', $webinar->id) }}"
                                            class="btn btn-primary text-white rounded-pill">
                                            Masuk
                                        </a>
                                    </div>
                                @else
                                    <div class="d-grid w-100">
                                        <a href="{{ route('class_webinar.show', $webinar->id) }}"
                                            class="btn btn-primary text-white rounded-pill">
                                            Masuk
                                        </a>
                                    </div>
                                @endif
                            @else
                                @if ($webinar->userWebinars()->count() >= $webinar->quota)
                                    <div class="d-grid w-100">
                                        <button type="button" class="btn btn-info text-white rounded-pill" disabled>
                                            Kuota Penuh
                                        </button>
                                    </div>
                                @else
                                    <div class="d-grid w-100">
                                        <button type="button" class="btn btn-primary text-white rounded-pill"
                                            onclick="showAlertInfo()">
                                            Daftar
                                        </button>
                                    </div>
                                @endif


                            @endif


                        </div>

                    </div>
                </div>

            </div>
        </section>

    </div>

</x-guest-layout>
