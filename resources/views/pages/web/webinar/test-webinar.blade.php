<div id="carouselExampleControls" class="carousel d-none d-sm-block" data-bs-ride="false">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="row g-4 justify-content-center">
                @foreach ($webinar as $item)
                    <div class="col-lg-4 col-md-6 wow fadeInUp mt-5" data-wow-delay="0.1s">
                        <div
                            class="courses-item d-flex flex-column bg-white overflow-hidden h-100 border shadow rounded">
                            <div class="d-inline-block bg-primary text-white fs-5 py-1 px-4 mb-4">
                                {{ $item->typeClass->name }}
                            </div>

                            <div class="text-center pt-0 mb-4">
                                <div class="row mb-2" style="height: 80px;">
                                    <div class="col-12">
                                        <a href="{{ route('guest.webinar-show', $item->id) }}"
                                            class="fs-6 text-dark fw-bold">{{ $item->class_name }}</a>
                                    </div>
                                </div>

                                <div class="row px-2 mb-4" style="height: 100px">
                                    <div class="col-12">
                                        @php
                                            $desc = $item->description ? strip_tags($item->description) : '';

                                        @endphp

                                        {{ $desc != '' ? Str::substr($desc, 0, 100) . '...' : '' }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <ol class="breadcrumb text-success justify-content-center mb-0">
                                            <li class="breadcrumb-item small">
                                                <i class="fas fa-balance-scale"></i>
                                                {{ $item->quota }} orang
                                            </li>
                                            <li class="breadcrumb-item small">
                                                <i class="fas fa-clock"></i>
                                                {{ $item->duration }} Menit
                                            </li>

                                            <li class="breadcrumb-item small">
                                                <i class="fas fa-certificate"></i>
                                                {{ $item->has_sertificated ? 'Bersertifikat' : 'Tidak Bersertifikat' }}
                                            </li>

                                            @if (auth()->user())
                                                <li class="breadcrumb-item small">
                                                    <i class="fas fa-users"></i>
                                                    {{ $item->userWebinars && $item->userWebinars()->count() > 0 ? $item->userWebinars()->count() . ' Orang' : 'Kosong' }}
                                                </li>
                                            @endif

                                        </ol>
                                    </div>
                                </div>

                            </div>

                            <div class="position-relative mt-auto">
                                <div class="position-relative mt-auto">
                                    @if ($item->filepath)
                                        <img class="img-fluid" src="{{ $item->filepath }}" alt=""
                                            style="width:100% !important; height: 300px !important;">
                                    @endif

                                    <div class="courses-overlay">
                                        <a href="{{ route('guest.webinar-show', $item->id) }}"
                                            class="btn btn-outline-primary border-2" href="">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    @if ($webinar->onFirstPage())
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev" disabled>
            <i class="fas fa-angle-left" aria-hidden="true"></i>
            <span class="visually-hidden">Previous</span>
        </button>
    @else
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev" onclick="fetchWebinar('{{ $webinar->currentPage() - 1 }}')">
            <i class="fas fa-angle-left" aria-hidden="true"></i>
            <span class="visually-hidden">Previous</span>
        </button>
    @endif

    @if ($webinar->hasMorePages())
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next" onclick="fetchWebinar('{{ $webinar->currentPage() + 1 }}')">
            <i class="fas fa-angle-right" aria-hidden="true"></i>
            <span class="visually-hidden">Next</span>
        </button>
    @else
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next" disabled>
            <i class="fas fa-angle-right" aria-hidden="true"></i>
            <span class="visually-hidden">Next</span>
        </button>
    @endif

</div>
