<?php

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\LMS\AboutUsController;
use App\Http\Controllers\LMS\AccessorController;
use App\Http\Controllers\LMS\ArticleController;
use App\Http\Controllers\LMS\BannerController;
use App\Http\Controllers\LMS\CategoryModuleController;
use App\Http\Controllers\LMS\CertificationTypeController;
use App\Http\Controllers\LMS\CityController;
use App\Http\Controllers\LMS\ClassWebinarController;
use App\Http\Controllers\LMS\ComplementDatatableController;
use App\Http\Controllers\LMS\ContactController;
use App\Http\Controllers\LMS\DatatablesController;
use App\Http\Controllers\LMS\DashboardController;
use App\Http\Controllers\LMS\DistrictController;
use App\Http\Controllers\LMS\EducationController;
use App\Http\Controllers\LMS\ExcelFileManagerController;
use App\Http\Controllers\LMS\FilesManagerController;
use App\Http\Controllers\LMS\FileTypeController;
use App\Http\Controllers\LMS\JobProfessionController;
use App\Http\Controllers\LMS\LearningModuleController;
use App\Http\Controllers\LMS\LogoController;
use App\Http\Controllers\LMS\LSPController;
use App\Http\Controllers\LMS\MemberController;
use App\Http\Controllers\LMS\NewsController;
use App\Http\Controllers\LMS\ProfileController;
use App\Http\Controllers\LMS\ProvinceController;
use App\Http\Controllers\LMS\QuestionController;
use App\Http\Controllers\LMS\QuizController;
use App\Http\Controllers\LMS\QuizTypeController;
use App\Http\Controllers\LMS\SocialMediaController;
use App\Http\Controllers\LMS\SocialMediaItemController;
use App\Http\Controllers\LMS\SubCategoryModuleController;
use App\Http\Controllers\LMS\TrainerController;
use App\Http\Controllers\LMS\TrainingRuleController;
use App\Http\Controllers\LMS\TypeClassController;
use App\Http\Controllers\LMS\TypeModuleController;
use App\Http\Controllers\LMS\UserController;
use App\Http\Controllers\LMS\WardController;
use App\Http\Controllers\LMS\WebDatatableController;
use App\Http\Controllers\LMS\WhatsappController;
use App\Http\Controllers\Major\ModuleController;
use App\Http\Controllers\Major\WebinarController;
use App\Http\Controllers\Web\AboutUsController as WebAboutUsController;
use App\Http\Controllers\Web\ArtikelController;
use App\Http\Controllers\Web\AturanPelatihanController;
use App\Http\Controllers\Web\BeritaController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\KelasWebinarController;
use App\Http\Controllers\Web\KontakKamiController;
use App\Http\Controllers\Web\ModulPembelajaranController;
use App\Http\Controllers\Web\WebLSPController;
use App\Http\Controllers\Web\WebModuleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('web.home');
Route::get('/tentang-kami', [WebAboutUsController::class, 'aboutUs'])->name('web.tentang_kami');
Route::get('/kelas-webinar', [KelasWebinarController::class, 'index'])->name('web.kelas_webinar');
Route::get('/aturan-pelatihan', [AturanPelatihanController::class, 'index'])->name('web.aturan_pelatihan');
Route::get('/berita', [BeritaController::class, 'news'])->name('web.berita');
Route::get('/artikel', [ArtikelController::class, 'articles'])->name('web.artikel');
Route::get('/data-lsp', [WebLSPController::class, 'index'])->name('web.data_lsp');
Route::get('/modul', [WebModuleController::class, 'index'])->name('web.modul');

Route::prefix('guest')->group(function () {
    Route::get('certification-types', [WebLSPController::class, 'getCertificationTypes'])->name('guest.certification_types');

    Route::get('category-modules', [WebModuleController::class, 'getCategoryModule'])->name('guest.category_modules');
    Route::get('sub-category-modules', [WebModuleController::class, 'getSubCategory'])->name('guest.sub_category_modules');
    Route::get('type-modules', [WebModuleController::class, 'getTypeModule'])->name('guest.type_modules');
    Route::get('modul/show/{id}', [WebModuleController::class, 'show'])->name('guest.modul-show');
    Route::get('datatable-lsp', [WebLSPController::class, 'getLSP'])->name('guest.datatable_lsp');

    // Webinar
    Route::get('type-class', [KelasWebinarController::class, 'getTypeClass'])->name('guest.type_class');
    Route::get('sertificated_status', [KelasWebinarController::class, 'getSertificatedStatus'])->name('guest.sertificated');
    Route::get('class-webinar/show/{id}', [KelasWebinarController::class, 'show'])->name('guest.webinar-show');


    Route::get('training-rules', [AturanPelatihanController::class, 'fetchRules'])->name('guest.training-rules');


    Route::get('latest-news', [BeritaController::class, 'latestNews'])->name('guest.news.latest');
    Route::get('oldest-news', [BeritaController::class, 'oldestNews'])->name('guest.news.oldest');
    Route::get('show-news/{id}', [BeritaController::class, 'showNews'])->name('guest.news.show');

    Route::get('latest-articles', [ArtikelController::class, 'latestArticles'])->name('guest.articles.latest');
    Route::get('oldest-articles', [ArtikelController::class, 'oldestArticles'])->name('guest.articles.oldest');
    Route::get('show-articles/{id}', [ArtikelController::class, 'showArticles'])->name('guest.articles.show');
});


Route::prefix('email')->group(function () {
    Route::get('account/activation/{token}', [RegisteredUserController::class, 'activationAccount'])->name('account.activation');

    Route::get('verification/{token}', [RegisteredUserController::class, 'verificationEmail'])->name('account.verification');
});


Route::middleware(['auth', 'is_verify_email'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::prefix('filetypes')->group(function () {
        Route::get('all', [FileTypeController::class, 'getAll'])->name('filetypes.all');
    });

    Route::prefix('profile')->group(function () {
        Route::get('preview', [ProfileController::class, 'preview'])->name('profile.preview');
        Route::get('edit', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::post('avatar/upload', [ProfileController::class, 'uploadAvatar'])->name('profile.avatar.upload');
        Route::post('update', [ProfileController::class, 'update'])->name('profile.update');
        Route::get('edit-password-view',  [ProfileController::class, 'editPassword'])->name('profile.edit_password_view');
        Route::post("change-password", [ProfileController::class, 'changePassword'])->name('profile.change_password');
    });


    Route::prefix('file-manager')->group(function () {

        Route::prefix('users')->group(function () {
            Route::get('/download/csv', [FilesManagerController::class, 'downloadUsersCSV'])->name('file_manager.users.download.csv');
            Route::post('/import/csv', [FilesManagerController::class, 'importUserCSV'])->name('file_manager.users.import.csv');
            Route::get('/export/csv', [FilesManagerController::class, 'exportUserCSV'])->name('file_manager.users.export.csv');
        });

        Route::prefix('accessor')->group(function () {
            Route::get('/download/csv', [FilesManagerController::class, 'downloadAccessorCSV'])->name('file_manager.accessor.download.csv');
            Route::post('/import/csv', [FilesManagerController::class, 'importAccessorCSV'])->name('file_manager.accessor.import.csv');
            Route::get('/export/csv', [FilesManagerController::class, 'exportAccessorCSV'])->name('file_manager.accessor.export.csv');
        });

        Route::prefix('lsp')->group(function () {
            Route::get('/export/csv', [FilesManagerController::class, 'exportLSPCSV'])->name('file_manager.lsp.export.csv');
        });


        Route::prefix('learning-modules')->group(function () {
            Route::get('/download/excel', [FilesManagerController::class, 'downloadLearningModules'])
                ->name('file_manager.learning-modules.download.excel');
        });

        Route::prefix('class-webinar')->group(function () {
            Route::get('/download/excel', [FilesManagerController::class, 'exportClassWebinar'])
                ->name('file_manager.class-webinar.export.csv');
        });
    });


    /** MASTER */
    Route::prefix('master')->group(function () {

        /** USERS */
        Route::prefix('users')->group(function () {

            // Pengguna
            Route::get('main/list', [UserController::class, 'index'])->name('master.users.main.list');
            Route::get('main/create', [UserController::class, 'create'])->name('master.users.main.create');
            Route::get('main/edit/{id}', [UserController::class, 'edit'])->name('master.users.main.edit');
            Route::post('main/store', [UserController::class, 'store'])->name('master.users.main.store');
            Route::put('main/update/{id}', [UserController::class, 'update'])->name('master.users.main.update');
            Route::post('main/change_status', [UserController::class, 'changeStatus'])->name('master.users.main.change_status');

            Route::post('main/upload-photo', [UserController::class, 'uploadPhoto'])->name('master.users.main.upload_photo');


            Route::get('main/show/export/{type}', [UserController::class, 'showExport'])->name('master.users.main.show.export');


            // Pengajar
            Route::get('trainers/list', [TrainerController::class, 'index'])->name('master.users.trainers.list');
            Route::get('trainers/all', [TrainerController::class, 'getTrainers'])->name('master.users.trainers.all');

            // Peserta
            Route::get('members/data', [MemberController::class, 'index'])->name('master.users.members.list');
        });

        /** MODULE */
        Route::prefix('module')->group(function () {

            // Kategori Modul
            Route::get('category/list', [CategoryModuleController::class, 'index'])
                ->name('master.module.category.list');

            Route::get('category/all', [CategoryModuleController::class, 'getCategoryModules'])
                ->name('master.module.category.all');

            Route::get('category/find/{id}', [CategoryModuleController::class, 'find'])
                ->name('master.module.category.find');

            Route::post('category/store', [CategoryModuleController::class, 'store'])
                ->name('master.module.category.store');

            Route::put('category/update/{id}', [CategoryModuleController::class, 'update'])
                ->name('master.module.category.update');

            Route::delete('category/delete/{id}', [CategoryModuleController::class, 'delete'])
                ->name('master.module.category.delete');

            Route::get('category/restore/{id}', [CategoryModuleController::class, 'restore'])
                ->name('master.module.category.restore');


            // Sub Kategori Modul
            Route::get('sub_category/list', [SubCategoryModuleController::class, 'index'])
                ->name('master.module.sub_category.list');

            Route::get('sub_category/all', [SubCategoryModuleController::class, 'getSubCategoryModules'])
                ->name('master.module.sub_category.all');

            Route::get('sub_category/find/{id}', [SubCategoryModuleController::class, 'find'])
                ->name('master.module.sub_category.find');

            Route::post('sub_category/store', [SubCategoryModuleController::class, 'store'])
                ->name('master.module.sub_category.store');

            Route::put('sub_category/update/{id}', [SubCategoryModuleController::class, 'update'])
                ->name('master.module.sub_category.update');

            Route::delete('sub_category/delete/{id}', [SubCategoryModuleController::class, 'delete'])
                ->name('master.module.sub_category.delete');

            Route::get('sub_category/restore/{id}', [SubCategoryModuleController::class, 'restore'])
                ->name('master.module.sub_category.restore');

            // Tipe Modul
            Route::get('type/list', [TypeModuleController::class, 'index'])
                ->name('master.module.type.list');

            Route::get('type/all', [TypeModuleController::class, 'getTypeModules'])
                ->name('master.module.type.all');

            Route::get('type/find/{id}', [TypeModuleController::class, 'find'])
                ->name('master.module.type.find');

            Route::post('type/store', [TypeModuleController::class, 'store'])
                ->name('master.module.type.store');

            Route::put('type/update/{id}', [TypeModuleController::class, 'update'])
                ->name('master.module.type.update');

            Route::delete('type/delete/{id}', [TypeModuleController::class, 'delete'])
                ->name('master.module.type.delete');

            Route::get('type/restore/{id}', [TypeModuleController::class, 'restore'])
                ->name('master.module.type.restore');
        });

        /** ACCESSOR */
        Route::prefix('accessor')->group(function () {
            Route::get('list', [AccessorController::class, 'index'])->name('master.accessor.list');
            Route::get('all', [AccessorController::class, 'getAccessor'])->name('master.accessor.all');
            Route::get('create', [AccessorController::class, 'create'])->name('master.accessor.create');
            Route::get('edit/{id}', [AccessorController::class, 'edit'])->name('master.accessor.edit');
            Route::get('find/{id}', [AccessorController::class, 'find'])->name('master.accessor.find');
            Route::post('store', [AccessorController::class, 'store'])->name('master.accessor.store');
            Route::put('update/{id}', [AccessorController::class, 'update'])->name('master.accessor.update');
            Route::delete('delete/{id}', [AccessorController::class, 'delete'])->name('master.accessor.delete');
            Route::get('restore/{id}', [AccessorController::class, 'restore'])->name('master.accessor.restore');
            Route::get('show/export', [AccessorController::class, 'showExport'])->name('master.accessor.show.export');
        });

        /** CERTIFICATION */
        Route::prefix('certification_types')->group(function () {
            Route::get('list', [CertificationTypeController::class, 'index'])->name('master.certification_types.list');
            Route::get('all', [CertificationTypeController::class, 'getCertificationTypes'])->name('master.certification_types.all');
            Route::get('find/{id}', [CertificationTypeController::class, 'find'])->name('master.certification_types.find');
            Route::post('store', [CertificationTypeController::class, 'store'])->name('master.certification_types.store');
            Route::put('update/{id}', [CertificationTypeController::class, 'update'])->name('master.certification_types.update');
            Route::delete('delete/{id}', [CertificationTypeController::class, 'delete'])->name('master.certification_types.delete');
            Route::get('restore/{id}', [CertificationTypeController::class, 'restore'])->name('master.certification_types.restore');
        });

        /** EDUCATION */
        Route::prefix('education')->group(function () {
            Route::get('list', [EducationController::class, 'index'])->name('master.education.list');
            Route::get('all', [EducationController::class, 'getEducations'])->name('master.education.all');
            Route::get('find/{id}', [EducationController::class, 'find'])->name('master.education.find');
            Route::post('store', [EducationController::class, 'store'])->name('master.education.store');
            Route::put('update/{id}', [EducationController::class, 'update'])->name('master.education.update');
            Route::delete('delete/{id}', [EducationController::class, 'delete'])->name('master.education.delete');
            Route::get('restore/{id}', [EducationController::class, 'restore'])->name('master.education.restore');
        });

        /** PROFESSION */
        Route::prefix('profession')->group(function () {
            Route::get('list', [JobProfessionController::class, 'index'])->name('master.profession.list');
            Route::get('all', [JobProfessionController::class, 'getProfession'])->name('master.profession.all');
            Route::get('find/{id}', [JobProfessionController::class, 'find'])->name('master.profession.find');
            Route::post('store', [JobProfessionController::class, 'store'])->name('master.profession.store');
            Route::put('update/{id}', [JobProfessionController::class, 'update'])->name('master.profession.update');
            Route::delete('delete/{id}', [JobProfessionController::class, 'delete'])->name('master.profession.delete');
            Route::get('restore/{id}', [JobProfessionController::class, 'restore'])->name('master.profession.restore');
        });

        /** CLASSES */
        Route::prefix('classes')->group(function () {
            // Tipe Kelas
            Route::get('type/list', [TypeClassController::class, 'index'])
                ->name('master.classes.type.list');

            Route::get('type/all', [TypeClassController::class, 'getClassTypes'])
                ->name('master.classes.type.all');

            Route::get('type/find/{id}', [TypeClassController::class, 'find'])
                ->name('master.classes.type.find');

            Route::post('type/store', [TypeClassController::class, 'store'])
                ->name('master.classes.type.store');

            Route::put('type/update/{id}', [TypeClassController::class, 'update'])
                ->name('master.classes.type.update');

            Route::delete('type/delete/{id}', [TypeClassController::class, 'delete'])
                ->name('master.classes.type.delete');

            Route::post('type/change_status', [TypeClassController::class, 'changeStatus'])
                ->name('master.classes.type.change_status');

            Route::get('type/restore/{id}', [TypeClassController::class, 'restore'])
                ->name('master.classes.type.restore');
        });

        /** LSP */
        Route::prefix("lsp")->group(function () {
            Route::get('list', [LSPController::class, 'index'])->name('master.lsp.list');
            Route::get('all', [LSPController::class, 'getLSP'])->name('master.lsp.all');
            Route::get('show/{id}', [LSPController::class, 'show'])->name('master.lsp.show');
            Route::get('create', [LSPController::class, 'create'])->name('master.lsp.create');
            Route::get('edit/{id}', [LSPController::class, 'edit'])->name('master.lsp.edit');
            Route::post('store', [LSPController::class, 'store'])->name('master.lsp.store');
            Route::put('update/{id}', [LSPController::class, 'update'])->name('master.lsp.update');
            Route::post('change-status', [LSPController::class, 'changeStatus'])->name('master.lsp.change_status');
            Route::get('export/show', [LSPController::class, 'showExport'])->name('master.lsp.show.export');
        });


        /** NEWS */
        Route::prefix("news")->group(function () {
            Route::get('list', [NewsController::class, 'index'])->name('master.news.list');
            Route::get('tags', [NewsController::class, 'getTags'])->name('master.news.tags');
            Route::get('show/{id}', [NewsController::class, 'show'])->name('master.news.show');
            Route::get('create', [NewsController::class, 'create'])->name('master.news.create');
            Route::get('edit/{id}', [NewsController::class, 'edit'])->name('master.news.edit');
            Route::post('store', [NewsController::class, 'store'])->name('master.news.store');
            Route::post('update/{id}', [NewsController::class, 'update'])->name('master.news.update');
            Route::post('change-status', [NewsController::class, 'changeStatus'])->name('master.news.change_status');
            Route::get('headline/{id}', [NewsController::class, 'setHeadline'])->name('master.news.headline');
            Route::get('disabled-headline/{id}', [NewsController::class, 'disabledHeadline'])
                ->name('master.news.disabled_headline');
        });


        /** ARTICLES */
        Route::prefix("articles")->group(function () {
            Route::get('list', [ArticleController::class, 'index'])->name('master.articles.list');
            Route::get('tags', [ArticleController::class, 'getArticleTags'])->name('master.articles.tags');
            Route::get('show/{id}', [ArticleController::class, 'show'])->name('master.articles.show');
            Route::get('create', [ArticleController::class, 'create'])->name('master.articles.create');
            Route::get('edit/{id}', [ArticleController::class, 'edit'])->name('master.articles.edit');
            Route::post('store', [ArticleController::class, 'store'])->name('master.articles.store');
            Route::post('update/{id}', [ArticleController::class, 'update'])->name('master.articles.update');
            Route::post('change-status', [ArticleController::class, 'changeStatus'])->name('master.articles.change_status');
            Route::get('headline/{id}', [ArticleController::class, 'setHeadline'])->name('master.articles.headline');
            Route::get('disabled-headline/{id}', [ArticleController::class, 'disabledHeadline'])
                ->name('master.articles.disabled_headline');
        });


        /** LOCATION */
        Route::prefix('location')->group(function () {
            // Propinsi
            Route::get('/province/list', [ProvinceController::class, 'index'])->name('master.location.province.list');
            Route::get('/province/all', [ProvinceController::class, 'getProvinces'])->name('master.location.province.all');
            Route::get('/province/find/{id}', [ProvinceController::class, 'find'])->name('master.location.province.find');
            Route::post('/province/store', [ProvinceController::class, 'store'])->name('master.location.province.store');
            Route::put('/province/update/{id}', [ProvinceController::class, 'update'])->name('master.location.province.update');
            Route::delete('/province/delete/{id}', [ProvinceController::class, 'delete'])->name('master.location.province.delete');
            Route::get('/province/restore/{id}', [ProvinceController::class, 'restore'])->name('master.location.province.restore');


            // Kabupaten-Kota
            Route::get('/city/list', [CityController::class, 'index'])->name('master.location.city.list');
            Route::get('/city/all', [CityController::class, 'getCities'])->name('master.location.city.all');
            Route::get('/city/find/{id}', [CityController::class, 'find'])->name('master.location.city.find');
            Route::post('/city/store', [CityController::class, 'store'])->name('master.location.city.store');
            Route::put('/city/update/{id}', [CityController::class, 'update'])->name('master.location.city.update');
            Route::delete('/city/delete/{id}', [CityController::class, 'delete'])->name('master.locatione.city.delete');
            Route::get('/city/restore/{id}', [CityController::class, 'restore'])->name('master.location.city.restore');


            // Kecamatan
            Route::get('/district/list', [DistrictController::class, 'index'])->name('master.location.district.list');
            Route::get('/district/all', [DistrictController::class, 'getDistricts'])->name('master.location.district.all');
            Route::get('/district/find/{id}', [DistrictController::class, 'find'])->name('master.location.district.find');
            Route::post('/district/store', [DistrictController::class, 'store'])->name('master.location.district.store');
            Route::put('/district/update/{id}', [DistrictController::class, 'update'])->name('master.location.district.update');
            Route::delete('/district/delete/{id}', [DistrictController::class, 'delete'])->name('master.location.district.delete');
            Route::get('/district/restore/{id}', [DistrictController::class, 'restore'])->name('master.location.district.restore');


            // Kelurahan
            Route::get('/ward/list', [WardController::class, 'index'])->name('master.location.ward.list');
            Route::get('/ward/all', [WardController::class, 'getWards'])->name('master.location.ward.all');
            Route::get('/ward/find/{id}', [WardController::class, 'find'])->name('master.location.wards.find');
            Route::get('/ward/detail/{id}', [WardController::class, 'getWardDetail'])->name('master.location.wards.detail');
            Route::post('/ward/store', [WardController::class, 'store'])->name('master.location.ward.store');
            Route::put('/ward/update/{id}', [WardController::class, 'update'])->name('master.location.ward.update');
            Route::delete('/ward/delete/{id}', [WardController::class, 'delete'])->name('master.location.ward.delete');
            Route::get('/ward/restore/{id}', [WardController::class, 'restore'])->name('master.location.ward.restore');
        });


        /** QUIZ TYPE */
        Route::prefix('quiz-type')->group(function () {
            Route::get('full', [QuizTypeController::class, 'getAll'])->name('quiz-type.full');
        });

        /** QUIZ */
        Route::prefix('quiz')->group(function () {
            Route::get('list', [QuizController::class, 'index'])->name('master.quiz.list');
            Route::get('full', [QuizController::class, 'getAll'])->name('quiz.full');
            Route::get('show/{id}', [QuizController::class, 'show'])->name('master.quiz.show');
            Route::get('edit/{id}', [QuizController::class, 'edit'])->name('master.quiz.edit');
            Route::get('create', [QuizController::class, 'create'])->name('master.quiz.create');
            Route::get('export/show', [QuizController::class, 'showExport'])->name('master.quiz.export');
            Route::post('store', [QuizController::class, 'store'])->name('master.quiz.store');
            Route::put('update/{id}', [QuizController::class, 'update'])->name('master.quiz.update');
            Route::post('change-status', [QuizController::class, 'changeStatus'])->name('master.quiz.change_status');
        });

        /** QUESTION */
        Route::prefix('question')->group(function () {

            Route::get('list', [QuestionController::class, 'index'])->name('master.question.index');
            Route::get('/quiz/{quizID}', [QuestionController::class, 'create'])->name('master.question.quiz');
        });


        /** SOCIAL MEDIA */
        Route::prefix("social-media")->group(function () {
            Route::get('list', [SocialMediaController::class, 'index'])->name('master.social-media.list');
            Route::get('show/{id}', [SocialMediaController::class, 'show'])->name('master.social-media.show');
            Route::get('fetch-medsos', [SocialMediaController::class, 'fetchMedsos'])->name('master.social-media.fetchMedsos');
            Route::get('create', [SocialMediaController::class, 'create'])->name('master.social-media.create');
            Route::get('edit/{id}', [SocialMediaController::class, 'edit'])->name('master.social-media.edit');
            Route::post('store', [SocialMediaController::class, 'store'])->name('master.social-media.store');
            Route::put('update/{id}', [SocialMediaController::class, 'update'])->name('master.social-media.update');
            Route::post('change-status', [SocialMediaController::class, 'changeStatus'])->name('master.social-media.change_status');
        });


        /** SOCIAL MEDIA */
        Route::prefix("icon-social-media")->group(function () {
            Route::get('list', [SocialMediaItemController::class, 'index'])
                ->name('master.icon-social-media.list');
            Route::get('fetch-medsos', [SocialMediaItemController::class, 'fetchMedsos'])->name('master.icon-social-media.fetch');
            Route::get('show/{id}', [SocialMediaItemController::class, 'show'])->name('master.icon-social-media.show');
            Route::post('store', [SocialMediaItemController::class, 'store'])->name('master.icon-social-media.store');
            Route::put('update/{id}', [SocialMediaItemController::class, 'update'])->name('master.icon-social-media.update');
            Route::post('change-status', [SocialMediaItemController::class, 'changeStatus'])->name('master.icon-social-media.change_status');
        });
    });

    /** LEARNING MODULES */
    Route::prefix("learning-modules")->group(function () {
        Route::get('list', [LearningModuleController::class, 'index'])
            ->name('learning_modules.list');

        Route::get('all', [LearningModuleController::class, 'getLearningModules'])
            ->name('learning_modules.all');

        Route::get('learning-quiz', [LearningModuleController::class, 'getLearningHasQuis'])
            ->name('learning_modules.learning_quiz');

        Route::get('full', [LearningModuleController::class, 'getAll'])
            ->name('learning_modules.full');

        Route::get('show/{id}', [LearningModuleController::class, 'show'])
            ->name('learning_modules.show');

        Route::get('create', [LearningModuleController::class, 'create'])
            ->name('learning_modules.create');

        Route::get('edit/{id}', [LearningModuleController::class, 'edit'])
            ->name('learning_modules.edit');

        Route::get('find/{id}', [LearningModuleController::class, 'find'])
            ->name('learning_modules.find');

        Route::post('store', [LearningModuleController::class, 'store'])
            ->name('learning_modules.store');

        Route::post('update/{id}', [LearningModuleController::class, 'update'])
            ->name('learning_modules.update');

        Route::post('change-status', [LearningModuleController::class, 'changeStatus'])
            ->name('learning_modules.change_status');
    });

    /** CLASS WEBINAR */
    Route::prefix("class-webinar")->group(function () {
        Route::get('list', [ClassWebinarController::class, 'index'])
            ->name('class_webinar.list');

        Route::get('all', [ClassWebinarController::class, 'getClassWebinars'])
            ->name('class_webinar.all');

        Route::get('webinar-quiz', [ClassWebinarController::class, 'getWebinarQuiz'])
            ->name('class_webinar.webinar_quiz');

        Route::get('full', [ClassWebinarController::class, 'getAll'])
            ->name('class_webinar.full');


        Route::get('show/{id}', [ClassWebinarController::class, 'show'])
            ->name('class_webinar.show');

        Route::get('create', [ClassWebinarController::class, 'create'])
            ->name('class_webinar.create');

        Route::get('edit/{id}', [ClassWebinarController::class, 'edit'])
            ->name('class_webinar.edit');

        Route::post('store', [ClassWebinarController::class, 'store'])
            ->name('class_webinar.store');

        Route::post('update/{id}', [ClassWebinarController::class, 'update'])
            ->name('class_webinar.update');

        Route::post('change-status', [ClassWebinarController::class, 'changeStatus'])
            ->name('class_webinar.change_status');

        Route::get('export/show', [ClassWebinarController::class, 'exportShow'])
            ->name('class_webinar.export_show');
    });

    /** SETTING */
    Route::prefix("setting")->group(function () {

        //KONTAK KAMI
        Route::prefix('contact-us')->group(function () {
            Route::get('/list', [ContactController::class, 'index'])->name('setting.contact_us.list');
            Route::get('/find/{id}', [ContactController::class, 'find'])->name('setting.contact_us.find');
            Route::post('/store', [ContactController::class, 'store'])->name('setting.contact_us.store');
            Route::put('/update/{id}', [ContactController::class, 'update'])->name('setting.contact_us.update');
        });

        //TENTANG KAMI
        Route::prefix('about-us')->group(function () {
            Route::get('/list', [AboutUsController::class, 'index'])->name('setting.about_us.list');
            Route::get('/create', [AboutUsController::class, 'create'])->name('setting.about_us.create');
            Route::get('/edit/{id}', [AboutUsController::class, 'edit'])->name('setting.about_us.edit');
            Route::get('/find/{id}', [AboutUsController::class, 'find'])->name('setting.about_us.find');
            Route::post('/store', [AboutUsController::class, 'store'])->name('setting.about_us.store');
            Route::post('/update/{id}', [AboutUsController::class, 'update'])->name('setting.about_us.update');
        });

        //Banner
        Route::prefix('banner')->group(function () {
            Route::get('/list', [BannerController::class, 'index'])->name('setting.banner.list');
            Route::get('/show/{id}', [BannerController::class, 'show'])->name('setting.banner.find');
            Route::get('/edit/{id}', [BannerController::class, 'edit'])->name('setting.banner.edit');
            Route::get('/create', [BannerController::class, 'create'])->name('setting.banner.create');
            Route::post('/store', [BannerController::class, 'store'])->name('setting.banner.store');
            Route::post('/update/{id}', [BannerController::class, 'update'])->name('setting.banner.update');
        });

        //training rule
        Route::prefix('training-rule')->group(function () {
            Route::get('/list', [TrainingRuleController::class, 'index'])->name('setting.training_rule.list');
            Route::get('/show/{id}', [TrainingRuleController::class, 'show'])->name('setting.training_rule.find');
            Route::get('/edit/{id}', [TrainingRuleController::class, 'edit'])->name('setting.training_rule.edit');
            Route::get('/create', [TrainingRuleController::class, 'create'])->name('setting.training_rule.create');
            Route::post('/store', [TrainingRuleController::class, 'store'])->name('setting.training_rule.store');
            Route::put('/update/{id}', [TrainingRuleController::class, 'update'])->name('setting.training_rule.update');
        });


        //Logo
        Route::prefix('logo')->group(function () {
            Route::get('list', [LogoController::class, 'index'])->name('setting.logo.list');
            Route::post('/store', [LogoController::class, 'store'])->name('setting.logo.store');
            Route::post('/update/{id}', [LogoController::class, 'update'])->name('setting.logo.update');
        });

        //Whatsapp Widget
        Route::prefix('wa-widget')->group(function () {
            Route::get('list', [WhatsappController::class, 'index'])->name('setting.wa-widget.list');
            Route::get('find/{id}', [WhatsappController::class, 'find'])->name('setting.wa-widget.find');
            Route::post('/store', [WhatsappController::class, 'store'])->name('setting.wa-widget.store');
            Route::put('/update/{id}', [WhatsappController::class, 'update'])->name('setting.wa-widget.update');
        });
    });

    /** DATATABLES */
    Route::prefix('datatables')->group(function () {
        Route::prefix('master')->group(function () {
            Route::get('users', [DatatablesController::class, 'getMasterUsers'])
                ->name('datatables.master.users');
            Route::get('category/modules', [DatatablesController::class, 'getCategoryModules'])
                ->name('datatables.master.category.modules');
            Route::get('sub_category/modules', [DatatablesController::class, 'getSubCategoryModules'])
                ->name('datatables.master.sub_category.modules');
            Route::get('type/modules', [DatatablesController::class, 'getTypeModules'])
                ->name('datatables.master.type.modules');
            Route::get('type/classes', [DatatablesController::class, 'getTypeClasses'])
                ->name('datatables.master.type.classes');
            Route::get('accessors', [DatatablesController::class, 'getAccessors'])
                ->name('datatables.master.accessors');
            Route::get('certification_types', [DatatablesController::class, 'getCertificationTypes'])
                ->name('datatables.master.certification_types');
            Route::get('education', [DatatablesController::class, 'getEducation'])
                ->name('datatables.master.education');
            Route::get('profession', [DatatablesController::class, 'getJobProfession'])
                ->name('datatables.master.profession');
            Route::get('lsp', [DatatablesController::class, 'getLSP'])
                ->name('datatables.master.lsp');
            Route::get('news', [DatatablesController::class, 'getNews'])
                ->name('datatables.master.news');
            Route::get('provinces', [DatatablesController::class, 'getProvinces'])
                ->name('master.datatables.provinces');
            Route::get('cities', [DatatablesController::class, 'getCities'])
                ->name('master.datatables.cities');
            Route::get('districts', [DatatablesController::class, 'getDistricts'])
                ->name('master.datatables.districts');
            Route::get('wards', [DatatablesController::class, 'getWards'])
                ->name('master.datatables.wards');
            Route::get('articles', [DatatablesController::class, 'getArticles'])
                ->name('datatables.master.articles');
            Route::get('quiz', [DatatablesController::class, 'getQuiz'])
                ->name('datatables.master.quiz');
        });

        Route::prefix('complements')->group(function () {
            Route::get('learning-modules', [ComplementDatatableController::class, 'getLearningModules'])
                ->name('datatables.complements.learning_module');
            Route::get('class-webinar', [ComplementDatatableController::class, 'getClassWebinar'])
                ->name('datatables.complements.class_webinar');
        });

        Route::prefix('web')->group(function () {
            Route::get('contact-us', [WebDatatableController::class, 'getContactUs'])
                ->name('datatables.web.contact_us');

            Route::get('about-us', [WebDatatableController::class, 'getAboutUs'])
                ->name('datatables.web.about_us');

            Route::get('banner', [WebDatatableController::class, 'getBanners'])
                ->name('datatables.web.banner');

            Route::get('training-rule', [WebDatatableController::class, 'getTrainingRules'])
                ->name('datatables.web.training_rule');
        });
    });

    /** EXCEL MANAGER */
    Route::prefix('excel-manager')->group(function () {
        Route::post('learning-modules', [ExcelFileManagerController::class, 'importLearningModule'])->name('excel-manager.learning-modules');
    });

    /** MAJOR */
    Route::prefix('major')->group(function () {
        Route::prefix('modules')->group(function () {
            Route::get('list', [ModuleController::class, 'index'])->name('major.modules.list');
            Route::get('paginate-module', [ModuleController::class, 'paginateModule'])->name('major.modules.paginate_module');
            Route::get('show/{id}', [ModuleController::class, 'show'])->name('major.modules.show');
            Route::get('update-profile/{module_id}', [ModuleController::class, 'updateProfil'])->name('major.modules.update_profile');
            Route::post('support-ticket', [ModuleController::class, 'postTicket'])->name('major.modules.support_ticket');
        });

        Route::prefix('webinar')->group(function () {
            Route::get('list', [WebinarController::class, 'index'])->name('major.webinar.list');
            Route::get('paginate-module', [WebinarController::class, 'paginateWebinar'])->name('major.webinar.paginate_webinar');
            Route::get('show/{id}', [WebinarController::class, 'show'])->name('major.webinar.show');
            Route::get('update-profile/{module_id}', [WebinarController::class, 'updateProfil'])->name('major.webinar.update_profile');
            Route::post('support-ticket', [WebinarController::class, 'postTicket'])->name('major.webinar.support_ticket');
        });
    });
});


require __DIR__ . '/auth.php';
